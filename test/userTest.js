const assert = require("chai").assert;
const User = require("../models/user");

//#region Tests

describe('User', () => {
    describe('getSkillSuggestions()', () => {
        it('should return correct skill suggestions (case 1) (simple)', () => {
            let actual = User.getSkillSuggestions(672, [622, 646, 671, 672, 673, 698, 722]);

            let expected = {
                furthestMinus: 622,
                middleMinus: 646,
                closestMinus: 671,
                closestPlus: 673,
                middlePlus: 698,
                furthestPlus: 722
            };
    
            skillAssert(actual, expected);
        });

        it('should return correct skill suggestions (case 2) (medium)', () => {
            let actual = User.getSkillSuggestions(672, [672]);

            let expected = {
                furthestMinus: null,
                middleMinus: null,
                closestMinus: null,
                closestPlus: null,
                middlePlus: null,
                furthestPlus: null
            };
    
            skillAssert(actual, expected);
        });

        it('should return correct skill suggestions (case 3) (medium)', () => {
            let actual = User.getSkillSuggestions(672, []);

            let expected = {
                furthestMinus: null,
                middleMinus: null,
                closestMinus: null,
                closestPlus: null,
                middlePlus: null,
                furthestPlus: null
            };
    
            skillAssert(actual, expected);
        });

        it('should return correct skill suggestions (case 4) (medium)', () => {
            let actual = User.getSkillSuggestions(672, [671, 672]);

            let expected = {
                furthestMinus: 671,
                middleMinus: 671,
                closestMinus: 671,
                closestPlus: null,
                middlePlus: null,
                furthestPlus: null
            };
    
            skillAssert(actual, expected);
        });

        it('should return correct skill suggestions (case 5) (medium)', () => {
            let actual = User.getSkillSuggestions(672, [672, 699]);

            let expected = {
                furthestMinus: null,
                middleMinus: null,
                closestMinus: null,
                closestPlus: 699,
                middlePlus: 699,
                furthestPlus: 699
            };
    
            skillAssert(actual, expected);
        });

        it('should return correct skill suggestions (case 6) (hard)', () => {
            let actual = User.getSkillSuggestions(200, [
                200, 151, 250, 236, 187, 199, 174, 178, 196, 164, 151, 243, 158, 195, 206, 232, 223, 165, 201, 219
            ]);

            let expected = {
                furthestMinus: 151,
                middleMinus: 174,
                closestMinus: 199,
                closestPlus: 201,
                middlePlus: 223,
                furthestPlus: 250
            };
    
            skillAssert(actual, expected);
        });
    });
});

//#endregion

//#region Helpers

var skillAssert = (actual, expected) => {
    assert.equal(actual.furthestMinus, expected.furthestMinus);
    assert.equal(actual.closestMinus, expected.closestMinus);
    assert.equal(actual.closestPlus, expected.closestPlus);
    assert.equal(actual.furthestPlus, expected.furthestPlus);
    assert.equal(actual.middleMinus, expected.middleMinus);
    assert.equal(actual.middlePlus, expected.middlePlus);
};

//#endregion