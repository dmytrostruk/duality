const assert = require("chai").assert;
const NotificationBuilder = require("../helpers/notification-builder");
const NotificationType = require("../enums/notification-type");

describe('NotificationBuilder', () => {
    describe('buildNotification()', () => {
        it('should return correct notification message (Notification Type - Created Duel)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = '<span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> ' + 
                         'wants to start <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">a duel</span> with you';

            var expected = NotificationBuilder.buildNotification(NotificationType.CreatedDuel, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Accepted Duel)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = '<span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> ' + 
                         'accepted your <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">duel</span>. Get working!';

            var expected = NotificationBuilder.buildNotification(NotificationType.AcceptedDuel, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Declined Duel)', () => {
            var params = { username: "johnwhite", fullname: "John White" };
            
            var actual = '<span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> ' + 
                         'declined your duel';

            var expected = NotificationBuilder.buildNotification(NotificationType.DeclinedDuel, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Opponent Uploaded Work)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = '<span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> ' + 
                         'uploaded their work for <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">a duel</span> with you. Hurry up!';

            var expected = NotificationBuilder.buildNotification(NotificationType.OpponentUploadedWork, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - New Vote)', () => {
            var params = { 
                username: "johnwhite", 
                fullname: "John White",
                duelId: "some-test-id",
                opponentUsername: "johnblack",
                opponentFullname: "John Black"
            };
            
            var actual = '<span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> ' + 
                         'voted for you in <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">a duel</span> with ' +
                         '<span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnblack">John Black</span>';

            var expected = NotificationBuilder.buildNotification(NotificationType.NewVote, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Won Duel)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = 'You are the winner in <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">a duel</span> ' + 
                         'with <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span>!';

            var expected = NotificationBuilder.buildNotification(NotificationType.WonDuel, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Lost Duel)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = 'You lost <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">a duel</span> ' + 
                         'with <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span>';

            var expected = NotificationBuilder.buildNotification(NotificationType.LostDuel, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - New follow)', () => {
            var params = { username: "johnwhite", fullname: "John White" };
            
            var actual = '<span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> ' + 
                         'started following you';

            var expected = NotificationBuilder.buildNotification(NotificationType.NewFollow, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Featured Duel)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = 'Your <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">duel</span> ' + 
                         'with <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> has been featured!';

            var expected = NotificationBuilder.buildNotification(NotificationType.FeaturedDuel, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Voted Duel Won)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = 'You voted for <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> ' + 
                         'and they won <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">the duel</span>!';

            var expected = NotificationBuilder.buildNotification(NotificationType.VotedDuelWon, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Voted Duel Lost)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = 'You voted for <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> ' + 
                         'and they lost <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">the duel</span>!';

            var expected = NotificationBuilder.buildNotification(NotificationType.VotedDuelLost, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Popular Duel)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = 'Your <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">duel</span> ' + 
                         'with <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> got on "Popular" page. Congrats!';

            var expected = NotificationBuilder.buildNotification(NotificationType.PopularDuel, params);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Ended Subscription)', () => {          
            var actual = 'Your Duelity Silver is ending this week and autorenewal is turned off. Want to renew it?';

            var expected = NotificationBuilder.buildNotification(NotificationType.EndedSubscription);

            assert.equal(actual, expected);
        });

        it('should return correct notification message (Notification Type - Accepted Duel)', () => {
            var params = { username: "johnwhite", fullname: "John White", duelId: "some-test-id" };
            
            var actual = 'Voting for your <span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="duel" data-identifier="some-test-id">duel</span> with ' + 
                         '<span style="border-bottom: 1px dotted black; cursor: pointer;" data-page="user" data-identifier="johnwhite">John White</span> has started';

            var expected = NotificationBuilder.buildNotification(NotificationType.StartedVoting, params);

            assert.equal(actual, expected);
        });
    });
});