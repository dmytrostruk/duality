const assert = require("chai").assert;
const mongoose = require("mongoose");
const Duel = require("../models/duel");

const duelsAmount = 100;

describe('DuelGenerator', () => {
    describe.skip('generateDuels', () => {
        before(() => {
            initializeDatabase();
        });

        it('should generate ended duels', () => {
            for(let i = 1; i <= duelsAmount; i++) {
                var duel = createDuel(i);
                duel.save();
            }
        });

        after(() => {
            console.log("Duels have been created.");
        });
    });
});

//#region Helpers

var initializeDatabase = () => {
    const databaseConfig = "mongodb://dmytrostruk:1qaz%40WSX@ds247141.mlab.com:47141/duelity_db";
    mongoose.connect(databaseConfig, { useNewUrlParser: true });
}

var createDuel = (numericId) => {
    const leftUser = "5b5a343fbfa731101c5b4249";
    const rightUser = "5b8467126f7f731d8808e405";

    const leftUserVotes = Math.floor(Math.random() * 100) + 1;
    const rightUserVotes = Math.floor(Math.random() * 100) + 1; 

    var duel = new Duel({
        leftUser: { 
            votes: [],
            votesAmount: leftUserVotes,
            lastVotesAmount: 0,
            userId: leftUser,
            user: leftUser,
            imageUrls: [{
                publicId: "duels/duel-5cfd319a8c00741100447665/user-5b84682f6f7f731d8808e414/work-images/txnvuvmzrjidzyrvvs9r",
                url: "https://res.cloudinary.com/duelity/image/upload/v1560097266/duels/duel-5cfd319a8c00741100447665/user-5b84682f6f7f731d8808e414/work-images/txnvuvmzrjidzyrvvs9r.jpg"
            }],
            previewImageUrl: {
                publicId: "duels/duel-5cfd319a8c00741100447665/user-5b84682f6f7f731d8808e414/preview-image/dlvksfs6br4zy5l6phem",
                url: "https://res.cloudinary.com/duelity/image/upload/v1560097271/duels/duel-5cfd319a8c00741100447665/user-5b84682f6f7f731d8808e414/preview-image/dlvksfs6br4zy5l6phem.jpg"
            },
            workDescription: "1"
        },
        rightUser: {
            votes: [],
            votesAmount: rightUserVotes,
            lastVotesAmount: 0,
            userId: rightUser,
            user: rightUser,
            imageUrls: [{
                publicId: "duels/duel-5cfd319a8c00741100447665/user-5b84682f6f7f731d8808e414/work-images/txnvuvmzrjidzyrvvs9r",
                url: "https://res.cloudinary.com/duelity/image/upload/v1560097266/duels/duel-5cfd319a8c00741100447665/user-5b84682f6f7f731d8808e414/work-images/txnvuvmzrjidzyrvvs9r.jpg"
            }],
            previewImageUrl: {
                publicId: "duels/duel-5cfd319a8c00741100447665/user-5b84682f6f7f731d8808e414/preview-image/dlvksfs6br4zy5l6phem",
                url: "https://res.cloudinary.com/duelity/image/upload/v1560097271/duels/duel-5cfd319a8c00741100447665/user-5b84682f6f7f731d8808e414/preview-image/dlvksfs6br4zy5l6phem.jpg"
            },
            workDescription: "1"
        },
        endDate: new Date(2019, 06, 09, 16, 31, 13),
        duelTask: "Make it as descriptive as possible. Remember, better described tasks get accepted way more! Make it as descriptive as possible. Remember, better described tasks get accepted way more!Make it as descriptive as possible. Remember, better described tasks get accepted way more!",
        duelCategory: "5b5c7cd47408444601b03d34",
        status: 3,
        deadline: new Date(2019, 06, 09, 16, 31, 13),
        createdAt: new Date(2019, 06, 05, 16, 31, 13),
        isFeatured: true,
        isPopular: true,
        numericId: numericId
    });

    return duel;
}

//#endregion 
