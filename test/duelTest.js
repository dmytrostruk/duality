const assert = require("chai").assert;
const Duel = require("../models/duel");
const User = require("../models/user");

//#region Tests

describe('Duel', () => {
    describe('updateSkillPoints()', () => {
        it('1. Winner has [less] skill. ([small] vote difference, [small] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 240, Votes 55), Loser = (Skill: 250, Votes: 45));\n\t' + 
           'Expected result - (Winner = 273, Loser = 217)\n', () => {
            var duel = createDuel(55, 45, 240, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 273);
            assert.equal(duel.rightUser.user.skillPoints, 217);
        });

        it('2. Winner has [more] skill. ([small] vote difference, [small] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 260, Votes 55), Loser = (Skill: 250, Votes: 45));\n\t' + 
           'Expected result - (Winner = 282, Loser = 228)\n', () => {
            var duel = createDuel(55, 45, 260, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 282);
            assert.equal(duel.rightUser.user.skillPoints, 228);
        });

        it('3. Winner has [the same] skill. ([small] vote difference, [no] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 250, Votes 55), Loser = (Skill: 250, Votes: 45));\n\t' + 
           'Expected result - (Winner = 278, Loser = 223)\n', () => {
            var duel = createDuel(55, 45, 250, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 278);
            assert.equal(duel.rightUser.user.skillPoints, 223);
        });

        it('4. Winner has [less] skill. ([big] vote difference, [small] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 240, Votes 95), Loser = (Skill: 250, Votes: 5));\n\t' + 
           'Expected result - (Winner = 297, Loser = 193)\n', () => {
            var duel = createDuel(95, 5, 240, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 297);
            assert.equal(duel.rightUser.user.skillPoints, 193);
        });

        it('5. Winner has [more] skill. ([big] vote difference, [small] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 260, Votes 95), Loser = (Skill: 250, Votes: 5));\n\t' + 
           'Expected result - (Winner = 298, Loser = 212)\n', () => {
            var duel = createDuel(95, 5, 260, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 298);
            assert.equal(duel.rightUser.user.skillPoints, 212);
        });

        it('6. Winner has [the same] skill. ([big] vote difference, [no] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 250, Votes 95), Loser = (Skill: 250, Votes: 5));\n\t' + 
           'Expected result - (Winner = 298, Loser = 203)\n', () => {
            var duel = createDuel(95, 5, 250, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 298);
            assert.equal(duel.rightUser.user.skillPoints, 203);
        });

        it('7. Winner has [less] skill. ([small] vote difference, [big] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 200, Votes 55), Loser = (Skill: 250, Votes: 45));\n\t' + 
           'Expected result - (Winner = 255, Loser = 195)\n', () => {
            var duel = createDuel(55, 45, 200, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 255);
            assert.equal(duel.rightUser.user.skillPoints, 195);
        });

        it('8. Winner has [more] skill. ([small] vote difference, [big] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 300, Votes 55), Loser = (Skill: 250, Votes: 45));\n\t' + 
           'Expected result - (Winner = 320, Loser = 230)\n', () => {
            var duel = createDuel(55, 45, 300, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 320);
            assert.equal(duel.rightUser.user.skillPoints, 230);
        });

        it('9. Winner has [less] skill. ([big] vote difference, [big] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 200, Votes: 100), Loser = (Skill: 250, Votes: 0));\n\t' + 
           'Expected result - (Winner = 300, Loser = 150)\n', () => {
            var duel = createDuel(100, 0, 200, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 300);
            assert.equal(duel.rightUser.user.skillPoints, 150);
        });

        it('10. Winner has [more] skill. ([big] vote difference, [big] skill difference)\n\t' + 
           'Conditions - (Winner = (Skill: 300, Votes: 100), Loser = (Skill: 250, Votes: 0));\n\t' + 
           'Expected result - (Winner = 320, Loser = 230)\n', () => {
            var duel = createDuel(100, 0, 300, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 320);
            assert.equal(duel.rightUser.user.skillPoints, 230);
        });

        it('11. Draw ([small] skill difference)\n\t' + 
           'Conditions - (First user = (Skill: 230, Votes: 50), Second user = (Skill: 250, Votes: 50));\n\t' + 
           'Expected result - (First user = 265, Second user = 270)\n', () => {
            var duel = createDuel(50, 50, 230, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 265);
            assert.equal(duel.rightUser.user.skillPoints, 270);
        });

        it('12. Draw ([big] skill difference)\n\t' + 
           'Conditions - (First user = (Skill: 200, Votes: 50), Second user = (Skill: 250, Votes: 50));\n\t' + 
           'Expected result - (First user = 250, Second user = 270)\n', () => {
            var duel = createDuel(50, 50, 200, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 250);
            assert.equal(duel.rightUser.user.skillPoints, 270);
        });

        it('13. Draw ([the same] skill)\n\t' + 
           'Conditions - (First user = (Skill: 250, Votes: 50), Second user = (Skill: 250, Votes: 50));\n\t' + 
           'Expected result - (First user = 275, Second user = 275)\n', () => {
            var duel = createDuel(50, 50, 250, 250);

            Duel.updateSkillPoints(duel);

            assert.equal(duel.leftUser.user.skillPoints, 275);
            assert.equal(duel.rightUser.user.skillPoints, 275);
        });
    });
});

//#endregion

//#region Helpers

var createDuel = (leftUserVotes, rightUserVotes, leftUserSkill, rightUserSkill) => {
    return new Duel({
        leftUser: { 
            votesAmount: leftUserVotes,
            user: new User({skillPoints: leftUserSkill, isPaidAccount: false})
        },
        rightUser: {
            votesAmount: rightUserVotes,
            user: new User({skillPoints : rightUserSkill, isPaidAccount: false})
        }
    });
}

//#endregion 