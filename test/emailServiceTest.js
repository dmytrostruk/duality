const assert = require("chai").assert;
const EmailService = require("../services/email-service");

describe('EmailService', () => {
    describe.skip('sendEmail()', () => {
        it('should send email', () => {
            let mailOptions = {
                from: '"Dmytro Struk 👻" <foo@example.com>',
                to: "dmytro.struk1@gmail.com",
                subject: "Email test ✔",
                text: "Hello, this is Duelity test email. Works great, right?",
                html: "<b>Hello, this is Duelity test email. Works great, right?</b>"
            };

            EmailService.sendEmail(mailOptions);
        });
    });
});