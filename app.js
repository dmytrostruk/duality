const express = require("express");
const subdomain = require("express-subdomain");
const http = require("http");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");
const cloudinary = require("cloudinary");

const databaseConfig = require("./config/database");
const cloudinaryConfig = require("./config/cloudinary");

const SocketService = require("./services/socket-service");

const apiRoute = require("./routes/api");
const userRoute = require("./routes/user");
const startDuelRoute = require("./routes/start-duel");
const duelRoute = require("./routes/duel");
const notificationRoute = require("./routes/notification");
const adminRoute = require("./routes/admin");
const categoryRoute = require("./routes/category");
const searchRoute = require("./routes/search");

const logger = require("./helpers/logger");

// Jobs
const DuelJob = require("./jobs/duel-job");

// App Object
const app = express();

// Server
const server = http.createServer(app);

// Socket
const io = require('socket.io').listen(server);

// Port Number
const port = process.env.PORT || 8080;

// Router  
const subDomain = express.Router();

initializeDatabase();
initializeServer();
initializeLibraries();
initializeJobs();
SocketService.initializeSocket(io);

function initializeServer() {
    // CORS Middleware
    app.use(cors());

    // Set Static Folder
    app.use(express.static(path.join(__dirname, "public")));

    // Body Parser Middleware
    app.use(bodyParser.json());

    // Passport Middleware
    app.use(passport.initialize());
    app.use(passport.session());

    require("./config/passport")(passport);

    // Routes
    app.use("/api", apiRoute);
    app.use("/user", userRoute);
    app.use("/start-duel", startDuelRoute);
    app.use("/duel", duelRoute);
    app.use("/notification", notificationRoute);
    app.use("/admin", adminRoute);
    app.use("/category", categoryRoute);
    app.use("/search", searchRoute);
    
    subDomain.get("/", function(req, res){
        res.sendFile(path.join(__dirname, 'admin-public/index.html'));
    });

    app.use(subdomain('admin', subDomain));

    app.get("*", function(req, res){
        res.sendFile(path.join(__dirname, 'public/index.html'));
    });

    // Start Server
    server.listen(port, () => {
        console.log("Server started on port " + port);
    });
}

function initializeDatabase() {
    mongoose.connect(databaseConfig.database, { useNewUrlParser: true });

    mongoose.connection.on("connected", () => {
        logger.info("Connected to database " + databaseConfig.database);    
    })

    mongoose.connection.on("error", (err) => {
        logger.error("Database error: " + err);
    });
}

function initializeJobs() {
    DuelJob.initializeMainJob();
}

function initializeLibraries() {
    cloudinary.config(cloudinaryConfig);
    logger.info("Initialized Cloudinary Service");   
}