const NotificationType = require("../enums/notification-type");

//#region Build Functions

var buildCreatedDuelNotification = (params) => { 
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `${createLinkElement(userLinkObject, params.fullname)} wants to start ` +
           `${createLinkElement(duelLinkObject, 'a duel')} with you`;
};

var buildAcceptedDuelNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `${createLinkElement(userLinkObject, params.fullname)} accepted your ` +
           `${createLinkElement(duelLinkObject, 'duel')}. Get working!`;
};

var buildDeclinedDuelNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username };

    return `${createLinkElement(userLinkObject, params.fullname)} declined your duel`;
};

var buildOpponentUploadedWorkNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `${createLinkElement(userLinkObject, params.fullname)} uploaded their work for ` +
           `${createLinkElement(duelLinkObject, 'a duel')} with you. Hurry up!`;
};

var buildNewVoteNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        opponentLinkObject = { page: userPage, identifier: params.opponentUsername }
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `${createLinkElement(userLinkObject, params.fullname)} voted for you in ` +
           `${createLinkElement(duelLinkObject, 'a duel')} with ${createLinkElement(opponentLinkObject, params.opponentFullname)}`;
};

var buildWonDuelNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `You are the winner in ${createLinkElement(duelLinkObject, 'a duel')} ` +
           `with ${createLinkElement(userLinkObject, params.fullname)}!`;
};

var buildLostDuelNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `You lost ${createLinkElement(duelLinkObject, 'a duel')} ` +
           `with ${createLinkElement(userLinkObject, params.fullname)}`;
};

var buildDrawDuelNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `It's a draw in ${createLinkElement(duelLinkObject, 'a duel')} ` +
           `between you and ${createLinkElement(userLinkObject, params.fullname)}!`;
};

var buildNewFollowNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username };

    return `${createLinkElement(userLinkObject, params.fullname)} started following you`;
};

var buildFeaturedDuelNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `Your ${createLinkElement(duelLinkObject, 'duel')} ` +
           `with ${createLinkElement(userLinkObject, params.fullname)} has been featured!`;
};

var buildVotedDuelWonNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `You voted for ${createLinkElement(userLinkObject, params.fullname)} and they won ` +
           `${createLinkElement(duelLinkObject, 'the duel')}!`;
};

var buildVotedDuelLostNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `You voted for ${createLinkElement(userLinkObject, params.fullname)} and they lost ` +
           `${createLinkElement(duelLinkObject, 'the duel')}!`;
};

var buildPopularDuelNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `Your ${createLinkElement(duelLinkObject, 'duel')} with ` +
           `${createLinkElement(userLinkObject, params.fullname)} got on "Popular" page. Congrats!`;
};

var buildEndedSubscriptionNotification = (params) => {
    return `Your Duelity Silver is ending this week and autorenewal is turned off. Want to renew it?`;
};

var buildStartedVotingNotification = (params) => {
    var userLinkObject = { page: userPage, identifier: params.username },
        duelLinkObject = { page: duelPage, identifier: params.duelId };

    return `Voting for your ${createLinkElement(duelLinkObject, 'duel')} with ` +
           `${createLinkElement(userLinkObject, params.fullname)} has started`;
}

//#endregion

//#region Private Constants 

const Mapper = {
    [NotificationType.CreatedDuel]: buildCreatedDuelNotification,
    [NotificationType.AcceptedDuel]: buildAcceptedDuelNotification,
    [NotificationType.DeclinedDuel]: buildDeclinedDuelNotification,
    [NotificationType.OpponentUploadedWork]: buildOpponentUploadedWorkNotification,
    [NotificationType.NewVote]: buildNewVoteNotification,
    [NotificationType.WonDuel]: buildWonDuelNotification,
    [NotificationType.LostDuel]: buildLostDuelNotification,
    [NotificationType.DrawDuel]: buildDrawDuelNotification,
    [NotificationType.NewFollow]: buildNewFollowNotification,
    [NotificationType.FeaturedDuel]: buildFeaturedDuelNotification,
    [NotificationType.VotedDuelWon]: buildVotedDuelWonNotification,
    [NotificationType.VotedDuelLost]: buildVotedDuelLostNotification,
    [NotificationType.PopularDuel]: buildPopularDuelNotification,
    [NotificationType.EndedSubscription]: buildEndedSubscriptionNotification,
    [NotificationType.StartedVoting]: buildStartedVotingNotification
}

const userPage = "user";
const duelPage = "duel";

//#endregion

const NotificationBuilder = {
    buildNotification: (type, params) => {
        return Mapper[type](params);
    }
}

//#region Helpers

var createLinkElement = (linkParams, text) => {
    var linkStyles = "border-bottom: 1px dotted black; cursor: pointer;";
    return `<span style="${linkStyles}" data-page="${linkParams.page}" data-identifier="${linkParams.identifier}">${text}</span>`;
}

//#endregion

module.exports = NotificationBuilder;