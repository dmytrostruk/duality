const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
 
const myFormat = printf(info => {
  return `${info.timestamp} --- ${info.level} --- ${info.message}`;
});
 
const logger = createLogger({
  format: combine(
    format.colorize(),
    timestamp(),
    myFormat
  ),
  transports: [
      new transports.Console(),
      new transports.File({filename: "app.log"})
    ]
});

module.exports = logger;