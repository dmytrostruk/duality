const colors = ['#FFE226', '#F55045', '#26AFFF'];

const AvatarRandomizer = {
    generateAvatarColor: () => colors[Math.floor(Math.random() * colors.length)]
}

module.exports = AvatarRandomizer;