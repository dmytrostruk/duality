const RegexHelper = {
    isOnlyNumbers: (str) => {
        var regex = /^\d+$/;
        return regex.test(str);
    }
}

module.exports = RegexHelper;