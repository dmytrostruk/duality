const Duel = require("../models/duel");
const User = require("../models/user");
const Notification = require("../models/notification");
const DuelStatus = require("../enums/duel-status");
const NotificationType = require("../enums/notification-type");
const schedule = require("node-schedule");
const logger = require("../helpers/logger");

const DuelJob = {
    initializeMainJob: () => {
        var scheduler = schedule;

        // Waiting to removed duels
        Duel.getDuelsByStatus(DuelStatus.Waiting)
        .then((duels) => {
            duels.forEach(duel => {
                initializeDeleteDuelJob(duel);
            });
        });

        // In progress  duels
        Duel.getDuelsByStatus(DuelStatus.InProgress)
        .then((duels) => {
            duels.forEach(duel => {
                initInProgressDuelJob(duel);
            });
        });

        // Voting to Ended duels
        Duel.getDuelsByStatus(DuelStatus.Voting)
        .then((duels) => {
            duels.forEach(duel => {
                initVotingDuelJob(duel);
            });
        });
    },

    waitingToRemovedDuelJob: (duel) => {
        initializeDeleteDuelJob(duel);
    },

    inProgressDuelJob: (duel) => {
        initInProgressDuelJob(duel);
    },

    votingDuelJob: (duel) => {
        initVotingDuelJob(duel);
    }
}

var initializeDeleteDuelJob = (duel) => {
    var scheduler = schedule;

    scheduler.scheduleJob(duel.endDate, () => {
        Duel.findById(duel.id)
        .then(foundedDuel => { 
            if(foundedDuel === null) {
                logger.info(`Requested duel has already been removed.`);
            } else if(foundedDuel.status === DuelStatus.Waiting) {
                foundedDuel.remove();
                logger.info(`Removed duel due to lack of response. Duel id: ${foundedDuel._id}`);
            }
        });
    });

    logger.info(`Initialized 'Waiting to removed duels' job. Duel id: ${duel.id}`);
}

var initInProgressDuelJob = (duel) => {
    var scheduler = schedule;

    scheduler.scheduleJob(duel.endDate, () => {
        Duel.findById(duel.id)
        .then(foundedDuel => {
            if(foundedDuel !== null && foundedDuel.status === DuelStatus.InProgress) {
                var date = new Date();
                var endDate = date.setDate(date.getDate() + 1);

                foundedDuel.status = DuelStatus.Voting;
                foundedDuel.endDate = endDate;
                foundedDuel.save();

                logger.info(`Changed duel status to Voting. Duel id: ${foundedDuel.id}`);

                initVotingDuelJob(foundedDuel);
            }
        });
    });

    logger.info(`Initialized 'In progress to Voting duels' job. Duel id: ${duel.id}`);
}

var initVotingDuelJob = (duel) => {
    var scheduler = schedule;

    initPopularDuelJob(duel);

    scheduler.scheduleJob(duel.endDate, async () => {
        var foundedDuel = await Duel.getDuelById(duel.id);
        
        if(foundedDuel !== null && foundedDuel.status === DuelStatus.Voting) {
            foundedDuel.status = DuelStatus.Ended;

            var leftUser = await User.getUserById(foundedDuel.leftUser.user.id),
                rightUser = await User.getUserById(foundedDuel.rightUser.user.id);

            await Duel.updateSkillPoints(foundedDuel, leftUser, rightUser);

            if(foundedDuel.leftUser.votesAmount === foundedDuel.rightUser.votesAmount) {
                var notificationParams = {
                    userId: foundedDuel.leftUser.userId,
                    type: NotificationType.DrawDuel,
                    textParams: {
                        fullname: foundedDuel.rightUser.user.fullname,
                        username: foundedDuel.rightUser.user.username,
                        duelId: duel.id
                    },
                    associatedUser: foundedDuel.rightUser.userId
                };
        
                Notification.pushNotification(notificationParams);

                notificationParams = {
                    userId: foundedDuel.rightUser.userId,
                    type: NotificationType.DrawDuel,
                    textParams: {
                        fullname: foundedDuel.leftUser.user.fullname,
                        username: foundedDuel.leftUser.user.username,
                        duelId: duel.id
                    },
                    associatedUser: foundedDuel.leftUser.userId
                };
        
                Notification.pushNotification(notificationParams);
            } else {
                var winner = Duel.getWinner(foundedDuel.leftUser, foundedDuel.rightUser),
                    loser = Duel.getLoser(foundedDuel.leftUser, foundedDuel.rightUser);

                var notificationParams = {
                    userId: winner.userId,
                    type: NotificationType.WonDuel,
                    textParams: {
                        fullname: loser.user.fullname,
                        username: loser.user.username,
                        duelId: duel.id
                    },
                    associatedUser: loser.userId
                };
        
                Notification.pushNotification(notificationParams);

                notificationParams = {
                    userId: loser.userId,
                    type: NotificationType.LostDuel,
                    textParams: {
                        fullname: winner.user.fullname,
                        username: winner.user.username,
                        duelId: duel.id
                    },
                    associatedUser: winner.userId
                };
        
                Notification.pushNotification(notificationParams);
            }

            foundedDuel.save();
            logger.info(`Changed duel status to Ended. Duel id: ${duel.id}`);
        }
    });

    logger.info(`Initialized 'Voting to Ended duels' job. Duel id: ${duel.id}`);
}

var initPopularDuelJob = (duel) => {
    var scheduler = schedule;

    var now = new Date(),
        testRule = "*/2 * * * *", // every 2 minutes
        prodRule = `${now.getMinutes()} * * * *`; // on N minute

    scheduler.scheduleJob({start: now, end: duel.endDate, rule: testRule}, async () => {
        var foundedDuel = await Duel.getDuelById(duel.id);

        if(foundedDuel) {
            var votesAmount = await Duel.getDuelVotesAmount(),
            duelVotesAmount = foundedDuel.leftUser.lastVotesAmount + foundedDuel.rightUser.lastVotesAmount;

            // Testing purposes
            votesAmount -= duelVotesAmount;
            
            var coefficient = votesAmount * 0.03;

            logger.info(`Checked duel popularity. Duel id: ${duel.id}`);
            logger.info(`Global Votes amount: ${votesAmount}`);
            logger.info(`Duel last votes amount ${duelVotesAmount}`);
            logger.info(`Coefficient: ${coefficient}`);

            if(duelVotesAmount > coefficient && !foundedDuel.isPopular) {
                foundedDuel.isPopular = true;
                logger.info(`Marked duel as popular. Duel id: ${duel.id}`);
            }

            foundedDuel.leftUser.lastVotesAmount = 0;
            foundedDuel.rightUser.lastVotesAmount = 0;

            foundedDuel.save();
        }
    });

    logger.info(`Initialized 'Popular Duel' job. Duel id: ${duel.id}`);
}

module.exports = DuelJob;