const OrderDirection = {
    Asc: 0,
    Desc: 1
}

module.exports = OrderDirection;