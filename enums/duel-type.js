const DuelType = {
    Following: 0,
    New: 1,
    Popular: 2,
    Featured: 3
}

module.exports = DuelType;