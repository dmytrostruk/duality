const NotificationType = {
    CreatedDuel: 0,
    AcceptedDuel: 1,
    DeclinedDuel: 2,
    OpponentUploadedWork: 3,
    NewVote: 4,
    WonDuel: 5,
    LostDuel: 6,
    DrawDuel: 7,
    NewFollow: 8,
    FeaturedDuel: 9,
    VotedDuelWon: 10,
    VotedDuelLost: 11,
    PopularDuel: 12,
    EndedSubscription: 13,
    StartedVoting: 14
}

module.exports = NotificationType;