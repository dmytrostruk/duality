const DuelStatus = {
    Waiting: 0,
    InProgress: 1,
    Voting: 2,
    Ended: 3
}

module.exports = DuelStatus;