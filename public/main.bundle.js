webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-alert></app-alert>\r\n<app-header></app-header>\r\n<app-body></app-body>\r\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(socketService) {
        this.socketService = socketService;
        this.socketService.initializeSocket();
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_socket_socket_service__["a" /* SocketService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_input_autocomplete__ = __webpack_require__("./node_modules/ng2-input-autocomplete/esm5/ng2-input-autocompleteModule.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_file_drop__ = __webpack_require__("./node_modules/ngx-file-drop/ngx-file-drop.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__cloudinary_angular_5_x__ = __webpack_require__("./node_modules/@cloudinary/angular-5.x/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__cloudinary_angular_5_x___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__cloudinary_angular_5_x__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_cloudinary_core__ = __webpack_require__("./node_modules/cloudinary-core/cloudinary-core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_cloudinary_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_cloudinary_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__ = __webpack_require__("./src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__guards_work_deactivate_guard__ = __webpack_require__("./src/app/guards/work-deactivate.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__guards_settings_deactivate_guard__ = __webpack_require__("./src/app/guards/settings-deactivate.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__directives_image_preview_directive__ = __webpack_require__("./src/app/directives/image-preview.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__directives_block_background_directive__ = __webpack_require__("./src/app/directives/block-background.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pipes_safe_html_pipe__ = __webpack_require__("./src/app/pipes/safe-html.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_date_formatter_date_formatter_service__ = __webpack_require__("./src/app/services/date-formatter/date-formatter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__services_duel_status_message_duel_status_message_service__ = __webpack_require__("./src/app/services/duel-status-message/duel-status-message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__services_duel_timer_duel_timer_service__ = __webpack_require__("./src/app/services/duel-timer/duel-timer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__services_start_duel_service_start_duel_service__ = __webpack_require__("./src/app/services/start-duel-service/start-duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__services_cloudinary_service_cloudinary_service__ = __webpack_require__("./src/app/services/cloudinary-service/cloudinary.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__services_pager_pager_service__ = __webpack_require__("./src/app/services/pager/pager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__services_notification_notification_service__ = __webpack_require__("./src/app/services/notification/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__services_vote_vote_service__ = __webpack_require__("./src/app/services/vote/vote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__services_socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__services_report_report_service__ = __webpack_require__("./src/app/services/report/report.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__services_dribbble_dribbble_service__ = __webpack_require__("./src/app/services/dribbble/dribbble.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__services_google_google_service__ = __webpack_require__("./src/app/services/google/google.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__services_twitter_twitter_service__ = __webpack_require__("./src/app/services/twitter/twitter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__services_category_category_service__ = __webpack_require__("./src/app/services/category/category.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__services_login_login_service__ = __webpack_require__("./src/app/services/login/login.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__services_tooltip_tooltip_service__ = __webpack_require__("./src/app/services/tooltip/tooltip.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__components_user_services_user_service__ = __webpack_require__("./src/app/components/user/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__components_user_services_follow_service__ = __webpack_require__("./src/app/components/user/services/follow.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__components_work_services_work_service__ = __webpack_require__("./src/app/components/work/services/work.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__components_my_duels_services_my_duels_service__ = __webpack_require__("./src/app/components/my-duels/services/my-duels.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__components_account_settings_services_account_settings_service__ = __webpack_require__("./src/app/components/account-settings/services/account-settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__child_components_default_picture_services_default_picture_service__ = __webpack_require__("./src/app/child-components/default-picture/services/default-picture.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__components_register_services_register_service__ = __webpack_require__("./src/app/components/register/services/register.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__child_components_user_list_services_user_list_service__ = __webpack_require__("./src/app/child-components/user-list/services/user-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__components_search_services_search_service__ = __webpack_require__("./src/app/components/search/services/search.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__components_password_restore_services_password_restore_service__ = __webpack_require__("./src/app/components/password-restore/services/password-restore.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__root_components_header_services_header_service__ = __webpack_require__("./src/app/root-components/header/services/header.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__child_components_duel_duel_component__ = __webpack_require__("./src/app/child-components/duel/duel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__child_components_work_modal_work_modal_component__ = __webpack_require__("./src/app/child-components/work-modal/work-modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__child_components_default_picture_default_picture_component__ = __webpack_require__("./src/app/child-components/default-picture/default-picture.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__child_components_report_comment_report_comment_component__ = __webpack_require__("./src/app/child-components/report-comment/report-comment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__child_components_user_list_user_list_component__ = __webpack_require__("./src/app/child-components/user-list/user-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__child_components_alert_alert_component__ = __webpack_require__("./src/app/child-components/alert/alert.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__child_components_input_input_component__ = __webpack_require__("./src/app/child-components/input/input.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__child_components_back_to_top_back_to_top_back_to_top_component__ = __webpack_require__("./src/app/child-components/back-to-top/back-to-top/back-to-top.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__components_login_login_component__ = __webpack_require__("./src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__components_register_register_component__ = __webpack_require__("./src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__components_user_user_component__ = __webpack_require__("./src/app/components/user/user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__components_start_duel_start_duel_component__ = __webpack_require__("./src/app/components/start-duel/start-duel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__components_my_duels_my_duels_component__ = __webpack_require__("./src/app/components/my-duels/my-duels.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__root_components_header_header_component__ = __webpack_require__("./src/app/root-components/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__root_components_body_body_component__ = __webpack_require__("./src/app/root-components/body/body.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__root_components_footer_footer_component__ = __webpack_require__("./src/app/root-components/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__components_following_duels_following_duels_component__ = __webpack_require__("./src/app/components/following-duels/following-duels.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__components_new_duels_new_duels_component__ = __webpack_require__("./src/app/components/new-duels/new-duels.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__components_popular_duels_popular_duels_component__ = __webpack_require__("./src/app/components/popular-duels/popular-duels.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__components_featured_duels_featured_duels_component__ = __webpack_require__("./src/app/components/featured-duels/featured-duels.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__components_work_work_component__ = __webpack_require__("./src/app/components/work/work.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__components_vote_vote_component__ = __webpack_require__("./src/app/components/vote/vote.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__components_notifications_notifications_component__ = __webpack_require__("./src/app/components/notifications/notifications.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__components_account_settings_account_settings_component__ = __webpack_require__("./src/app/components/account-settings/account-settings.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75__components_pro_account_pro_account_component__ = __webpack_require__("./src/app/components/pro-account/pro-account.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_76__components_auth_callback_auth_callback_component__ = __webpack_require__("./src/app/components/auth-callback/auth-callback.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_77__components_search_search_component__ = __webpack_require__("./src/app/components/search/search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_78__components_password_restore_password_restore_component__ = __webpack_require__("./src/app/components/password-restore/password-restore.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











// Guards



// Directives


// Pipes

// Global Services






















// Component Services











// Child Components








// Screen Components 





















var appRoutes = [
    { path: '', redirectTo: 'following', pathMatch: 'full' },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_60__components_register_register_component__["a" /* RegisterComponent */] },
    { path: 'following', component: __WEBPACK_IMPORTED_MODULE_67__components_following_duels_following_duels_component__["a" /* FollowingDuelsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'new', component: __WEBPACK_IMPORTED_MODULE_68__components_new_duels_new_duels_component__["a" /* NewDuelsComponent */] },
    { path: 'popular', component: __WEBPACK_IMPORTED_MODULE_69__components_popular_duels_popular_duels_component__["a" /* PopularDuelsComponent */] },
    { path: 'featured', component: __WEBPACK_IMPORTED_MODULE_70__components_featured_duels_featured_duels_component__["a" /* FeaturedDuelsComponent */] },
    { path: 'myduels', component: __WEBPACK_IMPORTED_MODULE_63__components_my_duels_my_duels_component__["a" /* MyDuelsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'user/:username', component: __WEBPACK_IMPORTED_MODULE_61__components_user_user_component__["a" /* UserComponent */] },
    { path: 'start', component: __WEBPACK_IMPORTED_MODULE_62__components_start_duel_start_duel_component__["a" /* StartDuelComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'work/:id', component: __WEBPACK_IMPORTED_MODULE_71__components_work_work_component__["a" /* WorkComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]], canDeactivate: [__WEBPACK_IMPORTED_MODULE_12__guards_work_deactivate_guard__["a" /* WorkDeactivateGuard */]] },
    { path: 'duel/:id', component: __WEBPACK_IMPORTED_MODULE_72__components_vote_vote_component__["a" /* VoteComponent */] },
    { path: 'notifications', component: __WEBPACK_IMPORTED_MODULE_73__components_notifications_notifications_component__["a" /* NotificationsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'settings', component: __WEBPACK_IMPORTED_MODULE_74__components_account_settings_account_settings_component__["a" /* AccountSettingsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]], canDeactivate: [__WEBPACK_IMPORTED_MODULE_13__guards_settings_deactivate_guard__["a" /* SettingsDeactivateGuard */]] },
    { path: 'silver', component: __WEBPACK_IMPORTED_MODULE_75__components_pro_account_pro_account_component__["a" /* ProAccountComponent */] },
    { path: 'search', component: __WEBPACK_IMPORTED_MODULE_77__components_search_search_component__["a" /* SearchComponent */] },
    { path: 'search/:query', component: __WEBPACK_IMPORTED_MODULE_77__components_search_search_component__["a" /* SearchComponent */] },
    { path: 'authcallback', component: __WEBPACK_IMPORTED_MODULE_76__components_auth_callback_auth_callback_component__["a" /* AuthCallbackComponent */] },
    { path: 'restore', component: __WEBPACK_IMPORTED_MODULE_78__components_password_restore_password_restore_component__["a" /* PasswordRestoreComponent */] },
    { path: '**', redirectTo: 'following', pathMatch: 'full' }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_58__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_59__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_60__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_61__components_user_user_component__["a" /* UserComponent */],
                __WEBPACK_IMPORTED_MODULE_50__child_components_duel_duel_component__["a" /* DuelComponent */],
                __WEBPACK_IMPORTED_MODULE_62__components_start_duel_start_duel_component__["a" /* StartDuelComponent */],
                __WEBPACK_IMPORTED_MODULE_63__components_my_duels_my_duels_component__["a" /* MyDuelsComponent */],
                __WEBPACK_IMPORTED_MODULE_64__root_components_header_header_component__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_65__root_components_body_body_component__["a" /* BodyComponent */],
                __WEBPACK_IMPORTED_MODULE_66__root_components_footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_67__components_following_duels_following_duels_component__["a" /* FollowingDuelsComponent */],
                __WEBPACK_IMPORTED_MODULE_68__components_new_duels_new_duels_component__["a" /* NewDuelsComponent */],
                __WEBPACK_IMPORTED_MODULE_69__components_popular_duels_popular_duels_component__["a" /* PopularDuelsComponent */],
                __WEBPACK_IMPORTED_MODULE_70__components_featured_duels_featured_duels_component__["a" /* FeaturedDuelsComponent */],
                __WEBPACK_IMPORTED_MODULE_71__components_work_work_component__["a" /* WorkComponent */],
                __WEBPACK_IMPORTED_MODULE_14__directives_image_preview_directive__["a" /* ImagePreviewDirective */],
                __WEBPACK_IMPORTED_MODULE_15__directives_block_background_directive__["a" /* BlockBackgroundDirective */],
                __WEBPACK_IMPORTED_MODULE_72__components_vote_vote_component__["a" /* VoteComponent */],
                __WEBPACK_IMPORTED_MODULE_51__child_components_work_modal_work_modal_component__["a" /* WorkModalComponent */],
                __WEBPACK_IMPORTED_MODULE_73__components_notifications_notifications_component__["a" /* NotificationsComponent */],
                __WEBPACK_IMPORTED_MODULE_16__pipes_safe_html_pipe__["a" /* SafeHtmlPipe */],
                __WEBPACK_IMPORTED_MODULE_74__components_account_settings_account_settings_component__["a" /* AccountSettingsComponent */],
                __WEBPACK_IMPORTED_MODULE_52__child_components_default_picture_default_picture_component__["a" /* DefaultPictureComponent */],
                __WEBPACK_IMPORTED_MODULE_75__components_pro_account_pro_account_component__["a" /* ProAccountComponent */],
                __WEBPACK_IMPORTED_MODULE_53__child_components_report_comment_report_comment_component__["a" /* ReportCommentComponent */],
                __WEBPACK_IMPORTED_MODULE_76__components_auth_callback_auth_callback_component__["a" /* AuthCallbackComponent */],
                __WEBPACK_IMPORTED_MODULE_54__child_components_user_list_user_list_component__["a" /* UserListComponent */],
                __WEBPACK_IMPORTED_MODULE_77__components_search_search_component__["a" /* SearchComponent */],
                __WEBPACK_IMPORTED_MODULE_78__components_password_restore_password_restore_component__["a" /* PasswordRestoreComponent */],
                __WEBPACK_IMPORTED_MODULE_55__child_components_alert_alert_component__["a" /* AlertComponent */],
                __WEBPACK_IMPORTED_MODULE_56__child_components_input_input_component__["a" /* InputComponent */],
                __WEBPACK_IMPORTED_MODULE_57__child_components_back_to_top_back_to_top_back_to_top_component__["a" /* BackToTopComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forRoot(appRoutes),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_5_ng2_input_autocomplete__["a" /* AutocompleteModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_ngx_file_drop__["a" /* FileDropModule */],
                __WEBPACK_IMPORTED_MODULE_7__cloudinary_angular_5_x__["CloudinaryModule"].forRoot(__WEBPACK_IMPORTED_MODULE_10_cloudinary_core__, __WEBPACK_IMPORTED_MODULE_9__environments_environment__["a" /* environment */].cloudinary),
                __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload__["FileUploadModule"]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_17__services_validation_validation_service__["a" /* ValidationService */],
                __WEBPACK_IMPORTED_MODULE_18__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
                __WEBPACK_IMPORTED_MODULE_19__services_auth_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_12__guards_work_deactivate_guard__["a" /* WorkDeactivateGuard */],
                __WEBPACK_IMPORTED_MODULE_39__components_user_services_user_service__["a" /* UserService */],
                __WEBPACK_IMPORTED_MODULE_20__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
                __WEBPACK_IMPORTED_MODULE_21__services_date_formatter_date_formatter_service__["a" /* DateFormatterService */],
                __WEBPACK_IMPORTED_MODULE_40__components_user_services_follow_service__["a" /* FollowService */],
                __WEBPACK_IMPORTED_MODULE_22__services_duel_status_message_duel_status_message_service__["a" /* DuelStatusMessageService */],
                __WEBPACK_IMPORTED_MODULE_23__services_duel_timer_duel_timer_service__["a" /* DuelTimerService */],
                __WEBPACK_IMPORTED_MODULE_24__services_start_duel_service_start_duel_service__["a" /* StartDuelService */],
                __WEBPACK_IMPORTED_MODULE_25__services_duel_service_duel_service__["a" /* DuelService */],
                __WEBPACK_IMPORTED_MODULE_41__components_work_services_work_service__["a" /* WorkService */],
                __WEBPACK_IMPORTED_MODULE_26__services_cloudinary_service_cloudinary_service__["a" /* CloudinaryService */],
                __WEBPACK_IMPORTED_MODULE_29__services_vote_vote_service__["a" /* VoteService */],
                __WEBPACK_IMPORTED_MODULE_27__services_pager_pager_service__["a" /* PagerService */],
                __WEBPACK_IMPORTED_MODULE_28__services_notification_notification_service__["a" /* NotificationService */],
                __WEBPACK_IMPORTED_MODULE_42__components_my_duels_services_my_duels_service__["a" /* MyDuelsService */],
                __WEBPACK_IMPORTED_MODULE_43__components_account_settings_services_account_settings_service__["a" /* AccountSettingsService */],
                __WEBPACK_IMPORTED_MODULE_44__child_components_default_picture_services_default_picture_service__["a" /* DefaultPictureService */],
                __WEBPACK_IMPORTED_MODULE_30__services_socket_socket_service__["a" /* SocketService */],
                __WEBPACK_IMPORTED_MODULE_31__services_report_report_service__["a" /* ReportService */],
                __WEBPACK_IMPORTED_MODULE_32__services_dribbble_dribbble_service__["a" /* DribbbleService */],
                __WEBPACK_IMPORTED_MODULE_33__services_google_google_service__["a" /* GoogleService */],
                __WEBPACK_IMPORTED_MODULE_34__services_twitter_twitter_service__["a" /* TwitterService */],
                __WEBPACK_IMPORTED_MODULE_35__services_category_category_service__["a" /* CategoryService */],
                __WEBPACK_IMPORTED_MODULE_45__components_register_services_register_service__["a" /* RegisterService */],
                __WEBPACK_IMPORTED_MODULE_46__child_components_user_list_services_user_list_service__["a" /* UserListService */],
                __WEBPACK_IMPORTED_MODULE_47__components_search_services_search_service__["a" /* SearchService */],
                __WEBPACK_IMPORTED_MODULE_48__components_password_restore_services_password_restore_service__["a" /* PasswordRestoreService */],
                __WEBPACK_IMPORTED_MODULE_36__services_alert_alert_service__["a" /* AlertService */],
                __WEBPACK_IMPORTED_MODULE_37__services_login_login_service__["a" /* LoginService */],
                __WEBPACK_IMPORTED_MODULE_38__services_tooltip_tooltip_service__["a" /* TooltipService */],
                __WEBPACK_IMPORTED_MODULE_13__guards_settings_deactivate_guard__["a" /* SettingsDeactivateGuard */],
                __WEBPACK_IMPORTED_MODULE_49__root_components_header_services_header_service__["a" /* HeaderService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_58__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/child-components/alert/alert.component.css":
/***/ (function(module, exports) {

module.exports = ".alert-wrapper {\r\n    position: fixed;\r\n    width: 100%;\r\n    text-align: center;\r\n    top: -90px;\r\n    z-index: 9999;\r\n    opacity: 0;\r\n}\r\n\r\n.alert-container {\r\n    background: #FFE226;\r\n    height: 50px;\r\n    border-radius: 100px;\r\n    -webkit-box-shadow: 0px 14px 34px rgba(255, 226, 38, 0.4);\r\n            box-shadow: 0px 14px 34px rgba(255, 226, 38, 0.4);\r\n    font-family: 'Menoe Grotesque Pro';\r\n    color: #262626;\r\n    font-size: 14px;\r\n    letter-spacing: -0.02em;\r\n    display: inline-block;\r\n    position: relative;\r\n}\r\n\r\n.alert-container p {\r\n    display: inline;\r\n    padding-right: 24px;\r\n    padding-left: 5px;\r\n    position: relative;\r\n    top: 13px;\r\n}\r\n\r\n.alert-container p.error {\r\n    color: #F55045;\r\n}\r\n\r\n.alert-icon {\r\n    width: 16px;\r\n    height: 16px;\r\n    display: inline-block;\r\n    position: relative;\r\n    top: 17px;\r\n    margin-left: 20px;\r\n}\r\n\r\n.alert-icon.success {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2216%22 viewBox%3D%220 0 16 16%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M8.0001 1.6001C4.4713 1.6001 1.6001 4.4713 1.6001 8.0001C1.6001 11.5289 4.4713 14.4001 8.0001 14.4001C11.5289 14.4001 14.4001 11.5289 14.4001 8.0001C14.4001 4.4713 11.5289 1.6001 8.0001 1.6001ZM8 16C3.5888 16 0 12.4112 0 8C0 3.5888 3.5888 0 8 0C12.4112 0 16 3.5888 16 8C16 12.4112 12.4112 16 8 16Z%22 fill%3D%22%23262626%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M7.32773 11.9416L4.09253 8.70724L5.50693 7.29284L7.07253 8.85844L10.3861 4.21924L12.0133 5.38084L7.32773 11.9416Z%22 fill%3D%22%23262626%22%2F%3E%0D%3C%2Fsvg%3E%0D\");\r\n}\r\n\r\n.alert-icon.error {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2216%22 viewBox%3D%220 0 16 16%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M8.0001 1.6001C4.4713 1.6001 1.6001 4.4713 1.6001 8.0001C1.6001 11.5289 4.4713 14.4001 8.0001 14.4001C11.5289 14.4001 14.4001 11.5289 14.4001 8.0001C14.4001 4.4713 11.5289 1.6001 8.0001 1.6001ZM8 16C3.5888 16 0 12.4112 0 8C0 3.5888 3.5888 0 8 0C12.4112 0 16 3.5888 16 8C16 12.4112 12.4112 16 8 16Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M11.7656 5.36509L10.6344 4.23389L7.99997 6.86829L5.36557 4.23389L4.23438 5.36509L6.86877 7.99949L4.23438 10.6339L5.36557 11.7651L7.99997 9.13149L10.6344 11.7651L11.7656 10.6339L9.13117 7.99949L11.7656 5.36509Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\");\r\n}"

/***/ }),

/***/ "./src/app/child-components/alert/alert.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"{{alertService.alertWrapperId}}\" class=\"alert-wrapper\">\r\n  <div id=\"{{alertService.alertContainerId}}\" class=\"alert-container\">\r\n    <span *ngIf=\"alertService.isSuccess\" class=\"alert-icon success\"></span>\r\n    <span *ngIf=\"!alertService.isSuccess\" class=\"alert-icon error\"></span>\r\n    <p [class.error]=\"!alertService.isSuccess\">{{alertService.currentMessage}}</p>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/child-components/alert/alert.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AlertComponent = /** @class */ (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
    }
    AlertComponent.prototype.ngOnInit = function () {
    };
    AlertComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-alert',
            template: __webpack_require__("./src/app/child-components/alert/alert.component.html"),
            styles: [__webpack_require__("./src/app/child-components/alert/alert.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_alert_alert_service__["a" /* AlertService */]])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/child-components/back-to-top/back-to-top/back-to-top.component.css":
/***/ (function(module, exports) {

module.exports = "div {\r\n    position: fixed;\r\n    right: 25px;\r\n    bottom: 25px;\r\n    width: 140px;\r\n    height: 50px;\r\n    background: #D8E2E9 url(\"data:image/svg+xml,%3Csvg width%3D%2211%22 height%3D%2210%22 viewBox%3D%220 0 11 10%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M5.5 10L5.5 1M5.5 1L10 5.5M5.5 1L1 5.5%22 stroke%3D%22%23F55045%22 stroke-width%3D%222%22 stroke-linejoin%3D%22round%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 109px 50% no-repeat;\r\n    -webkit-box-shadow: 0px 14px 34px rgba(216, 226, 233, 0.5);\r\n            box-shadow: 0px 14px 34px rgba(216, 226, 233, 0.5);\r\n    border-radius: 100px;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    line-height: 17px;\r\n    padding-left: 23px;\r\n    color: #5D5D5D;\r\n    z-index: 9999;\r\n    line-height: 50px;\r\n    cursor: pointer;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n}\r\n\r\ndiv.hidden {\r\n    visibility: hidden;\r\n    opacity: 0;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n}"

/***/ }),

/***/ "./src/app/child-components/back-to-top/back-to-top/back-to-top.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"backToTop\" class=\"hidden\" (click)=\"scrollToTop()\">Back to Top</div>"

/***/ }),

/***/ "./src/app/child-components/back-to-top/back-to-top/back-to-top.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackToTopComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BackToTopComponent = /** @class */ (function () {
    function BackToTopComponent() {
    }
    BackToTopComponent.prototype.ngOnInit = function () {
        this.initializeAnimation();
    };
    BackToTopComponent.prototype.scrollToTop = function () {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    };
    BackToTopComponent.prototype.initializeAnimation = function () {
        var buttonElement = $("#backToTop");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 500) {
                buttonElement.removeClass("hidden");
            }
            else {
                buttonElement.addClass("hidden");
            }
        });
    };
    BackToTopComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-back-to-top',
            template: __webpack_require__("./src/app/child-components/back-to-top/back-to-top/back-to-top.component.html"),
            styles: [__webpack_require__("./src/app/child-components/back-to-top/back-to-top/back-to-top.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BackToTopComponent);
    return BackToTopComponent;
}());



/***/ }),

/***/ "./src/app/child-components/default-picture/default-picture.component.css":
/***/ (function(module, exports) {

module.exports = "img {\r\n    border-radius: 50%;\r\n}\r\n\r\n.background {\r\n    border-radius: 50%;\r\n    text-align: center;\r\n    display: inline-block;\r\n}\r\n\r\nh1 {\r\n    opacity: .4;\r\n    text-transform: uppercase;\r\n    text-align: center;\r\n    vertical-align: middle;\r\n}\r\n\r\n.online-status {   \r\n    border-radius: 50%;\r\n    background-color: #31D431;\r\n    position: relative;\r\n    -webkit-box-shadow: 0 0 0 2px #fff;\r\n            box-shadow: 0 0 0 2px #fff;\r\n}\r\n\r\n/* XX-small */\r\n\r\n.xx-small {\r\n    width: 24px;\r\n    height: 24px;\r\n}\r\n\r\n.xx-small img,\r\n.xx-small .background {\r\n    width: 24px;\r\n    height: 24px;\r\n}\r\n\r\n.xx-small .background.with-border {\r\n    -webkit-box-shadow: 0 0 0 2px #fff;\r\n            box-shadow: 0 0 0 2px #fff;\r\n}\r\n\r\n.xx-small h1 {\r\n    font-size: 12px;\r\n    line-height: 26px;\r\n}\r\n\r\n.xx-small .online-status-wrapper {\r\n    width: 7px;\r\n    height: 7px;\r\n    margin-top: -7px;\r\n    margin-left: 18px;\r\n}\r\n\r\n.xx-small .online-status {\r\n    width: 7px;\r\n    height: 7px; \r\n}\r\n\r\n/* XX-small End */\r\n\r\n/* X-small */\r\n\r\n.x-small {\r\n    width: 36px;\r\n    height: 36px;\r\n}\r\n\r\n.x-small img,\r\n.x-small .background {\r\n    width: 36px;\r\n    height: 36px;\r\n}\r\n\r\n.x-small .background.with-border {\r\n    -webkit-box-shadow: 0 0 0 2px #fff;\r\n            box-shadow: 0 0 0 2px #fff;\r\n}\r\n\r\n.x-small h1 {\r\n    font-size: 15px;\r\n    line-height: 36px;\r\n}\r\n\r\n.x-small .online-status-wrapper {\r\n    width: 10px;\r\n    height: 10px;\r\n    margin-top: -10px; \r\n    margin-left: 26px; \r\n}\r\n\r\n.x-small .online-status {\r\n    width: 10px;\r\n    height: 10px; \r\n}\r\n\r\n/* X-small End */\r\n\r\n/* Small */\r\n\r\n.small {\r\n    width: 48px;\r\n    height: 48px; \r\n}\r\n\r\n.small img,\r\n.small .background {\r\n    width: 48px;\r\n    height: 48px;\r\n}\r\n\r\n.small h1 {\r\n    font-size: 20px;\r\n    line-height: 48px;\r\n}\r\n\r\n.small .online-status-wrapper {\r\n    width: 10px;\r\n    height: 10px;\r\n    margin-top: -10px; \r\n    margin-left: 38px; \r\n}\r\n\r\n.small .online-status {\r\n    width: 10px;\r\n    height: 10px; \r\n}\r\n\r\n/* Small End */\r\n\r\n/* X-medium */\r\n\r\n.x-medium {\r\n    width: 56px;\r\n    height: 56px;\r\n}\r\n\r\n.x-medium img,\r\n.x-medium .background {\r\n    width: 56px;\r\n    height: 56px;\r\n}\r\n\r\n.x-medium h1 {\r\n    font-size: 26px;\r\n    line-height: 56px;\r\n}\r\n\r\n.x-medium .online-status-wrapper {\r\n    width: 12px;\r\n    height: 12px;\r\n    margin-top: -15px; \r\n    margin-left: 44px; \r\n}\r\n\r\n.x-medium .online-status {\r\n    width: 12px;\r\n    height: 12px; \r\n}\r\n\r\n/* X-medium End */\r\n\r\n/* Medium */\r\n\r\n.medium {\r\n    width: 72px;\r\n    height: 72px;\r\n}\r\n\r\n.medium img,\r\n.medium .background {\r\n    width: 72px;\r\n    height: 72px;\r\n}\r\n\r\n.medium h1 {\r\n    font-size: 30px;\r\n    line-height: 72px;\r\n}\r\n\r\n.medium .online-status-wrapper {\r\n    width: 10px;\r\n    height: 10px;\r\n    margin-top: -15px; \r\n    margin-left: 57px; \r\n}\r\n\r\n.medium .online-status {\r\n    width: 10px;\r\n    height: 10px; \r\n}\r\n\r\n/* Medium End */\r\n\r\n/* Large */\r\n\r\n.large {\r\n    width: 144px;\r\n    height: 144px;\r\n}\r\n\r\n.large img,\r\n.large .background {\r\n    width: 144px;\r\n    height: 144px;\r\n}\r\n\r\n.large h1 {\r\n    font-size: 62px;\r\n    line-height: 144px;\r\n}\r\n\r\n.large .online-status-wrapper {\r\n    width: 16px;\r\n    height: 16px;\r\n    margin-top: -26px; \r\n    margin-left: 118px; \r\n}\r\n\r\n.large .online-status {\r\n    width: 16px;\r\n    height: 16px; \r\n    -webkit-box-shadow: 0 0 0 4px #fff; \r\n            box-shadow: 0 0 0 4px #fff;\r\n}\r\n\r\n/* Large End */"

/***/ }),

/***/ "./src/app/child-components/default-picture/default-picture.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"{{elementClass}}\">\r\n  <img *ngIf=\"backgroundImage\" src=\"{{backgroundImage}}\" />\r\n  <div class=\"background\" *ngIf=\"!backgroundImage\"[ngStyle]=\"{backgroundColor: backgroundColor}\" [class.with-border]=\"hasBorder\">\r\n    <h1>{{getPictureText()}}</h1>\r\n  </div>\r\n  <div class=\"online-status-wrapper\">\r\n    <div *ngIf=\"socketService.onlineUsers.indexOf(userId) >= 0\" class=\"online-status\"></div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/child-components/default-picture/default-picture.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DefaultPictureComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_default_picture_service__ = __webpack_require__("./src/app/child-components/default-picture/services/default-picture.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DefaultPictureComponent = /** @class */ (function () {
    function DefaultPictureComponent(defaultPictureService, socketService) {
        this.defaultPictureService = defaultPictureService;
        this.socketService = socketService;
    }
    DefaultPictureComponent.prototype.ngOnInit = function () {
        if (!this.backgroundColor) {
            this.backgroundColor = this.defaultPictureService.generateRandomColor();
        }
    };
    DefaultPictureComponent.prototype.getPictureText = function () {
        if (!this.fullname) {
            return "";
        }
        this.fullname = this.fullname.trim();
        var words = this.fullname.split(' '), firstLetter = words[0].split('')[0], lastLetter = words[words.length - 1] ? words[words.length - 1].split('')[0] : "";
        return firstLetter + (words.length > 1 ? lastLetter : "");
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], DefaultPictureComponent.prototype, "userId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], DefaultPictureComponent.prototype, "fullname", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], DefaultPictureComponent.prototype, "elementClass", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], DefaultPictureComponent.prototype, "backgroundColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], DefaultPictureComponent.prototype, "backgroundImage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], DefaultPictureComponent.prototype, "hasBorder", void 0);
    DefaultPictureComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-profile-picture',
            template: __webpack_require__("./src/app/child-components/default-picture/default-picture.component.html"),
            styles: [__webpack_require__("./src/app/child-components/default-picture/default-picture.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_default_picture_service__["a" /* DefaultPictureService */],
            __WEBPACK_IMPORTED_MODULE_2__services_socket_socket_service__["a" /* SocketService */]])
    ], DefaultPictureComponent);
    return DefaultPictureComponent;
}());



/***/ }),

/***/ "./src/app/child-components/default-picture/services/default-picture.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DefaultPictureService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DefaultPictureService = /** @class */ (function () {
    function DefaultPictureService() {
    }
    DefaultPictureService_1 = DefaultPictureService;
    DefaultPictureService.prototype.generateRandomColor = function () {
        var arr = DefaultPictureService_1.backgroundColorArray;
        return arr[Math.floor(Math.random() * arr.length)];
    };
    DefaultPictureService.backgroundColorArray = ['#FFE226', '#F55045', '#26AFFF'];
    DefaultPictureService = DefaultPictureService_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], DefaultPictureService);
    return DefaultPictureService;
    var DefaultPictureService_1;
}());



/***/ }),

/***/ "./src/app/child-components/duel/duel.component.css":
/***/ (function(module, exports) {

module.exports = ".duel.opened {\r\n    width: 692px;\r\n}\r\n\r\n.duel {\r\n    width: 590px;\r\n    height: 250px;\r\n    border: 1px solid #D8E2E9;\r\n    z-index: 100;\r\n    padding-left: 30px;\r\n    padding-right: 30px;\r\n    padding-bottom: 20px;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.duel.hoverable {\r\n    cursor: pointer;\r\n}\r\n\r\n.duel.hoverable:hover {\r\n    -webkit-box-shadow: 0px 20px 40px rgba(193, 199, 203, 0.2);\r\n            box-shadow: 0px 20px 40px rgba(193, 199, 203, 0.2);\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.duel .category {\r\n    border-left: 1px solid #D8E2E9;\r\n    border-bottom: 1px solid #D8E2E9;\r\n    border-right: 1px solid #D8E2E9;\r\n    display: inline-block;\r\n    position: relative;\r\n    top: -5px;\r\n    padding-left: 8px;\r\n    padding-right: 8px;\r\n    font-size: 12px;\r\n    margin-bottom: 0px;\r\n}\r\n\r\n.duel .digits {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 20px;\r\n    color: #262626;\r\n}\r\n\r\n.duel .text {\r\n    font-size: 12px;\r\n}\r\n\r\n.duel .open-task-button {\r\n    margin-top: 37px;\r\n}\r\n\r\n.duel .rating {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 14px;\r\n    line-height: 21px;\r\n    letter-spacing: -0.02em;\r\n    color: #C1C7CB;\r\n}\r\n\r\n.duel h3 {\r\n    display: inline-block;\r\n}\r\n\r\n.duel .silver-label {\r\n    position: relative;\r\n    top: 3px;\r\n    left: 3px;\r\n}\r\n\r\n.duel .right-user-block {\r\n    position: relative;\r\n    text-align: right;\r\n}\r\n\r\n.duel .tooltip-label {\r\n    position: relative;\r\n    top: 3px;\r\n}\r\n\r\n.duel .control-panel .button:last-child {\r\n    margin-left: 18px;\r\n}\r\n\r\n#duelTaskModal button.close {\r\n    position: relative;\r\n    right: -80px;\r\n    top: -35px;\r\n    font-size: 27px;\r\n    opacity: .6;\r\n}\r\n\r\n#duelTaskModal .modal-body {\r\n    padding-top: 40px;\r\n    padding-bottom: 40px;\r\n    padding-left: 45px;\r\n    padding-right: 45px;\r\n}\r\n\r\n#duelTaskModal .modal-body h2 {\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.duel .work-status-block {\r\n    width: 158px;\r\n    height: 144px;\r\n    outline: 1px solid rgba(220, 220, 220, 0.5);\r\n    margin-bottom: 7px;\r\n}\r\n\r\n.duel .work-status-block > div {\r\n    width: 158px;\r\n    height: 144px;\r\n}\r\n\r\n.duel .work-status-block .upload-work {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2222%22 height%3D%2224%22 viewBox%3D%220 0 22 24%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M15.4393 4.707L10.7323 0L6.02527 4.707L7.43927 6.121L9.73227 3.828V12.414H11.7323V3.828L14.0253 6.121L15.4393 4.707Z%22 fill%3D%22black%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M2.12 22L3.453 18H18.012L19.345 22H2.12ZM21.3649 21.735L19.7319 16.837V8C19.7319 7.448 19.2849 7 18.7319 7H15.7319V9H17.7319V16H3.73195V9H5.73195V7H2.73195C2.17995 7 1.73195 7.448 1.73195 8V16.837L0.0999475 21.734C-0.0860525 22.292 -0.0100525 22.864 0.307948 23.306C0.624948 23.747 1.14395 24 1.73195 24H19.7319C20.3199 24 20.8389 23.747 21.1569 23.306C21.4749 22.864 21.5509 22.292 21.3649 21.735Z%22 fill%3D%22black%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M12.7323 21H8.7323V19H12.7323V21Z%22 fill%3D%22black%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 40px no-repeat;\r\n}\r\n\r\n.duel .work-status-block .upload-work > div {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    text-align: center;\r\n    position: relative;\r\n    top: 85px;\r\n    color: #262626;\r\n}\r\n\r\n.duel .work-status-block .waiting-for-work {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2215%22 height%3D%2221%22 viewBox%3D%220 0 15 21%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M12.4616 3.33067C12.4616 3.60682 12.3505 3.87772 12.1573 4.07302L7.26931 9.01537L2.38127 4.07302C2.18811 3.87772 2.077 3.60682 2.077 3.33067V2.10007H12.4616V3.33067ZM12.4616 17.6706V18.9001H2.077V17.6706C2.077 17.3902 2.185 17.1267 2.38127 16.9282L7.26931 11.9848L12.1573 16.9282C12.3536 17.1267 12.4616 17.3902 12.4616 17.6706ZM14.5385 3.3306V1.05C14.5385 0.4704 14.0732 0 13.5 0H1.03846C0.465231 0 0 0.4704 0 1.05V3.3306C0 4.1601 0.332308 4.9707 0.912808 5.55765L5.80085 10.5L0.912808 15.4434C0.324 16.0387 0 16.8284 0 17.6705V19.95C0 20.5296 0.465231 21 1.03846 21H13.5C14.0732 21 14.5385 20.5296 14.5385 19.95V17.6705C14.5385 16.8284 14.2145 16.0387 13.6257 15.4434L8.73761 10.5L13.6257 5.55765C14.2062 4.9707 14.5385 4.1601 14.5385 3.3306Z%22 fill%3D%22%23D8E2E9%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M4.15381 17.8501L7.26919 14.7001L10.3846 17.8501H4.15381Z%22 fill%3D%22%23D8E2E9%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 26px no-repeat;\r\n}\r\n\r\n.duel .work-status-block .waiting-for-work > p {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 12px;\r\n    line-height: 17px;\r\n    text-align: center;\r\n    color: #5D5D5D;\r\n    position: relative;\r\n    top: 67px;\r\n}\r\n\r\n.duel .work-status-block .edit-work {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2222%22 height%3D%2223%22 viewBox%3D%220 0 22 23%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M12.1694 11.0448C12.1974 11.0317 12.2231 11.0144 12.2457 10.9935L20.8644 3.02494C21.478 2.45758 21.4814 1.48872 20.8717 0.917098C20.3206 0.400369 19.4638 0.397413 18.9091 0.910327L10.1436 9.01573C10.119 9.0385 10.0984 9.06524 10.0826 9.09483L8.42429 12.2044C8.28855 12.4589 8.55519 12.7397 8.81637 12.6172L12.1694 11.0448ZM21.3649 20.735L19.7319 15.837V9.00002H17.7319V15H3.73195V8.00002H7.73195L9.73195 6.00002H2.73195C2.17995 6.00002 1.73195 6.44802 1.73195 7.00002V15.837L0.0999475 20.734C-0.0860525 21.292 -0.0100525 21.864 0.307948 22.306C0.624948 22.747 1.14395 23 1.73195 23H19.7319C20.3199 23 20.8389 22.747 21.1569 22.306C21.4749 21.864 21.5509 21.292 21.3649 20.735ZM3.45299 17L2.12 21H19.345L18.012 17H3.45299ZM8.7323 20H12.7323V18H8.7323V20Z%22 fill%3D%22white%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 40px no-repeat, -webkit-gradient(linear, left bottom, left top, from(rgba(0, 0, 0, 0.5)), to(rgba(0, 0, 0, 0.5)));\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2222%22 height%3D%2223%22 viewBox%3D%220 0 22 23%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M12.1694 11.0448C12.1974 11.0317 12.2231 11.0144 12.2457 10.9935L20.8644 3.02494C21.478 2.45758 21.4814 1.48872 20.8717 0.917098C20.3206 0.400369 19.4638 0.397413 18.9091 0.910327L10.1436 9.01573C10.119 9.0385 10.0984 9.06524 10.0826 9.09483L8.42429 12.2044C8.28855 12.4589 8.55519 12.7397 8.81637 12.6172L12.1694 11.0448ZM21.3649 20.735L19.7319 15.837V9.00002H17.7319V15H3.73195V8.00002H7.73195L9.73195 6.00002H2.73195C2.17995 6.00002 1.73195 6.44802 1.73195 7.00002V15.837L0.0999475 20.734C-0.0860525 21.292 -0.0100525 21.864 0.307948 22.306C0.624948 22.747 1.14395 23 1.73195 23H19.7319C20.3199 23 20.8389 22.747 21.1569 22.306C21.4749 21.864 21.5509 21.292 21.3649 20.735ZM3.45299 17L2.12 21H19.345L18.012 17H3.45299ZM8.7323 20H12.7323V18H8.7323V20Z%22 fill%3D%22white%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 40px no-repeat, linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5));\r\n}\r\n\r\n.duel .work-status-block .edit-work > div {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    line-height: 17px;\r\n    text-align: center;\r\n    color: #FFFFFF;\r\n    position: relative;\r\n    top: 85px;\r\n}\r\n\r\n.duel .work-status-block .work-uploaded {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2222%22 height%3D%2220%22 viewBox%3D%220 0 22 20%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M7.37284 20L0 12.3457L2 10L7.0973 15.2762L19.5 0L22 1.43503L7.37284 20Z%22 fill%3D%22%2331D431%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 26px no-repeat;\r\n}\r\n\r\n.duel .work-status-block .work-uploaded > p {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 12px;\r\n    line-height: 17px;\r\n    text-align: center;\r\n    color: #5D5D5D;\r\n    position: relative;\r\n    top: 67px;\r\n}\r\n\r\n.duel .in-progress .digits-block {\r\n    margin-top: -107px;\r\n}\r\n\r\n.duel .voting .digits-block {\r\n    margin-top: -140px;\r\n}\r\n\r\n.duel .space {\r\n    height: 17px;\r\n}\r\n\r\n.duel .voting-wrapper {\r\n    margin: 0 auto;\r\n}\r\n\r\n.duel .voting-bar-container {\r\n    margin: 0 auto;\r\n    text-align: left;\r\n    position: relative;\r\n    top: -6px;\r\n}\r\n\r\n.duel .voting-bar {\r\n    height: 4px;\r\n    background-color: #D8E2E9;\r\n    display: inline-block;\r\n    position: relative;\r\n}\r\n\r\n.duel .percentage-block {\r\n    margin-top: 27px;\r\n}\r\n\r\n.duel .voting-bar.cover {\r\n    background-color: #26AFFF;\r\n}\r\n\r\n.duel .lost-voting-bar-wrapper,\r\n.duel .win-voting-bar-wrapper {\r\n    width: 137px;\r\n    height: 4px;\r\n    position: relative;\r\n    top: -39px;\r\n}\r\n\r\n.duel .win-voting-bar-wrapper {\r\n    top: -43px;\r\n}\r\n\r\n.duel .voting-bar.cover.ended {\r\n    background-color: #31D431;\r\n}\r\n\r\n.duel .voting-digits {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 20px;\r\n    line-height: 23px;\r\n    color: #5D5D5D;\r\n}\r\n\r\n.duel .voting-digits.small {\r\n    font-size: 12px;\r\n    line-height: 20px;\r\n}\r\n\r\n.duel .votes-amount-container {\r\n    margin-top: -30px;\r\n}\r\n\r\n.duel .winner-label {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    text-align: center;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #5D5D5D;\r\n    background-color: #FFE226;\r\n    padding: 3px;\r\n    position: relative;\r\n    top: 1px;\r\n}\r\n\r\n.duel .winner-label.left {\r\n    left: 5px;\r\n}\r\n\r\n.duel .winner-label.right {\r\n    right: 5px;\r\n}\r\n\r\n.duel .draw-label {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    text-align: center;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #5D5D5D;\r\n    background-color: #D8E2E9;\r\n    padding: 3px;\r\n    position: relative;\r\n    top: -4px;\r\n}\r\n\r\n.duel .votes-circle {\r\n    width: 30px;\r\n    display: block;\r\n    margin: 0 auto;\r\n    margin-top: -22px;\r\n}\r\n\r\n.duel.featured {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2222%22 viewBox%3D%220 0 16 22%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M0 22V0H16V22L8 13.8947L0 22Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 8px 0px no-repeat;\r\n}\r\n\r\n.duel .voting .work-status-block.open-modal-hover {\r\n    margin-top: -151px;\r\n    background: rgba(38, 38, 38, 0.3) url(\"data:image/svg+xml,%3Csvg width%3D%2224%22 height%3D%2224%22 viewBox%3D%220 0 24 24%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13 0H11V11H0V13H11V24H13V13H24V11H13V0Z%22 fill%3D%22white%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 46px no-repeat;\r\n    cursor: pointer;\r\n    position: relative;\r\n    -webkit-transition: opacity .2s ease-in-out;\r\n    transition: opacity .2s ease-in-out;\r\n    opacity: 0;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 205px;\r\n    text-align: center;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #FFFFFF;\r\n}\r\n\r\n.duel .voting .work-status-block.open-modal-hover:hover {\r\n    -webkit-transition: opacity .2s ease-in-out;\r\n    transition: opacity .2s ease-in-out;\r\n    opacity: 1;\r\n}"

/***/ }),

/***/ "./src/app/child-components/duel/duel.component.html":
/***/ (function(module, exports) {

module.exports = "<div #duelBlock *ngIf=\"duel && showComponent\" class=\"duel {{sizeClass}}\" [class.hoverable]=\"(duel.status === duelStatus.Voting || duel.status === duelStatus.Ended) && leftUserModalId === undefined && rightUserModalId === undefined\"\r\n  [class.featured]=\"duel.isFeatured\">\r\n  <!-- Modals -->\r\n  <app-work-modal *ngIf=\"leftUserModalId\" [current]=\"duel.leftUser\" [opponent]=\"duel.rightUser\" [modalId]=\"leftUserModalId\"></app-work-modal>\r\n  <app-work-modal *ngIf=\"rightUserModalId\" [current]=\"duel.rightUser\" [opponent]=\"duel.leftUser\" [modalId]=\"rightUserModalId\"></app-work-modal>\r\n  <div class=\"modal fade\" id=\"duelTaskModal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-body\">\r\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n          <h2>Duel Task</h2>\r\n          <p>{{duel.duelTask}}</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- End Modals -->\r\n  <!-- Waiting status  -->\r\n  <div *ngIf=\"duel.status === duelStatus.Waiting\">\r\n    <div class=\"center\">\r\n      <p class=\"category\">{{duel.duelCategory.name}}</p>\r\n    </div>\r\n    <div class=\"space\"></div>\r\n    <div class=\"left\">\r\n      <div>\r\n        <span class=\"digits\">{{duel.date}}</span>\r\n        <p *ngIf=\"isCurrentUser(duel.leftUser.user.id)\" class=\"text\">left for {{duel.rightUser.user.fullname}} to decide\r\n          <span #tooltip class=\"tooltip-label custom-tooltip duel-tooltip\">\r\n            <span class=\"tooltiptext\">Some text</span>\r\n          </span>\r\n        </p>\r\n        <p *ngIf=\"!isCurrentUser(duel.leftUser.user.id)\" class=\"text\">left for you to decide\r\n          <span #tooltip class=\"tooltip-label custom-tooltip duel-tooltip\">\r\n            <span class=\"tooltiptext\">Some text</span>\r\n          </span>\r\n        </p>\r\n      </div>\r\n      <div>\r\n        <span class=\"digits\">{{duel.deadlineInDays}} days</span>\r\n        <p class=\"text\">deadline for this duel</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"right\">\r\n      <div *ngIf=\"isCurrentUser(duel.leftUser.user.id)\">\r\n        <a class=\"button secondary-button\" (click)=\"removeDuel(duel.id, 'revoked')\">\r\n          <span>Revoke</span>\r\n        </a>\r\n      </div>\r\n      <div *ngIf=\"!isCurrentUser(duel.leftUser.user.id)\" class=\"control-panel\">\r\n        <a class=\"button secondary-button\" (click)=\"removeDuel(duel.id, 'declined')\">\r\n          <span>Decline</span>\r\n        </a>\r\n        <a class=\"button primary-button\" (click)=\"acceptDuel(duel.id)\">\r\n          <span>Accept</span>\r\n        </a>\r\n      </div>\r\n      <button class=\"open-task-button right\" (click)=\"openModal('duelTaskModal')\">Open task</button>\r\n      <div class=\"clearfix\"></div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <div>\r\n      <div class=\"left\">\r\n        <div>\r\n          <span class=\"rating\">#{{duel.leftUser.rating}}</span>\r\n        </div>\r\n        <div>\r\n          <h3>{{duel.leftUser.user.fullname}}</h3>\r\n          <span *ngIf=\"duel.leftUser.user.isPaidAccount\" class=\"silver-label\"></span>\r\n        </div>\r\n      </div>\r\n      <div class=\"right-user-block right\">\r\n        <div>\r\n          <span class=\"rating\">#{{duel.rightUser.rating}}</span>\r\n        </div>\r\n        <div>\r\n          <h3>{{duel.rightUser.user.fullname}}</h3>\r\n          <span *ngIf=\"duel.rightUser.user.isPaidAccount\" class=\"silver-label\"></span>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n    </div>\r\n  </div>\r\n  <!-- End Waiting status -->\r\n  <!-- In Progress status -->\r\n  <div *ngIf=\"duel.status === duelStatus.InProgress\" class=\"in-progress\">\r\n    <div class=\"center\">\r\n      <p class=\"category\">{{duel.duelCategory.name}}</p>\r\n    </div>\r\n    <div class=\"left\">\r\n      <div *ngIf=\"isCurrentUser(duel.leftUser.user.id)\" (click)=\"openWorkPage(duel.numericId)\" blockBackground [background]=\"duel.leftUser.previewImageUrl\"\r\n        class=\"work-status-block\">\r\n        <div *ngIf=\"!isWorkUploaded(duel.leftUser.previewImageUrl)\" class=\"upload-work\">\r\n          <div>Upload Work</div>\r\n        </div>\r\n        <div *ngIf=\"isWorkUploaded(duel.leftUser.previewImageUrl)\" class=\"edit-work\">\r\n          <div>Edit Work</div>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"!isCurrentUser(duel.leftUser.user.id)\" class=\"work-status-block\">\r\n        <div *ngIf=\"!isWorkUploaded(duel.leftUser.previewImageUrl)\" class=\"waiting-for-work\">\r\n          <p>Still waiting for\r\n            <br>{{duel.leftUser.user.fullname}}\r\n            <br>work</p>\r\n        </div>\r\n        <div *ngIf=\"isWorkUploaded(duel.leftUser.previewImageUrl)\" class=\"work-uploaded\">\r\n          <p>{{duel.leftUser.user.fullname}}\r\n            <br> handed in their work.\r\n            <br> It's all on you now!</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"right\">\r\n      <div *ngIf=\"isCurrentUser(duel.rightUser.user.id)\" (click)=\"openWorkPage(duel.numericId)\" blockBackground [background]=\"duel.rightUser.previewImageUrl\"\r\n        class=\"work-status-block\">\r\n        <div *ngIf=\"!isWorkUploaded(duel.rightUser.previewImageUrl)\" class=\"upload-work\">\r\n          <div>Upload Work</div>\r\n        </div>\r\n        <div *ngIf=\"isWorkUploaded(duel.rightUser.previewImageUrl)\" class=\"edit-work\">\r\n          <div>Edit Work</div>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"!isCurrentUser(duel.rightUser.user.id)\" class=\"work-status-block\">\r\n        <div *ngIf=\"!isWorkUploaded(duel.rightUser.previewImageUrl)\" class=\"waiting-for-work\">\r\n          <p>Still waiting for\r\n            <br>{{duel.rightUser.user.fullname}}\r\n            <br>work</p>\r\n        </div>\r\n        <div *ngIf=\"isWorkUploaded(duel.rightUser.previewImageUrl)\" class=\"work-uploaded\">\r\n          <p>{{duel.rightUser.user.fullname}}\r\n            <br> handed in their work.\r\n            <br> It's all on you now!</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <div class=\"digits-block center\">\r\n      <span class=\"digits\">{{duel.date}}</span>\r\n      <p class=\"small\">left to upload works</p>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <div>\r\n      <div class=\"left\">\r\n        <div>\r\n          <span class=\"rating\">#{{duel.leftUser.rating}}</span>\r\n        </div>\r\n        <div>\r\n          <h3>{{duel.leftUser.user.fullname}}</h3>\r\n          <span *ngIf=\"duel.leftUser.user.isPaidAccount\" class=\"silver-label\"></span>\r\n        </div>\r\n      </div>\r\n      <div class=\"right-user-block right\">\r\n        <div>\r\n          <span class=\"rating\">#{{duel.rightUser.rating}}</span>\r\n        </div>\r\n        <div>\r\n          <h3>{{duel.rightUser.user.fullname}}</h3>\r\n          <span *ngIf=\"duel.rightUser.user.isPaidAccount\" class=\"silver-label\"></span>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n    </div>\r\n  </div>\r\n  <!-- End In progress status -->\r\n  <!-- Voting status -->\r\n  <div *ngIf=\"duel.status === duelStatus.Voting\" class=\"voting\">\r\n    <div class=\"center\">\r\n      <p class=\"category\">{{duel.duelCategory.name}}</p>\r\n    </div>\r\n    <div class=\"left\">\r\n      <div class=\"work-status-block\">\r\n        <img width=\"158\" height=\"144\" alt=\"Preview Image\" src=\"{{duel.leftUser.previewImageUrl.url}}\">\r\n      </div>\r\n      <div *ngIf=\"leftUserModalId !== undefined\" class=\"work-status-block open-modal-hover\" (click)=\"openModal(leftUserModalId)\">\r\n        Open solution\r\n      </div>\r\n    </div>\r\n    <div class=\"right\">\r\n      <div class=\"work-status-block\">\r\n        <img width=\"158\" height=\"144\" alt=\"Preview Image\" src=\"{{duel.rightUser.previewImageUrl.url}}\">\r\n      </div>\r\n      <div *ngIf=\"rightUserModalId !== undefined\" class=\"work-status-block open-modal-hover\" (click)=\"openModal(rightUserModalId)\">\r\n        Open solution\r\n      </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <div class=\"digits-block center\">\r\n      <span class=\"digits\">{{duel.date}}</span>\r\n      <p class=\"small\">until voting ends</p>\r\n    </div>\r\n    <div #votingWrapper class=\"center voting-wrapper\">\r\n      <div class=\"percentage-block\">\r\n        <div class=\"left voting-digits\">{{getPercentageValue(duel.leftUser.votesAmount, duel.leftUser.votesAmount + duel.rightUser.votesAmount)}}%</div>\r\n        <div class=\"right voting-digits\">{{getPercentageValue(duel.rightUser.votesAmount, duel.leftUser.votesAmount + duel.rightUser.votesAmount)}}%</div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"voting-bar-container\">\r\n        <div class=\"lost-voting-bar-wrapper\">\r\n          <div #lostVotingBar id=\"lost-voting-bar\" class=\"voting-bar\"></div>\r\n        </div>\r\n        <div class=\"win-voting-bar-wrapper\">\r\n          <div #winVotingBar id=\"win-voting-bar\" class=\"voting-bar cover\"></div>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"votes-amount-container\">\r\n        <div class=\"left voting-digits small\">{{duel.leftUser.votesAmount}}</div>\r\n        <div class=\"right voting-digits small\">{{duel.rightUser.votesAmount}}</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <div>\r\n      <div class=\"left\">\r\n        <div>\r\n          <span class=\"rating\">#{{duel.leftUser.rating}}</span>\r\n        </div>\r\n        <div>\r\n          <h3>{{duel.leftUser.user.fullname}}</h3>\r\n          <span *ngIf=\"duel.leftUser.user.isPaidAccount\" class=\"silver-label\"></span>\r\n        </div>\r\n      </div>\r\n      <div class=\"right-user-block right\">\r\n        <div>\r\n          <span class=\"rating\">#{{duel.rightUser.rating}}</span>\r\n        </div>\r\n        <div>\r\n          <h3>{{duel.rightUser.user.fullname}}</h3>\r\n          <span *ngIf=\"duel.rightUser.user.isPaidAccount\" class=\"silver-label\"></span>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n    </div>\r\n    <div *ngIf=\"duel.leftUser.isSelected || duel.rightUser.isSelected\" class=\"votes-circle\">\r\n      <div class=\"circle\" [class.selected]=\"duel.leftUser.isSelected\"></div>\r\n      <div class=\"circle\" [class.selected]=\"duel.rightUser.isSelected\"></div>\r\n    </div>\r\n  </div>\r\n  <!-- End Voting status -->\r\n  <!-- Ended status -->\r\n  <div *ngIf=\"duel.status === duelStatus.Ended\" class=\"voting\">\r\n    <div class=\"center\">\r\n      <p class=\"category\">{{duel.duelCategory.name}}</p>\r\n    </div>\r\n    <div class=\"left\">\r\n      <div class=\"work-status-block\">\r\n        <img width=\"158\" height=\"144\" alt=\"Preview Image\" src=\"{{duel.leftUser.previewImageUrl.url}}\">\r\n      </div>\r\n      <div *ngIf=\"leftUserModalId !== undefined\" class=\"work-status-block open-modal-hover\" (click)=\"openModal(leftUserModalId)\">\r\n        Open solution\r\n      </div>\r\n    </div>\r\n    <div class=\"right\">\r\n      <div class=\"work-status-block\">\r\n        <img width=\"158\" height=\"144\" alt=\"Preview Image\" src=\"{{duel.rightUser.previewImageUrl.url}}\">\r\n      </div>\r\n      <div *ngIf=\"rightUserModalId !== undefined\" class=\"work-status-block open-modal-hover\" (click)=\"openModal(rightUserModalId)\">\r\n        Open solution\r\n      </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <div class=\"digits-block center\">\r\n      <span class=\"digits\">{{duel.date}}</span>\r\n      <p class=\"small\">duel has ended</p>\r\n    </div>\r\n    <div #votingWrapper class=\"center voting-wrapper\">\r\n      <div class=\"percentage-block\">\r\n        <div class=\"left voting-digits\">{{getPercentageValue(duel.leftUser.votesAmount, duel.leftUser.votesAmount + duel.rightUser.votesAmount)}}%</div>\r\n        <span class=\"left winner-label\" *ngIf=\"duel.leftUser.votesAmount > duel.rightUser.votesAmount\">Won</span>\r\n        <span class=\"draw-label\" *ngIf=\"duel.leftUser.votesAmount === duel.rightUser.votesAmount\">Draw</span>\r\n        <div class=\"right voting-digits\">{{getPercentageValue(duel.rightUser.votesAmount, duel.leftUser.votesAmount + duel.rightUser.votesAmount)}}%</div>\r\n        <span class=\"right winner-label\" *ngIf=\"duel.leftUser.votesAmount < duel.rightUser.votesAmount\">Won</span>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"voting-bar-container\">\r\n        <div class=\"lost-voting-bar-wrapper\">\r\n          <div #lostVotingBar class=\"voting-bar\"></div>\r\n        </div>\r\n        <div class=\"win-voting-bar-wrapper\">\r\n          <div #winVotingBar class=\"voting-bar cover ended\"></div>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"votes-amount-container\">\r\n        <div class=\"left voting-digits small\">{{duel.leftUser.votesAmount}}</div>\r\n        <div class=\"right voting-digits small\">{{duel.rightUser.votesAmount}}</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <div>\r\n      <div class=\"left\">\r\n        <div>\r\n          <span class=\"rating\">#{{duel.leftUser.rating}}</span>\r\n        </div>\r\n        <div>\r\n          <h3>{{duel.leftUser.user.fullname}}</h3>\r\n          <span *ngIf=\"duel.leftUser.user.isPaidAccount\" class=\"silver-label\"></span>\r\n        </div>\r\n      </div>\r\n      <div class=\"right-user-block right\">\r\n        <div>\r\n          <span class=\"rating\">#{{duel.rightUser.rating}}</span>\r\n        </div>\r\n        <div>\r\n          <h3>{{duel.rightUser.user.fullname}}</h3>\r\n          <span *ngIf=\"duel.rightUser.user.isPaidAccount\" class=\"silver-label\"></span>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n    </div>\r\n    <div *ngIf=\"duel.leftUser.isSelected || duel.rightUser.isSelected\" class=\"votes-circle\">\r\n      <div class=\"circle\" [class.selected]=\"duel.leftUser.isSelected\"></div>\r\n      <div class=\"circle\" [class.selected]=\"duel.rightUser.isSelected\"></div>\r\n    </div>\r\n  </div>\r\n  <!-- End Ended status -->\r\n</div>"

/***/ }),

/***/ "./src/app/child-components/duel/duel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DuelComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__ = __webpack_require__("./src/app/enums/duel-status.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_duel_status_message_duel_status_message_service__ = __webpack_require__("./src/app/services/duel-status-message/duel-status-message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_duel_timer_duel_timer_service__ = __webpack_require__("./src/app/services/duel-timer/duel-timer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_date_formatter_date_formatter_service__ = __webpack_require__("./src/app/services/date-formatter/date-formatter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_user_services_user_service__ = __webpack_require__("./src/app/components/user/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_tooltip_tooltip_service__ = __webpack_require__("./src/app/services/tooltip/tooltip.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var DuelComponent = /** @class */ (function () {
    function DuelComponent(duelStatusMessageService, duelTimer, dateFormatter, authService, duelService, userService, validationService, alertService, router, tooltipService) {
        this.duelStatusMessageService = duelStatusMessageService;
        this.duelTimer = duelTimer;
        this.dateFormatter = dateFormatter;
        this.authService = authService;
        this.duelService = duelService;
        this.userService = userService;
        this.validationService = validationService;
        this.alertService = alertService;
        this.router = router;
        this.tooltipService = tooltipService;
        this.notify = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.duelStatus = __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */];
        this.showComponent = false;
    }
    DuelComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.showComponent = true;
            setTimeout(function () {
                _this.tooltipService.initializeTooltip('.duel-tooltip');
            }, 0);
        }, this.duelTimer.timerInterval);
    };
    DuelComponent.prototype.ngOnChanges = function (changes) {
        this.setDuelInformation(changes.duel.currentValue);
    };
    DuelComponent.prototype.getPercentageValue = function (value, totalValue) {
        if (totalValue === 0) {
            return 0;
        }
        return Math.round(value * 100 / totalValue);
    };
    DuelComponent.prototype.setDuelInformation = function (duel) {
        var _this = this;
        duel.statusString = this.duelStatusMessageService.getStatusMessage(duel);
        if (duel.status === __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].Waiting) {
            duel.deadlineInDays = this.dateFormatter.getDateDifferenceInDays(duel.deadline, duel.createdAt) - 1;
        }
        if (duel.status !== __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].Ended) {
            if (duel.status === __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].Voting) {
                setTimeout(function () {
                    _this.setVotingBarStyles(duel.leftUser.votesAmount, duel.rightUser.votesAmount);
                }, 1005);
            }
            var timerIntervalId = setInterval(function () {
                var timerDate = _this.duelTimer.timer(duel.endDate);
                if (timerDate !== null) {
                    duel.date = _this.getTimerString(timerDate);
                }
                else {
                    switch (duel.status) {
                        case __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].Waiting:
                        case __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].InProgress:
                        case __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].Voting:
                            _this.notify.emit(_this.duelOwner);
                            break;
                    }
                    clearInterval(timerIntervalId);
                }
            }, this.duelTimer.timerInterval);
        }
        else {
            duel.date = this.dateFormatter.toShortFormatWithoutYear(duel.endDate);
            setTimeout(function () {
                _this.setVotingBarStyles(duel.leftUser.votesAmount, duel.rightUser.votesAmount);
            }, 1005);
        }
    };
    DuelComponent.prototype.isCurrentUser = function (userId) {
        return this.authService.getCurrentUser().id === userId;
    };
    DuelComponent.prototype.isWorkUploaded = function (previewImage) {
        return this.validationService.isObjectValid(previewImage.url);
    };
    DuelComponent.prototype.removeDuel = function (duelId, messageState) {
        var _this = this;
        this.duelService.removeDuel(duelId).subscribe(function (response) {
            if (response.success) {
                _this.notify.emit(response.duels);
                _this.alertService.success("Duel has been " + messageState + " and deleted");
            }
            else {
                console.log(response.error);
                _this.alertService.error("Duel could not be " + messageState + ". Please try again");
            }
        });
    };
    DuelComponent.prototype.acceptDuel = function (duelId) {
        var _this = this;
        this.duelService.acceptDuel(duelId).subscribe(function (response) {
            if (response.success) {
                _this.notify.emit(response.duels);
                _this.alertService.success("Duel has been accepted. Get working!");
            }
            else {
                console.log(response.error);
                _this.alertService.error("Duel could not be accepted. Please try again");
            }
        });
    };
    DuelComponent.prototype.openWorkPage = function (duelId) {
        this.router.navigate(["/work", duelId]);
    };
    DuelComponent.prototype.openModal = function (modalId) {
        if (modalId) {
            $("#" + modalId).modal('show');
        }
    };
    DuelComponent.prototype.setVotingBarStyles = function (leftVotesAmount, rightVotesAmount) {
        var votingBarSpace = 416;
        if (this.winVotingBar !== undefined && this.lostVotingBar !== undefined) {
            var winVotingBar = $(this.winVotingBar.nativeElement), lostVotingBar = $(this.lostVotingBar.nativeElement), votingWrapper = $(this.votingWrapper.nativeElement);
            var votingBarWidth = Math.round(this.duelBlock.nativeElement.offsetWidth - votingBarSpace);
            winVotingBar.css("width", votingBarWidth);
            lostVotingBar.css("width", votingBarWidth);
            votingWrapper.css("width", votingBarWidth);
            if (leftVotesAmount > rightVotesAmount) {
                winVotingBar.css("z-index", "10");
                lostVotingBar.css("z-index", "1");
                winVotingBar.css("width", votingBarWidth * this.getPercentageValue(leftVotesAmount, leftVotesAmount + rightVotesAmount) / 100);
            }
            else if (leftVotesAmount < rightVotesAmount) {
                lostVotingBar.css("z-index", "10");
                winVotingBar.css("z-index", "1");
                lostVotingBar.css("width", votingBarWidth * this.getPercentageValue(leftVotesAmount, leftVotesAmount + rightVotesAmount) / 100);
            }
            else {
                winVotingBar.css("z-index", "10");
                lostVotingBar.css("z-index", "1");
                winVotingBar.css("width", votingBarWidth);
            }
        }
    };
    DuelComponent.prototype.getTimerString = function (timer) {
        var timerString = timer.hours + ":" + timer.minutes;
        var daysNumber = parseInt(timer.days);
        if (daysNumber > 0) {
            timerString = timer.days + " day" + (daysNumber > 1 ? "s" : "");
        }
        return timerString;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DuelComponent.prototype, "duel", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DuelComponent.prototype, "duelOwner", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], DuelComponent.prototype, "leftUserModalId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], DuelComponent.prototype, "rightUserModalId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], DuelComponent.prototype, "sizeClass", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], DuelComponent.prototype, "notify", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lostVotingBar'),
        __metadata("design:type", Object)
    ], DuelComponent.prototype, "lostVotingBar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('winVotingBar'),
        __metadata("design:type", Object)
    ], DuelComponent.prototype, "winVotingBar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('duelBlock'),
        __metadata("design:type", Object)
    ], DuelComponent.prototype, "duelBlock", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('votingWrapper'),
        __metadata("design:type", Object)
    ], DuelComponent.prototype, "votingWrapper", void 0);
    DuelComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-duel',
            template: __webpack_require__("./src/app/child-components/duel/duel.component.html"),
            styles: [__webpack_require__("./src/app/child-components/duel/duel.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_duel_status_message_duel_status_message_service__["a" /* DuelStatusMessageService */],
            __WEBPACK_IMPORTED_MODULE_4__services_duel_timer_duel_timer_service__["a" /* DuelTimerService */],
            __WEBPACK_IMPORTED_MODULE_5__services_date_formatter_date_formatter_service__["a" /* DateFormatterService */],
            __WEBPACK_IMPORTED_MODULE_6__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_7__services_duel_service_duel_service__["a" /* DuelService */],
            __WEBPACK_IMPORTED_MODULE_8__components_user_services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_9__services_validation_validation_service__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_10__services_alert_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_11__services_tooltip_tooltip_service__["a" /* TooltipService */]])
    ], DuelComponent);
    return DuelComponent;
}());



/***/ }),

/***/ "./src/app/child-components/input/input.component.css":
/***/ (function(module, exports) {

module.exports = ".custom-input {\r\n    position: relative;\r\n    margin: auto;\r\n    width: 100%;\r\n    border: 1px solid #C1C7CB;\r\n    height: 60px;\r\n}\r\n\r\n.custom-input.text-area {\r\n    height: 128px;\r\n}\r\n\r\n.custom-input.big-textarea.text-area {\r\n    height: 200px;\r\n}\r\n\r\n.custom-input .label {\r\n    position: absolute;\r\n    top: 20px;\r\n    left: 0;\r\n    -webkit-transform-origin: 0 0;\r\n            transform-origin: 0 0;\r\n    -webkit-transition: all 0.2s ease;\r\n    transition: all 0.2s ease;\r\n    display: inline;\r\n    color: #C1C7CB;\r\n    background-color: #fff;\r\n    margin-left: 20px;\r\n    padding-left: 10px;\r\n    padding-right: 10px;\r\n}\r\n\r\n.custom-input input,\r\n.custom-input textarea {\r\n    -webkit-appearance: none;\r\n    width: 100%;\r\n    border: 0;\r\n    padding: 12px 0;\r\n    background: none;\r\n    height: 60px;\r\n    border-radius: 0;\r\n    border: 0;\r\n    -webkit-transition: all 0.15s ease;\r\n    transition: all 0.15s ease;\r\n    padding-left: 30px;\r\n    padding-right: 30px;\r\n}\r\n\r\n.custom-input textarea {\r\n    height: 100px;\r\n    -ms-overflow-style: none; \r\n    overflow: -moz-scrollbars-none;\r\n    margin-top: 12px;\r\n}\r\n\r\n.custom-input.big-textarea textarea {\r\n    height: 160px;\r\n}\r\n\r\n.custom-input textarea::-webkit-scrollbar { \r\n    width: 0 !important; \r\n}\r\n\r\n.custom-input input.error,\r\n.custom-input textarea.error {\r\n    -webkit-appearance: none;\r\n    width: 100%;\r\n    border: 0;\r\n    padding: 12px 0;\r\n    height: 60px;\r\n    border: 1px solid #F55045;\r\n    background: none;\r\n    border-radius: 0;\r\n    -webkit-transition: all 0.15s ease;\r\n    transition: all 0.15s ease;\r\n    padding-left: 30px;\r\n}\r\n\r\n.custom-input input:not(:placeholder-shown) + p,\r\n.custom-input textarea:not(:placeholder-shown) + p {\r\n    -webkit-transform: translateY(-30px);\r\n            transform: translateY(-30px);\r\n    color: #5D5D5D;\r\n}\r\n\r\n.custom-input input:focus,\r\n.custom-input textarea:focus {\r\n    background: none;\r\n    outline: none;\r\n}\r\n\r\n.custom-input input:focus + p,\r\n.custom-input textarea:focus + p {\r\n    color: #5D5D5D;\r\n    -webkit-transform: translateY(-30px);\r\n            transform: translateY(-30px);\r\n}\r\n\r\n.custom-input .error {\r\n    height: 18px;\r\n    margin: 0;\r\n    padding-left: 30px;\r\n    display: inline-block;\r\n    line-height: 24px;\r\n}\r\n\r\n.custom-input input.password {\r\n    -webkit-text-security: disc;\r\n}\r\n\r\np.error {\r\n    opacity: 0;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n}\r\n\r\np.error.visible {\r\n    opacity: 1;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n}\r\n\r\n.custom-input p.counter {\r\n    right: 0;\r\n    padding-right: 10px;\r\n    font-family: Menoe Grotesque Pro;\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 12px;\r\n    line-height: 20px;\r\n    color: #F55045;\r\n    opacity: 0.5;\r\n    bottom: -15px;\r\n    position: absolute;\r\n}"

/***/ }),

/***/ "./src/app/child-components/input/input.component.html":
/***/ (function(module, exports) {

module.exports = "<label class=\"{{elementClass}} custom-input\" [class.text-area]=\"type === inputType.Textarea\">\r\n  <input *ngIf=\"type !== inputType.Textarea\" [(ngModel)]=\"value\" (ngModelChange)=\"onChange($event)\" type=\"text\" placeholder=\"&nbsp;\" [class.password]=\"type === inputType.Password\" [class.error]=\"showError\">\r\n  <textarea *ngIf=\"type === inputType.Textarea\" [(ngModel)]=\"value\" (ngModelChange)=\"onChange($event)\" type=\"text\" placeholder=\"&nbsp;\" [class.password]=\"type === inputType.Password\" [class.error]=\"showError\"></textarea>\r\n  <p class=\"label\">{{placeholder}}</p>\r\n  <p *ngIf=\"counterMinLength !== undefined && getCounterValue() > 0\" class=\"counter\">\r\n    <span>{{getCounterValue()}}</span>\r\n  </p>\r\n  <p class=\"error\" [class.visible]=\"showError\">{{errorMessage}}</p>\r\n</label>"

/***/ }),

/***/ "./src/app/child-components/input/input.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__enums_input_type__ = __webpack_require__("./src/app/enums/input-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InputComponent = /** @class */ (function () {
    function InputComponent(validationService) {
        this.validationService = validationService;
        this.showError = false;
        this.elementClass = '';
        this.onValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.inputType = __WEBPACK_IMPORTED_MODULE_1__enums_input_type__["a" /* InputType */];
    }
    Object.defineProperty(InputComponent.prototype, "error", {
        set: function (error) {
            var _this = this;
            this.showError = this.validationService.isStringValid(error);
            if (this.showError) {
                this.errorMessage = error;
            }
            else {
                setTimeout(function () { _this.errorMessage = error; }, 300);
            }
        },
        enumerable: true,
        configurable: true
    });
    InputComponent.prototype.ngOnInit = function () {
    };
    InputComponent.prototype.onChange = function (e) {
        this.onValueChange.emit(this.value);
    };
    InputComponent.prototype.getCounterValue = function () {
        return this.counterMinLength - this.value.replace(/ /g, '').length;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], InputComponent.prototype, "placeholder", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], InputComponent.prototype, "value", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], InputComponent.prototype, "type", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], InputComponent.prototype, "counterMinLength", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], InputComponent.prototype, "elementClass", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], InputComponent.prototype, "onValueChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], InputComponent.prototype, "error", null);
    InputComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-input',
            template: __webpack_require__("./src/app/child-components/input/input.component.html"),
            styles: [__webpack_require__("./src/app/child-components/input/input.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_validation_validation_service__["a" /* ValidationService */]])
    ], InputComponent);
    return InputComponent;
}());



/***/ }),

/***/ "./src/app/child-components/report-comment/report-comment.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/child-components/report-comment/report-comment.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"reportCommentModal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h1>Reporting a Comment</h1>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <p>Do you really want to report this comment?</p>\r\n        <div>\r\n          <button class=\"inline\" data-dismiss=\"modal\">Cancel</button>\r\n          <button class=\"inline\" (click)=\"onConfirmReport()\">Report</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/child-components/report-comment/report-comment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportCommentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_report_report_service__ = __webpack_require__("./src/app/services/report/report.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReportCommentComponent = /** @class */ (function () {
    function ReportCommentComponent(reportService) {
        this.reportService = reportService;
    }
    ReportCommentComponent.prototype.ngOnInit = function () {
    };
    ReportCommentComponent.prototype.onConfirmReport = function () {
        var _this = this;
        this.reportService.reportComment().subscribe(function (response) {
            if (response.success) {
                alert("Report has been successfully sent!");
            }
            else {
                console.log(response.error);
            }
            _this.reportService.closeReportCommentModal();
        });
    };
    ReportCommentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-report-comment',
            template: __webpack_require__("./src/app/child-components/report-comment/report-comment.component.html"),
            styles: [__webpack_require__("./src/app/child-components/report-comment/report-comment.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_report_report_service__["a" /* ReportService */]])
    ], ReportCommentComponent);
    return ReportCommentComponent;
}());



/***/ }),

/***/ "./src/app/child-components/user-list/services/user-list.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserListService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_pager_pager_service__ = __webpack_require__("./src/app/services/pager/pager.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UserListService = /** @class */ (function () {
    function UserListService(tokenProviderService, http, headersProviderService, pager) {
        this.tokenProviderService = tokenProviderService;
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.pager = pager;
        this.modalId = "userList";
        this.userIds = [];
        this.totalUserCount = 0;
        this.heading = null;
        this.users = [];
        this.pageSize = 6;
        this.pages = [];
    }
    UserListService.prototype.closeUserListModal = function () {
        $('#' + this.modalId).modal('hide');
        this.heading = null;
        this.users = [];
    };
    UserListService.prototype.getUsersAndOpenModal = function (heading, userIds, orderCriteria) {
        var _this = this;
        this.userIds = userIds;
        this.heading = heading;
        this.orderCriteria = orderCriteria;
        this.getUserList(this.userIds, orderCriteria, 1, this.pageSize).subscribe(function (response) {
            if (response.success) {
                $('#' + _this.modalId).modal();
                _this.users = response.data.users;
                _this.totalUserCount = response.data.totalCount;
                _this.pages = _this.pager.getPager(response.data.totalCount, 1, _this.pageSize);
            }
            else {
                console.log(response.error);
            }
        });
    };
    UserListService.prototype.setPage = function (page, isActive, isCurrentPage) {
        var _this = this;
        if (isActive === void 0) { isActive = true; }
        if (isCurrentPage === void 0) { isCurrentPage = false; }
        if (isActive && !isCurrentPage) {
            this.getUserList(this.userIds, this.orderCriteria, page, this.pageSize).subscribe(function (response) {
                if (response.success) {
                    _this.users = response.data.users;
                    _this.totalUserCount = response.data.totalCount;
                    _this.pages = _this.pager.getPager(response.data.totalCount, page, _this.pageSize);
                }
                else {
                    console.log(response.error);
                }
            });
        }
    };
    UserListService.prototype.getUserList = function (userIds, orderCriteria, page, pageSize) {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].serverEndpoint + "/user/get-user-list", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                userIds: userIds,
                page: page,
                pageSize: pageSize,
                orderCriteria: orderCriteria
            }
        })
            .map(function (res) { return res.json(); });
    };
    UserListService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_2__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_5__services_pager_pager_service__["a" /* PagerService */]])
    ], UserListService);
    return UserListService;
}());



/***/ }),

/***/ "./src/app/child-components/user-list/user-list.component.css":
/***/ (function(module, exports) {

module.exports = ".modal-dialog {\r\n    min-width: 720px;\r\n}\r\n\r\n.pager-block {\r\n    display: inline-block;\r\n}\r\n\r\n.modal-header {\r\n    padding: 0;\r\n    border: 0;\r\n    margin-bottom: 30px;\r\n}\r\n\r\n.modal-header h2 span {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 20px;\r\n    line-height: 24px;\r\n    color: #C1C7CB;\r\n}\r\n\r\n.modal-content {\r\n    padding: 40px;\r\n}\r\n\r\n.user-name {\r\n    color: #5D5D5D;\r\n}\r\n\r\n.skill-points {\r\n    color: #5D5D5D;\r\n    opacity: .5;\r\n}\r\n\r\n.user-info {\r\n    position: relative;\r\n    top: 6px;\r\n}\r\n\r\n.hr-line {\r\n    height: 1px;\r\n    background-color: #DCDCDC;\r\n    opacity: .5;\r\n    margin-top: 25px;\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.silver-label {\r\n    position: relative;\r\n    top: 3px;\r\n}\r\n\r\n.avatar {\r\n    margin-right: 15px;\r\n}\r\n\r\n.pager-block {\r\n    margin-top: 10px;\r\n}"

/***/ }),

/***/ "./src/app/child-components/user-list/user-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"{{userListService.modalId}}\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h2>{{userListService.heading}}\r\n          <span *ngIf=\"userListService.totalUserCount > 0\">{{userListService.totalUserCount}}</span>\r\n        </h2>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div *ngIf=\"userListService.users.length > 0\">\r\n          <div (click)=\"goToPerson(user.username)\" *ngFor=\"let user of userListService.users\" class=\"clickable\">\r\n            <div class=\"inline avatar\">\r\n              <app-profile-picture elementClass=\"medium\" fullname={{user.fullname}} userId={{user.id}} backgroundImage={{user.imageUrl}}\r\n                backgroundColor={{user.imageColor}}>\r\n              </app-profile-picture>\r\n            </div>\r\n            <div class=\"inline user-info\">\r\n              <h3 class=\"user-name\">{{user.fullname}}\r\n                <span *ngIf=\"user.isPaidAccount\" class=\"silver-label\"></span>\r\n              </h3>\r\n              <p class=\"skill-points\">{{user.skillPoints}} skill</p>\r\n            </div>\r\n            <div *ngIf=\"userListService.users.indexOf(user) !== userListService.users.length - 1\" class=\"hr-line\"></div>\r\n          </div>\r\n        </div>\r\n        <div class=\"center\">\r\n          <div class=\"pager-block\">\r\n            <div *ngFor=\"let page of userListService.pages\" [class.active]=\"page.isActive\" [class.selected]=\"page.isCurrentPage\" (click)=\"userListService.setPage(page.symbol, page.isActive, page.isCurrentPage)\">\r\n              <div>{{page.symbol}}</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"footer-block\"></div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/child-components/user-list/user-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_list_service__ = __webpack_require__("./src/app/child-components/user-list/services/user-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserListComponent = /** @class */ (function () {
    function UserListComponent(userListService, router) {
        this.userListService = userListService;
        this.router = router;
    }
    UserListComponent.prototype.ngOnInit = function () {
    };
    UserListComponent.prototype.goToPerson = function (username) {
        this.userListService.closeUserListModal();
        this.router.navigate(["/user", username]);
    };
    UserListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-user-list',
            template: __webpack_require__("./src/app/child-components/user-list/user-list.component.html"),
            styles: [__webpack_require__("./src/app/child-components/user-list/user-list.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_list_service__["a" /* UserListService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], UserListComponent);
    return UserListComponent;
}());



/***/ }),

/***/ "./src/app/child-components/work-modal/work-modal.component.css":
/***/ (function(module, exports) {

module.exports = ".modal-dialog {\r\n    max-width: 100%;\r\n    margin-top: 30px;\r\n}\r\n\r\n.modal-content {\r\n    width: 1200px;\r\n    display: block;\r\n    margin:0 auto;\r\n}\r\n\r\n.modal-header {\r\n    border-bottom: 0;\r\n    padding: 0;\r\n}\r\n\r\n.modal-body img {   \r\n    width: 100%;\r\n}\r\n\r\n.modal-body {\r\n    padding: 0;\r\n}\r\n\r\n.modal-body.fixed {\r\n    max-height: 85vh;\r\n    overflow-y: auto;\r\n    -ms-overflow-style: none; \r\n    overflow: -moz-scrollbars-none;\r\n}\r\n\r\n.modal-body::-webkit-scrollbar { \r\n    width: 0 !important \r\n}\r\n\r\n.modal-header .user-block {\r\n    width: 100%;\r\n    cursor: pointer;\r\n    border-top: 6px solid transparent;\r\n}\r\n\r\n.modal-header .user-block .metadata h2 {\r\n    position: relative;\r\n    top: 4px;\r\n    margin-left: 10px;\r\n}\r\n\r\n.modal-header .user-block .metadata span.rating {\r\n    position: relative;\r\n    top: 2px;\r\n    margin-left: 3px;\r\n}\r\n\r\n.modal-header .user-block .work-description {\r\n    margin-top: 19px;\r\n    word-break: break-all;\r\n}\r\n\r\n.modal-header .user-block .arrows-text {\r\n    opacity: .5;\r\n    position: absolute;\r\n    bottom: 0;\r\n}\r\n\r\n.modal-header .user-block .arrows-text svg:first-child {\r\n    position: relative;\r\n    right: -1.5px;\r\n}\r\n\r\n.modal-header .user-block .arrows-text svg:last-child {\r\n    position: relative;\r\n    right: 1.5px;\r\n}\r\n\r\n.modal-header .user-block .metadata * {\r\n    display: inline-block;\r\n}\r\n\r\n.modal-header .user-block > div.unfixed-block {\r\n    padding-left: 40px;\r\n    padding-right: 40px;\r\n    padding-top: 30px;\r\n    padding-bottom: 10px;\r\n}\r\n\r\n.modal-header .user-block > div.fixed-block {\r\n    padding: 16px;\r\n    padding-top: 14px;\r\n}\r\n\r\n.modal-header .user-block > div.fixed-block h3 {\r\n    margin-left: 3px;\r\n}\r\n\r\n.modal-header .user-block {\r\n    background-color: #D8E2E9;\r\n    opacity: 0.3;\r\n    border-top: 6px solid transparent;\r\n    position: relative;\r\n    padding-bottom: 20px;\r\n}\r\n\r\n.modal-header .user-block.selected {\r\n    background-color: #ffffff;\r\n    opacity: 1;\r\n    border-top: 6px solid #F55045;\r\n}\r\n\r\n.modal-header .user-block.selected {\r\n    background-color: #ffffff;\r\n    opacity: 1;\r\n    border-top: 6px solid #F55045;\r\n}\r\n\r\n.modal-header .user-block.fixed {\r\n    border-top: 3px solid transparent;\r\n}\r\n\r\n.modal-header .user-block.fixed.selected {\r\n    border-top: 3px solid #F55045;\r\n}\r\n\r\n.modal-header .user-block .rating {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 20px;\r\n    line-height: 24px;\r\n    color: #C1C7CB;\r\n}\r\n\r\n.modal-header .user-block-front {\r\n    background-color: #D8E2E9;\r\n    opacity: 0.3;\r\n    visibility: hidden;\r\n}\r\n\r\n.modal-header .user-block-front.visible {\r\n    visibility: visible;\r\n}"

/***/ }),

/***/ "./src/app/child-components/work-modal/work-modal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"{{modalId}}\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <div class=\"left user-block\" [class.fixed]=\"isHeaderFixed\" [class.selected]=\"isFirstUserSelected\" (click)=\"selectUser(true)\">\r\n          <div *ngIf=\"!isHeaderFixed\" class=\"unfixed-block\">\r\n            <div class=\"metadata\">\r\n              <app-profile-picture elementClass=\"x-small\" fullname={{current.user.fullname}} backgroundImage={{current.user.imageUrl}}\r\n                backgroundColor={{current.user.imageColor}}>\r\n              </app-profile-picture>\r\n              <h2>{{current.user.fullname}}</h2>\r\n              <span class=\"rating\">#{{current.rating}}</span>\r\n            </div>\r\n            <p class=\"work-description\">{{current.workDescription}}</p>\r\n            <p *ngIf=\"isFirstUserSelected\" class=\"arrows-text\">Use arrows\r\n              <svg width=\"16\" height=\"16\" viewBox=\"0 0 16 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M1 3C1 1.89543 1.89543 1 3 1H13C14.1046 1 15 1.89543 15 3V13C15 14.1046 14.1046 15 13 15H3C1.89543 15 1 14.1046 1 13V3ZM0 3C0 1.34315 1.34315 0 3 0H13C14.6569 0 16 1.34315 16 3V13C16 14.6569 14.6569 16 13 16H3C1.34315 16 0 14.6569 0 13V3ZM7.35355 10.6464L5.20711 8.5H12V7.5H5.20711L7.35355 5.35355L6.64645 4.64645L3.64645 7.64645C3.45118 7.84171 3.45118 8.15829 3.64645 8.35355L6.64645 11.3536L7.35355 10.6464Z\"\r\n                  fill=\"#5D5D5D\" />\r\n              </svg>\r\n              <svg width=\"16\" height=\"16\" viewBox=\"0 0 16 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M15 3C15 1.89543 14.1046 1 13 1H3C1.89543 1 1 1.89543 1 3V13C1 14.1046 1.89543 15 3 15H13C14.1046 15 15 14.1046 15 13V3ZM16 3C16 1.34315 14.6569 0 13 0H3C1.34315 0 0 1.34315 0 3V13C0 14.6569 1.34315 16 3 16H13C14.6569 16 16 14.6569 16 13V3ZM8.64645 10.6464L10.7929 8.5H4V7.5H10.7929L8.64645 5.35355L9.35355 4.64645L12.3536 7.64645C12.5488 7.84171 12.5488 8.15829 12.3536 8.35355L9.35355 11.3536L8.64645 10.6464Z\"\r\n                  fill=\"#5D5D5D\" />\r\n              </svg>\r\n              to navigate between works</p>\r\n          </div>\r\n          <div *ngIf=\"isHeaderFixed\" class=\"fixed-block\">\r\n            <div class=\"metadata\">\r\n              <app-profile-picture elementClass=\"xx-small\" fullname={{current.user.fullname}} backgroundImage={{current.user.imageUrl}}\r\n                backgroundColor={{current.user.imageColor}}>\r\n              </app-profile-picture>\r\n              <h3>{{current.user.fullname}}</h3>\r\n              <span class=\"rating\">#{{current.rating}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"right user-block\" [class.fixed]=\"isHeaderFixed\" [class.selected]=\"!isFirstUserSelected\" (click)=\"selectUser(false)\">\r\n          <div *ngIf=\"!isHeaderFixed\" class=\"unfixed-block\">\r\n            <div class=\"metadata\">\r\n              <app-profile-picture elementClass=\"x-small\" fullname={{opponent.user.fullname}} backgroundImage={{opponent.user.imageUrl}}\r\n                backgroundColor={{opponent.user.imageColor}}>\r\n              </app-profile-picture>\r\n              <h2>{{opponent.user.fullname}}</h2>\r\n              <span class=\"rating\">#{{opponent.rating}}</span>\r\n            </div>\r\n            <p class=\"work-description\">{{opponent.workDescription}}</p>\r\n            <p *ngIf=\"!isFirstUserSelected\" class=\"arrows-text\">Use arrows\r\n              <svg width=\"16\" height=\"16\" viewBox=\"0 0 16 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M1 3C1 1.89543 1.89543 1 3 1H13C14.1046 1 15 1.89543 15 3V13C15 14.1046 14.1046 15 13 15H3C1.89543 15 1 14.1046 1 13V3ZM0 3C0 1.34315 1.34315 0 3 0H13C14.6569 0 16 1.34315 16 3V13C16 14.6569 14.6569 16 13 16H3C1.34315 16 0 14.6569 0 13V3ZM7.35355 10.6464L5.20711 8.5H12V7.5H5.20711L7.35355 5.35355L6.64645 4.64645L3.64645 7.64645C3.45118 7.84171 3.45118 8.15829 3.64645 8.35355L6.64645 11.3536L7.35355 10.6464Z\"\r\n                  fill=\"#5D5D5D\" />\r\n              </svg>\r\n              <svg width=\"16\" height=\"16\" viewBox=\"0 0 16 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M15 3C15 1.89543 14.1046 1 13 1H3C1.89543 1 1 1.89543 1 3V13C1 14.1046 1.89543 15 3 15H13C14.1046 15 15 14.1046 15 13V3ZM16 3C16 1.34315 14.6569 0 13 0H3C1.34315 0 0 1.34315 0 3V13C0 14.6569 1.34315 16 3 16H13C14.6569 16 16 14.6569 16 13V3ZM8.64645 10.6464L10.7929 8.5H4V7.5H10.7929L8.64645 5.35355L9.35355 4.64645L12.3536 7.64645C12.5488 7.84171 12.5488 8.15829 12.3536 8.35355L9.35355 11.3536L8.64645 10.6464Z\"\r\n                  fill=\"#5D5D5D\" />\r\n              </svg>\r\n              to navigate between works</p>\r\n          </div>\r\n          <div *ngIf=\"isHeaderFixed\" class=\"fixed-block\">\r\n            <div class=\"metadata\">\r\n              <app-profile-picture elementClass=\"xx-small\" fullname={{opponent.user.fullname}} backgroundImage={{opponent.user.imageUrl}}\r\n                backgroundColor={{opponent.user.imageColor}}>\r\n              </app-profile-picture>\r\n              <h3>{{opponent.user.fullname}}</h3>\r\n              <span class=\"rating\">#{{opponent.rating}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"clearfix\"></div>\r\n        <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button> -->\r\n      </div>\r\n      <div class=\"modal-body\" [class.fixed]=\"isHeaderFixed\">\r\n        <img *ngFor=\"let imageUrl of currentImages\" src=\"{{imageUrl.url}}\">\r\n      </div>\r\n      <div class=\"footer-block\"></div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/child-components/work-modal/work-modal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorkModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WorkModalComponent = /** @class */ (function () {
    function WorkModalComponent() {
        this.isFirstUserSelected = true;
        this.isModalOpened = false;
        this.isHeaderFixed = false;
        this.currentImages = [];
    }
    WorkModalComponent.prototype.handleKeyboardEvent = function (event) {
        var left = 37;
        var right = 39;
        var keycode = event.keyCode;
        if (keycode == left && this.isModalOpened) {
            this.selectUser(true);
        }
        if (keycode == right && this.isModalOpened) {
            this.selectUser(false);
        }
    };
    WorkModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selectUser(true);
        setTimeout(function () {
            _this.initModal();
        }, 100);
    };
    WorkModalComponent.prototype.initModal = function () {
        var self = this;
        $("#" + this.modalId).off("shown.bs.modal");
        $('#' + this.modalId).on('shown.bs.modal', function () {
            self.isFirstUserSelected = true;
            self.isModalOpened = true;
            var blockHeight = $("#" + self.modalId).find(".user-block").map(function (index, element) { return $(element).height(); });
            var maxHeight = Math.max.apply(Math, blockHeight);
            $("#" + self.modalId + " .user-block").height(maxHeight);
            // var headerPosition = $("#" + self.modalId + " .modal-header").offset().top;
            // var previousCounter = 0;
            // $("#" + self.modalId).scroll((e) => {
            //   var scrollPosition =  $("#" + self.modalId).scrollTop();
            //   if(Math.abs(scrollPosition) > 10) {
            //     self.isHeaderFixed = true;
            //   } else {
            //     self.isHeaderFixed = false;
            //   }
            // });
        });
        $("#" + this.modalId).off("hidden.bs.modal");
        $("#" + this.modalId).on('hidden.bs.modal', function () {
            self.isModalOpened = false;
        });
    };
    WorkModalComponent.prototype.selectUser = function (isFirstUser) {
        this.isFirstUserSelected = isFirstUser;
        this.currentImages = isFirstUser ? this.current.imageUrls : this.opponent.imageUrls;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], WorkModalComponent.prototype, "current", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], WorkModalComponent.prototype, "opponent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], WorkModalComponent.prototype, "modalId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('document:keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], WorkModalComponent.prototype, "handleKeyboardEvent", null);
    WorkModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-work-modal',
            template: __webpack_require__("./src/app/child-components/work-modal/work-modal.component.html"),
            styles: [__webpack_require__("./src/app/child-components/work-modal/work-modal.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WorkModalComponent);
    return WorkModalComponent;
}());



/***/ }),

/***/ "./src/app/components/account-settings/account-settings.component.css":
/***/ (function(module, exports) {

module.exports = "#account-settings {\r\n    padding-bottom: 100px;\r\n}\r\n\r\n.settings-wrapper {\r\n    margin-top: 80px;\r\n}\r\n\r\n.input-wrapper {\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.menu {\r\n    vertical-align: top;\r\n    display: inline-block;\r\n    margin-right: 120px;\r\n}\r\n\r\n.content {\r\n    vertical-align: top;\r\n    display: inline-block;\r\n}\r\n\r\n.content h1 {\r\n    margin-bottom: 30px;\r\n}\r\n\r\n.menu div {\r\n    width: 182px;\r\n    height: 50px;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n    padding-left: 20px;\r\n    margin-bottom: 3px;\r\n}\r\n\r\n.menu div h3 {\r\n    line-height: 50px;\r\n}\r\n\r\n.menu div:hover,\r\n.menu div.selected {\r\n    background-color: #feedec;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.menu div:hover h3,\r\n.menu div.selected h3 {\r\n    color: #F55045 !important;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.general-wrapper {\r\n    width: 590px;\r\n}\r\n\r\n.content h2 {\r\n    margin-top: 60px;\r\n    margin-bottom: 37px;\r\n}\r\n\r\n.primary-button {\r\n    margin-top: 35px;\r\n}\r\n\r\n.social-profiles-block .custom-input-wrapper {\r\n    width: 285px;\r\n}\r\n\r\n.inputfile {\r\n\twidth: 0.1px;\r\n\theight: 0.1px;\r\n\topacity: 0;\r\n\toverflow: hidden;\r\n\tposition: absolute;\r\n\tz-index: -1;\r\n}\r\n\r\n.inputfile + label {\r\n    height: 50px;\r\n    font-family: Rubik;\r\n    font-size: 14px;\r\n    font-weight: 500;\r\n    color: #5D5D5D !important; \r\n    border: 2px solid #FFE226;\r\n    display: -webkit-inline-box;\r\n    display: -ms-inline-flexbox;\r\n    display: inline-flex;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    -webkit-box-pack: center;\r\n        -ms-flex-pack: center;\r\n            justify-content: center;\r\n    padding-left: 24px;\r\n    padding-right: 24px;\r\n    letter-spacing: -0.2px;\r\n    -webkit-transition: all 0.1s ease-in-out;\r\n    transition: all 0.1s ease-in-out;\r\n    cursor: pointer;\r\n}\r\n\r\n.inputfile + label:hover {\r\n    background: rgba(255, 226, 38, 0.3);\r\n    -webkit-transition: all 0.1s ease-in-out;\r\n    transition: all 0.1s ease-in-out;\r\n    text-decoration: none;\r\n}\r\n\r\n.avatar-block .right {\r\n    margin-top: 14px;\r\n    margin-right: 75px;\r\n}\r\n\r\n.avatar {\r\n    margin-right: 20px;\r\n}\r\n\r\n.file-upload-block {\r\n    position: relative;\r\n    top: 15px;\r\n}\r\n\r\n.file-upload-block p {\r\n    opacity: .5;\r\n    width: 180px;\r\n    text-align: center;\r\n}\r\n\r\n.delete-picture-block {\r\n    position: relative;\r\n    top: -30px;\r\n    margin-left: 40px;\r\n}\r\n\r\n.delete-picture-block a {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    line-height: 17px;\r\n    color: #F55045;\r\n    position: relative;\r\n    top: 1px;\r\n    left: 5px;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.delete-picture-block:hover a {\r\n    color: #A04437;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.delete-picture-block svg path {\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.delete-picture-block:hover svg path {\r\n    fill: #A04437;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.custom-input-wrapper .input-container {\r\n    border: 1px solid #C1C7CB;\r\n    height: 60px;\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 14px;\r\n    line-height: 22px;\r\n    letter-spacing: -0.02em;\r\n    color: #5D5D5D;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n}\r\n\r\n.custom-input-wrapper .input-container.error {\r\n    border: 1px solid #F55045;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n}\r\n\r\n.custom-input-wrapper span {\r\n    display: inline-block;\r\n    padding-left: 10px;\r\n    padding-right: 10px;\r\n    margin-left: 20px;\r\n    margin-bottom: 0px;\r\n    position: relative;\r\n    top: -11px;\r\n    background-color: #fff;\r\n    pointer-events: none;\r\n}\r\n\r\n.custom-input-wrapper p.error {\r\n    min-height: 18px;\r\n    padding-left: 30px;\r\n}\r\n\r\n.custom-input-wrapper .input-block {\r\n    margin-top: -4px;\r\n    padding-left: 30px;\r\n}\r\n\r\n.custom-input-wrapper .input-block input {\r\n    border: 0;\r\n    outline: 0;\r\n    width: 93%;\r\n    display: inline-block;\r\n    margin-left: -7px;\r\n}\r\n\r\n.custom-input-wrapper input::-webkit-input-placeholder {\r\n    color: #C1C7CB;\r\n}\r\n\r\n.custom-input-wrapper input::-moz-placeholder {\r\n    color: #C1C7CB;\r\n}\r\n\r\n.custom-input-wrapper input:-ms-input-placeholder {\r\n    color: #C1C7CB;\r\n}\r\n\r\n.custom-input-wrapper input:-moz-placeholder {\r\n    color: #C1C7CB;\r\n}\r\n\r\n.social-profiles-block .custom-input-wrapper input {\r\n    margin-left: -8px;\r\n}\r\n\r\n.social-profiles-block .custom-input-wrapper.behance input {\r\n    width: 68%;\r\n}\r\n\r\n.social-profiles-block .custom-input-wrapper.dribbble input {\r\n    width: 48%;\r\n}\r\n\r\n.social-profiles-block .custom-input-wrapper.twitter input {\r\n    width: 51%;\r\n}\r\n\r\n.social-profiles-block .custom-input-wrapper.instagram input {\r\n    width: 45%;\r\n}\r\n\r\n.modal .modal-body {\r\n    padding: 40px;\r\n}\r\n\r\n.modal h2 {\r\n    margin-top: -10px;\r\n}\r\n\r\n.modal button.close {\r\n    position: relative;\r\n    right: -80px;\r\n    top: -37px;\r\n    font-size: 27px;\r\n    opacity: .6;\r\n}\r\n\r\n.modal button.close:hover {\r\n    opacity: 1;\r\n}\r\n\r\n.modal p {\r\n    margin-top: 7px;\r\n    margin-bottom: 30px;\r\n    width: 335px;\r\n}\r\n\r\n.modal .button-block {\r\n    margin-top: -35px;\r\n}\r\n\r\n#changeEmailModal .modal-dialog {\r\n    width: 580px;\r\n    max-width: 580px;\r\n}\r\n\r\n#notSavedModal .modal-dialog {\r\n    width: 590px;\r\n    max-width: 590px;\r\n}\r\n\r\n.modal .button-block a:first-child {\r\n    margin-right: 18px;\r\n}\r\n\r\n#notSavedModal .button-block a {\r\n    width: 120px;\r\n    padding-left: 0;\r\n    padding-right: 0;\r\n}\r\n\r\n#categories {\r\n    max-width: 700px;\r\n    margin:0 auto;\r\n}\r\n\r\n#categories p.main-paragraph {\r\n    max-width: 692px;\r\n    margin-top: 15px;\r\n    margin-bottom: 40px;\r\n}\r\n\r\n#categories h3 {\r\n    margin-top: 25px;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n#categories .category-block > p { \r\n    line-height: 29px;\r\n}\r\n\r\n#categories .categories-container {\r\n    max-width: 520px;\r\n}\r\n\r\n.category-block {\r\n    height: 30px;\r\n    border: 1px solid #D8E2E9;\r\n    -webkit-box-sizing: border-box;\r\n            box-sizing: border-box;\r\n    border-radius: 100px;\r\n    display: inline-block;\r\n    cursor: pointer;\r\n    padding-left: 12px;\r\n    padding-right: 12px;\r\n    text-align: center;\r\n    color: #5D5D5D !important;\r\n    margin-right: 16px;\r\n    margin-bottom: 16px;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.category-block:hover {\r\n    border: 1px solid rgba(245, 80, 69, 0.4);\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.category-block.selected {\r\n    background: #feedec;\r\n    border: 1px solid #feedec;\r\n}\r\n\r\n#categories .category-block > p {\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#categories .category-block:hover > p,\r\n#categories .category-block.selected > p { \r\n    color: #F55045 !important;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#categories .button {\r\n    margin-top: -10px;\r\n}\r\n\r\n#notifications .notification-wrapper {\r\n    max-width: 800px;\r\n}\r\n\r\n#notifications .heading-paragraph {\r\n    margin-top: -20px;\r\n    margin-bottom: 40px;\r\n}\r\n\r\n#notifications .divider {\r\n    width: 520px;\r\n    height: 1px;\r\n    background-color: #DCDCDC;\r\n    opacity: 0.5;\r\n    margin-top: 18px;\r\n    margin-bottom: 22px;\r\n}\r\n\r\n#notifications h3 {\r\n    display: inline-block;\r\n    margin-left: 5px;\r\n}\r\n\r\n#notifications .switch + h3 + p {\r\n    margin-top: 5px;\r\n}\r\n\r\n#billing .heading-paragraph {\r\n    margin-top: -5px;\r\n    margin-bottom: 5px;\r\n}\r\n\r\n#billing .payment-block {\r\n    border: 1px solid #D8E2E9;\r\n    width: 590px;\r\n    height: 205px;\r\n    margin-top: 25px;\r\n}\r\n\r\n#billing .payment-block .payment-block-border {\r\n    background-color: #F55045;\r\n    height: 6px;\r\n    width: 590px;\r\n    margin-left: -1px;\r\n    margin-top: -1px;\r\n    margin-bottom: 20px;\r\n}\r\n\r\n#billing .payment-block .payment-block-wrapper {\r\n    padding-left: 35px;\r\n    padding-right: 35px;\r\n}\r\n\r\n#billing .payment-block label {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #262626;\r\n}\r\n\r\n#billing .payment-block .silver-label {\r\n    position: relative;\r\n    top: 3px;\r\n    left: 5px;\r\n}\r\n\r\n#billing .payment-block h2 {\r\n    margin-top: 5px;\r\n    margin-bottom: 0;\r\n}\r\n\r\n#billing .payment-block .divider {\r\n    width: 100%;\r\n    height: 1px;\r\n    background-color: #DCDCDC;\r\n    margin-top: 25px;\r\n    margin-bottom: 35px;\r\n}\r\n\r\n#billing .payment-block button{\r\n    border: 0;\r\n    outline: 0;\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2220%22 height%3D%2220%22 viewBox%3D%220 0 20 20%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M2 10.0001C2 5.58911 5.589 2.00011 10 2.00011C14.411 2.00011 18 5.58911 18 10.0001C18 14.4111 14.411 18.0001 10 18.0001C5.589 18.0001 2 14.4111 2 10.0001ZM0 10.0001C0 15.5141 4.486 20.0001 10 20.0001C15.514 20.0001 20 15.5141 20 10.0001C20 4.48611 15.514 0.000106812 10 0.000106812C4.486 0.000106812 0 4.48611 0 10.0001ZM13.293 5.29259L14.707 6.70659L11.414 9.99959L14.707 13.2926L13.293 14.7066L9.99997 11.4146L6.70697 14.7066L5.29297 13.2926L8.58597 9.99959L5.29297 6.70659L6.70697 5.29259L9.99997 8.58559L13.293 5.29259Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 50% no-repeat;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    line-height: 17px;\r\n    color: #F55045;\r\n    padding-left: 28px;\r\n    height: 20px;\r\n}\r\n\r\n#billing .payment-block button:hover,\r\n#billing .payment-block button:focus {\r\n    border: 0;\r\n    outline: 0;\r\n}\r\n\r\n#billing .history-container {\r\n    width: 520px;\r\n}\r\n\r\n#billing .history-container .divider {\r\n    height: 1px;\r\n    background-color: #DCDCDC;\r\n    opacity: 0.5;\r\n    margin-top: 15px;\r\n}\r\n\r\n#billing .history-block {\r\n    margin-top: 30px;\r\n}\r\n\r\n#billing .history-block span {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 20px;\r\n    line-height: 24px;\r\n    text-align: right;\r\n    color: #262626;\r\n}"

/***/ }),

/***/ "./src/app/components/account-settings/account-settings.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"account-settings\">\r\n  <!-- Change email modal -->\r\n  <div class=\"modal fade\" id=\"{{emailModalId}}\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-body\">\r\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n          <h2>Changing Email</h2>\r\n          <p>You need to enter your account password so we know it's you changing the email</p>\r\n          <div class=\"input-wrapper\">\r\n            <app-input (onValueChange)=\"onEmailVerificationPasswordChange($event)\" [value]=\"emailVerificationPassword\" [placeholder]=\"'Account password'\"\r\n              [type]=\"inputType.Password\" [error]=\"emailVerificationPasswordErrorMessage\"></app-input>\r\n          </div>\r\n          <div class=\"button-block\">\r\n            <a class=\"button secondary-button\" (click)=\"closeEmailModal()\">\r\n              <span>Cancel</span>\r\n            </a>\r\n            <a class=\"button primary-button\" (click)=\"checkPassword()\">\r\n              <span>It's Actually Me</span>\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- End Change email modal -->\r\n  <!-- Not saved modal -->\r\n  <div class=\"modal fade\" id=\"{{notSavedModalId}}\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-body\">\r\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n          <h2>You Have Unsaved Changes</h2>\r\n          <p>Are you sure you want to exit without saving?</p>\r\n          <div class=\"button-block\">\r\n            <a class=\"button secondary-button\" (click)=\"notSavedModalChoose(false)\">\r\n              <span>No, Cancel</span>\r\n            </a>\r\n            <a class=\"button primary-button\" (click)=\"notSavedModalChoose(true)\">\r\n              <span>Yes, Exit</span>\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- End Not saved modal -->\r\n  <!-- Page -->\r\n  <div class=\"settings-wrapper\">\r\n    <div class=\"menu\">\r\n      <div [class.selected]=\"settingsType === settingsTypes.General\">\r\n        <a (click)=\"changeSettingsTypes(settingsTypes.General)\">\r\n          <h3>General</h3>\r\n        </a>\r\n      </div>\r\n      <div [class.selected]=\"settingsType === settingsTypes.DuelCategories\">\r\n        <a (click)=\"changeSettingsTypes(settingsTypes.DuelCategories)\">\r\n          <h3>Duel Categories</h3>\r\n        </a>\r\n      </div>\r\n      <div [class.selected]=\"settingsType === settingsTypes.Notifications\">\r\n        <a (click)=\"changeSettingsTypes(settingsTypes.Notifications)\">\r\n          <h3>Notifications</h3>\r\n        </a>\r\n      </div>\r\n      <div [class.selected]=\"settingsType === settingsTypes.Billing\">\r\n        <a (click)=\"changeSettingsTypes(settingsTypes.Billing)\">\r\n          <h3>Duelity Silver</h3>\r\n        </a>\r\n      </div>\r\n    </div>\r\n    <div class=\"content\">\r\n      <div class=\"general-wrapper\" *ngIf=\"settingsType === settingsTypes.General && accountSettings !== undefined && accountSettings !== null\">\r\n        <h1>General settings</h1>\r\n        <div>\r\n          <div class=\"avatar-block\">\r\n            <div class=\"left\">\r\n              <div class=\"avatar\">\r\n                <app-profile-picture elementClass=\"large\" fullname={{accountSettings.fullname}} backgroundImage={{accountSettings.imageUrl}}\r\n                  backgroundColor={{accountSettings.imageColor}}>\r\n                </app-profile-picture>\r\n              </div>\r\n            </div>\r\n            <div class=\"right\">\r\n              <div class=\"inline file-upload-block\">\r\n                <div>\r\n                  <input type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" (change)=\"onAvatarUpload($event)\" />\r\n                  <label for=\"file\">Upload New Picture</label>\r\n                </div>\r\n                <p>JPG or PNG, 5MB Max</p>\r\n              </div>\r\n              <div class=\"inline delete-picture-block clickable\" (click)=\"onDeletePicture()\">\r\n                <svg width=\"18\" height=\"19\" viewBox=\"0 0 18 19\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                  <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4 7H14L14.001 17H4V7ZM2 17C2 18.103 2.897 19 4 19H14C15.103 19 16 18.103 16 17V5H2V17Z\"\r\n                    fill=\"#F55045\" />\r\n                  <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M12 2V0H6V2H0V4H18V2H12Z\" fill=\"#F55045\" />\r\n                  <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M6 15H8V9H6V15Z\" fill=\"#F55045\" />\r\n                  <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M10 15H12V9H10V15Z\" fill=\"#F55045\" />\r\n                </svg>\r\n                <a>Delete picture</a>\r\n              </div>\r\n            </div>\r\n            <div class=\"clearfix\"></div>\r\n          </div>\r\n          <div style=\"height: 20px;\" class=\"clearfix\"></div>\r\n          <div>\r\n            <h2>Info</h2>\r\n          </div>\r\n          <div class=\"input-wrapper\">\r\n            <app-input (onValueChange)=\"onFullNameChange($event)\" [value]=\"accountSettings.fullname\" [placeholder]=\"'Full Name'\" [type]=\"inputType.Text\"\r\n              [error]=\"nameErrorMessage\"></app-input>\r\n          </div>\r\n          <div class=\"input-wrapper\">\r\n            <app-input (onValueChange)=\"onDescriptionChange($event)\" [value]=\"accountSettings.description\" [placeholder]=\"'Description'\"\r\n              [type]=\"inputType.Text\" [error]=\"descriptionErrorMessage\"></app-input>\r\n          </div>\r\n          <div class=\"custom-input-wrapper\">\r\n            <div class=\"input-container\" [class.error]=\"usernameErrorMessage !== null && usernameErrorMessage !== undefined && usernameErrorMessage !== ''\">\r\n              <span>Username</span>\r\n              <div class=\"input-block\">\r\n                <label>@</label>\r\n                <input [(ngModel)]=\"accountSettings.username\" type=\"text\" placeholder=\"username\" />\r\n              </div>\r\n            </div>\r\n            <p class=\"error\">{{usernameErrorMessage}}</p>\r\n          </div>\r\n          <div class=\"input-wrapper\">\r\n            <app-input (onValueChange)=\"onEmailChange($event)\" [value]=\"accountSettings.email\" [placeholder]=\"'Email'\" [type]=\"inputType.Text\"\r\n              [error]=\"emailErrorMessage\"></app-input>\r\n          </div>\r\n        </div>\r\n        <div class=\"social-profiles-block\">\r\n          <h2>Social Profiles</h2>\r\n          <div class=\"custom-input-wrapper behance left\">\r\n            <div class=\"input-container\" [class.error]=\"behanceErrorMessage !== null && behanceErrorMessage !== undefined && behanceErrorMessage !== ''\">\r\n              <span>Behance</span>\r\n              <div class=\"input-block\">\r\n                <label>be.net/</label>\r\n                <input [(ngModel)]=\"accountSettings.behance\" type=\"text\" placeholder=\"username\" />\r\n              </div>\r\n            </div>\r\n            <p class=\"error\">{{behanceErrorMessage}}</p>\r\n          </div>\r\n          <div class=\"custom-input-wrapper dribbble right\">\r\n            <div class=\"input-container\" [class.error]=\"dribbbleErrorMessage !== null && dribbbleErrorMessage !== undefined && dribbbleErrorMessage !== ''\">\r\n              <span>Dribbble</span>\r\n              <div class=\"input-block\">\r\n                <label>dribbble.com/</label>\r\n                <input [(ngModel)]=\"accountSettings.dribbble\" type=\"text\" placeholder=\"username\" />\r\n              </div>\r\n            </div>\r\n            <p class=\"error\">{{dribbbleErrorMessage}}</p>\r\n          </div>\r\n          <div class=\"clearfix\"></div>\r\n          <div class=\"custom-input-wrapper twitter left\">\r\n            <div class=\"input-container\" [class.error]=\"twitterErrorMessage !== null && twitterErrorMessage !== undefined && twitterErrorMessage !== ''\">\r\n              <span>Twitter</span>\r\n              <div class=\"input-block\">\r\n                <label>twitter.com/</label>\r\n                <input [(ngModel)]=\"accountSettings.twitter\" type=\"text\" placeholder=\"username\" />\r\n              </div>\r\n            </div>\r\n            <p class=\"error\">{{twitterErrorMessage}}</p>\r\n          </div>\r\n          <div class=\"custom-input-wrapper instagram right\">\r\n            <div class=\"input-container\" [class.error]=\"instagramErrorMessage !== null && instagramErrorMessage !== undefined && instagramErrorMessage !== ''\">\r\n              <span>Instagram</span>\r\n              <div class=\"input-block\">\r\n                <label>instagram.com/</label>\r\n                <input [(ngModel)]=\"accountSettings.instagram\" type=\"text\" placeholder=\"username\" />\r\n              </div>\r\n            </div>\r\n            <p class=\"error\">{{instagramErrorMessage}}</p>\r\n          </div>\r\n          <div class=\"clearfix\"></div>\r\n        </div>\r\n        <div>\r\n          <h2>Password</h2>\r\n          <div class=\"input-wrapper\">\r\n            <app-input (onValueChange)=\"onOldPasswordChange($event)\" [value]=\"accountSettings.oldPassword\" [placeholder]=\"'Old Password'\"\r\n              [type]=\"inputType.Password\" [error]=\"oldPasswordErrorMessage\"></app-input>\r\n          </div>\r\n          <div class=\"input-wrapper\">\r\n            <app-input (onValueChange)=\"onNewPasswordChange($event)\" [value]=\"accountSettings.newPassword\" [placeholder]=\"'New Password'\"\r\n              [type]=\"inputType.Password\" [error]=\"newPasswordErrorMessage\"></app-input>\r\n          </div>\r\n        </div>\r\n        <div>\r\n          <a class=\"button primary-button\" (click)=\"onSaveSettings()\">\r\n            <span>Save Changes</span>\r\n          </a>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"settingsType === settingsTypes.DuelCategories\" id=\"categories\">\r\n        <h1>Duel Categories Settings</h1>\r\n        <p class=\"main-paragraph\">You will only be able to duel with people who have a category in common with you. One duel — one category, choose\r\n          as much as you feel comfortable dealing with</p>\r\n        <div class=\"categories-container\">\r\n          <div *ngFor=\"let categoryGroup of categories\">\r\n            <h3>{{categoryGroup[0].group}}</h3>\r\n            <div *ngFor=\"let category of categoryGroup\" class=\"category-block\" [class.selected]=\"category.selected\" (click)=\"categoryService.pickCategory(category)\">\r\n              <p>{{category.name}}</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <br>\r\n        <br>\r\n        <a class=\"button primary-button\" [class.disabled]=\"!validateCategories()\" (click)=\"onSaveCategories()\">\r\n          <span>Save Changes</span>\r\n        </a>\r\n      </div>\r\n      <div *ngIf=\"settingsType === settingsTypes.Notifications\" id=\"notifications\">\r\n        <div class=\"notification-wrapper\">\r\n          <h1>Notification Settings</h1>\r\n          <p class=\"heading-paragraph\">All of the notification below are delivered to\r\n            <span>\r\n              <a href=\"mailto:{{accountSettings.email}}\">{{accountSettings.email}}</a>\r\n            </span>\r\n          </p>\r\n          <div>\r\n            <label class=\"switch\">\r\n              <input type=\"checkbox\" checked>\r\n              <span class=\"slider round\"></span>\r\n            </label>\r\n            <h3>New duel for you</h3>\r\n            <p>We’ll drop you a message if someone starts a duel with you. Remember, you only have 24 hours to accept it</p>\r\n          </div>\r\n          <div class=\"divider\"></div>\r\n          <div>\r\n            <label class=\"switch\">\r\n              <input type=\"checkbox\" checked>\r\n              <span class=\"slider round\"></span>\r\n            </label>\r\n            <h3>Acception/rejection of your duel</h3>\r\n            <p>When someone you started a duel with makes their decision, we’ll notify you</p>\r\n          </div>\r\n          <div class=\"divider\"></div>\r\n          <div>\r\n            <label class=\"switch\">\r\n              <input type=\"checkbox\" checked>\r\n              <span class=\"slider round\"></span>\r\n            </label>\r\n            <h3>Duel results</h3>\r\n            <p>Just as your duel ends you will receive the news. Bad or good, depends on your skill</p>\r\n          </div>\r\n          <div class=\"divider\"></div>\r\n          <div>\r\n            <label class=\"switch\">\r\n              <input type=\"checkbox\" checked>\r\n              <span class=\"slider round\"></span>\r\n            </label>\r\n            <h3>Monthly digest</h3>\r\n            <p>We handpick the most crazy tasks & the highest quality works and deliver them straight to your inbox. This is\r\n              email exclusive\r\n            </p>\r\n          </div>\r\n          <div class=\"divider\"></div>\r\n          <div>\r\n            <label class=\"switch\">\r\n              <input type=\"checkbox\" checked>\r\n              <span class=\"slider round\"></span>\r\n            </label>\r\n            <h3>Duelity news</h3>\r\n            <p>Want to know about updates and new releases first? Then this is for you</p>\r\n          </div>\r\n          <a class=\"button primary-button\">\r\n            <span>Save Changes</span>\r\n          </a>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"settingsType === settingsTypes.Billing\" id=\"billing\">\r\n        <h1>Duelity Silver</h1>\r\n        <p class=\"heading-paragraph\">Thanks for the support!\r\n          <a>Explore your new features</a>\r\n        </p>\r\n        <div class=\"payment-block\">\r\n          <div class=\"payment-block-border\"></div>\r\n          <div class=\"payment-block-wrapper\">\r\n            <label>Duelity Silver active</label>\r\n            <span class=\"silver-label\"></span>\r\n            <h2>12 Jun, 2019 — 12 Jun, 2020</h2>\r\n            <div class=\"divider\"></div>\r\n            <div>\r\n              <button class=\"left\">Cancel Subsciption</button>\r\n              <p class=\"right\">128 days left</p>\r\n              <div class=\"clearfix\"></div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"history-container\">\r\n          <h2>Payment History</h2>\r\n          <div class=\"history-block\">\r\n            <p class=\"left\">12 Jun, 2019</p>\r\n            <span class=\"right\">$24</span>\r\n            <div class=\"clearfix\"></div>\r\n            <div class=\"divider\"></div>\r\n          </div>\r\n          <div class=\"history-block\">\r\n            <p class=\"left\">8 Jun, 2020</p>\r\n            <span class=\"right\">$24</span>\r\n            <div class=\"clearfix\"></div>\r\n            <div class=\"divider\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/account-settings/account-settings.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountSettingsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__enums_settings_type__ = __webpack_require__("./src/app/enums/settings-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_account_settings_service__ = __webpack_require__("./src/app/components/account-settings/services/account-settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_account_settings__ = __webpack_require__("./src/app/components/account-settings/models/account-settings.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__child_components_default_picture_services_default_picture_service__ = __webpack_require__("./src/app/child-components/default-picture/services/default-picture.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_cloudinary_service_cloudinary_service__ = __webpack_require__("./src/app/services/cloudinary-service/cloudinary.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_category_category_service__ = __webpack_require__("./src/app/services/category/category.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__enums_input_type__ = __webpack_require__("./src/app/enums/input-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var AccountSettingsComponent = /** @class */ (function () {
    function AccountSettingsComponent(accountSettingsService, validationService, authService, defaultPictureService, cloudinaryService, categoryService, titleService, alertService) {
        this.accountSettingsService = accountSettingsService;
        this.validationService = validationService;
        this.authService = authService;
        this.defaultPictureService = defaultPictureService;
        this.cloudinaryService = cloudinaryService;
        this.categoryService = categoryService;
        this.titleService = titleService;
        this.alertService = alertService;
        this.emailModalId = "changeEmailModal";
        this.notSavedModalId = "notSavedModal";
        this.settingsType = __WEBPACK_IMPORTED_MODULE_1__enums_settings_type__["a" /* SettingsType */].General;
        this.settingsTypes = __WEBPACK_IMPORTED_MODULE_1__enums_settings_type__["a" /* SettingsType */];
        this.accountSettingsType = __WEBPACK_IMPORTED_MODULE_3__models_account_settings__["a" /* AccountSettings */];
        this.inputType = __WEBPACK_IMPORTED_MODULE_11__enums_input_type__["a" /* InputType */];
        this.accountSettings = null;
        this.emailVerificationPassword = "";
        this.categories = [];
        this.nameErrorMessage = "";
        this.descriptionErrorMessage = "";
        this.usernameErrorMessage = "";
        this.emailErrorMessage = "";
        this.behanceErrorMessage = "";
        this.dribbbleErrorMessage = "";
        this.twitterErrorMessage = "";
        this.instagramErrorMessage = "";
        this.oldPasswordErrorMessage = "";
        this.newPasswordErrorMessage = "";
        this.deactivateSubject = new __WEBPACK_IMPORTED_MODULE_12_rxjs__["Subject"]();
    }
    AccountSettingsComponent.prototype.ngOnInit = function () {
        this.initializeModelCheck();
        this.changeSettingsTypes(__WEBPACK_IMPORTED_MODULE_1__enums_settings_type__["a" /* SettingsType */].General);
        this.titleService.setTitle("Settings / " + this.authService.getCurrentUser().fullname + " - Duelity");
        this.initializeFileUploadInput();
        setTimeout(this.initializeNotSavedModal.bind(this), 0);
    };
    AccountSettingsComponent.prototype.changeSettingsTypes = function (settingsType) {
        var _this = this;
        this.settingsType = settingsType;
        switch (this.settingsType) {
            case __WEBPACK_IMPORTED_MODULE_1__enums_settings_type__["a" /* SettingsType */].General:
                this.accountSettingsService.getAccountSettings().subscribe(function (response) {
                    if (response.success) {
                        _this.setAccountSettings(response.data);
                        if (settingsType === __WEBPACK_IMPORTED_MODULE_1__enums_settings_type__["a" /* SettingsType */].General) {
                            _this.cloudinaryService.initAvatarUploader(response.data.id, _this.saveAvatar.bind(_this));
                        }
                    }
                    else {
                        console.log("Something goes wrong during fetching account info.");
                    }
                });
                break;
            case __WEBPACK_IMPORTED_MODULE_1__enums_settings_type__["a" /* SettingsType */].DuelCategories:
                this.initializeCategories();
                break;
        }
    };
    AccountSettingsComponent.prototype.onFullNameChange = function (fullname) {
        this.accountSettings.fullname = fullname;
    };
    AccountSettingsComponent.prototype.onDescriptionChange = function (description) {
        this.accountSettings.description = description;
    };
    AccountSettingsComponent.prototype.onEmailChange = function (email) {
        this.accountSettings.email = email;
    };
    AccountSettingsComponent.prototype.onBehanceChange = function (behance) {
        this.accountSettings.behance = behance;
    };
    AccountSettingsComponent.prototype.onDribbbleChange = function (dribbble) {
        this.accountSettings.dribbble = dribbble;
    };
    AccountSettingsComponent.prototype.onTwitterChange = function (twitter) {
        this.accountSettings.twitter = twitter;
    };
    AccountSettingsComponent.prototype.onInstagramChange = function (instagram) {
        this.accountSettings.instagram = instagram;
    };
    AccountSettingsComponent.prototype.onOldPasswordChange = function (oldPassword) {
        this.accountSettings.oldPassword = oldPassword;
    };
    AccountSettingsComponent.prototype.onNewPasswordChange = function (newPassword) {
        this.accountSettings.newPassword = newPassword;
    };
    AccountSettingsComponent.prototype.onEmailVerificationPasswordChange = function (emailVerificationPassword) {
        this.emailVerificationPassword = emailVerificationPassword;
    };
    AccountSettingsComponent.prototype.onSaveSettings = function () {
        var _this = this;
        this.initializeModelCheck();
        if (this.validationService.isEmailValid(this.accountSettings.email) && this.accountSettings.email !== this.previousSettings.email) {
            this.openEmailModal();
            return;
        }
        if (this.validationService.isStringValid(this.accountSettings.newPassword)) {
            this.newPasswordErrorMessage = this.validationService.validatePassword(this.accountSettings.newPassword, true);
            this.oldPasswordErrorMessage = this.validationService.validatePassword(this.accountSettings.oldPassword, false);
            this.authService.checkUserPassword(this.accountSettings.oldPassword).subscribe(function (response) {
                if (response.success) {
                    _this.saveSettings();
                }
                else {
                    _this.alertService.error("Wrong password!");
                    _this.setAccountSettings(_this.previousSettings);
                }
            });
            return;
        }
        this.saveSettings();
    };
    AccountSettingsComponent.prototype.onSaveCategories = function () {
        var _this = this;
        var userId = this.authService.getCurrentUser().id, categories = this.categoryService.collectCategories(this.categories);
        if (categories.length === 0) {
            return;
        }
        this.categoryService.saveUserCategories(userId, categories).subscribe(function (response) {
            if (response.success) {
                _this.categories = response.categories;
                _this.alertService.success("New settings saved");
            }
            else {
                _this.alertService.error("Your new settings could not be saved. Please try again");
            }
        });
    };
    AccountSettingsComponent.prototype.setAccountSettings = function (settings) {
        this.accountSettings = __WEBPACK_IMPORTED_MODULE_3__models_account_settings__["a" /* AccountSettings */].map(settings);
        this.accountSettings.denormalizeSocialNetworkLinks();
        this.previousSettings = __WEBPACK_IMPORTED_MODULE_3__models_account_settings__["a" /* AccountSettings */].map(this.accountSettings);
    };
    AccountSettingsComponent.prototype.openEmailModal = function () {
        $("#" + this.emailModalId).modal();
    };
    AccountSettingsComponent.prototype.closeEmailModal = function () {
        $("#" + this.emailModalId).modal('hide');
    };
    AccountSettingsComponent.prototype.openNotSavedModal = function () {
        $("#" + this.notSavedModalId).modal();
    };
    AccountSettingsComponent.prototype.closeNotSavedModal = function () {
        $("#" + this.notSavedModalId).modal('hide');
        $('.modal-backdrop').remove();
    };
    AccountSettingsComponent.prototype.notSavedModalChoose = function (result) {
        this.deactivateSubject.next(result);
        this.closeNotSavedModal();
    };
    AccountSettingsComponent.prototype.initializeNotSavedModal = function () {
        var self = this;
        $("#" + this.notSavedModalId).on('hidden.bs.modal', function () {
            self.deactivateSubject.next(false);
        });
    };
    AccountSettingsComponent.prototype.checkPassword = function () {
        var _this = this;
        if (!this.validationService.isStringValid(this.emailVerificationPassword)) {
            alert("Password cannot be empty!");
            return;
        }
        this.authService.checkUserPassword(this.emailVerificationPassword).subscribe(function (response) {
            if (response.success) {
                _this.saveSettings();
            }
            else {
                _this.alertService.error("Wrong password!");
                _this.setAccountSettings(_this.previousSettings);
            }
            _this.emailVerificationPassword = "";
            _this.closeEmailModal();
        });
    };
    AccountSettingsComponent.prototype.onDeletePicture = function () {
        this.accountSettings.imageUrl = "";
        this.accountSettings.imagePublicId = "";
        if (!this.accountSettings.imageColor) {
            this.accountSettings.imageColor = this.defaultPictureService.generateRandomColor();
            this.saveAvatar(this.accountSettings.imageUrl, this.accountSettings.imagePublicId, this.accountSettings.imageColor);
        }
    };
    AccountSettingsComponent.prototype.validateCategories = function () {
        return this.categories.filter(function (categoryGroup) { return categoryGroup.filter(function (category) { return category.selected; }).length > 0; }).length > 0;
    };
    ;
    AccountSettingsComponent.prototype.saveAvatar = function (imageUrl, imagePublicId, imageColor) {
        var _this = this;
        this.accountSettingsService.saveAvatar(imageUrl, imagePublicId, imageColor)
            .subscribe(function (response) {
            if (response.success) {
                _this.setAccountSettings(response.data);
                _this.authService.updateUserData(response.userData);
                if (_this.validationService.isStringValid(response.data.imageUrl)) {
                    _this.alertService.success("New picture has been uploaded");
                }
                else {
                    _this.alertService.success("Picture has been deleted");
                }
            }
            else {
                _this.alertService.error("Error occured during picture saving. Please try again");
            }
        });
    };
    AccountSettingsComponent.prototype.onAvatarUpload = function (e) {
        var file = e.target.files[0];
        if (file) {
            if (this.validationService.isImageValid(file.type)) {
                this.cloudinaryService.avatarUploader.addToQueue(e.target.files);
            }
            else {
                this.alertService.error("Image format is not valid");
            }
        }
    };
    AccountSettingsComponent.prototype.saveSettings = function () {
        var _this = this;
        if (this.isSettingsValid()) {
            var settings = __WEBPACK_IMPORTED_MODULE_3__models_account_settings__["a" /* AccountSettings */].map(this.accountSettings);
            settings.normalizeSocialNetworkLinks();
            this.accountSettingsService.saveAccountSettings(settings).subscribe(function (response) {
                if (response.success) {
                    _this.setAccountSettings(response.data);
                    _this.alertService.success("New settings saved");
                    _this.authService.updateUserData(response.userData);
                }
                else {
                    _this.alertService.error("Your new settings could not be saved. Please try again");
                    _this.setAccountSettings(_this.previousSettings);
                }
            });
        }
        else {
            setTimeout(this.scrollToError, 500);
        }
    };
    AccountSettingsComponent.prototype.scrollToError = function () {
        var offset = 105;
        var firstError = $("p.error").filter(function (index, element) { return $(element).text() !== ""; })[0];
        var top = $(firstError).offset().top - offset;
        window.scrollTo({ top: top, behavior: 'smooth' });
    };
    AccountSettingsComponent.prototype.isSettingsValid = function () {
        this.nameErrorMessage = this.validationService.validateFullname(this.accountSettings.fullname, true);
        this.emailErrorMessage = this.validationService.validateEmail(this.accountSettings.email, true);
        this.descriptionErrorMessage = this.validationService.validateDescription(this.accountSettings.description, false);
        this.usernameErrorMessage = this.validationService.validateUsername(this.accountSettings.username, true);
        if (this.validationService.isStringValid(this.accountSettings.behance)) {
            this.behanceErrorMessage = this.validationService.validateSocialNetwork("Behance", this.accountSettings.behance, false);
        }
        if (this.validationService.isStringValid(this.accountSettings.dribbble)) {
            this.dribbbleErrorMessage = this.validationService.validateSocialNetwork("Dribbble", this.accountSettings.dribbble, false);
        }
        if (this.validationService.isStringValid(this.accountSettings.twitter)) {
            this.twitterErrorMessage = this.validationService.validateSocialNetwork("Twitter", this.accountSettings.twitter, false);
        }
        if (this.validationService.isStringValid(this.accountSettings.instagram)) {
            this.instagramErrorMessage = this.validationService.validateSocialNetwork("Instagram", this.accountSettings.instagram, false);
        }
        return !(this.validationService.isStringValid(this.nameErrorMessage) ||
            this.validationService.isStringValid(this.usernameErrorMessage) ||
            this.validationService.isStringValid(this.emailErrorMessage) ||
            this.validationService.isStringValid(this.descriptionErrorMessage) ||
            this.validationService.isStringValid(this.behanceErrorMessage) ||
            this.validationService.isStringValid(this.dribbbleErrorMessage) ||
            this.validationService.isStringValid(this.twitterErrorMessage) ||
            this.validationService.isStringValid(this.instagramErrorMessage));
    };
    AccountSettingsComponent.prototype.initializeCategories = function () {
        var _this = this;
        var userId = this.authService.getCurrentUser().id;
        this.categoryService.getUserCategories(userId).subscribe(function (response) {
            if (response.success) {
                _this.categories = response.categories;
            }
            else {
                console.log(response.error);
            }
        });
    };
    AccountSettingsComponent.prototype.initializeModelCheck = function () {
        this.nameErrorMessage = "";
        this.descriptionErrorMessage = "";
        this.usernameErrorMessage = "";
        this.emailErrorMessage = "";
        this.behanceErrorMessage = "";
        this.dribbbleErrorMessage = "";
        this.twitterErrorMessage = "";
        this.instagramErrorMessage = "";
        this.oldPasswordErrorMessage = "";
        this.newPasswordErrorMessage = "";
    };
    AccountSettingsComponent.prototype.initializeFileUploadInput = function () {
        var inputs = document.querySelectorAll('.inputfile');
        Array.prototype.forEach.call(inputs, function (input) {
            var label = input.nextElementSibling, labelVal = label.innerHTML;
            input.addEventListener('change', function (e) {
                var fileName = '';
                if (this.files && this.files.length > 1)
                    fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                else
                    fileName = e.target.value.split('\\').pop();
                if (fileName)
                    label.querySelector('span').innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });
            // Firefox bug fix
            input.addEventListener('focus', function () { input.classList.add('has-focus'); });
            input.addEventListener('blur', function () { input.classList.remove('has-focus'); });
        });
    };
    AccountSettingsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-account-settings',
            template: __webpack_require__("./src/app/components/account-settings/account-settings.component.html"),
            styles: [__webpack_require__("./src/app/components/account-settings/account-settings.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_account_settings_service__["a" /* AccountSettingsService */],
            __WEBPACK_IMPORTED_MODULE_4__services_validation_validation_service__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_6__child_components_default_picture_services_default_picture_service__["a" /* DefaultPictureService */],
            __WEBPACK_IMPORTED_MODULE_7__services_cloudinary_service_cloudinary_service__["a" /* CloudinaryService */],
            __WEBPACK_IMPORTED_MODULE_8__services_category_category_service__["a" /* CategoryService */],
            __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_10__services_alert_alert_service__["a" /* AlertService */]])
    ], AccountSettingsComponent);
    return AccountSettingsComponent;
}());



/***/ }),

/***/ "./src/app/components/account-settings/models/account-settings.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountSettings; });
var AccountSettings = /** @class */ (function () {
    function AccountSettings() {
    }
    AccountSettings.map = function (source) {
        var settings = new AccountSettings();
        settings.id = source.id;
        settings.fullname = source.fullname;
        settings.description = source.description;
        settings.username = source.username;
        settings.email = source.email;
        settings.behance = source.behance;
        settings.dribbble = source.dribbble;
        settings.twitter = source.twitter;
        settings.instagram = source.instagram;
        settings.oldPassword = source.oldPassword;
        settings.newPassword = source.newPassword;
        settings.imageUrl = source.imageUrl;
        settings.imageColor = source.imageColor;
        settings.imagePublicId = source.imagePublicId;
        return settings;
    };
    AccountSettings.prototype.normalizeSocialNetworkLinks = function () {
        this.behance = this.behance ? AccountSettings.behanceLink + this.behance : "";
        this.dribbble = this.dribbble ? AccountSettings.dribbbleLink + this.dribbble : "";
        this.twitter = this.twitter ? AccountSettings.twitterLink + this.twitter : "";
        this.instagram = this.instagram ? AccountSettings.instagramLink + this.instagram : "";
    };
    AccountSettings.prototype.denormalizeSocialNetworkLinks = function () {
        this.behance = this.behance ? this.behance.replace(AccountSettings.behanceLink, "") : "";
        this.dribbble = this.dribbble ? this.dribbble.replace(AccountSettings.dribbbleLink, "") : "";
        this.twitter = this.twitter ? this.twitter.replace(AccountSettings.twitterLink, "") : "";
        this.instagram = this.instagram ? this.instagram.replace(AccountSettings.instagramLink, "") : "";
    };
    AccountSettings.prototype.hasChanges = function (previousSettings) {
        return !(this.fullname === previousSettings.fullname &&
            this.description === previousSettings.description &&
            this.username === previousSettings.username &&
            this.email === previousSettings.email &&
            this.behance === previousSettings.behance &&
            this.dribbble === previousSettings.dribbble &&
            this.twitter === previousSettings.twitter &&
            this.instagram === previousSettings.instagram &&
            this.newPassword === previousSettings.newPassword);
    };
    AccountSettings.behanceLink = "be.net/";
    AccountSettings.dribbbleLink = "dribbble.com/";
    AccountSettings.twitterLink = "twitter.com/";
    AccountSettings.instagramLink = "instagram.com/";
    return AccountSettings;
}());



/***/ }),

/***/ "./src/app/components/account-settings/services/account-settings.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountSettingsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AccountSettingsService = /** @class */ (function () {
    function AccountSettingsService(tokenProviderService, authService, headersProviderService, router, http) {
        this.tokenProviderService = tokenProviderService;
        this.authService = authService;
        this.headersProviderService = headersProviderService;
        this.router = router;
        this.http = http;
    }
    AccountSettingsService.prototype.getAccountSettings = function () {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].serverEndpoint + "/user/account-settings", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                currentUserId: this.authService.getCurrentUser().id
            }
        })
            .map(function (res) { return res.json(); });
    };
    AccountSettingsService.prototype.saveAccountSettings = function (settings) {
        this.tokenProviderService.loadToken();
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].serverEndpoint + "/user/save-account-settings", settings, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    AccountSettingsService.prototype.saveAvatar = function (imageUrl, imagePublicId, imageColor) {
        this.tokenProviderService.loadToken();
        var data = {
            currentUserId: this.authService.getCurrentUser().id,
            imageUrl: imageUrl,
            imagePublicId: imagePublicId,
            imageColor: imageColor
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].serverEndpoint + "/user/save-avatar", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    AccountSettingsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_6__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], AccountSettingsService);
    return AccountSettingsService;
}());



/***/ }),

/***/ "./src/app/components/auth-callback/auth-callback.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/auth-callback/auth-callback.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Syncing data from your account</h1>\r\n<p>You will be redirected shortly as soon as everything is finished</p>"

/***/ }),

/***/ "./src/app/components/auth-callback/auth-callback.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthCallbackComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_twitter_twitter_service__ = __webpack_require__("./src/app/services/twitter/twitter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__register_services_register_service__ = __webpack_require__("./src/app/components/register/services/register.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthCallbackComponent = /** @class */ (function () {
    function AuthCallbackComponent(activatedRoute, twitterService, authService, socketService, registerService, router) {
        this.activatedRoute = activatedRoute;
        this.twitterService = twitterService;
        this.authService = authService;
        this.socketService = socketService;
        this.registerService = registerService;
        this.router = router;
    }
    AuthCallbackComponent.prototype.ngOnInit = function () {
        this.initTwitterCallback();
    };
    AuthCallbackComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    AuthCallbackComponent.prototype.initTwitterCallback = function () {
        var _this = this;
        this.subscription = this.activatedRoute.queryParams.subscribe(function (params) {
            var token = params['oauth_token'], verifier = params['oauth_verifier'], denied = params['denied'];
            if (denied) {
                _this.router.navigate(["/register"]);
                return;
            }
            _this.twitterService.verifyUser(token, verifier).subscribe(function (response) {
                if (response.success) {
                    if (response.newUser) {
                        _this.registerService.initializeAuthRegister(response.user.email, response.user.fullname);
                        _this.router.navigate(["/register"]);
                    }
                    else {
                        _this.authService.storeUserData(response.token, response.user);
                        _this.socketService.addOnlineUser();
                        _this.router.navigate(["/following"]);
                    }
                }
                else {
                    console.log(response.error);
                }
            });
        });
    };
    AuthCallbackComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-auth-callback',
            template: __webpack_require__("./src/app/components/auth-callback/auth-callback.component.html"),
            styles: [__webpack_require__("./src/app/components/auth-callback/auth-callback.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__services_twitter_twitter_service__["a" /* TwitterService */],
            __WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__services_socket_socket_service__["a" /* SocketService */],
            __WEBPACK_IMPORTED_MODULE_5__register_services_register_service__["a" /* RegisterService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], AuthCallbackComponent);
    return AuthCallbackComponent;
}());



/***/ }),

/***/ "./src/app/components/featured-duels/featured-duels.component.css":
/***/ (function(module, exports) {

module.exports = ".featured-duels {\r\n    margin-bottom: 100px;\r\n    margin-top: 100px;\r\n}\r\n\r\n.avatars-block {\r\n    position: relative;\r\n    right: 10px;\r\n}\r\n\r\n.avatars-label {\r\n    position: relative;\r\n}\r\n\r\n.avatar {\r\n    position: relative;\r\n    cursor: pointer;\r\n}\r\n\r\nbutton.additional-users {\r\n    width: 38px;\r\n    height: 38px;\r\n    border-radius: 50%;\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 12px;\r\n    line-height: 38px;\r\n    text-align: center;\r\n    background-color: #D8E2E9;\r\n    color: #5D5D5D;\r\n    position: relative;\r\n    top: -1px;\r\n}\r\n\r\n.profile-avatar {\r\n    position: relative;\r\n    top: 0px;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.profile-avatar:hover {\r\n    top: -8px;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}"

/***/ }),

/***/ "./src/app/components/featured-duels/featured-duels.component.html":
/***/ (function(module, exports) {

module.exports = "<app-back-to-top></app-back-to-top>\r\n<div class=\"featured-duels\">\r\n    <div *ngIf=\"duels !== undefined && duels.length > 0\" class=\"duels\">\r\n        <app-user-list></app-user-list>\r\n        <div>\r\n            <h2 class=\"left\">Featured</h2>\r\n            <div class=\"right\">\r\n                <div *ngIf=\"authService.getCurrentUser() !== null\" class=\"right\" [class.avatars-block]=\"totalUserCount < previewUserAmount\">\r\n                    <p class=\"inline avatars-label\">Most Featured</p>\r\n                    <div class=\"inline\">\r\n                        <div *ngFor=\"let user of previewUsers\" class=\"avatar inline custom-tooltip\">\r\n                            <span class=\"tooltiptext\">{{user.fullname}}</span>\r\n                            <app-profile-picture (click)=\"goToPerson(user.username)\" elementClass=\"x-small\" fullname={{user.fullname}} userId={{user.id}}\r\n                                backgroundImage={{user.imageUrl}} backgroundColor={{user.imageColor}} hasBorder=\"true\" class=\"profile-avatar\">\r\n                            </app-profile-picture>\r\n                        </div>\r\n                    </div>\r\n                    <button class=\"inline additional-users\" (click)=\"openUserList()\" *ngIf=\"totalUserCount > previewUserAmount\">+{{totalUserCount - previewUserAmount}}</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"clearfix\"></div>\r\n        <p>Handpicked duels that were choosen among the others</p>\r\n        <div class=\"duels-container\">\r\n            <div *ngFor=\"let duel of duels\" (click)=\"openVotePage(duel)\" class=\"duel-wrapper\">\r\n                <app-duel [duel]=\"duel\" [duelOwner]=\"currentUserId\" (notify)=\"onNotify($event)\"></app-duel>\r\n            </div>\r\n        </div>\r\n        <div *ngIf=\"duels.length < totalDuelsAmount\" class=\"duels-end-block-button\">\r\n            <a class=\"button secondary-button\" (click)=\"loadNextPage()\">\r\n                <span>Load More</span>\r\n            </a>\r\n        </div>\r\n        <div *ngIf=\"duels.length >= totalDuelsAmount\" class=\"duels-end-block-message\">\r\n            <h2>Wow, you’ve reached the end of the list</h2>\r\n            <p>Good job exploring featured duels. Try looking through new ones now!</p>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"duels === undefined || duels.length === 0\">\r\n        <h1>No duels.</h1>\r\n    </div>\r\n</div>\r\n<div class=\"clearfix\"></div>"

/***/ }),

/***/ "./src/app/components/featured-duels/featured-duels.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeaturedDuelsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_vote_vote_service__ = __webpack_require__("./src/app/services/vote/vote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__enums_duel_type__ = __webpack_require__("./src/app/enums/duel-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__child_components_user_list_services_user_list_service__ = __webpack_require__("./src/app/child-components/user-list/services/user-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__enums_user_list_order_criteria__ = __webpack_require__("./src/app/enums/user-list-order-criteria.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_tooltip_tooltip_service__ = __webpack_require__("./src/app/services/tooltip/tooltip.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var FeaturedDuelsComponent = /** @class */ (function () {
    function FeaturedDuelsComponent(duelService, voteService, userListService, router, titleService, authService, tooltipService) {
        this.duelService = duelService;
        this.voteService = voteService;
        this.userListService = userListService;
        this.router = router;
        this.titleService = titleService;
        this.authService = authService;
        this.tooltipService = tooltipService;
        this.previewUserAmount = 5;
        this.totalUserCount = 0;
        this.duelsPage = 1;
        this.totalDuelsAmount = 0;
        this.duels = [];
        this.previewUsers = [];
    }
    FeaturedDuelsComponent.prototype.ngOnInit = function () {
        this.initializeData();
        this.titleService.setTitle("Featured Duels - Duelity");
    };
    FeaturedDuelsComponent.prototype.onNotify = function () {
        this.initializeData();
    };
    FeaturedDuelsComponent.prototype.openVotePage = function (duel) {
        this.voteService.openVotePage(duel);
    };
    FeaturedDuelsComponent.prototype.goToPerson = function (username) {
        this.router.navigate(["/user", username]);
    };
    FeaturedDuelsComponent.prototype.openUserList = function () {
        this.userListService.getUsersAndOpenModal("Featured", [], __WEBPACK_IMPORTED_MODULE_6__enums_user_list_order_criteria__["a" /* UserListOrderCriteria */].FeaturedPoints);
    };
    FeaturedDuelsComponent.prototype.loadNextPage = function () {
        this.duelsPage++;
        this.loadDuels();
    };
    FeaturedDuelsComponent.prototype.initializeData = function () {
        var _this = this;
        this.duelService.getDuelByType(__WEBPACK_IMPORTED_MODULE_3__enums_duel_type__["a" /* DuelType */].Featured, this.duelsPage).subscribe(function (response) {
            if (response.success) {
                (_a = _this.duels).push.apply(_a, response.duels);
                _this.totalDuelsAmount = response.totalAmount;
                if (_this.duels.length > 0) {
                    _this.userListService.getUserList([], __WEBPACK_IMPORTED_MODULE_6__enums_user_list_order_criteria__["a" /* UserListOrderCriteria */].FeaturedPoints, 1, _this.previewUserAmount).subscribe(function (response) {
                        if (response.success) {
                            _this.previewUsers = response.data.users;
                            _this.totalUserCount = response.data.totalCount;
                            _this.initializePreviewUsers();
                            setTimeout(function () { _this.tooltipService.initializeTooltip('.custom-tooltip'); }, 0);
                        }
                        else {
                            console.log(response.error);
                        }
                    });
                }
            }
            else {
                console.log("Something goes wrong during fetching featured duels.");
            }
            var _a;
        });
    };
    FeaturedDuelsComponent.prototype.loadDuels = function () {
        var _this = this;
        this.duelService.getDuelByType(__WEBPACK_IMPORTED_MODULE_3__enums_duel_type__["a" /* DuelType */].Featured, this.duelsPage).subscribe(function (response) {
            if (response.success) {
                (_a = _this.duels).push.apply(_a, response.duels);
                _this.totalDuelsAmount = response.totalAmount;
            }
            else {
                console.log("Something goes wrong during fetching popular duels.");
            }
            var _a;
        });
    };
    FeaturedDuelsComponent.prototype.initializePreviewUsers = function () {
        var _this = this;
        setTimeout(function () {
            var avatars = $(".avatar");
            var rightPosition = _this.totalUserCount > _this.previewUserAmount ? 10 : 0;
            var zIndex = 6;
            if (avatars.length > 0) {
                for (var i = avatars.length - 1; i >= 0; i--) {
                    var avatar = $(avatars[i]);
                    avatar.css("right", "-" + rightPosition + "px");
                    avatar.css("z-index", zIndex);
                    rightPosition += 5;
                    zIndex += 1;
                }
                $(".avatars-label").css("right", 16 - (rightPosition - 5) + "px");
            }
        }, 0);
    };
    FeaturedDuelsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-featured-duels',
            template: __webpack_require__("./src/app/components/featured-duels/featured-duels.component.html"),
            styles: [__webpack_require__("./src/app/components/featured-duels/featured-duels.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_duel_service_duel_service__["a" /* DuelService */],
            __WEBPACK_IMPORTED_MODULE_2__services_vote_vote_service__["a" /* VoteService */],
            __WEBPACK_IMPORTED_MODULE_4__child_components_user_list_services_user_list_service__["a" /* UserListService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_8__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_9__services_tooltip_tooltip_service__["a" /* TooltipService */]])
    ], FeaturedDuelsComponent);
    return FeaturedDuelsComponent;
}());



/***/ }),

/***/ "./src/app/components/following-duels/following-duels.component.css":
/***/ (function(module, exports) {

module.exports = ".following-duels {\r\n    margin-bottom: 100px;\r\n    margin-top: 100px;\r\n}\r\n\r\n.duels-container {\r\n    margin-top: 20px;\r\n}\r\n\r\nh2 .following-amount {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 25px;\r\n    line-height: 29px;\r\n    color: #C1C7CB;\r\n    margin-left: 10px;\r\n}\r\n\r\n.avatars-block {\r\n    position: relative;\r\n    right: 10px;\r\n}\r\n\r\n.avatars-label {\r\n    position: relative;\r\n}\r\n\r\n.avatar {\r\n    position: relative;\r\n    cursor: pointer;\r\n}\r\n\r\nbutton.additional-users {\r\n    width: 38px;\r\n    height: 38px;\r\n    border-radius: 50%;\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 12px;\r\n    line-height: 38px;\r\n    text-align: center;\r\n    background-color: #D8E2E9;\r\n    color: #5D5D5D;\r\n    position: relative;\r\n    top: -1px;\r\n}\r\n\r\n.profile-avatar {\r\n    position: relative;\r\n    top: 0px;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.profile-avatar:hover {\r\n    top: -8px;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.duels-end-block-message {\r\n    margin-bottom: 40px;\r\n    margin-top: 60px;\r\n}"

/***/ }),

/***/ "./src/app/components/following-duels/following-duels.component.html":
/***/ (function(module, exports) {

module.exports = "<app-back-to-top></app-back-to-top>\r\n<div class=\"following-duels\">\r\n    <div *ngIf=\"duels !== undefined && duels.length > 0\" class=\"duels\">\r\n        <app-user-list></app-user-list>\r\n        <div>\r\n            <h2 class=\"left\">Following\r\n                <span class=\"following-amount\">{{following.length}}</span>\r\n            </h2>\r\n            <div *ngIf=\"authService.getCurrentUser() !== null && following.length > 0\" class=\"right\" [class.avatars-block]=\"totalUserCount < previewUserAmount\">\r\n                <div class=\"inline\">\r\n                    <div *ngFor=\"let user of previewUsers\" class=\"avatar inline custom-tooltip\">\r\n                        <span class=\"tooltiptext\">{{user.fullname}}</span>\r\n                        <app-profile-picture (click)=\"goToPerson(user.username)\" elementClass=\"x-small\" fullname={{user.fullname}} userId={{user.id}}\r\n                            backgroundImage={{user.imageUrl}} backgroundColor={{user.imageColor}} hasBorder=\"true\" class=\"profile-avatar\">\r\n                        </app-profile-picture>\r\n                    </div>\r\n                </div>\r\n                <button class=\"inline additional-users\" (click)=\"openUserFollowing()\" *ngIf=\"totalUserCount > previewUserAmount\">+{{totalUserCount - previewUserAmount}}</button>\r\n            </div>\r\n            <div class=\"clearfix\"></div>\r\n        </div>\r\n        <div class=\"clearfix\"></div>\r\n        <div *ngIf=\"isPopularDuels && following.length === 0\" class=\"duels-end-block-message\">\r\n            <h2>You’re not following anyone yet, showing you popular duels instead</h2>\r\n            <p>Follow your favorite designers and their duels will appear here</p>\r\n        </div>\r\n        <div *ngIf=\"isPopularDuels && following.length > 0\" class=\"duels-end-block-message\">\r\n            <h2>You are following lazy people, showing you popular duels instead</h2>\r\n            <p>People you are following don't have a single duel, try following more people</p>\r\n        </div>\r\n        <div class=\"duels-container\">\r\n            <div *ngFor=\"let duel of duels\" (click)=\"openVotePage(duel)\" class=\"duel-wrapper\">\r\n                <app-duel [duel]=\"duel\" [duelOwner]=\"currentUserId\" (notify)=\"onNotify($event)\"></app-duel>\r\n            </div>\r\n        </div>\r\n        <div *ngIf=\"duels.length < totalDuelsAmount\" class=\"duels-end-block-button\">\r\n            <a class=\"button secondary-button\" (click)=\"loadNextPage()\">\r\n                <span>Load More</span>\r\n            </a>\r\n        </div>\r\n        <div *ngIf=\"duels.length >= totalDuelsAmount\" class=\"duels-end-block-message\">\r\n            <h2>Wow, you’ve reached the end of the list</h2>\r\n            <p>Good job exploring following duels. Try looking through popular ones now!</p>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"duels === undefined || duels.length === 0\">\r\n        <h1>No duels.</h1>\r\n    </div>\r\n</div>\r\n<div class=\"clearfix\"></div>"

/***/ }),

/***/ "./src/app/components/following-duels/following-duels.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FollowingDuelsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_vote_vote_service__ = __webpack_require__("./src/app/services/vote/vote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__enums_duel_type__ = __webpack_require__("./src/app/enums/duel-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__child_components_user_list_services_user_list_service__ = __webpack_require__("./src/app/child-components/user-list/services/user-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__enums_user_list_order_criteria__ = __webpack_require__("./src/app/enums/user-list-order-criteria.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_tooltip_tooltip_service__ = __webpack_require__("./src/app/services/tooltip/tooltip.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var FollowingDuelsComponent = /** @class */ (function () {
    function FollowingDuelsComponent(duelService, voteService, authService, userListService, router, titleService, tooltipService) {
        this.duelService = duelService;
        this.voteService = voteService;
        this.authService = authService;
        this.userListService = userListService;
        this.router = router;
        this.titleService = titleService;
        this.tooltipService = tooltipService;
        this.previewUserAmount = 5;
        this.totalUserCount = 0;
        this.duelsPage = 1;
        this.totalDuelsAmount = 0;
        this.isPopularDuels = false;
        this.duels = [];
        this.following = [];
        this.previewUsers = [];
    }
    FollowingDuelsComponent.prototype.ngOnInit = function () {
        this.initializeData();
        this.titleService.setTitle("Duelity - one on one design duels with equal opponents");
    };
    FollowingDuelsComponent.prototype.onNotify = function () {
        this.initializeData();
    };
    FollowingDuelsComponent.prototype.openVotePage = function (duel) {
        this.voteService.openVotePage(duel);
    };
    FollowingDuelsComponent.prototype.goToPerson = function (username) {
        this.router.navigate(["/user", username]);
    };
    FollowingDuelsComponent.prototype.openUserFollowing = function () {
        var followingIds = this.following.map(function (following) { return following.userId; });
        this.userListService.getUsersAndOpenModal("Following", followingIds, __WEBPACK_IMPORTED_MODULE_7__enums_user_list_order_criteria__["a" /* UserListOrderCriteria */].SkillPoints);
    };
    FollowingDuelsComponent.prototype.loadNextPage = function () {
        this.duelsPage++;
        this.loadDuels();
    };
    FollowingDuelsComponent.prototype.initializeData = function () {
        var _this = this;
        this.duelService.getDuelByType(__WEBPACK_IMPORTED_MODULE_3__enums_duel_type__["a" /* DuelType */].Following, this.duelsPage).subscribe(function (response) {
            if (response.success) {
                (_a = _this.duels).push.apply(_a, response.duels);
                _this.totalDuelsAmount = response.totalAmount;
                _this.isPopularDuels = response.isPopularDuels;
                if (_this.duels.length > 0) {
                    _this.authService.getUserProfile().subscribe(function (profile) {
                        _this.following = profile.user.following;
                        var followingIds = _this.following.map(function (following) { return following.userId; });
                        _this.userListService.getUserList(followingIds, __WEBPACK_IMPORTED_MODULE_7__enums_user_list_order_criteria__["a" /* UserListOrderCriteria */].SkillPoints, 1, _this.previewUserAmount).subscribe(function (response) {
                            if (response.success) {
                                _this.previewUsers = response.data.users;
                                _this.totalUserCount = response.data.totalCount;
                                _this.initializePreviewUsers();
                            }
                            else {
                                console.log(response.error);
                            }
                        });
                    });
                }
            }
            else {
                console.log("Something goes wrong during fetching following duels.");
            }
            var _a;
        });
    };
    FollowingDuelsComponent.prototype.loadDuels = function () {
        var _this = this;
        this.duelService.getDuelByType(__WEBPACK_IMPORTED_MODULE_3__enums_duel_type__["a" /* DuelType */].Popular, this.duelsPage).subscribe(function (response) {
            if (response.success) {
                (_a = _this.duels).push.apply(_a, response.duels);
                _this.totalDuelsAmount = response.totalAmount;
            }
            else {
                console.log("Something goes wrong during fetching popular duels.");
            }
            var _a;
        });
    };
    FollowingDuelsComponent.prototype.initializePreviewUsers = function () {
        var _this = this;
        setTimeout(function () {
            var avatars = $(".avatar");
            var rightPosition = _this.totalUserCount > _this.previewUserAmount ? 10 : 0;
            var zIndex = 6;
            if (avatars.length > 0) {
                for (var i = avatars.length - 1; i >= 0; i--) {
                    var avatar = $(avatars[i]);
                    avatar.css("right", "-" + rightPosition + "px");
                    avatar.css("z-index", zIndex);
                    rightPosition += 5;
                    zIndex += 1;
                }
                $(".avatars-label").css("right", 16 - (rightPosition - 5) + "px");
                _this.tooltipService.initializeTooltip('.custom-tooltip');
            }
        }, 0);
    };
    FollowingDuelsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-following-duels',
            template: __webpack_require__("./src/app/components/following-duels/following-duels.component.html"),
            styles: [__webpack_require__("./src/app/components/following-duels/following-duels.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_duel_service_duel_service__["a" /* DuelService */],
            __WEBPACK_IMPORTED_MODULE_2__services_vote_vote_service__["a" /* VoteService */],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__child_components_user_list_services_user_list_service__["a" /* UserListService */],
            __WEBPACK_IMPORTED_MODULE_6__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_9__services_tooltip_tooltip_service__["a" /* TooltipService */]])
    ], FollowingDuelsComponent);
    return FollowingDuelsComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ".modal-dialog {\r\n    max-width: 785px !important;\r\n}\r\n\r\n.modal-body {\r\n    padding-top: 50px;\r\n    padding-left: 28px;\r\n    padding-right: 95px;\r\n    padding-bottom: 40px;\r\n}\r\n\r\n.main-illustration {\r\n    margin-top: 30px;\r\n}\r\n\r\n.form {\r\n    width: 330px;\r\n    margin-left: 50px;\r\n}\r\n\r\n.form h2 {\r\n    margin-bottom: 10px;\r\n    margin-top: -30px;\r\n}\r\n\r\n.form .social-buttons {\r\n    margin-top: 25px;\r\n    margin-bottom: 30px;\r\n}\r\n\r\n.form .button {\r\n    width: 100%;\r\n}\r\n\r\n.form .input-wrapper {\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.form .other-options {\r\n    margin-top: 16px;\r\n}\r\n\r\n.form .other-options > div {\r\n    width: 162px;\r\n    text-align: center;\r\n}\r\n\r\n.form .other-options .delimiter {\r\n    width: 1px;\r\n    height: 19px;\r\n    background-color: #D8E2E9;\r\n    float:left\r\n}\r\n\r\n.form .other-options a {\r\n    opacity: 1;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.form .other-options a:hover {\r\n    opacity: .7;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.form p > a {\r\n    color: #5D5D5D !important;\r\n    border-bottom: 1px dotted #5D5D5D !important;\r\n}\r\n\r\n#google-button, #twitter-button {\r\n    width: 160px;\r\n    height: 50px;\r\n    display: inline-block;\r\n}\r\n\r\n#google-button {\r\n    background: #4285F4 url(\"data:image/svg+xml,%3Csvg width%3D%2226%22 height%3D%2226%22 viewBox%3D%220 0 26 26%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Ccircle cx%3D%2213%22 cy%3D%2213%22 r%3D%2213%22 fill%3D%22white%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M21.64 13.2046C21.64 12.5664 21.5827 11.9527 21.4764 11.3636H13V14.845H17.8436C17.635 15.97 17.0009 16.9232 16.0477 17.5614V19.8196H18.9564C20.6582 18.2527 21.64 15.9455 21.64 13.2046Z%22 fill%3D%22%234285F4%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13 22C15.43 22 17.4673 21.1941 18.9564 19.8196L16.0477 17.5614C15.2418 18.1014 14.2109 18.4205 13 18.4205C10.6559 18.4205 8.67182 16.8373 7.96409 14.71H4.95728V17.0418C6.43818 19.9832 9.48182 22 13 22Z%22 fill%3D%22%2334A853%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M7.96409 14.71C7.78409 14.17 7.68182 13.5932 7.68182 13C7.68182 12.4068 7.78409 11.83 7.96409 11.29V8.95819H4.95727C4.34773 10.1732 4 11.5477 4 13C4 14.4523 4.34773 15.8268 4.95727 17.0418L7.96409 14.71Z%22 fill%3D%22%23FBBC05%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13 7.57955C14.3214 7.57955 15.5077 8.03364 16.4405 8.92545L19.0218 6.34409C17.4632 4.89182 15.4259 4 13 4C9.48182 4 6.43818 6.01682 4.95728 8.95818L7.96409 11.29C8.67182 9.16273 10.6559 7.57955 13 7.57955Z%22 fill%3D%22%23EA4335%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 50% no-repeat;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#google-button:hover {\r\n    background: #3f7bdf url(\"data:image/svg+xml,%3Csvg width%3D%2226%22 height%3D%2226%22 viewBox%3D%220 0 26 26%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Ccircle cx%3D%2213%22 cy%3D%2213%22 r%3D%2213%22 fill%3D%22white%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M21.64 13.2046C21.64 12.5664 21.5827 11.9527 21.4764 11.3636H13V14.845H17.8436C17.635 15.97 17.0009 16.9232 16.0477 17.5614V19.8196H18.9564C20.6582 18.2527 21.64 15.9455 21.64 13.2046Z%22 fill%3D%22%234285F4%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13 22C15.43 22 17.4673 21.1941 18.9564 19.8196L16.0477 17.5614C15.2418 18.1014 14.2109 18.4205 13 18.4205C10.6559 18.4205 8.67182 16.8373 7.96409 14.71H4.95728V17.0418C6.43818 19.9832 9.48182 22 13 22Z%22 fill%3D%22%2334A853%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M7.96409 14.71C7.78409 14.17 7.68182 13.5932 7.68182 13C7.68182 12.4068 7.78409 11.83 7.96409 11.29V8.95819H4.95727C4.34773 10.1732 4 11.5477 4 13C4 14.4523 4.34773 15.8268 4.95727 17.0418L7.96409 14.71Z%22 fill%3D%22%23FBBC05%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13 7.57955C14.3214 7.57955 15.5077 8.03364 16.4405 8.92545L19.0218 6.34409C17.4632 4.89182 15.4259 4 13 4C9.48182 4 6.43818 6.01682 4.95728 8.95818L7.96409 11.29C8.67182 9.16273 10.6559 7.57955 13 7.57955Z%22 fill%3D%22%23EA4335%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 50% no-repeat;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#twitter-button {\r\n    background: #1DA1F2 url(\"data:image/svg+xml,%3Csvg width%3D%2218%22 height%3D%2214%22 viewBox%3D%220 0 18 14%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M18 1.65642C17.3374 1.93751 16.6262 2.12814 15.8798 2.21325C16.6424 1.77597 17.2266 1.08239 17.5024 0.258473C16.7871 0.663417 15.9974 0.957459 15.1564 1.11686C14.483 0.428656 13.5243 0 12.4615 0C10.4225 0 8.76878 1.5832 8.76878 3.53473C8.76878 3.81151 8.80123 4.08184 8.86478 4.34034C5.79537 4.19277 3.07482 2.78514 1.25346 0.646184C0.935697 1.16747 0.754507 1.77487 0.754507 2.42325C0.754507 3.64997 1.4076 4.73236 2.39739 5.36562C1.79162 5.34624 1.22236 5.18684 0.72476 4.92189V4.96605C0.72476 6.67849 1.99715 8.10766 3.686 8.43294C3.37635 8.51264 3.05048 8.55679 2.71244 8.55679C2.47446 8.55679 2.24459 8.53418 2.01743 8.49109C2.48798 9.89659 3.85096 10.9187 5.4668 10.9467C4.20388 11.8944 2.61103 12.4577 0.880259 12.4577C0.582783 12.4577 0.289363 12.4405 0 12.4093C1.63477 13.4141 3.57512 14 5.66016 14C12.4521 14 16.1651 8.61281 16.1651 3.94077L16.1529 3.48305C16.8791 2.98759 17.5065 2.3651 18 1.65642Z%22 fill%3D%22white%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 50% no-repeat;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#twitter-button:hover {\r\n    background: #1e95de url(\"data:image/svg+xml,%3Csvg width%3D%2218%22 height%3D%2214%22 viewBox%3D%220 0 18 14%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M18 1.65642C17.3374 1.93751 16.6262 2.12814 15.8798 2.21325C16.6424 1.77597 17.2266 1.08239 17.5024 0.258473C16.7871 0.663417 15.9974 0.957459 15.1564 1.11686C14.483 0.428656 13.5243 0 12.4615 0C10.4225 0 8.76878 1.5832 8.76878 3.53473C8.76878 3.81151 8.80123 4.08184 8.86478 4.34034C5.79537 4.19277 3.07482 2.78514 1.25346 0.646184C0.935697 1.16747 0.754507 1.77487 0.754507 2.42325C0.754507 3.64997 1.4076 4.73236 2.39739 5.36562C1.79162 5.34624 1.22236 5.18684 0.72476 4.92189V4.96605C0.72476 6.67849 1.99715 8.10766 3.686 8.43294C3.37635 8.51264 3.05048 8.55679 2.71244 8.55679C2.47446 8.55679 2.24459 8.53418 2.01743 8.49109C2.48798 9.89659 3.85096 10.9187 5.4668 10.9467C4.20388 11.8944 2.61103 12.4577 0.880259 12.4577C0.582783 12.4577 0.289363 12.4405 0 12.4093C1.63477 13.4141 3.57512 14 5.66016 14C12.4521 14 16.1651 8.61281 16.1651 3.94077L16.1529 3.48305C16.8791 2.98759 17.5065 2.3651 18 1.65642Z%22 fill%3D%22white%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 50% no-repeat;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\nbutton.close {\r\n    position: relative;\r\n    right: -130px;\r\n    top: -50px;\r\n    font-size: 27px;\r\n    opacity: .6;\r\n}"

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"{{loginService.modalId}}\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-body\">\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n        <img class=\"left main-illustration\" width=\"278\" height=\"415\" src=\"../../../assets/img/login/login-illustration.png\">\r\n        <div class=\"right form\">\r\n          <h2>Sign In to Duelity</h2>\r\n          <p>Sign in or create an account to be able to duel, vote, comment & more!</p>\r\n          <div class=\"social-buttons\">\r\n            <button id=\"google-button\" #googleButton class=\"inline left\"></button>\r\n            <button id=\"twitter-button\" class=\"inline right\" (click)=\"twitterService.onTwitterSignIn()\"></button>\r\n            <div class=\"clearfix\"></div>\r\n          </div>\r\n          <form id=\"loginForm\" (submit)=\"onLoginSubmit()\" action=\"#\">\r\n            <div class=\"input-wrapper\">\r\n              <app-input (onValueChange)=\"onUsernameChange($event)\" [value]=\"username\" [placeholder]=\"'Username or Email'\" [type]=\"inputType.Text\"\r\n                [error]=\"usernameErrorMessage\"></app-input>\r\n            </div>\r\n            <div class=\"input-wrapper\">\r\n              <app-input (onValueChange)=\"onPasswordChange($event)\" [value]=\"password\" [placeholder]=\"'Password'\" [type]=\"inputType.Password\"\r\n                [error]=\"passwordErrorMessage\"></app-input>\r\n            </div>\r\n            <button type=\"submit\" class=\"button primary-button\" (click)=\"onLoginSubmit()\">\r\n              <span>Sign In</span>\r\n            </button>\r\n          </form>\r\n          <div class=\"other-options\">\r\n            <div class=\"left\">\r\n              <p>\r\n                <a (click)=\"goToRegister()\">Create Account</a>\r\n              </p>\r\n            </div>\r\n            <div class=\"delimiter\"></div>\r\n            <div class=\"right\">\r\n              <p>\r\n                <a (click)=\"goToPasswordRestore()\">Restore Password</a>\r\n              </p>\r\n            </div>\r\n            <div class=\"clearfix\"></div>\r\n          </div>\r\n        </div>\r\n        <div class=\"clearfix\"></div>\r\n      </div>\r\n      <div class=\"footer-block\"></div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_google_google_service__ = __webpack_require__("./src/app/services/google/google.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_twitter_twitter_service__ = __webpack_require__("./src/app/services/twitter/twitter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__register_services_register_service__ = __webpack_require__("./src/app/components/register/services/register.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_login_login_service__ = __webpack_require__("./src/app/services/login/login.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__enums_input_type__ = __webpack_require__("./src/app/enums/input-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_notification_notification_service__ = __webpack_require__("./src/app/services/notification/notification.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, authService, validationService, socketService, googleService, twitterService, registerService, alertService, loginService, notificationService) {
        this.router = router;
        this.authService = authService;
        this.validationService = validationService;
        this.socketService = socketService;
        this.googleService = googleService;
        this.twitterService = twitterService;
        this.registerService = registerService;
        this.alertService = alertService;
        this.loginService = loginService;
        this.notificationService = notificationService;
        this.usernameErrorMessage = "";
        this.passwordErrorMessage = "";
        this.inputType = __WEBPACK_IMPORTED_MODULE_10__enums_input_type__["a" /* InputType */];
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.googleService.initGoogleAuth(this.googleButton.nativeElement);
    };
    LoginComponent.prototype.goToRegister = function () {
        this.loginService.closeModal();
        this.registerService.goToRegister();
    };
    LoginComponent.prototype.goToPasswordRestore = function () {
        this.loginService.closeModal();
        this.router.navigate(["/restore"]);
    };
    LoginComponent.prototype.onUsernameChange = function (username) {
        this.username = username;
    };
    LoginComponent.prototype.onPasswordChange = function (password) {
        this.password = password;
    };
    LoginComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        var success = true;
        this.usernameErrorMessage = "";
        this.passwordErrorMessage = "";
        var user = {
            username: this.username,
            password: this.password
        };
        if (!this.validationService.isStringValid(user.username)) {
            this.usernameErrorMessage = "This field can't be empty";
            success = false;
        }
        if (!this.validationService.isStringValid(user.password)) {
            this.passwordErrorMessage = "This field can't be empty";
            success = false;
        }
        if (!success)
            return;
        this.authService.authenticateUser(user).subscribe(function (data) {
            if (data.success) {
                _this.authService.storeUserData(data.token, data.user);
                _this.socketService.addOnlineUser();
                _this.loginService.closeModal();
                _this.router.navigate([_this.router.url]);
                window.location.reload();
            }
            else {
                _this.alertService.error(data.message);
            }
        });
    };
    LoginComponent.prototype.isUserValid = function (user) {
        return this.validationService.isObjectValid(user) &&
            this.validationService.isStringValid(user.username) &&
            this.validationService.isStringValid(user.password);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('googleButton'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], LoginComponent.prototype, "googleButton", void 0);
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("./src/app/components/login/login.component.html"),
            styles: [__webpack_require__("./src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__services_validation_validation_service__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_4__services_socket_socket_service__["a" /* SocketService */],
            __WEBPACK_IMPORTED_MODULE_5__services_google_google_service__["a" /* GoogleService */],
            __WEBPACK_IMPORTED_MODULE_6__services_twitter_twitter_service__["a" /* TwitterService */],
            __WEBPACK_IMPORTED_MODULE_7__register_services_register_service__["a" /* RegisterService */],
            __WEBPACK_IMPORTED_MODULE_8__services_alert_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_9__services_login_login_service__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_11__services_notification_notification_service__["a" /* NotificationService */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/my-duels/my-duels.component.css":
/***/ (function(module, exports) {

module.exports = ".wrapper {\r\n    padding-bottom: 100px;\r\n    padding-top: 80px;\r\n}\r\n\r\n.filter-block {\r\n    margin-top: 5px;\r\n}\r\n\r\n.filter-block button {\r\n    height: 39px;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 11px;\r\n    text-align: center;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #5D5D5D;\r\n    text-align: center;\r\n    padding-left: 16px;\r\n    padding-right: 16px;\r\n    padding-top: 13px;\r\n    padding-bottom: 13px;\r\n    -webkit-box-sizing: border-box;\r\n            box-sizing: border-box;\r\n    border: 2px solid #D8E2E9;\r\n    background-color: #fff;\r\n    margin: 0;\r\n    margin-left: -7px;\r\n    position: relative;\r\n    z-index: 6;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.filter-block button:first-child {\r\n    margin-left: 0px;\r\n}\r\n\r\n.filter-block button:hover {\r\n    background-color: #ecf1f4;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.filter-block button.selected {\r\n    background-color: #F55045;\r\n    color: #fff;\r\n    border: 2px solid #F55045;\r\n    z-index: 10;\r\n\r\n}\r\n\r\n.filter-block button span {\r\n    height: 17px;\r\n    text-align: center;\r\n    border-radius: 34px;\r\n    background-color: #FFE226;\r\n    -webkit-box-shadow: 0px 4px 14px rgba(255, 226, 38, 0.3);\r\n            box-shadow: 0px 4px 14px rgba(255, 226, 38, 0.3);\r\n    padding-top: 3px;\r\n    padding-bottom: 3px;\r\n    padding-left: 6px;\r\n    padding-right: 6px;\r\n    margin-left: 5px;\r\n}\r\n\r\n.filter-block button.selected span {\r\n    color: #F55145;\r\n}\r\n\r\n.right p.small {\r\n    width: 270px;\r\n    display: inline-block;\r\n    background-color: #fef6f6;\r\n    padding-left: 15px;\r\n    padding-top: 11px;\r\n    padding-right: 46px;\r\n    padding-bottom: 7px;\r\n    cursor: pointer;\r\n    line-height: 14px;\r\n    height: 50px;\r\n    position: relative;\r\n    top: -2px;\r\n}\r\n\r\n.right .space-block {\r\n    display: inline-block;\r\n    height: 40px;\r\n}\r\n\r\n.right p.small span.text {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    text-align: center;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #F55045;\r\n}\r\n\r\n.right p.small .silver-label {\r\n    position: relative;\r\n    top: 2.5px;\r\n    width: 13px;\r\n    height: 13px;\r\n    background-size: contain;\r\n}\r\n\r\n#start-duel-button {\r\n    position: relative;\r\n    top: -10px;\r\n    margin-left: 16px;\r\n}\r\n\r\n.information-block {\r\n    border-left: 1px solid #D8E2E9;\r\n    border-right: 1px solid #D8E2E9;\r\n    border-bottom: 1px solid #D8E2E9;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n    cursor: pointer;\r\n    background-clip: padding-box;\r\n}\r\n\r\n.information-block .information-block-border {\r\n    height: 6px;\r\n    width: 590px;\r\n    margin-left: -1px;\r\n    background-color: #F55045; \r\n}\r\n\r\n.information-block:hover {\r\n    -webkit-box-shadow: 0px 20px 40px rgba(193, 199, 203, 0.2);\r\n            box-shadow: 0px 20px 40px rgba(193, 199, 203, 0.2);\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.information-block .information-block-wrapper {\r\n    padding-left: 40px;\r\n    padding-top: 34px;\r\n    padding-right: 40px;\r\n}\r\n\r\n.information-block p {\r\n    width: 440px;\r\n    margin-top: 7px;\r\n    margin-bottom: 22px;\r\n}\r\n\r\n.information-block .close-button {\r\n    margin-top: 6px;\r\n}\r\n\r\n.information-block .close-button path {\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.information-block .close-button:hover path {\r\n    fill: #000;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.info-label {\r\n    display: inline-block;\r\n    width: 20px;\r\n    height: 20px;\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2220%22 height%3D%2220%22 viewBox%3D%220 0 20 20%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M10 2.00012C5.589 2.00012 2 5.58912 2 10.0001C2 14.4111 5.589 18.0001 10 18.0001C14.411 18.0001 18 14.4111 18 10.0001C18 5.58912 14.411 2.00012 10 2.00012ZM10 20.0001C4.486 20.0001 0 15.5151 0 10.0001C0 4.48612 4.486 0.00012207 10 0.00012207C15.514 0.00012207 20 4.48612 20 10.0001C20 15.5151 15.514 20.0001 10 20.0001Z%22 fill%3D%22%2326AFFF%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M11 13.0001V9.00012C11 8.44812 10.552 8.00012 10 8.00012H8V10.0001H9V13.0001H7V15.0001H13V13.0001H11Z%22 fill%3D%22%2326AFFF%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M11.25 6.00012C11.25 6.69012 10.69 7.25012 10 7.25012C9.31 7.25012 8.75 6.69012 8.75 6.00012C8.75 5.31012 9.31 4.75012 10 4.75012C10.69 4.75012 11.25 5.31012 11.25 6.00012Z%22 fill%3D%22%2326AFFF%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 50% no-repeat;\r\n    position: relative;\r\n    top: 1px;\r\n    margin-right: 5px;\r\n}\r\n\r\n.empty-state {\r\n    width: 985px;\r\n    margin: 0 auto;\r\n    margin-top: 110px;\r\n}\r\n\r\n.empty-state .right {\r\n    width: 590px;\r\n}\r\n\r\n.empty-state .right h2 {\r\n    margin-top: 19px;\r\n    margin-bottom: 16px;\r\n}\r\n\r\n.empty-state .right p {\r\n    margin-bottom: 35px;\r\n}\r\n\r\n.duels-container {\r\n    margin-top: 30px;\r\n}"

/***/ }),

/***/ "./src/app/components/my-duels/my-duels.component.html":
/***/ (function(module, exports) {

module.exports = "<app-back-to-top></app-back-to-top>\r\n<div *ngIf=\"showPage\" class=\"wrapper\">\r\n    <div class=\"filter-block left\">\r\n        <button [class.selected]=\"myDuelsService.currentStatus === duelStatus.Waiting\" (click)=\"changeCurrentStatus(duelStatus.Waiting)\">Unaccepted\r\n            <span *ngIf=\"duelsAmount.waitingDuelsCount > 0\">{{duelsAmount.waitingDuelsCount}}</span>\r\n        </button>\r\n        <button [class.selected]=\"myDuelsService.currentStatus === duelStatus.InProgress\" (click)=\"changeCurrentStatus(duelStatus.InProgress)\">Waiting for works\r\n            <span *ngIf=\"duelsAmount.inProgressDuelsCount > 0\">{{duelsAmount.inProgressDuelsCount}}</span>\r\n        </button>\r\n        <button [class.selected]=\"myDuelsService.currentStatus === duelStatus.Voting\" (click)=\"changeCurrentStatus(duelStatus.Voting)\">Voting\r\n            <span *ngIf=\"duelsAmount.votingDuelsCount > 0\">{{duelsAmount.votingDuelsCount}}</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"right\">\r\n        <p class=\"small\" *ngIf=\"!currentUser.isPaidAccount\">Unleash awesome features with\r\n            <span class=\"text\">Duelity Silver</span>\r\n            <span class=\"silver-label\"></span>\r\n        </p>\r\n        <div *ngIf=\"currentUser.isPaidAccount\" class=\"space-block\"></div>\r\n        <a id=\"start-duel-button\" class=\"button primary-button\" routerLink=\"/start\">\r\n            <span>Start New Duel</span>\r\n        </a>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <div *ngIf=\"duels !== undefined && duels.length > 0\" class=\"duels-container\">\r\n        <div *ngIf=\"!authService.getCurrentUser().isHelpOff\" class=\"duel-wrapper information-block\">\r\n            <div class=\"information-block-border\"></div>\r\n            <div class=\"information-block-wrapper\">\r\n                <div>\r\n                    <h2 class=\"left\">\r\n                        <span class=\"info-label\"></span>\r\n                        Let us show you around\r\n                    </h2>\r\n                    <div class=\"right close-button\" (click)=\"turnOffHelp()\">\r\n                        <svg width=\"16\" height=\"17\" viewBox=\"0 0 16 17\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                            <path opacity=\"0.5\" fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M5.87865 8.00121L0.000466347 2.12096L2.12216 1.14441e-05L7.99959 5.87952L13.877 2.8491e-05L15.9987 2.12097L10.1205 8.00121L15.9992 13.8819L13.8775 16.0029L7.99959 10.1229L2.1217 16.0029L0 13.8819L5.87865 8.00121Z\"\r\n                                fill=\"#5D5D5D\" />\r\n                        </svg>\r\n                    </div>\r\n                    <div class=\"clearfix\"></div>\r\n                </div>\r\n                <p>We get it, it’s confusing for new ones like you. We have a convenient page where you can learn how Duelity\r\n                    works though!\r\n                </p>\r\n                <a class=\"button secondary-button\" routerLink=\"/about\">\r\n                    <span>Yeah, Show Me</span>\r\n                </a>\r\n            </div>\r\n        </div>\r\n        <div *ngFor=\"let duel of duels\" (click)=\"openVotePage(duel)\" class=\"duel-wrapper\">\r\n            <app-duel [duel]=\"duel\" [duelOwner]=\"currentUserId\" (notify)=\"onNotify($event)\"></app-duel>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"duels === undefined || duels.length === 0\" class=\"empty-state\">\r\n        <div class=\"left\">\r\n            <img width=\"304\" height=\"240\" src=\"../../../assets/img/my-duels/empty-state-illustration.png\">\r\n        </div>\r\n        <div class=\"right\">\r\n            <h2 *ngIf=\"myDuelsService.currentStatus === duelStatus.Waiting\">Zero duels are waiting for acception now</h2>\r\n            <h2 *ngIf=\"myDuelsService.currentStatus === duelStatus.InProgress\">No duels are waiting for works now</h2>\r\n            <h2 *ngIf=\"myDuelsService.currentStatus === duelStatus.Voting\">No public duels for people to vote yet</h2>\r\n            <p *ngIf=\"myDuelsService.currentStatus === duelStatus.Waiting\">All the duels that have been sent to you or by you will appear on this tab. Finished duels can be found in your\r\n                profile</p>\r\n            <p *ngIf=\"myDuelsService.currentStatus === duelStatus.InProgress\">All the duels that have been accepted by you or your opponent will appear here. You both need to upload works\r\n                until the deadline for voting to start and for duel to become public</p>\r\n            <p *ngIf=\"myDuelsService.currentStatus === duelStatus.Voting\">After both you and opponent upload your works duel will become public. Voting lasts exactly 24 hours from the\r\n                moment second work has been uploaded</p>\r\n            <a id=\"start-duel-button-empty\" class=\"button primary-button\" routerLink=\"/start\">\r\n                <span>Find an Opponent</span>\r\n            </a>\r\n        </div>\r\n        <div class=\"clearfix\"></div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/my-duels/my-duels.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyDuelsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__ = __webpack_require__("./src/app/enums/duel-status.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_services_user_service__ = __webpack_require__("./src/app/components/user/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_vote_vote_service__ = __webpack_require__("./src/app/services/vote/vote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_my_duels_service__ = __webpack_require__("./src/app/components/my-duels/services/my-duels.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyDuelsComponent = /** @class */ (function () {
    function MyDuelsComponent(userService, authService, voteService, myDuelsService, titleService) {
        this.userService = userService;
        this.authService = authService;
        this.voteService = voteService;
        this.myDuelsService = myDuelsService;
        this.titleService = titleService;
        this.duels = [];
        this.duelsAmount = {};
        this.duelStatus = __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */];
        this.showPage = false;
    }
    MyDuelsComponent.prototype.ngOnInit = function () {
        this.initializeData();
        this.titleService.setTitle("My Duels - Duelity");
    };
    MyDuelsComponent.prototype.onNotify = function (userId) {
        this.getUserDuels(this.currentUser.id);
    };
    MyDuelsComponent.prototype.changeCurrentStatus = function (status) {
        this.myDuelsService.currentStatus = status;
        this.getUserDuels(this.currentUser.id);
    };
    MyDuelsComponent.prototype.openVotePage = function (duel) {
        this.voteService.openVotePage(duel);
    };
    MyDuelsComponent.prototype.turnOffHelp = function () {
        var _this = this;
        this.userService.turnOffHelp().subscribe(function (response) {
            if (response.success) {
                _this.authService.updateUserData(response.user);
            }
            else {
                console.log(response.error);
            }
        });
    };
    MyDuelsComponent.prototype.initializeData = function () {
        this.currentUser = this.authService.getCurrentUser();
        this.getUserDuels(this.currentUser.id);
    };
    MyDuelsComponent.prototype.getUserDuels = function (userId) {
        var _this = this;
        this.userService.getUserDuels(userId, [this.myDuelsService.currentStatus]).subscribe(function (response) {
            if (response.success) {
                _this.duels = response.duels;
                _this.duelsAmount = response.duelsAmount;
                _this.showPage = true;
            }
            else {
                console.log(response.error);
            }
        });
    };
    MyDuelsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-my-duels',
            template: __webpack_require__("./src/app/components/my-duels/my-duels.component.html"),
            styles: [__webpack_require__("./src/app/components/my-duels/my-duels.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__user_services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__services_vote_vote_service__["a" /* VoteService */],
            __WEBPACK_IMPORTED_MODULE_5__services_my_duels_service__["a" /* MyDuelsService */],
            __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Title */]])
    ], MyDuelsComponent);
    return MyDuelsComponent;
}());



/***/ }),

/***/ "./src/app/components/my-duels/services/my-duels.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyDuelsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__ = __webpack_require__("./src/app/enums/duel-status.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MyDuelsService = /** @class */ (function () {
    function MyDuelsService() {
        this.currentStatus = __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].Waiting;
        this.myDuelsPage = "myduels";
    }
    MyDuelsService.prototype.getMyDuelsPage = function () {
        return this.myDuelsPage;
    };
    MyDuelsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], MyDuelsService);
    return MyDuelsService;
}());



/***/ }),

/***/ "./src/app/components/new-duels/new-duels.component.css":
/***/ (function(module, exports) {

module.exports = ".new-duels {\r\n    margin-top: 100px;\r\n    margin-bottom: 100px;\r\n}\r\n\r\n.duels-container {\r\n    margin-top: 20px;\r\n}"

/***/ }),

/***/ "./src/app/components/new-duels/new-duels.component.html":
/***/ (function(module, exports) {

module.exports = "<app-back-to-top></app-back-to-top>\r\n<div class=\"new-duels\">\r\n    <h2 class=\"left main-heading\">New Duels</h2>\r\n    <div class=\"clearfix\"></div>\r\n    <div *ngIf=\"duels !== undefined && duels.length > 0\" class=\"duels\">\r\n        <div class=\"duels-container\">\r\n            <div *ngFor=\"let duel of duels\" (click)=\"openVotePage(duel)\" class=\"duel-wrapper\">\r\n                <app-duel [duel]=\"duel\" [duelOwner]=\"currentUserId\" (notify)=\"onNotify($event)\"></app-duel>\r\n            </div>\r\n        </div>\r\n        <div *ngIf=\"duels.length < totalDuelsAmount\" class=\"duels-end-block-button\">\r\n            <a class=\"button secondary-button\" (click)=\"loadNextPage()\">\r\n                <span>Load More</span>\r\n            </a>\r\n        </div>\r\n        <div *ngIf=\"duels.length >= totalDuelsAmount\" class=\"duels-end-block-message\">\r\n            <h2>Wow, you’ve reached the end of the list</h2>\r\n            <p>Good job exploring new duels. Try looking through following ones now!</p>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"duels === undefined || duels.length === 0\">\r\n        <h1>No duels.</h1>\r\n    </div>\r\n</div>\r\n<div class=\"clearfix\"></div>"

/***/ }),

/***/ "./src/app/components/new-duels/new-duels.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewDuelsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_vote_vote_service__ = __webpack_require__("./src/app/services/vote/vote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__enums_duel_type__ = __webpack_require__("./src/app/enums/duel-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NewDuelsComponent = /** @class */ (function () {
    function NewDuelsComponent(duelService, voteService, titleService) {
        this.duelService = duelService;
        this.voteService = voteService;
        this.titleService = titleService;
        this.duelsPage = 1;
        this.totalDuelsAmount = 0;
        this.duels = [];
    }
    NewDuelsComponent.prototype.ngOnInit = function () {
        this.initializeData();
        this.titleService.setTitle("New Duels - Duelity");
    };
    NewDuelsComponent.prototype.onNotify = function () {
        this.initializeData();
    };
    NewDuelsComponent.prototype.openVotePage = function (duel) {
        this.voteService.openVotePage(duel);
    };
    NewDuelsComponent.prototype.initializeData = function () {
        var _this = this;
        this.duelService.getDuelByType(__WEBPACK_IMPORTED_MODULE_3__enums_duel_type__["a" /* DuelType */].New, this.duelsPage).subscribe(function (response) {
            if (response.success) {
                (_a = _this.duels).push.apply(_a, response.duels);
                _this.totalDuelsAmount = response.totalAmount;
            }
            else {
                console.log("Something goes wrong during fetching new duels.");
            }
            var _a;
        });
    };
    NewDuelsComponent.prototype.loadNextPage = function () {
        this.duelsPage++;
        this.initializeData();
    };
    NewDuelsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-new-duels',
            template: __webpack_require__("./src/app/components/new-duels/new-duels.component.html"),
            styles: [__webpack_require__("./src/app/components/new-duels/new-duels.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_duel_service_duel_service__["a" /* DuelService */],
            __WEBPACK_IMPORTED_MODULE_2__services_vote_vote_service__["a" /* VoteService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* Title */]])
    ], NewDuelsComponent);
    return NewDuelsComponent;
}());



/***/ }),

/***/ "./src/app/components/notifications/notifications.component.css":
/***/ (function(module, exports) {

module.exports = ".notifications-wrapper {\r\n    padding-top: 100px;\r\n    padding-bottom: 100px;\r\n}\r\n\r\n.notification-container {\r\n    padding-top: 0px;\r\n    padding-bottom: 45px;\r\n    border-bottom: 1px solid #ededed;\r\n}\r\n\r\n.notification-container.no-border {\r\n    border-bottom: 0;\r\n}\r\n\r\n.notification-container > div {\r\n    display: inline-block;\r\n    position: relative;\r\n    top: 23px;\r\n}\r\n\r\n.notification-container > div > div {\r\n    display: inline-block;\r\n}\r\n\r\n.notification-container .is-new-block {\r\n    width: 20px;\r\n    margin-left: -22px;\r\n    position: relative;\r\n    top: 16px;\r\n    right: 10px; \r\n}\r\n\r\n.notification-container .avatar {\r\n    position: relative;\r\n    top: -7px;\r\n}\r\n\r\n.notification-container .notification-text-block {\r\n    position: relative;\r\n    top: 10px;\r\n    left: 56px;\r\n}\r\n\r\n.notification-container .is-new-block > div {\r\n    width: 10px;\r\n    height: 10px;\r\n    border-radius: 50%;\r\n    background-color: #F55045;\r\n    opacity: 1;\r\n}\r\n\r\n.notification-container .is-new-block > div.hidden {\r\n    opacity: 0;\r\n}\r\n\r\n.notification-container .image-block img {\r\n    width: 36px;\r\n    height: 36px;\r\n    border-radius: 50%;\r\n}\r\n\r\n.notification-container .timestamp-block {\r\n    margin-left: 20px;\r\n    font-family: Menoe Grotesque Pro;\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 14px;\r\n    line-height: 25px;\r\n    text-align: right;\r\n    letter-spacing: -0.02em;\r\n    color: #C1C7CB;\r\n    position: relative;\r\n    top: 40px;\r\n}\r\n\r\n.notifications-content {\r\n    width: 794px;\r\n    margin: 0 auto;\r\n}\r\n\r\n.notifications-content .notification-text-block p {\r\n    width: 415px;\r\n    padding-left: 33px;\r\n}\r\n\r\n.notifications-content .notification-text-block p.created-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2218%22 viewBox%3D%220 0 17 18%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M14.4472 15.0371C14.7456 15.3448 14.7542 15.8515 14.4472 16.1672C14.155 16.4693 13.6449 16.4693 13.3511 16.1672L10.0318 12.745L11.1279 11.6149L14.4472 15.0371ZM6.49711 14.1292L4.00493 15.4136L5.25063 12.8441L12.2264 5.65281L13.4721 6.93794L6.49711 14.1292ZM5.37227 7.94095C5.5963 7.80509 5.81024 7.65004 5.99861 7.45583C6.1862 7.26162 6.33659 7.04264 6.46836 6.81167L7.68926 8.06962L6.59317 9.19971L5.37227 7.94095ZM1.57457 3.24079C2.24199 3.5397 2.79004 3.51252 3.31173 3.48615C4.68534 3.41582 5.35586 4.36368 5.35586 5.19566C5.35586 5.62244 5.19462 6.02364 4.90161 6.32654C4.31558 6.93075 3.29468 6.93075 2.70865 6.32654C1.90479 5.49616 1.64433 4.22382 1.57457 3.24079ZM15.1905 3.87937C15.5347 4.23422 15.5347 4.81124 15.1913 5.1653L14.568 5.80786L13.3223 4.52273L13.9456 3.88016C14.2781 3.5365 14.8595 3.5373 15.1905 3.87937ZM13.3511 10.2419L14.4472 11.372L16.7727 8.97433C17.0758 8.66184 17.0758 8.15674 16.7727 7.84425L15.7789 6.81966L16.2882 6.29538C17.2347 5.31715 17.2347 3.72752 16.2874 2.74928C15.3696 1.80302 13.7689 1.80302 12.8488 2.74928L8.78531 6.93954L6.88613 4.98068C6.83575 4.20944 6.5342 3.48775 5.99856 2.9355C4.89316 1.79583 4.02342 1.84857 3.23507 1.88933C2.72811 1.91571 2.24285 2.05477 1.36613 1.15166L0.249106 0L0.0491117 1.616C0.0305076 1.76865 -0.390411 5.39067 1.61263 7.45583C2.14905 8.00809 2.84903 8.32058 3.59785 8.37173L5.49702 10.3298L4.06528 11.8059C4.00637 11.8659 3.95753 11.9362 3.92032 12.0129L1.57775 16.8434C1.42892 17.1511 1.48783 17.5227 1.72348 17.7656C1.96146 18.0118 2.32347 18.0677 2.61803 17.9159L7.3024 15.5007C7.37682 15.4623 7.44503 15.412 7.50394 15.3512L8.93569 13.8751L12.255 17.2973C12.6945 17.7505 13.2782 17.9998 13.8991 17.9998C14.52 17.9998 15.1045 17.7505 15.5433 17.2981C15.982 16.8458 16.2246 16.2431 16.2246 15.6022C16.2246 14.962 15.9828 14.3594 15.5433 13.9071L12.224 10.4848L14.6828 7.94974L15.1285 8.40929L13.3511 10.2419Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 1px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.accepted-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2212%22 viewBox%3D%220 0 16 12%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M1 5.66667L5.66667 10.3333L15 1%22 stroke%3D%22%2331D431%22 stroke-width%3D%222%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 1px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.declined-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2214%22 height%3D%2214%22 viewBox%3D%220 0 14 14%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M14 1.13679L12.8632 0L7 5.86321L1.13679 0L0 1.13679L5.86321 7L0 12.8632L1.13679 14L7 8.13759L12.8632 14L14 12.8632L8.13679 7L14 1.13679Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 1px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.uploaded-work {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2218%22 height%3D%2220%22 viewBox%3D%220 0 18 20%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M12.947 3.9225L8.99985 0L5.05266 3.9225L6.23841 5.10083L8.16127 3.19V10.345H9.83843V3.19L11.7613 5.10083L12.947 3.9225Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M1.77778 18.3333L2.89561 15H15.1045L16.2223 18.3333H1.77778ZM17.9162 18.1125L16.5468 14.0308V6.66667C16.5468 6.20667 16.1719 5.83333 15.7082 5.83333H13.1925V7.5H14.8696V13.3333H3.12953V7.5H4.80669V5.83333H2.29095C1.82806 5.83333 1.45237 6.20667 1.45237 6.66667V14.0308L0.0838138 18.1117C-0.0721618 18.5767 -0.00842979 19.0533 0.258238 19.4217C0.524068 19.7892 0.95929 20 1.45237 20H16.5468C17.0399 20 17.4751 19.7892 17.7418 19.4217C18.0084 19.0533 18.0722 18.5767 17.9162 18.1125Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M10.677 17.5H7.32272V15.8333H10.677V17.5Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 1px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.new-vote {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%227%22 viewBox%3D%220 0 16 7%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Crect width%3D%227%22 height%3D%227%22 rx%3D%223.5%22 fill%3D%22%23F55045%22%2F%3E%0D%3Crect x%3D%2210%22 y%3D%221%22 width%3D%225%22 height%3D%225%22 rx%3D%222.5%22 stroke%3D%22%23C1C7CB%22 stroke-width%3D%222%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.won-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2217%22 viewBox%3D%220 0 17 17%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M15.2989 4.28317C15.2989 4.54242 15.1824 4.78552 14.9801 4.94702L13.5938 6.05627C13.5946 6.02057 13.5989 5.98572 13.5989 5.95002V2.55002H15.2989V4.28317ZM8.49375 9.34998C6.61865 9.34998 5.09375 7.82508 5.09375 5.94998V1.69998H11.8937V5.94998C11.8937 7.82508 10.3688 9.34998 8.49375 9.34998ZM2.02077 4.94787C1.81762 4.78552 1.70117 4.54242 1.70117 4.28317V2.55002H3.40117V5.95002C3.40117 5.98572 3.40542 6.02057 3.40627 6.05627L2.02077 4.94787ZM10.1988 15.3H6.79883V14.45H10.1988V15.3ZM16.15 0.85H13.3637C13.0688 0.34425 12.5264 0 11.9 0H5.1C4.47355 0 3.93125 0.34425 3.6363 0.85H0.85C0.3808 0.85 0 1.2308 0 1.7V4.28315C0 5.06175 0.3485 5.78765 0.9571 6.2747L4.5084 9.1154C5.27595 10.081 6.3835 10.7601 7.65 10.9735V12.75H5.95C5.4808 12.75 5.1 13.1308 5.1 13.6V15.3H4.25V17H12.75V15.3H11.9V13.6C11.9 13.1308 11.5192 12.75 11.05 12.75H9.35V10.9735C10.6165 10.7601 11.7241 10.081 12.4916 9.1154L16.0438 6.2747C16.6515 5.78765 17 5.06175 17 4.28315V1.7C17 1.2308 16.6192 0.85 16.15 0.85Z%22 fill%3D%22%2331D431%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 1px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.lost-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2217%22 viewBox%3D%220 0 17 17%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M8.50074 15.3001C4.75139 15.3001 1.70074 12.2494 1.70074 8.50007C1.70074 4.75072 4.75139 1.70007 8.50074 1.70007C12.2501 1.70007 15.3007 4.75072 15.3007 8.50007C15.3007 12.2494 12.2501 15.3001 8.50074 15.3001ZM8.5 0C3.8131 0 0 3.8131 0 8.5C0 13.1877 3.8131 17 8.5 17C13.1869 17 17 13.1877 17 8.5C17 3.8131 13.1869 0 8.5 0ZM12.8586 6.79993C12.8586 6.79993 11.9023 7.86583 11.9023 8.39368C11.9023 8.92153 12.3307 9.34993 12.8586 9.34993C13.3864 9.34993 13.8148 8.92153 13.8148 8.39368C13.8148 7.86583 12.8586 6.79993 12.8586 6.79993ZM4.25 5.10004H7.65V6.80004H4.25V5.10004ZM12.7477 5.10004H9.34766V6.80004H12.7477V5.10004ZM4.94051 12.4987L3.75391 11.2807C5.03146 10.0354 6.71701 9.35034 8.50116 9.35034C10.3185 9.35034 12.0261 10.0575 13.3088 11.341L12.106 12.5429C11.1438 11.5807 9.86371 11.0503 8.50116 11.0503C7.16326 11.0503 5.89846 11.5646 4.94051 12.4987Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 2px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.draw-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%227%22 viewBox%3D%220 0 16 7%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Crect width%3D%227%22 height%3D%227%22 rx%3D%223.5%22 fill%3D%22%23F55045%22%2F%3E%0D%3Crect x%3D%229%22 width%3D%227%22 height%3D%227%22 rx%3D%223.5%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.new-follow {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2219%22 height%3D%2218%22 viewBox%3D%220 0 19 18%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M6.90681 5.1818C5.95421 5.1818 5.17952 5.95562 5.17952 6.90908C5.17952 7.86168 5.95421 8.63637 6.90681 8.63637C7.8594 8.63637 8.63409 7.86168 8.63409 6.90908C8.63409 5.95562 7.8594 5.1818 6.90681 5.1818ZM6.90948 10.3637C5.00429 10.3637 3.45491 8.81433 3.45491 6.90914C3.45491 5.00394 5.00429 3.45456 6.90948 3.45456C8.81468 3.45456 10.3641 5.00394 10.3641 6.90914C10.3641 8.81433 8.81468 10.3637 6.90948 10.3637Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M1.86523 15.5456H11.9552C11.4387 13.9841 9.50761 12.9547 6.90977 12.9547C4.31194 12.9547 2.38169 13.9841 1.86523 15.5456ZM13.8183 17.2729H0V16.4092C0 13.358 2.84139 11.2274 6.90915 11.2274C10.9778 11.2274 13.8183 13.358 13.8183 16.4092V17.2729Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M18.9995 2.59093H16.4085V0H14.6813V2.59093H12.0903V4.31822H14.6813V6.90915H16.4085V4.31822H18.9995V2.59093Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 1px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.featured-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2213%22 height%3D%2216%22 viewBox%3D%220 0 13 16%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M0 16V0H13V16L6.5 10.1053L0 16Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 1px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.popular-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2219%22 height%3D%2211%22 viewBox%3D%220 0 19 11%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M12.2511 0V1.87922H15.7085L10.3229 7.12787L6.46638 3.36944L0 9.67139L1.36327 11L6.46638 6.02665L10.3229 9.78509L17.0718 3.20782V6.57726H19V0H12.2511Z%22 fill%3D%22%2331D431%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 1px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.ended-subscription {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2216%22 viewBox%3D%220 0 16 16%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M6.34766 11.802C6.8457 12.0073 7.44336 12.11 8.14062 12.11C8.78516 12.11 9.35742 12.0147 9.85547 11.824C10.3633 11.626 10.7539 11.3473 11.0332 10.988C11.3203 10.6213 11.4629 10.1923 11.4629 9.701C11.4629 9.25367 11.3672 8.88334 11.1758 8.59C10.9863 8.28934 10.6855 8.04733 10.2734 7.86401C9.87109 7.67334 9.32812 7.51567 8.64648 7.39101C8.22852 7.303 7.90625 7.21867 7.67773 7.138C7.45898 7.05 7.29688 6.95467 7.19531 6.85201C7.09961 6.74934 7.05078 6.621 7.05078 6.467C7.05078 6.23967 7.13867 6.06734 7.31445 5.95C7.40625 5.88928 7.51758 5.84427 7.65039 5.81497C7.77344 5.78766 7.91406 5.774 8.07422 5.774C8.36133 5.774 8.59961 5.83267 8.78906 5.95C8.98828 6.06734 9.10742 6.21033 9.15234 6.37901C9.19727 6.445 9.24414 6.49267 9.29492 6.522C9.35352 6.55134 9.42773 6.566 9.51562 6.566H10.9668C11.0078 6.566 11.0449 6.55734 11.0781 6.54002C11.0977 6.52973 11.1152 6.5164 11.1328 6.5C11.1758 6.456 11.1992 6.401 11.1992 6.335C11.1895 6.12651 11.1309 5.9165 11.0234 5.70495C10.9648 5.5891 10.8906 5.47278 10.8027 5.356C10.5605 5.01867 10.2051 4.74001 9.73633 4.52C9.26562 4.3 8.71289 4.19 8.07422 4.19C7.45898 4.19 6.91992 4.289 6.45703 4.487C5.99609 4.68501 5.64062 4.96 5.39062 5.312C5.14062 5.664 5.01562 6.06367 5.01562 6.511C5.01562 7.17101 5.23242 7.68067 5.66602 8.04C6.10547 8.39201 6.76562 8.65967 7.64453 8.843C8.12109 8.94567 8.48047 9.03734 8.72266 9.118C8.8418 9.15784 8.94531 9.20035 9.0332 9.24554C9.12305 9.29188 9.19531 9.34103 9.25195 9.39301C9.36914 9.49567 9.42773 9.62767 9.42773 9.789C9.42773 10.0163 9.31641 10.196 9.09766 10.328C8.87695 10.46 8.55859 10.526 8.14062 10.526C7.78906 10.526 7.50586 10.4637 7.29297 10.339C7.08008 10.2143 6.92578 10.0567 6.83203 9.866C6.77344 9.8 6.71484 9.75233 6.65625 9.723C6.60352 9.68633 6.53516 9.668 6.44727 9.668H5.06055C4.99414 9.668 4.93555 9.69367 4.88477 9.745C4.83984 9.789 4.81836 9.84034 4.81836 9.899C4.83398 10.295 4.96484 10.6617 5.21484 10.999C5.4707 11.329 5.84961 11.5967 6.34766 11.802Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M0 8C0 3.58173 3.58203 0 8 0C12.418 0 16 3.58173 16 8C16 12.4183 12.418 16 8 16C3.58203 16 0 12.4183 0 8ZM1.5 8C1.5 4.68629 4.68555 1.5 8 1.5C11.3145 1.5 14.5 4.68629 14.5 8C14.5 11.3137 11.3145 14.5 8 14.5C4.68555 14.5 1.5 11.3137 1.5 8Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 1px no-repeat;\r\n}\r\n\r\n.notifications-content .notification-text-block p.started-voting {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%227%22 viewBox%3D%220 0 16 7%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Crect x%3D%221%22 y%3D%221%22 width%3D%225%22 height%3D%225%22 rx%3D%222.5%22 stroke%3D%22%23C1C7CB%22 stroke-width%3D%222%22%2F%3E%0D%3Crect x%3D%2210%22 y%3D%221%22 width%3D%225%22 height%3D%225%22 rx%3D%222.5%22 stroke%3D%22%23C1C7CB%22 stroke-width%3D%222%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.empty-state-wrapper {\r\n    text-align: center;\r\n    padding-top: 70px;\r\n    padding-bottom: 115px;\r\n}\r\n\r\n.empty-state-block {\r\n    display: inline-block;\r\n}\r\n\r\n.empty-state-block img {\r\n    margin-bottom: 35px;\r\n}\r\n\r\n.empty-state-block h2 {\r\n    margin-bottom: 8px;\r\n}\r\n\r\n.empty-state-block p {\r\n    margin-bottom: 30px;\r\n}\r\n\r\n.empty-state-block .primary-button {\r\n    width: 180px;\r\n}\r\n\r\n.pager-block {\r\n    width: 100%; \r\n    margin-top: 35px;\r\n}"

/***/ }),

/***/ "./src/app/components/notifications/notifications.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"notificationService.notifications\" id=\"notifications\">\r\n  <div *ngIf=\"notificationService.notifications.length > 0\" class=\"notifications-wrapper\">\r\n    <div class=\"notifications-content\">\r\n      <h1>Notifications</h1>\r\n      <div class=\"notification-container\" [class.no-border]=\"i === notificationService.notifications.length - 1\" *ngFor=\"let notification of notificationService.notifications; let i = index\">\r\n        <div>\r\n          <div>\r\n            <div class=\"image-block left\">\r\n              <div class=\"is-new-block\">\r\n                <div [class.hidden]=\"!notification.isNewNotification\"></div>\r\n              </div>\r\n              <app-profile-picture class=\"avatar\" elementClass=\"x-small\" userId={{notification.userId}} fullname={{notification.fullname}}\r\n                backgroundImage={{notification.imageUrl}} backgroundColor={{notification.imageColor}}>\r\n              </app-profile-picture>\r\n            </div>\r\n            <div class=\"notification-text-block\">\r\n              <p [className]=\"notificationService.notificationIcons[notification.type]\" [innerHTML]=\"notification.text | safeHtml\"></p>\r\n            </div>\r\n            <div class=\"clearfix\"></div>\r\n          </div>\r\n          <div class=\"clearfix\"></div>\r\n        </div>\r\n        <div class=\"timestamp-block right\">\r\n          <span>{{dateFormatter.toRelativeFormat(notification.timestamp)}}</span>\r\n        </div>\r\n        <div class=\"clearfix\"></div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"center\">\r\n        <div class=\"pager-block\">\r\n          <div *ngFor=\"let page of notificationService.pages\" [class.active]=\"page.isActive\" [class.selected]=\"page.isCurrentPage\" (click)=\"notificationService.getNotifications(page.symbol, page.isActive, page.isCurrentPage)\">\r\n            <div>{{page.symbol}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"notificationService.notifications.length === 0\" class=\"empty-state-wrapper\">\r\n    <div class=\"empty-state-block\">\r\n      <img width=\"342\" height=\"319\" src=\"../../../assets/img/notifications/empty-state-illustration.png\">\r\n      <h2>Still not a single notification!</h2>\r\n      <p>Start doing any activities like voting, following people or dueling</p>\r\n      <a class=\"button primary-button\" routerLink=\"/start\">\r\n        <span>Start a Duel</span>\r\n      </a>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/notifications/notifications.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_notification_notification_service__ = __webpack_require__("./src/app/services/notification/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_date_formatter_date_formatter_service__ = __webpack_require__("./src/app/services/date-formatter/date-formatter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(notificationService, dateFormatter, router, titleService, authService, duelService) {
        this.notificationService = notificationService;
        this.dateFormatter = dateFormatter;
        this.router = router;
        this.titleService = titleService;
        this.authService = authService;
        this.duelService = duelService;
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        this.notificationService.getNotifications(1);
        this.titleService.setTitle("Notifications / " + this.authService.getCurrentUser().fullname + " - Duelity");
    };
    NotificationsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-notifications',
            template: __webpack_require__("./src/app/components/notifications/notifications.component.html"),
            styles: [__webpack_require__("./src/app/components/notifications/notifications.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_notification_notification_service__["a" /* NotificationService */],
            __WEBPACK_IMPORTED_MODULE_3__services_date_formatter_date_formatter_service__["a" /* DateFormatterService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_6__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__services_duel_service_duel_service__["a" /* DuelService */]])
    ], NotificationsComponent);
    return NotificationsComponent;
}());



/***/ }),

/***/ "./src/app/components/password-restore/password-restore.component.css":
/***/ (function(module, exports) {

module.exports = ".form {\r\n    max-width: 488px;\r\n    margin: 0 auto;\r\n    margin-top: 100px;\r\n}\r\n\r\n.form h1 {\r\n    margin-bottom: 8px;\r\n}\r\n\r\n.form > p {\r\n    margin-bottom: 25px;\r\n}\r\n\r\n.form .button {\r\n    margin-top: 10px;\r\n    margin-bottom: 100px;\r\n}\r\n\r\n.form .input-wrapper {\r\n    margin-bottom: 10px;\r\n}"

/***/ }),

/***/ "./src/app/components/password-restore/password-restore.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form\" *ngIf=\"isCodeValid !== null && isCodeValid === false\">\r\n  <h1>Restore your Password</h1>\r\n  <p>Enter email associated with your account below and we will send you instructions to restore access. Or you can go back\r\n    to\r\n    <a (click)=\"loginService.openModal()\">Sign In</a>\r\n  </p>\r\n  <div class=\"input-wrapper\">\r\n    <app-input (onValueChange)=\"onEmailChange($event)\" [value]=\"email\" [placeholder]=\"'Email'\" [type]=\"inputType.Text\" [error]=\"emailErrorMessage\"></app-input>\r\n  </div>\r\n  <a class=\"button primary-button\" (click)=\"sendRestoreLink()\">\r\n    <span>Send Instructions</span>\r\n  </a>\r\n</div>\r\n<div class=\"form\" *ngIf=\"isCodeValid !== null && isCodeValid === true\">\r\n  <h1>Hey, nice to see you again!</h1>\r\n  <p>Create a new password for you account. Don’t forget it this time!</p>\r\n  <div class=\"input-wrapper\">\r\n    <app-input (onValueChange)=\"onPasswordChange($event)\" [value]=\"password\" [placeholder]=\"'New Password'\" [type]=\"inputType.Password\"\r\n      [error]=\"passwordErrorMessage\"></app-input>\r\n  </div>\r\n  <div class=\"input-wrapper\">\r\n    <app-input (onValueChange)=\"onSecondPasswordChange($event)\" [value]=\"secondPassword\" [placeholder]=\"'Repeat New Password'\" [type]=\"inputType.Password\"\r\n      [error]=\"secondPasswordErrorMessage\"></app-input>\r\n  </div>\r\n  <a class=\"button primary-button\" (click)=\"savePassword()\">\r\n    <span>Save Password</span>\r\n  </a>\r\n</div>"

/***/ }),

/***/ "./src/app/components/password-restore/password-restore.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordRestoreComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_password_restore_service__ = __webpack_require__("./src/app/components/password-restore/services/password-restore.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_login_login_service__ = __webpack_require__("./src/app/services/login/login.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__enums_input_type__ = __webpack_require__("./src/app/enums/input-type.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var PasswordRestoreComponent = /** @class */ (function () {
    function PasswordRestoreComponent(loginService, validationService, passwordRestoreService, authService, socketService, alertService, activatedRoute, router) {
        this.loginService = loginService;
        this.validationService = validationService;
        this.passwordRestoreService = passwordRestoreService;
        this.authService = authService;
        this.socketService = socketService;
        this.alertService = alertService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.email = "";
        this.password = "";
        this.secondPassword = "";
        this.restoreEmail = "";
        this.emailErrorMessage = "";
        this.passwordErrorMessage = "";
        this.secondPasswordErrorMessage = "";
        this.code = "";
        this.isCodeValid = null;
        this.inputType = __WEBPACK_IMPORTED_MODULE_8__enums_input_type__["a" /* InputType */];
    }
    PasswordRestoreComponent.prototype.ngOnInit = function () {
        this.initComponent();
    };
    PasswordRestoreComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    PasswordRestoreComponent.prototype.onEmailChange = function (email) {
        this.email = email;
    };
    PasswordRestoreComponent.prototype.onPasswordChange = function (password) {
        this.password = password;
    };
    PasswordRestoreComponent.prototype.onSecondPasswordChange = function (secondPassword) {
        this.secondPassword = secondPassword;
    };
    PasswordRestoreComponent.prototype.sendRestoreLink = function () {
        var _this = this;
        this.emailErrorMessage = "";
        if (!this.validationService.isStringValid(this.email)) {
            this.emailErrorMessage = "This field can't be empty";
            return;
        }
        if (!this.validationService.isEmailValid(this.email)) {
            this.emailErrorMessage = "This is not an email";
            return;
        }
        this.passwordRestoreService.sendRestoreLink(this.email).subscribe(function (response) {
            if (response.success) {
                _this.alertService.success("Instructions have been sent, please check your email");
                _this.router.navigate(["/login"]);
            }
            else {
                _this.alertService.error("Oops, we encountered a problem. Please try again");
                console.log(response.error);
            }
        });
    };
    PasswordRestoreComponent.prototype.savePassword = function () {
        var _this = this;
        var success = true;
        this.passwordErrorMessage = "";
        this.secondPasswordErrorMessage = "";
        if (!this.validationService.isStringValid(this.password)) {
            this.passwordErrorMessage = "This field can't be empty";
            success = false;
        }
        if (!this.validationService.isStringValid(this.secondPassword)) {
            this.secondPasswordErrorMessage = "This field can't be empty";
            success = false;
        }
        if (this.password !== this.secondPassword) {
            this.secondPasswordErrorMessage = "Passwords do not match";
            success = false;
        }
        if (!success)
            return;
        this.passwordRestoreService.saveNewPassword(this.restoreEmail, this.password, this.code).subscribe(function (response) {
            if (response.success) {
                _this.authService.storeUserData(response.token, response.user);
                _this.socketService.addOnlineUser();
                _this.router.navigate(["/following"]);
                _this.alertService.success("Password successfully restored. Welcome back!");
            }
            else {
                _this.alertService.error("There was a problem setting your new password. Please try again");
                console.log(response.error);
            }
        });
    };
    PasswordRestoreComponent.prototype.initComponent = function () {
        var _this = this;
        this.subscription = this.activatedRoute.queryParams.subscribe(function (params) {
            var code = params['code'];
            _this.code = code;
            if (_this.validationService.isObjectValid(code)) {
                _this.passwordRestoreService.validateRestoreCode(code).subscribe(function (response) {
                    if (response.success) {
                        _this.restoreEmail = response.email;
                        _this.isCodeValid = true;
                    }
                    else {
                        _this.isCodeValid = false;
                        _this.router.navigate(["/following"]);
                        console.log(response.error);
                    }
                });
            }
            else {
                _this.isCodeValid = false;
            }
        });
    };
    PasswordRestoreComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-password-restore',
            template: __webpack_require__("./src/app/components/password-restore/password-restore.component.html"),
            styles: [__webpack_require__("./src/app/components/password-restore/password-restore.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__services_login_login_service__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_2__services_validation_validation_service__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_3__services_password_restore_service__["a" /* PasswordRestoreService */],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__services_socket_socket_service__["a" /* SocketService */],
            __WEBPACK_IMPORTED_MODULE_6__services_alert_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], PasswordRestoreComponent);
    return PasswordRestoreComponent;
}());



/***/ }),

/***/ "./src/app/components/password-restore/services/password-restore.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordRestoreService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PasswordRestoreService = /** @class */ (function () {
    function PasswordRestoreService(tokenProviderService, http, headersProviderService) {
        this.tokenProviderService = tokenProviderService;
        this.http = http;
        this.headersProviderService = headersProviderService;
    }
    PasswordRestoreService.prototype.sendRestoreLink = function (email) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/api/restore-password", {
            params: {
                email: email
            }
        })
            .map(function (res) { return res.json(); });
    };
    PasswordRestoreService.prototype.validateRestoreCode = function (code) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/api/validate-restore-code", {
            params: {
                code: code
            }
        })
            .map(function (res) { return res.json(); });
    };
    PasswordRestoreService.prototype.saveNewPassword = function (email, password, code) {
        var data = {
            email: email,
            password: password,
            code: code
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/api/save-new-password", data)
            .map(function (res) { return res.json(); });
    };
    PasswordRestoreService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */]])
    ], PasswordRestoreService);
    return PasswordRestoreService;
}());



/***/ }),

/***/ "./src/app/components/popular-duels/popular-duels.component.css":
/***/ (function(module, exports) {

module.exports = ".popular-duels {\r\n    margin-bottom: 100px;\r\n    margin-top: 100px;\r\n}\r\n\r\n.ad {\r\n    height: 400px;\r\n    background-color: rgba(255, 226, 38, 0.2);\r\n    width: 100vw;\r\n    position: relative;\r\n    left: 50%;\r\n    right: 50%;\r\n    margin-left: -50vw;\r\n    margin-right: -50vw;\r\n    margin-top: 40px;\r\n    margin-bottom: 65px;\r\n}\r\n\r\n.ad h1 {\r\n    width: 600px;\r\n    margin-top: 24px;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.ad p {\r\n    width: 360px;\r\n    margin-bottom: 70px;\r\n}\r\n\r\n#ad-content {\r\n    width: 1200px;\r\n    margin: 0 auto;\r\n    padding-top: 40px;\r\n}\r\n\r\nh2.main-heading {\r\n    margin-bottom: 25px;\r\n}\r\n\r\n.avatars-block {\r\n    position: relative;\r\n    right: 10px;\r\n}\r\n\r\n.avatars-label {\r\n    position: relative;\r\n    /* right: -16px; */\r\n}\r\n\r\n.avatar {\r\n    position: relative;\r\n    cursor: pointer;\r\n}\r\n\r\nbutton.additional-users {\r\n    width: 38px;\r\n    height: 38px;\r\n    border-radius: 50%;\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 12px;\r\n    line-height: 38px;\r\n    text-align: center;\r\n    background-color: #D8E2E9;\r\n    color: #5D5D5D;\r\n    position: relative;\r\n    top: -1px;\r\n}\r\n\r\n.profile-avatar {\r\n    position: relative;\r\n    top: 0px;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.profile-avatar:hover {\r\n    top: -8px;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}"

/***/ }),

/***/ "./src/app/components/popular-duels/popular-duels.component.html":
/***/ (function(module, exports) {

module.exports = "<app-back-to-top></app-back-to-top>\r\n<div class=\"popular-duels\">\r\n    <div *ngIf=\"authService.getCurrentUser() === null\" class=\"ad\">\r\n        <div id=\"ad-content\">\r\n            <div class=\"left\">\r\n                <h1>Try yourself in one on one design duels with equal opponents</h1>\r\n                <p>Create an account to start and accept duels from more than {{getRoundedUserAmount()}} designers</p>\r\n                <a class=\"button primary-button\" (click)=\"registerService.goToRegister()\">\r\n                    <span>Create Account</span>\r\n                </a>\r\n            </div>\r\n            <div class=\"right\">\r\n                <img width=\"409\" height=\"321\" src=\"../../../assets/img/popular/ad-illustration.png\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"duels !== undefined && duels.length > 0\">\r\n        <app-user-list></app-user-list>\r\n        <div>\r\n            <h2 class=\"left main-heading\">Popular Duels</h2>\r\n            <div *ngIf=\"authService.getCurrentUser() !== null\" class=\"right\" [class.avatars-block]=\"totalUserCount < previewUserAmount\">\r\n                <p class=\"inline avatars-label\">Most skilled designers</p>\r\n                <div class=\"inline\">\r\n                    <div *ngFor=\"let user of previewUsers\" class=\"avatar inline custom-tooltip\">\r\n                        <span class=\"tooltiptext\">{{user.fullname}}</span>\r\n                        <app-profile-picture (click)=\"goToPerson(user.username)\" elementClass=\"x-small\" fullname={{user.fullname}} userId={{user.id}}\r\n                            backgroundImage={{user.imageUrl}} backgroundColor={{user.imageColor}} hasBorder=\"true\" class=\"profile-avatar\">\r\n                        </app-profile-picture>\r\n                    </div>\r\n                </div>\r\n                <button class=\"inline additional-users\" (click)=\"openUserList()\" *ngIf=\"totalUserCount > previewUserAmount\">+{{totalUserCount - previewUserAmount}}</button>\r\n            </div>\r\n            <div class=\"clearfix\"></div>\r\n        </div>\r\n        <div class=\"clearfix\"></div>\r\n        <div class=\"duels-container\">\r\n            <div *ngFor=\"let duel of duels\" (click)=\"openVotePage(duel)\" class=\"duel-wrapper\">\r\n                <app-duel [duel]=\"duel\" [duelOwner]=\"currentUserId\" (notify)=\"onNotify($event)\"></app-duel>\r\n            </div>\r\n        </div>\r\n        <div *ngIf=\"duels.length < totalDuelsAmount\" class=\"duels-end-block-button\">\r\n            <a class=\"button secondary-button\" (click)=\"loadNextPage()\">\r\n                <span>Load More</span>\r\n            </a>\r\n        </div>\r\n        <div *ngIf=\"duels.length >= totalDuelsAmount\" class=\"duels-end-block-message\">\r\n            <h2>Wow, you’ve reached the end of the list</h2>\r\n            <p>Good job exploring popular duels. Try looking through featured ones now!</p>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"duels === undefined || duels.length === 0\">\r\n        <h1>No duels.</h1>\r\n    </div>\r\n</div>\r\n<div class=\"clearfix\"></div>"

/***/ }),

/***/ "./src/app/components/popular-duels/popular-duels.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopularDuelsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_vote_vote_service__ = __webpack_require__("./src/app/services/vote/vote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__enums_duel_type__ = __webpack_require__("./src/app/enums/duel-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__child_components_user_list_services_user_list_service__ = __webpack_require__("./src/app/child-components/user-list/services/user-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__enums_user_list_order_criteria__ = __webpack_require__("./src/app/enums/user-list-order-criteria.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__register_services_register_service__ = __webpack_require__("./src/app/components/register/services/register.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_tooltip_tooltip_service__ = __webpack_require__("./src/app/services/tooltip/tooltip.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var PopularDuelsComponent = /** @class */ (function () {
    function PopularDuelsComponent(duelService, voteService, userListService, router, titleService, authService, registerService, socketService, tooltipService) {
        this.duelService = duelService;
        this.voteService = voteService;
        this.userListService = userListService;
        this.router = router;
        this.titleService = titleService;
        this.authService = authService;
        this.registerService = registerService;
        this.socketService = socketService;
        this.tooltipService = tooltipService;
        this.previewUserAmount = 5;
        this.totalUserCount = 0;
        this.duelsPage = 1;
        this.totalDuelsAmount = 0;
        this.duels = [];
        this.previewUsers = [];
    }
    PopularDuelsComponent.prototype.ngOnInit = function () {
        this.duels = [];
        this.previewUsers = [];
        this.duelsPage = 1;
        this.totalDuelsAmount = 0;
        this.totalUserCount = 0;
        this.initializeData();
        this.initializeAd();
        this.titleService.setTitle("Popular Duels - Duelity");
    };
    PopularDuelsComponent.prototype.onNotify = function () {
        this.initializeData();
    };
    PopularDuelsComponent.prototype.openVotePage = function (duel) {
        this.voteService.openVotePage(duel);
    };
    PopularDuelsComponent.prototype.goToPerson = function (username) {
        this.router.navigate(["/user", username]);
    };
    PopularDuelsComponent.prototype.openUserList = function () {
        this.userListService.getUsersAndOpenModal("Popular", [], __WEBPACK_IMPORTED_MODULE_5__enums_user_list_order_criteria__["a" /* UserListOrderCriteria */].SkillPoints);
    };
    PopularDuelsComponent.prototype.getRoundedUserAmount = function () {
        return Math.floor(this.socketService.usersAmount / 10) * 10;
    };
    PopularDuelsComponent.prototype.loadNextPage = function () {
        this.duelsPage++;
        this.loadDuels();
    };
    PopularDuelsComponent.prototype.initializeData = function () {
        var _this = this;
        this.duelService.getDuelByType(__WEBPACK_IMPORTED_MODULE_3__enums_duel_type__["a" /* DuelType */].Popular, this.duelsPage).subscribe(function (response) {
            if (response.success) {
                (_a = _this.duels).push.apply(_a, response.duels);
                _this.totalDuelsAmount = response.totalAmount;
                if (_this.duels.length > 0) {
                    _this.userListService.getUserList([], __WEBPACK_IMPORTED_MODULE_5__enums_user_list_order_criteria__["a" /* UserListOrderCriteria */].SkillPoints, 1, _this.previewUserAmount).subscribe(function (response) {
                        if (response.success) {
                            _this.previewUsers = response.data.users;
                            _this.totalUserCount = response.data.totalCount;
                            _this.initializePreviewUsers();
                            setTimeout(function () { _this.tooltipService.initializeTooltip('.custom-tooltip'); }, 0);
                        }
                        else {
                            console.log(response.error);
                        }
                    });
                }
            }
            else {
                console.log("Something goes wrong during fetching popular duels.");
            }
            var _a;
        });
    };
    PopularDuelsComponent.prototype.loadDuels = function () {
        var _this = this;
        this.duelService.getDuelByType(__WEBPACK_IMPORTED_MODULE_3__enums_duel_type__["a" /* DuelType */].Popular, this.duelsPage).subscribe(function (response) {
            if (response.success) {
                (_a = _this.duels).push.apply(_a, response.duels);
                _this.totalDuelsAmount = response.totalAmount;
            }
            else {
                console.log("Something goes wrong during fetching popular duels.");
            }
            var _a;
        });
    };
    PopularDuelsComponent.prototype.initializeAd = function () {
        var _this = this;
        if (this.authService.getCurrentUser() === null) {
            this.registerService.getUsersAmount().subscribe(function (response) {
                if (response.success) {
                    _this.socketService.usersAmount = response.data;
                }
            });
        }
    };
    PopularDuelsComponent.prototype.initializePreviewUsers = function () {
        var _this = this;
        setTimeout(function () {
            var avatars = $(".avatar");
            var rightPosition = _this.totalUserCount > _this.previewUserAmount ? 10 : 0;
            var zIndex = 6;
            if (avatars.length > 0) {
                for (var i = avatars.length - 1; i >= 0; i--) {
                    var avatar = $(avatars[i]);
                    avatar.css("right", "-" + rightPosition + "px");
                    avatar.css("z-index", zIndex);
                    rightPosition += 5;
                    zIndex += 1;
                }
                $(".avatars-label").css("right", 16 - (rightPosition - 5) + "px");
            }
        }, 0);
    };
    PopularDuelsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-popular-duels',
            template: __webpack_require__("./src/app/components/popular-duels/popular-duels.component.html"),
            styles: [__webpack_require__("./src/app/components/popular-duels/popular-duels.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_duel_service_duel_service__["a" /* DuelService */],
            __WEBPACK_IMPORTED_MODULE_2__services_vote_vote_service__["a" /* VoteService */],
            __WEBPACK_IMPORTED_MODULE_4__child_components_user_list_services_user_list_service__["a" /* UserListService */],
            __WEBPACK_IMPORTED_MODULE_6__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_8__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_9__register_services_register_service__["a" /* RegisterService */],
            __WEBPACK_IMPORTED_MODULE_10__services_socket_socket_service__["a" /* SocketService */],
            __WEBPACK_IMPORTED_MODULE_11__services_tooltip_tooltip_service__["a" /* TooltipService */]])
    ], PopularDuelsComponent);
    return PopularDuelsComponent;
}());



/***/ }),

/***/ "./src/app/components/pro-account/pro-account.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/pro-account/pro-account.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"currentUser\">\r\n  <div *ngIf=\"currentUser.isPaidAccount\">\r\n    <h2>Your subscription is active, enjoy!</h2>\r\n    <p>Thank you for the support, we greatly appreciate that.</p>\r\n    <h3>December 23, 2019</h3>\r\n    <p>Renews at</p>\r\n  </div>\r\n  <div *ngIf=\"!currentUser.isPaidAccount\">\r\n    <h2>Unleash amazing features and show your support</h2>\r\n    <p>Duelity Silver is a great way to unleash new features & break all limitations. You will love it</p>\r\n    <h3>$2/mo</h3>\r\n    <p>Billed Annually</p>\r\n    <button>Purchase</button>\r\n  </div>\r\n  <h2 *ngIf=\"currentUser.isPaidAccount\">Thanks and enjoy your new features</h2>\r\n  <div>\r\n    <h1>Big block of features</h1>\r\n  </div>\r\n  <div *ngIf=\"!currentUser.isPaidAccount\">\r\n    <h3>And all of that for the price of a pizza slice!</h3>\r\n    <p>Cancel anytime, but you won’t</p>\r\n    <div>\r\n      <h3>Viola Kharysh</h3>\r\n      <p>\r\n        I love Duelity Silver because it let’s me grow as a digital designer so much. \r\n        Nothing encourages more than the feel of competition. \r\n        It’s also great so see how the same task can be solved in so different ways.\r\n      </p>\r\n      <h3>$2/mo</h3>\r\n      <p>Billed Annually</p>\r\n      <button>Purchase</button>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/pro-account/pro-account.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProAccountComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProAccountComponent = /** @class */ (function () {
    function ProAccountComponent(authService) {
        this.authService = authService;
        this.currentUser = null;
    }
    ProAccountComponent.prototype.ngOnInit = function () {
        this.currentUser = this.authService.getCurrentUser();
    };
    ProAccountComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-pro-account',
            template: __webpack_require__("./src/app/components/pro-account/pro-account.component.html"),
            styles: [__webpack_require__("./src/app/components/pro-account/pro-account.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_auth_service__["a" /* AuthService */]])
    ], ProAccountComponent);
    return ProAccountComponent;
}());



/***/ }),

/***/ "./src/app/components/register/register.component.css":
/***/ (function(module, exports) {

module.exports = ".register {\r\n    padding-top: 70px;\r\n    padding-bottom: 37px;\r\n}\r\n\r\n.register h1 {\r\n    margin-bottom: 15px;\r\n}\r\n\r\n.register .form {\r\n    max-width: 420px;\r\n    margin-top: 35px;\r\n    display: inline-block;\r\n}\r\n\r\n.register .form .input-wrapper {\r\n    margin-bottom: 14px;\r\n}\r\n\r\n.register .form .button {\r\n    width: 100%;\r\n    margin-top: 20px;\r\n}\r\n\r\n.register .form .sign-in-with-label {\r\n    margin-top: 115px;\r\n    padding-bottom: 12px;\r\n    border-bottom: 1px solid #D8E2E9;\r\n    margin-bottom: 20px;\r\n}\r\n\r\n#google-button, #twitter-button {\r\n    width: 202px;\r\n    height: 50px;\r\n    font-family: Rubik;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    color: #FFFFFF;\r\n    text-align: left;\r\n}\r\n\r\n#google-button {\r\n    padding-left: 50px;\r\n    background: #4285F4 url(\"data:image/svg+xml,%3Csvg width%3D%2226%22 height%3D%2226%22 viewBox%3D%220 0 26 26%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Ccircle cx%3D%2213%22 cy%3D%2213%22 r%3D%2213%22 fill%3D%22white%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M21.64 13.2046C21.64 12.5664 21.5827 11.9527 21.4764 11.3636H13V14.845H17.8436C17.635 15.97 17.0009 16.9232 16.0477 17.5614V19.8196H18.9564C20.6582 18.2527 21.64 15.9455 21.64 13.2046Z%22 fill%3D%22%234285F4%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13 22C15.43 22 17.4673 21.1941 18.9564 19.8196L16.0477 17.5614C15.2418 18.1014 14.2109 18.4205 13 18.4205C10.6559 18.4205 8.67182 16.8373 7.96409 14.71H4.95728V17.0418C6.43818 19.9832 9.48182 22 13 22Z%22 fill%3D%22%2334A853%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M7.96409 14.71C7.78409 14.17 7.68182 13.5932 7.68182 13C7.68182 12.4068 7.78409 11.83 7.96409 11.29V8.95819H4.95727C4.34773 10.1732 4 11.5477 4 13C4 14.4523 4.34773 15.8268 4.95727 17.0418L7.96409 14.71Z%22 fill%3D%22%23FBBC05%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13 7.57955C14.3214 7.57955 15.5077 8.03364 16.4405 8.92545L19.0218 6.34409C17.4632 4.89182 15.4259 4 13 4C9.48182 4 6.43818 6.01682 4.95728 8.95818L7.96409 11.29C8.67182 9.16273 10.6559 7.57955 13 7.57955Z%22 fill%3D%22%23EA4335%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 13px 50% no-repeat;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#google-button:hover {\r\n    background: #3f7bdf url(\"data:image/svg+xml,%3Csvg width%3D%2226%22 height%3D%2226%22 viewBox%3D%220 0 26 26%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Ccircle cx%3D%2213%22 cy%3D%2213%22 r%3D%2213%22 fill%3D%22white%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M21.64 13.2046C21.64 12.5664 21.5827 11.9527 21.4764 11.3636H13V14.845H17.8436C17.635 15.97 17.0009 16.9232 16.0477 17.5614V19.8196H18.9564C20.6582 18.2527 21.64 15.9455 21.64 13.2046Z%22 fill%3D%22%234285F4%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13 22C15.43 22 17.4673 21.1941 18.9564 19.8196L16.0477 17.5614C15.2418 18.1014 14.2109 18.4205 13 18.4205C10.6559 18.4205 8.67182 16.8373 7.96409 14.71H4.95728V17.0418C6.43818 19.9832 9.48182 22 13 22Z%22 fill%3D%22%2334A853%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M7.96409 14.71C7.78409 14.17 7.68182 13.5932 7.68182 13C7.68182 12.4068 7.78409 11.83 7.96409 11.29V8.95819H4.95727C4.34773 10.1732 4 11.5477 4 13C4 14.4523 4.34773 15.8268 4.95727 17.0418L7.96409 14.71Z%22 fill%3D%22%23FBBC05%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13 7.57955C14.3214 7.57955 15.5077 8.03364 16.4405 8.92545L19.0218 6.34409C17.4632 4.89182 15.4259 4 13 4C9.48182 4 6.43818 6.01682 4.95728 8.95818L7.96409 11.29C8.67182 9.16273 10.6559 7.57955 13 7.57955Z%22 fill%3D%22%23EA4335%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 13px 50% no-repeat;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#twitter-button {\r\n    padding-left: 46px;\r\n    background: #1DA1F2 url(\"data:image/svg+xml,%3Csvg width%3D%2218%22 height%3D%2214%22 viewBox%3D%220 0 18 14%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M18 1.65642C17.3374 1.93751 16.6262 2.12814 15.8798 2.21325C16.6424 1.77597 17.2266 1.08239 17.5024 0.258473C16.7871 0.663417 15.9974 0.957459 15.1564 1.11686C14.483 0.428656 13.5243 0 12.4615 0C10.4225 0 8.76878 1.5832 8.76878 3.53473C8.76878 3.81151 8.80123 4.08184 8.86478 4.34034C5.79537 4.19277 3.07482 2.78514 1.25346 0.646184C0.935697 1.16747 0.754507 1.77487 0.754507 2.42325C0.754507 3.64997 1.4076 4.73236 2.39739 5.36562C1.79162 5.34624 1.22236 5.18684 0.72476 4.92189V4.96605C0.72476 6.67849 1.99715 8.10766 3.686 8.43294C3.37635 8.51264 3.05048 8.55679 2.71244 8.55679C2.47446 8.55679 2.24459 8.53418 2.01743 8.49109C2.48798 9.89659 3.85096 10.9187 5.4668 10.9467C4.20388 11.8944 2.61103 12.4577 0.880259 12.4577C0.582783 12.4577 0.289363 12.4405 0 12.4093C1.63477 13.4141 3.57512 14 5.66016 14C12.4521 14 16.1651 8.61281 16.1651 3.94077L16.1529 3.48305C16.8791 2.98759 17.5065 2.3651 18 1.65642Z%22 fill%3D%22white%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 16px 50% no-repeat;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#twitter-button:hover {\r\n    background: #1e95de url(\"data:image/svg+xml,%3Csvg width%3D%2218%22 height%3D%2214%22 viewBox%3D%220 0 18 14%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M18 1.65642C17.3374 1.93751 16.6262 2.12814 15.8798 2.21325C16.6424 1.77597 17.2266 1.08239 17.5024 0.258473C16.7871 0.663417 15.9974 0.957459 15.1564 1.11686C14.483 0.428656 13.5243 0 12.4615 0C10.4225 0 8.76878 1.5832 8.76878 3.53473C8.76878 3.81151 8.80123 4.08184 8.86478 4.34034C5.79537 4.19277 3.07482 2.78514 1.25346 0.646184C0.935697 1.16747 0.754507 1.77487 0.754507 2.42325C0.754507 3.64997 1.4076 4.73236 2.39739 5.36562C1.79162 5.34624 1.22236 5.18684 0.72476 4.92189V4.96605C0.72476 6.67849 1.99715 8.10766 3.686 8.43294C3.37635 8.51264 3.05048 8.55679 2.71244 8.55679C2.47446 8.55679 2.24459 8.53418 2.01743 8.49109C2.48798 9.89659 3.85096 10.9187 5.4668 10.9467C4.20388 11.8944 2.61103 12.4577 0.880259 12.4577C0.582783 12.4577 0.289363 12.4405 0 12.4093C1.63477 13.4141 3.57512 14 5.66016 14C12.4521 14 16.1651 8.61281 16.1651 3.94077L16.1529 3.48305C16.8791 2.98759 17.5065 2.3651 18 1.65642Z%22 fill%3D%22white%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 16px 50% no-repeat;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.information-block {\r\n    display: inline-block;\r\n    vertical-align: top;\r\n    margin-top: -120px;\r\n    margin-left: 165px;\r\n    max-width: 440px;\r\n}\r\n\r\n.information-block img {\r\n    margin-bottom: 46px;\r\n}\r\n\r\np.error {\r\n    opacity: 0;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n    padding-left: 30px;\r\n    height: 20px;\r\n}\r\n\r\np.error.visible {\r\n    opacity: 1;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n}\r\n\r\np.numeric {\r\n    margin-top: 40px;\r\n    margin-bottom: 3px;\r\n}\r\n\r\n#categories {\r\n    max-width: 605px;\r\n    margin:0 auto;\r\n    margin-top: 100px;\r\n}\r\n\r\n#categories p.main-paragraph {\r\n    max-width: 520px;\r\n    margin-top: 15px;\r\n    margin-bottom: 40px;\r\n}\r\n\r\n#categories h3 {\r\n    margin-top: 25px;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n#categories .category-block > p { \r\n    line-height: 29px;\r\n}\r\n\r\n.category-block {\r\n    height: 30px;\r\n    border: 1px solid #D8E2E9;\r\n    -webkit-box-sizing: border-box;\r\n            box-sizing: border-box;\r\n    border-radius: 100px;\r\n    display: inline-block;\r\n    cursor: pointer;\r\n    padding-left: 12px;\r\n    padding-right: 12px;\r\n    text-align: center;\r\n    color: #5D5D5D !important;\r\n    margin-right: 16px;\r\n    margin-bottom: 16px;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.category-block:hover {\r\n    border: 1px solid rgba(245, 80, 69, 0.4);\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.category-block.selected {\r\n    background: #feedec;\r\n    border: 1px solid #feedec;\r\n}\r\n\r\n#categories .category-block > p {\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#categories .category-block:hover > p,\r\n#categories .category-block.selected > p { \r\n    color: #F55045 !important;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#categories .button {\r\n    margin-bottom: 100px;\r\n    margin-top: -10px;\r\n}"

/***/ }),

/***/ "./src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!registerService.chooseCategoryFlag\" class=\"register\">\r\n  <h1>Joining Duelity</h1>\r\n  <p>Already a member?\r\n    <a (click)=\"loginService.openModal()\">Sign In</a>\r\n  </p>\r\n  <div class=\"form\">\r\n    <div class=\"input-wrapper\">\r\n      <app-input (onValueChange)=\"onFullNameChange($event)\" [value]=\"name\" [placeholder]=\"'Full Name'\" [type]=\"inputType.Text\"\r\n        [error]=\"nameErrorMessage\"></app-input>\r\n    </div>\r\n    <div class=\"input-wrapper\">\r\n      <app-input (onValueChange)=\"onEmailChange($event)\" [value]=\"email\" [placeholder]=\"'Email'\" [type]=\"inputType.Text\" [error]=\"emailErrorMessage\"></app-input>\r\n    </div>\r\n    <div class=\"input-wrapper\">\r\n      <app-input (onValueChange)=\"onPasswordChange($event)\" [value]=\"password\" [placeholder]=\"'Password'\" [type]=\"inputType.Password\"\r\n        [error]=\"passwordErrorMessage\"></app-input>\r\n    </div>\r\n    <div>\r\n      <input class=\"styled-checkbox\" id=\"terms-checkbox\" type=\"checkbox\" [(ngModel)]=\"isTermsAccepted\">\r\n      <label for=\"terms-checkbox\" [class.error]=\"showTermsError === true\">\r\n        <span>I have read and agree to the\r\n          <a routerLink=\"/terms\">Terms of Use</a> and to the processing of the personal data accordingly</span>\r\n      </label>\r\n    </div>\r\n    <a class=\"button primary-button\" (click)=\"goToCategories()\">\r\n      <span>Continue</span>\r\n    </a>\r\n    <p class=\"sign-in-with-label\">Or sign in with</p>\r\n    <button id=\"google-button\" #googleButton class=\"inline left\">Sign up with Google</button>\r\n    <button id=\"twitter-button\" class=\"inline right\" (click)=\"twitterService.onTwitterSignIn()\">Sign up with Twitter</button>\r\n    <div class=\"clearfix\"></div>\r\n  </div>\r\n  <div class=\"information-block\">\r\n    <img width=\"404\" height=\"327\" src=\"../../../assets/img/signing/register-illustration.png\">\r\n    <p>Join a community of digital designers who like to compete in creating solutions for interesting problems in strict deadlines.</p>\r\n    <p>It’s a way of communicating and collaborating with designers around the world with any skill or category. With Duelity\r\n      your skill goes up faster, you learn new things in the process, watch the same task solved in completely different\r\n      ways and get useful tips from votes.</p>\r\n    <p *ngIf=\"usersAmount !== null\" class=\"numeric\">{{socketService.usersAmount}}</p>\r\n    <p *ngIf=\"usersAmount !== null\">designers stepping their game up</p>\r\n  </div>\r\n  <div class=\"clearfix\"></div>\r\n</div>\r\n<div *ngIf=\"registerService.chooseCategoryFlag\" id=\"categories\">\r\n  <h1>Mind telling us what you’re good at?</h1>\r\n  <p class=\"main-paragraph\">You will only be able to duel with people who have a category in common with you. One duel — one category, choose as much\r\n    as you feel comfortable dealing with</p>\r\n  <div>\r\n    <div *ngFor=\"let categoryGroup of categories\">\r\n      <h3>{{categoryGroup[0].group}}</h3>\r\n      <div *ngFor=\"let category of categoryGroup\" class=\"category-block\" [class.selected]=\"category.selected\" (click)=\"categoryService.pickCategory(category)\">\r\n        <p>{{category.name}}</p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br>\r\n  <br>\r\n  <a class=\"button primary-button\" (click)=\"onRegisterSubmit()\">\r\n    <span>Create Account</span>\r\n  </a>\r\n</div>"

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_category_category_service__ = __webpack_require__("./src/app/services/category/category.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_register_service__ = __webpack_require__("./src/app/components/register/services/register.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_login_login_service__ = __webpack_require__("./src/app/services/login/login.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_twitter_twitter_service__ = __webpack_require__("./src/app/services/twitter/twitter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_google_google_service__ = __webpack_require__("./src/app/services/google/google.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__enums_input_type__ = __webpack_require__("./src/app/enums/input-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(authService, validationService, socketService, router, activatedRoute, titleService, googleService, loginService, twitterService, registerService, categoryService, alertService) {
        this.authService = authService;
        this.validationService = validationService;
        this.socketService = socketService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.titleService = titleService;
        this.googleService = googleService;
        this.loginService = loginService;
        this.twitterService = twitterService;
        this.registerService = registerService;
        this.categoryService = categoryService;
        this.alertService = alertService;
        this.name = "";
        this.email = "";
        this.password = "";
        this.isTermsAccepted = false;
        this.nameErrorMessage = "";
        this.emailErrorMessage = "";
        this.passwordErrorMessage = "";
        this.showTermsError = false;
        this.categories = [];
        this.inputType = __WEBPACK_IMPORTED_MODULE_11__enums_input_type__["a" /* InputType */];
    }
    RegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initializeCategories();
        this.initializeData();
        this.titleService.setTitle("Register at Duelity");
        setTimeout(function () {
            _this.googleService.initGoogleAuth(_this.googleButton.nativeElement);
        }, 0);
    };
    RegisterComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var user = {
            name: this.registerService.isViaAuthService ? this.registerService.userFullname : this.name,
            email: this.registerService.isViaAuthService ? this.registerService.userEmail : this.email,
            password: this.password,
            categories: this.categoryService.collectCategories(this.categories)
        };
        if (user.categories.length === 0) {
            this.alertService.error("Pick at least one category to proceed");
            return;
        }
        this.authService.registerUser(user).subscribe(function (data) {
            if (data.success) {
                _this.authService.storeUserData(data.token, data.user);
                _this.socketService.addOnlineUser();
                _this.router.navigate(["/following"]);
            }
            else {
                alert(data.message);
            }
        });
    };
    RegisterComponent.prototype.onFullNameChange = function (fullname) {
        this.name = fullname;
    };
    RegisterComponent.prototype.onEmailChange = function (email) {
        this.email = email;
    };
    RegisterComponent.prototype.onPasswordChange = function (password) {
        this.password = password;
    };
    RegisterComponent.prototype.goToCategories = function () {
        var _this = this;
        var success = true;
        this.nameErrorMessage = "";
        this.emailErrorMessage = "";
        this.passwordErrorMessage = "";
        var user = {
            name: this.name,
            email: this.email,
            password: this.password
        };
        if (!this.validationService.isStringValid(user.name)) {
            this.nameErrorMessage = "This field can't be empty";
            success = false;
        }
        else if (!this.validationService.isEnglishString(user.name)) {
            this.nameErrorMessage = "Full name should contains only English letters without numbers and characters";
            success = false;
        }
        if (!this.validationService.isStringValid(user.email)) {
            this.emailErrorMessage = "This field can't be empty";
            success = false;
        }
        else if (!this.validationService.isEmailValid(user.email)) {
            this.emailErrorMessage = "Please, write a correct email";
            success = false;
        }
        if (!this.validationService.isStringValid(user.password)) {
            this.passwordErrorMessage = "This field can't be empty";
            success = false;
        }
        if (!this.isTermsAccepted) {
            this.showTermsError = true;
            success = false;
        }
        else {
            this.showTermsError = false;
        }
        if (!success) {
            return;
        }
        this.authService.checkUserEmail(user.email).subscribe(function (response) {
            if (response.success) {
                if (response.exists) {
                    _this.emailErrorMessage = "User with that email already exists";
                }
                else {
                    _this.registerService.chooseCategoryFlag = true;
                }
            }
            else {
                console.log(response.error);
            }
        });
    };
    RegisterComponent.prototype.initializeData = function () {
        var _this = this;
        this.registerService.getUsersAmount().subscribe(function (response) {
            if (response.success) {
                _this.socketService.usersAmount = response.data;
            }
        });
    };
    RegisterComponent.prototype.initializeCategories = function () {
        var _this = this;
        this.categoryService.getCategories().subscribe(function (response) {
            if (response.success) {
                _this.categories = response.categories;
            }
            else {
                console.log(response.error);
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('googleButton'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], RegisterComponent.prototype, "googleButton", void 0);
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("./src/app/components/register/register.component.html"),
            styles: [__webpack_require__("./src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__services_validation_validation_service__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_5__services_socket_socket_service__["a" /* SocketService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_10__services_google_google_service__["a" /* GoogleService */],
            __WEBPACK_IMPORTED_MODULE_8__services_login_login_service__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_9__services_twitter_twitter_service__["a" /* TwitterService */],
            __WEBPACK_IMPORTED_MODULE_6__services_register_service__["a" /* RegisterService */],
            __WEBPACK_IMPORTED_MODULE_4__services_category_category_service__["a" /* CategoryService */],
            __WEBPACK_IMPORTED_MODULE_12__services_alert_alert_service__["a" /* AlertService */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/components/register/services/register.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegisterService = /** @class */ (function () {
    function RegisterService(router, tokenProviderService, http, headersProviderService) {
        this.router = router;
        this.tokenProviderService = tokenProviderService;
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.chooseCategoryFlag = false;
        this.isViaAuthService = false;
        this.userEmail = null;
        this.userFullname = null;
    }
    RegisterService.prototype.goToRegister = function () {
        this.initializeManualRegister();
        this.router.navigate(['/register']);
    };
    RegisterService.prototype.initializeAuthRegister = function (email, fullname) {
        this.chooseCategoryFlag = true;
        this.isViaAuthService = true;
        this.userEmail = email;
        this.userFullname = fullname;
    };
    RegisterService.prototype.initializeManualRegister = function () {
        this.chooseCategoryFlag = false;
        this.isViaAuthService = false;
        this.userEmail = null;
        this.userFullname = null;
    };
    RegisterService.prototype.getUsersAmount = function () {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].serverEndpoint + "/admin/users-amount", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken)
        })
            .map(function (res) { return res.json(); });
    };
    RegisterService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_5__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */]])
    ], RegisterService);
    return RegisterService;
}());



/***/ }),

/***/ "./src/app/components/search/search.component.css":
/***/ (function(module, exports) {

module.exports = "#search {\r\n    padding-bottom: 100px;\r\n}\r\n\r\n.search-input {\r\n    width: 902px;\r\n    margin: 0 auto;\r\n    display: inline-block;\r\n    height: 65px;\r\n    border: 0;\r\n    outline: 0;\r\n}\r\n\r\n.search-container { \r\n    width: 1200px;\r\n    height: 72px;\r\n    margin: 0 auto;\r\n    display: block;\r\n    border-top: 1px solid #D8E2E9;\r\n    border-bottom: 1px solid #D8E2E9;\r\n    border-left: 1px solid #D8E2E9;\r\n    margin-bottom: 50px;\r\n    margin-top: 70px;\r\n}\r\n\r\n.switch-mode-button {\r\n    width: 149px;\r\n    height: 72px;\r\n    border: 0;\r\n    outline: 0;\r\n    margin: 0;\r\n    padding: 0;\r\n    cursor: pointer;\r\n    text-transform: uppercase;\r\n    background: white;\r\n    color: black;\r\n    border: 2px solid #D8E2E9;\r\n    margin-top: -1px;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    text-align: center;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #5D5D5D;\r\n}\r\n\r\n.switch-mode-button:last-child {\r\n    margin-left: -11px;\r\n}\r\n\r\n.switch-mode-button.active {\r\n    background: -webkit-gradient(linear, left bottom, left top, from(#F55045), to(#F55045)), #F87269;\r\n    background: linear-gradient(0deg, #F55045, #F55045), #F87269;\r\n    color: white;\r\n    border: 0;\r\n}\r\n\r\n.more-results-button {\r\n    margin-top: 40px;\r\n}\r\n\r\n.search-header {\r\n    margin-top: -10px;\r\n    margin-bottom: 30px;\r\n}\r\n\r\n.user-name {\r\n    color: #5D5D5D;\r\n}\r\n\r\n.skill-points {\r\n    color: #5D5D5D;\r\n    opacity: .5;\r\n}\r\n\r\n.avatar {\r\n    margin-right: 15px;\r\n}\r\n\r\n.user-info {\r\n    position: relative;\r\n    top: 6px;\r\n}\r\n\r\n.hr-line {\r\n    height: 1px;\r\n    background-color: #DCDCDC;\r\n    opacity: .5;\r\n    margin-top: 20px;\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.silver-label {\r\n    position: relative;\r\n    top: 3px;\r\n}\r\n\r\n.following-button {\r\n    position: relative;\r\n    top: 8px;\r\n}\r\n\r\nbutton.switch-mode-button span {\r\n    height: 17px;\r\n    text-align: center;\r\n    border-radius: 34px;\r\n    background-color: #FFE226;\r\n    -webkit-box-shadow: 0px 4px 14px rgba(255, 226, 38, 0.3);\r\n            box-shadow: 0px 4px 14px rgba(255, 226, 38, 0.3);\r\n    padding-top: 3px;\r\n    padding-bottom: 3px;\r\n    padding-left: 6px;\r\n    padding-right: 6px;\r\n    margin-left: 5px;\r\n}\r\n\r\nbutton.switch-mode-button.active span {\r\n    color: #F55145;\r\n}\r\n\r\n.empty-state-block h1 {\r\n    margin-top: 53px;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.duels-pointer {\r\n    margin-right: 55px;\r\n}\r\n\r\n.duels-pointer p,\r\n.duels-pointer img {\r\n    display: inline-block;\r\n}\r\n\r\n.duels-pointer p {\r\n    width: 150px;\r\n    text-align: right;\r\n    position: relative;\r\n    top: 25px;\r\n    margin-right: 14px;\r\n}\r\n\r\n.duels-pointer img {\r\n    margin-top: -30px;\r\n}"

/***/ }),

/***/ "./src/app/components/search/search.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"search\">\r\n  <div class=\"search-container\">\r\n    <input id=\"searchMainInput\" class=\"search-input\" [(ngModel)]=\"searchString\" (keyup.enter)=\"setSearchMode(currentMode)\" />\r\n    <button id=\"users-mode\" class=\"switch-mode-button\" (click)=\"setSearchMode(searchType.Users)\">\r\n      Users\r\n      <span *ngIf=\"usersCount > 0\">{{usersCount}}</span>\r\n    </button>\r\n    <button id=\"duels-mode\" class=\"switch-mode-button\" (click)=\"setSearchMode(searchType.Duels)\">\r\n      Duels\r\n      <span *ngIf=\"duelsCount > 0\">{{duelsCount}}</span>\r\n    </button>\r\n  </div>\r\n  <div *ngIf=\"users\">\r\n    <div *ngIf=\"currentMode === searchType.Users\">\r\n      <h2 *ngIf=\"searchStringOutput !== '' && usersCount > 0\" class=\"search-header\">Users for \"{{searchStringOutput}}\"</h2>\r\n      <div *ngIf=\"users.length === 0\" class=\"center empty-state-block\">\r\n        <div class=\"right duels-pointer\" *ngIf=\"duelsCount > 0\">\r\n          <p>There’s something here though!</p>\r\n          <img src=\"../../../assets/img/search/duels-arrow.svg\">\r\n        </div>\r\n        <div class=\"clearfix\"></div>\r\n        <img width=\"506\" height=\"311\" src=\"../../../assets/img/search/empty-state-illustration.png\">\r\n        <h1>No users found for your search</h1>\r\n        <p>Try different keywords and make sure your request is spelled properly</p>\r\n      </div>\r\n      <div *ngIf=\"users.length > 0\">\r\n        <div *ngFor=\"let user of users\">\r\n          <div (click)=\"goToPerson(user.username)\" class=\"left clickable\">\r\n            <div class=\"inline avatar\">\r\n              <app-profile-picture elementClass=\"medium\" fullname={{user.fullname}} userId={{user.id}} backgroundImage={{user.imageUrl}}\r\n                backgroundColor={{user.imageColor}}>\r\n              </app-profile-picture>\r\n            </div>\r\n            <div class=\"inline user-info\">\r\n              <h3 class=\"user-name\">{{user.fullname}}\r\n                <span *ngIf=\"user.isPaidAccount\" class=\"silver-label\"></span>\r\n              </h3>\r\n              <p class=\"skill-points\">{{user.skillPoints}} skill</p>\r\n            </div>\r\n          </div>\r\n          <div *ngIf=\"currentUserId !== null\" class=\"right following-button\">\r\n            <div *ngIf=\"currentUserId !== user.id\">\r\n              <a *ngIf=\"!isFollower(user.followers)\" class=\"button secondary-button\" (click)=\"followUser(user.id)\">\r\n                <span>Follow</span>\r\n              </a>\r\n              <a *ngIf=\"isFollower(user.followers)\" class=\"button secondary-button\" (click)=\"unfollowUser(user.id)\">\r\n                <span>Unfollow</span>\r\n              </a>\r\n            </div>\r\n          </div>\r\n          <div class=\"clearfix\"></div>\r\n          <div *ngIf=\"users.indexOf(user) !== users.length - 1\" class=\"hr-line\"></div>\r\n        </div>\r\n        <div *ngIf=\"usersCount !== users.length\" class=\"center more-results-button\">\r\n          <a class=\"button secondary-button\" (click)=\"loadMore()\">\r\n            <span>Load More Search Results</span>\r\n          </a>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"currentMode === searchType.Duels\">\r\n      <div>\r\n        <div *ngIf=\"duels.length === 0\" class=\"center empty-state-block\">\r\n          <img width=\"506\" height=\"311\" src=\"../../../assets/img/search/empty-state-illustration.png\">\r\n          <h1>No duels found for your search</h1>\r\n          <p>Try different keywords and make sure your request is spelled properly</p>\r\n        </div>\r\n        <div *ngIf=\"duels.length > 0\">\r\n          <div class=\"duels-container\">\r\n            <div *ngFor=\"let duel of duels\" (click)=\"openVotePage(duel)\" class=\"duel-wrapper\">\r\n              <app-duel [duel]=\"duel\" [duelOwner]=\"currentUserId\" (notify)=\"onNotify($event)\"></app-duel>\r\n            </div>\r\n          </div>\r\n          <div class=\"clearfix\"></div>\r\n          <div *ngIf=\"duelsCount !== duels.length\" class=\"center more-results-button\">\r\n            <a class=\"button secondary-button\" (click)=\"loadMore()\">\r\n              <span>Load More Search Results</span>\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_search_service__ = __webpack_require__("./src/app/components/search/services/search.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__enums_search_type__ = __webpack_require__("./src/app/enums/search-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_vote_vote_service__ = __webpack_require__("./src/app/services/vote/vote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_services_follow_service__ = __webpack_require__("./src/app/components/user/services/follow.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SearchComponent = /** @class */ (function () {
    function SearchComponent(searchService, voteService, authService, followService, router, titleService, activatedRoute, validationService) {
        this.searchService = searchService;
        this.voteService = voteService;
        this.authService = authService;
        this.followService = followService;
        this.router = router;
        this.titleService = titleService;
        this.activatedRoute = activatedRoute;
        this.validationService = validationService;
        this.searchString = "";
        this.searchStringOutput = "";
        this.users = [];
        this.duels = [];
        this.usersCount = 0;
        this.duelsCount = 0;
        this.page = 1;
        this.searchType = __WEBPACK_IMPORTED_MODULE_3__enums_search_type__["a" /* SearchType */];
    }
    SearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.activatedRoute.params.subscribe(function (params) {
            _this.currentUserId = _this.authService.getCurrentUserId();
            _this.searchString = params['query'];
            if (!_this.validationService.isObjectValid(_this.searchString)) {
                _this.searchString = "";
                _this.searchStringOutput = _this.searchString;
            }
            _this.setSearchMode(__WEBPACK_IMPORTED_MODULE_3__enums_search_type__["a" /* SearchType */].Users);
        });
    };
    SearchComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    SearchComponent.prototype.goToPerson = function (username) {
        this.router.navigate(["/user", username]);
    };
    SearchComponent.prototype.openVotePage = function (duel) {
        this.voteService.openVotePage(duel);
    };
    SearchComponent.prototype.setSearchMode = function (searchType) {
        this.initializeModeButtons();
        this.searchStringOutput = this.searchString;
        switch (searchType) {
            case __WEBPACK_IMPORTED_MODULE_3__enums_search_type__["a" /* SearchType */].Users:
                this.setUsersMode();
                break;
            case __WEBPACK_IMPORTED_MODULE_3__enums_search_type__["a" /* SearchType */].Duels:
                this.setDuelsMode();
                break;
        }
    };
    SearchComponent.prototype.followUser = function (userId) {
        var _this = this;
        this.followService.followUser(userId).subscribe(function (data) {
            if (data.success) {
                _this.getUsers(true);
            }
        });
    };
    SearchComponent.prototype.unfollowUser = function (userId) {
        var _this = this;
        this.followService.unfollowUser(userId).subscribe(function (data) {
            if (data.success) {
                _this.getUsers(true);
            }
        });
    };
    SearchComponent.prototype.isFollower = function (followers) {
        return followers.map(function (follower) { return follower.followerId; }).indexOf(this.currentUserId) >= 0;
    };
    SearchComponent.prototype.loadMore = function () {
        this.page++;
        switch (this.currentMode) {
            case __WEBPACK_IMPORTED_MODULE_3__enums_search_type__["a" /* SearchType */].Users:
                this.getUsers();
                break;
            case __WEBPACK_IMPORTED_MODULE_3__enums_search_type__["a" /* SearchType */].Duels:
                this.getDuels();
                break;
        }
    };
    SearchComponent.prototype.setUsersMode = function () {
        $("#users-mode").addClass("active");
        this.users = [];
        this.duels = [];
        this.page = 1;
        this.currentMode = __WEBPACK_IMPORTED_MODULE_3__enums_search_type__["a" /* SearchType */].Users;
        this.titleService.setTitle("Users Search");
        this.getUsers();
    };
    SearchComponent.prototype.setDuelsMode = function () {
        $("#duels-mode").addClass("active");
        this.users = [];
        this.duels = [];
        this.page = 1;
        this.currentMode = __WEBPACK_IMPORTED_MODULE_3__enums_search_type__["a" /* SearchType */].Duels;
        this.titleService.setTitle("Duels Search");
        this.getDuels();
    };
    SearchComponent.prototype.initializeModeButtons = function () {
        $(".switch-mode-button").removeClass("active");
    };
    SearchComponent.prototype.getUsers = function (refreshData) {
        var _this = this;
        if (refreshData === void 0) { refreshData = false; }
        this.searchService.getUsers(this.searchString, this.page).subscribe(function (response) {
            if (response) {
                if (refreshData) {
                    _this.users = _this.users.slice(0, -6);
                }
                (_a = _this.users).push.apply(_a, response.data.users);
                _this.usersCount = response.data.usersCount;
                _this.duelsCount = response.data.duelsCount;
            }
            else {
                console.log(response.error);
            }
            var _a;
        });
    };
    SearchComponent.prototype.getDuels = function () {
        var _this = this;
        this.searchService.getDuels(this.searchString, this.page).subscribe(function (response) {
            if (response) {
                (_a = _this.duels).push.apply(_a, response.data.duels);
                _this.usersCount = response.data.usersCount;
                _this.duelsCount = response.data.duelsCount;
            }
            else {
                console.log(response.error);
            }
            var _a;
        });
    };
    SearchComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-search',
            template: __webpack_require__("./src/app/components/search/search.component.html"),
            styles: [__webpack_require__("./src/app/components/search/search.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_search_service__["a" /* SearchService */],
            __WEBPACK_IMPORTED_MODULE_4__services_vote_vote_service__["a" /* VoteService */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_7__user_services_follow_service__["a" /* FollowService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_8__services_validation_validation_service__["a" /* ValidationService */]])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./src/app/components/search/services/search.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SearchService = /** @class */ (function () {
    function SearchService(tokenProviderService, headersProviderService, http) {
        this.tokenProviderService = tokenProviderService;
        this.headersProviderService = headersProviderService;
        this.http = http;
    }
    SearchService.prototype.getUsers = function (searchString, page) {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/search/get-users", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                searchString: searchString,
                page: page
            }
        })
            .map(function (res) { return res.json(); });
    };
    SearchService.prototype.getDuels = function (searchString, page) {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/search/get-duels", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                searchString: searchString,
                page: page
            }
        })
            .map(function (res) { return res.json(); });
    };
    SearchService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"]])
    ], SearchService);
    return SearchService;
}());



/***/ }),

/***/ "./src/app/components/start-duel/start-duel.component.css":
/***/ (function(module, exports) {

module.exports = ".suggested-opponents-container {\r\n    margin-top: 10px;\r\n}\r\n\r\n.suggested-opponent {\r\n    display: inline-block;\r\n    vertical-align: top;\r\n    position: relative;\r\n}\r\n\r\n.suggested-opponent img {\r\n    border-radius: 50%;\r\n    width: 36px;\r\n    height: 36px;\r\n}\r\n\r\n.start-duel-page {\r\n    padding-top: 100px;\r\n    padding-bottom: 100px;\r\n}\r\n\r\ninput[type=\"date\"]::-webkit-clear-button {\r\n    -webkit-appearance: none;\r\n    display: none;\r\n}\r\n\r\n.start-duel-page .duel-counter {\r\n    display: inline-block;\r\n    margin-right: 140px;\r\n    margin-top: 30px;\r\n}\r\n\r\n.start-duel-page .duel-counter:last-child {\r\n    margin-right: 0;\r\n}\r\n\r\n.start-duel-page .duel-counter > span {\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 25px;\r\n    line-height: 30px;\r\n    color: #31D431;\r\n}\r\n\r\n.start-duel-page .duel-counter span.digits {\r\n    font-family: Menoe Grotesque Pro;\r\n}\r\n\r\n.start-duel-page .duel-counter span.digits.zero {\r\n    color: #5D5D5D;\r\n}\r\n\r\n.start-duel-page .duel-counter p {\r\n    margin-top: 6px;\r\n}\r\n\r\n.start-duel-page .duel-counter .tooltip-label {\r\n    position: relative;\r\n    top: 3px;\r\n}\r\n\r\n.start-duel-page .delimiter {\r\n    height: 1px;\r\n    background-color: #D8E2E9;\r\n    opacity: 0.7;\r\n    margin-top: 40px;\r\n}\r\n\r\n.start-duel-page .paid-account-promotion {\r\n    width: 590px;\r\n    background-color: #fffce9;\r\n    border-left: 4px solid #FFE226;\r\n    padding-top: 20px;\r\n    padding-left: 28px;\r\n    padding-bottom: 5px;\r\n    cursor: pointer;\r\n    margin-top: 8px;\r\n}\r\n\r\n.start-duel-page .paid-account-promotion p {\r\n    margin-top: 5px;\r\n}\r\n\r\n.start-duel-page .paid-account-promotion svg {\r\n    margin-left: 7px;\r\n}\r\n\r\n.start-duel-page .duels-limit-block {\r\n    margin-top: 100px;\r\n}\r\n\r\n.start-duel-page .duels-limit-block img {\r\n    margin-left: 75px;\r\n    margin-right: 73px;\r\n}\r\n\r\n.start-duel-page .duels-limit-block > div {\r\n    width: 590px;\r\n    vertical-align: middle;\r\n}\r\n\r\n.start-duel-page .duels-limit-block > div > h2 {\r\n    margin-bottom: 13px;\r\n}\r\n\r\n.start-duel-page .duels-limit-block > div > p {\r\n    margin-bottom: 30px;\r\n}\r\n\r\n.start-duel-page .settings-block h2.opponent-heading {\r\n    margin-top: 55px;\r\n    margin-bottom: 7px;\r\n}\r\n\r\n.start-duel-page .settings-block .opponent-list-heading {\r\n    margin-top: 25px;\r\n}\r\n\r\n.start-duel-page .settings-block .opponent-input {\r\n    width: 387px;\r\n    margin-top: 25px;\r\n}\r\n\r\n.start-duel-page .settings-block .start-duel-info {\r\n    width: 590px;\r\n    border: 1px solid #D8E2E9;\r\n    margin-top: -37px;\r\n}\r\n\r\n.start-duel-page .settings-block .start-duel-info .top-border {\r\n    width: 590px;\r\n    height: 6px;\r\n    background-color: #F55045;\r\n    position: relative;\r\n    top: -1px;\r\n    left: -1px;\r\n}\r\n\r\n.start-duel-page .settings-block .start-duel-info .content {\r\n    padding-top: 20px;\r\n    padding-left: 30px;\r\n}\r\n\r\n.start-duel-page .settings-block .start-duel-info ul {\r\n    list-style: none;\r\n    padding-left: 14px;\r\n    margin-top: 5px;\r\n}\r\n\r\n.start-duel-page .settings-block .start-duel-info ul li {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 14px;\r\n    line-height: 25px;\r\n    letter-spacing: -0.02em;\r\n    color: #5D5D5D;\r\n}\r\n\r\n.start-duel-page .settings-block .start-duel-info ul li > span {\r\n    margin-left: -5px;\r\n}\r\n\r\n.start-duel-page .settings-block .start-duel-info ul li::before {\r\n    content: \"•\"; \r\n    color: #F55045;\r\n    display: inline-block; \r\n    width: 1em;\r\n    margin-left: -1em\r\n}\r\n\r\n.start-duel-page .settings-block .start-duel-info p.small {\r\n    margin-top: -5px;\r\n    opacity: .5;\r\n    margin-left: 16px;\r\n}\r\n\r\n.start-duel-page .settings-block .custom-tooltip {\r\n    position: relative;\r\n    top: 3px;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container {\r\n    margin-top: 25px;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .avatar {\r\n    display: inline-block;\r\n}\r\n\r\n.start-duel-page .suggested-opponent > div {\r\n    display: inline-block;\r\n}\r\n\r\n.start-duel-page .suggested-opponent p.skill-points {\r\n    margin-top: 10px;\r\n    background-color: #fff;\r\n    padding-left: 8px;\r\n    padding-right: 8px;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .furthest-minus-opponent {\r\n    width: 147px;\r\n    text-align: left;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .middle-minus-opponent {\r\n    width: 218px;\r\n    text-align: center;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .closest-minus-opponent {\r\n    width: 147px;\r\n    text-align: right;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .current-user {\r\n    width: 144px;\r\n    text-align: center;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .closest-plus-opponent {\r\n    width: 147px;\r\n    text-align: left;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .middle-plus-opponent {\r\n    width: 218px;\r\n    text-align: center;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .furthest-plus-opponent {\r\n    width: 147px;\r\n    text-align: right;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .suggested-opponent > div {\r\n    text-align: center;\r\n}\r\n\r\n.start-duel-page .settings-block .opponents-delimiter {\r\n    width: 1100px;\r\n    margin: 0 auto;\r\n    height: 1px;\r\n    background-color: #D8E2E9;\r\n    margin-top: -65px;\r\n}\r\n\r\n.start-duel-page .settings-block .suggested-opponents-container .current-user p:last-child {\r\n    color: #C1C7CB;\r\n}\r\n\r\n.start-duel-page .settings-block h3.categories-heading {\r\n    margin-top: 100px;\r\n    text-align: center;\r\n}\r\n\r\n.start-duel-page .custom-radio .checkmark {\r\n    width: 16px;\r\n    height: 16px;\r\n}\r\n\r\n.start-duel-page .custom-radio.container .checkmark::after {\r\n    width: 8.5px;\r\n    height: 8.5px;\r\n    position: absolute;\r\n    top: 16.4%;\r\n    left: 15.4%;\r\n}\r\n\r\n.custom-radio-wrapper {\r\n    width: 16px;\r\n    position: relative;\r\n    top: -9px;\r\n}\r\n\r\n.categories-container {\r\n    text-align: center;\r\n    height: 200px;\r\n}\r\n\r\n.categories-container h3 {\r\n    margin-bottom: 16px;\r\n}\r\n\r\n.category-block {\r\n    height: 30px;\r\n    border: 1px solid #D8E2E9;\r\n    -webkit-box-sizing: border-box;\r\n            box-sizing: border-box;\r\n    border-radius: 100px;\r\n    display: inline-block;\r\n    cursor: pointer;\r\n    padding-left: 12px;\r\n    padding-right: 12px;\r\n    text-align: center;\r\n    color: #5D5D5D !important;\r\n    margin-right: 16px;\r\n    margin-bottom: 16px;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.category-block:hover {\r\n    border: 1px solid rgba(245, 80, 69, 0.4);\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.category-block.selected {\r\n    background: #feedec;\r\n    border: 1px solid #feedec;\r\n}\r\n\r\n.category-block > p {\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n    line-height: 28px;\r\n    font-family: 'Menoe Grotesque Pro Italic';\r\n}\r\n\r\n.category-block:hover > p,\r\n.category-block.selected > p { \r\n    color: #F55045 !important;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.task-block h2 {\r\n    margin-top: -50px;\r\n}\r\n\r\n.start-duel-page .start-duel-info.tips .content {\r\n    padding-bottom: 5px;\r\n}\r\n\r\nspan > a.link {\r\n    color: #F55045 !important;\r\n    text-decoration: none;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\nspan > a.link:hover {\r\n    color: #A04437 !important;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.task-input {\r\n    width: 488px;\r\n    margin-top: 30px;\r\n}\r\n\r\n.deadline-block {\r\n    width: 730px;\r\n    margin: 0 auto;\r\n    margin-top: 100px;\r\n}\r\n\r\n.deadline-block h2 {\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.work-deadline-container input:first-child {\r\n    margin-left: 0px;\r\n}\r\n\r\n.work-deadline-container input {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    text-align: center;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    background-color: #fff;\r\n    outline: 0;\r\n    color: #5D5D5D;\r\n    cursor: pointer;\r\n    width: 151.66px;\r\n    height: 60px;\r\n    border: 1px solid #D8E2E9;\r\n    -webkit-box-sizing: border-box;\r\n            box-sizing: border-box;\r\n    margin-left: -5px;\r\n}\r\n\r\n.work-deadline-container input[type='button'].selected {\r\n    background-color: #F55045;\r\n    color: #fff;\r\n}\r\n\r\n.work-deadline-container span {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 14px;\r\n    line-height: 25px;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    letter-spacing: -0.02em;\r\n    color: #5D5D5D;\r\n    margin-left: 15px;\r\n    margin-right: 19px;\r\n}\r\n\r\n.work-deadline-container input[type='date'] {\r\n    width: 179px;\r\n}\r\n\r\n.datepicker {\r\n    left: -35px;\r\n}\r\n\r\n.start-duel-block {\r\n    text-align: center;\r\n    margin-top: 65px;\r\n}\r\n\r\n.start-duel-block p.inline {\r\n    margin-left: 27px;\r\n}"

/***/ }),

/***/ "./src/app/components/start-duel/start-duel.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"start-duel-page\" *ngIf=\"startDuelInfo\">\r\n  <h1>Starting a duel</h1>\r\n  <div class=\"left\">\r\n    <div class=\"duel-counter\">\r\n      <span *ngIf=\"startDuelInfo.isPaidAccount\">&#8734;</span>\r\n      <span class=\"digits\" [class.zero]=\"duelsLeft === 0\" *ngIf=\"!startDuelInfo.isPaidAccount\">{{duelsLeft}}</span>\r\n      <p>Duels Left This Week\r\n        <span *ngIf=\"!startDuelInfo.isPaidAccount\" #tooltip class=\"tooltip-label custom-tooltip digits-tooltip inverse\">\r\n          <span class=\"tooltiptext\">Some text</span>\r\n        </span>\r\n      </p>\r\n    </div>\r\n    <div class=\"duel-counter\">\r\n      <span *ngIf=\"startDuelInfo.isPaidAccount\">&#8734;</span>\r\n      <span class=\"digits\" [class.zero]=\"currentDuels === 0\" *ngIf=\"!startDuelInfo.isPaidAccount\">{{currentDuels}}</span>\r\n      <p>Current Duels\r\n        <span *ngIf=\"!startDuelInfo.isPaidAccount\" #tooltip class=\"tooltip-label custom-tooltip digits-tooltip inverse\">\r\n          <span class=\"tooltiptext\">Some text</span>\r\n        </span>\r\n      </p>\r\n    </div>\r\n  </div>\r\n  <div class=\"right paid-account-promotion\" *ngIf=\"!startDuelInfo.isPaidAccount\" (click)=\"goToPage('silver')\">\r\n    <h3>Not Enough?\r\n      <svg width=\"10\" height=\"11\" viewBox=\"0 0 10 11\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n        <path d=\"M0 5.5L9 5.5M9 5.5L4.5 10M9 5.5L4.5 1\" stroke=\"#F55045\" stroke-width=\"2\" stroke-linejoin=\"round\" />\r\n      </svg>\r\n    </h3>\r\n    <p>Get Duelity Silver to cut all limitations & unleash new features!</p>\r\n  </div>\r\n  <div class=\"clearfix\"></div>\r\n  <div class=\"delimiter\"></div>\r\n  <div *ngIf=\"canStartDuel\" class=\"settings-block\">\r\n    <h2 class=\"opponent-heading\">Opponent and Category</h2>\r\n    <div class=\"left\">\r\n      <p>Search for opponent below</p>\r\n      <div class=\"input-wrapper opponent-input\">\r\n        <app-input (onValueChange)=\"onSelectedUserChange($event)\" [value]=\"selectedUser.value\" [placeholder]=\"'Opponent'\" [type]=\"inputType.Text\"></app-input>\r\n      </div>\r\n    </div>\r\n    <div class=\"right start-duel-info\">\r\n      <div class=\"top-border\"></div>\r\n      <div class=\"content\">\r\n        <h3>You can start a duel with users that</h3>\r\n        <ul>\r\n          <li>\r\n            <span>have from -50 to +50 skill points relative to you</span>\r\n          </li>\r\n          <li>\r\n            <span>have at least 1 common category with you</span>\r\n          </li>\r\n          <li>\r\n            <span>did not use up all 3 duels this week* </span>\r\n          </li>\r\n          <li>\r\n            <span>don’t have an active duel*</span>\r\n          </li>\r\n        </ul>\r\n        <p class=\"small\">*ignored, if user has Duelity Silver</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <p class=\"opponent-list-heading\">\r\n      or pick from the personal suggestion list\r\n      <span #tooltip class=\"tooltip-label custom-tooltip inverse\">\r\n        <span class=\"tooltiptext text-left\">Suggestion list shows the closest opponents, middle opponents and furthest available opponents. They’re random each\r\n          time\r\n        </span>\r\n      </span>\r\n    </p>\r\n    <div>\r\n      <div *ngIf=\"startDuelInfo.suggestedOpponents.closestMinus || startDuelInfo.suggestedOpponents.closestPlus\" class=\"suggested-opponents-container\">\r\n        <div class=\"suggested-opponent furthest-minus-opponent\">\r\n          <div>\r\n            <div *ngIf=\"startDuelInfo.suggestedOpponents.furthestMinus\">\r\n              <app-profile-picture class=\"avatar\" elementClass=\"x-small\" userId={{startDuelInfo.suggestedOpponents.furthestMinus.id}} fullname={{startDuelInfo.suggestedOpponents.furthestMinus.fullname}}\r\n                backgroundImage={{startDuelInfo.suggestedOpponents.furthestMinus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.furthestMinus.imageColor}}>\r\n              </app-profile-picture>\r\n              <p class=\"skill-points\">{{startDuelInfo.suggestedOpponents.furthestMinus.skillPoints}} skill</p>\r\n              <div class=\"inline custom-radio-wrapper\">\r\n                <label class=\"custom-radio container\">\r\n                  <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.furthestMinus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.furthestMinus)\"\r\n                  />\r\n                  <span class=\"checkmark\"></span>\r\n                </label>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"suggested-opponent middle-minus-opponent\">\r\n          <div>\r\n            <div *ngIf=\"startDuelInfo.suggestedOpponents.middleMinus\">\r\n              <app-profile-picture class=\"avatar\" elementClass=\"x-small\" userId={{startDuelInfo.suggestedOpponents.middleMinus.id}} fullname={{startDuelInfo.suggestedOpponents.middleMinus.fullname}}\r\n                backgroundImage={{startDuelInfo.suggestedOpponents.middleMinus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.middleMinus.imageColor}}>\r\n              </app-profile-picture>\r\n              <p class=\"skill-points\">{{startDuelInfo.suggestedOpponents.middleMinus.skillPoints}} skill</p>\r\n              <div class=\"inline custom-radio-wrapper\">\r\n                <label class=\"custom-radio container\">\r\n                  <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.middleMinus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.middleMinus)\"\r\n                  />\r\n                  <span class=\"checkmark\"></span>\r\n                </label>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"suggested-opponent closest-minus-opponent\">\r\n          <div>\r\n            <div *ngIf=\"startDuelInfo.suggestedOpponents.closestMinus\">\r\n              <app-profile-picture class=\"avatar\" elementClass=\"x-small\" userId={{startDuelInfo.suggestedOpponents.closestMinus.id}} fullname={{startDuelInfo.suggestedOpponents.closestMinus.fullname}}\r\n                backgroundImage={{startDuelInfo.suggestedOpponents.closestMinus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.closestMinus.imageColor}}>\r\n              </app-profile-picture>\r\n              <p class=\"skill-points\">{{startDuelInfo.suggestedOpponents.closestMinus.skillPoints}} skill</p>\r\n              <div class=\"inline custom-radio-wrapper\">\r\n                <label class=\"custom-radio container\">\r\n                  <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.closestMinus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.closestMinus)\"\r\n                  />\r\n                  <span class=\"checkmark\"></span>\r\n                </label>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"suggested-opponent current-user\">\r\n          <div>\r\n            <div *ngIf=\"startDuelInfo.currentUser\">\r\n              <app-profile-picture class=\"avatar\" elementClass=\"x-small\" userId={{startDuelInfo.currentUser.id}} fullname={{startDuelInfo.currentUser.fullname}}\r\n                backgroundImage={{startDuelInfo.currentUser.imageUrl}} backgroundColor={{startDuelInfo.currentUser.imageColor}}>\r\n              </app-profile-picture>\r\n              <p class=\"skill-points\">{{startDuelInfo.currentUser.skillPoints}} skill</p>\r\n              <p>You</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"suggested-opponent closest-plus-opponent\">\r\n          <div>\r\n            <div *ngIf=\"startDuelInfo.suggestedOpponents.closestPlus\">\r\n              <app-profile-picture class=\"avatar\" elementClass=\"x-small\" userId={{startDuelInfo.suggestedOpponents.closestPlus.id}} fullname={{startDuelInfo.suggestedOpponents.closestPlus.fullname}}\r\n                backgroundImage={{startDuelInfo.suggestedOpponents.closestPlus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.closestPlus.imageColor}}>\r\n              </app-profile-picture>\r\n              <p class=\"skill-points\">{{startDuelInfo.suggestedOpponents.closestPlus.skillPoints}} skill</p>\r\n              <div class=\"inline custom-radio-wrapper\">\r\n                <label class=\"custom-radio container\">\r\n                  <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.closestPlus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.closestPlus)\"\r\n                  />\r\n                  <span class=\"checkmark\"></span>\r\n                </label>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"suggested-opponent middle-plus-opponent\">\r\n          <div>\r\n            <div *ngIf=\"startDuelInfo.suggestedOpponents.middlePlus\">\r\n              <app-profile-picture class=\"avatar\" elementClass=\"x-small\" userId={{startDuelInfo.suggestedOpponents.middlePlus.id}} fullname={{startDuelInfo.suggestedOpponents.middlePlus.fullname}}\r\n                backgroundImage={{startDuelInfo.suggestedOpponents.middlePlus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.middlePlus.imageColor}}>\r\n              </app-profile-picture>\r\n              <p class=\"skill-points\">{{startDuelInfo.suggestedOpponents.middlePlus.skillPoints}} skill</p>\r\n              <div class=\"inline custom-radio-wrapper\">\r\n                <label class=\"custom-radio container\">\r\n                  <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.middlePlus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.middlePlus)\"\r\n                  />\r\n                  <span class=\"checkmark\"></span>\r\n                </label>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"suggested-opponent furthest-plus-opponent\">\r\n          <div *ngIf=\"startDuelInfo.suggestedOpponents.furthestPlus\">\r\n            <app-profile-picture class=\"avatar\" elementClass=\"x-small\" userId={{startDuelInfo.suggestedOpponents.furthestPlus.id}} fullname={{startDuelInfo.suggestedOpponents.furthestPlus.fullname}}\r\n              backgroundImage={{startDuelInfo.suggestedOpponents.furthestPlus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.furthestPlus.imageColor}}>\r\n            </app-profile-picture>\r\n            <p class=\"skill-points\">{{startDuelInfo.suggestedOpponents.furthestPlus.skillPoints}} skill</p>\r\n            <div class=\"inline custom-radio-wrapper\">\r\n              <label class=\"custom-radio container\">\r\n                <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.furthestPlus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.furthestPlus)\"\r\n                />\r\n                <span class=\"checkmark\"></span>\r\n              </label>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"opponents-delimiter\"></div>\r\n      <div class=\"categories-container\">\r\n        <div *ngIf=\"selectedUser.fullname\">\r\n          <h3 class=\"categories-heading\">List of common categories with {{selectedUser.fullname}}</h3>\r\n          <div>\r\n            <div *ngFor=\"let category of startDuelInfo.commonCategories\" class=\"category-block\" [class.selected]=\"category.selected\"\r\n              (click)=\"categoryService.pickOneCategory(category, startDuelInfo.commonCategories)\">\r\n              <p>{{category.name}}</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"left task-block\">\r\n        <h2>Task</h2>\r\n        <div class=\"input-wrapper task-input\">\r\n          <app-input (onValueChange)=\"onSelectedTaskText($event)\" [value]=\"selectedTaskText\" [value]=\"selectedTaskText\" [placeholder]=\"'Duel Task'\"\r\n            [type]=\"inputType.Textarea\" [counterMinLength]=\"100\"></app-input>\r\n        </div>\r\n      </div>\r\n      <div class=\"right start-duel-info tips\">\r\n        <div class=\"top-border\"></div>\r\n        <div class=\"content\">\r\n          <h3>Tips on duel task</h3>\r\n          <ul>\r\n            <li>\r\n              <span>start with the overall description</span>\r\n            </li>\r\n            <li>\r\n              <span>mention restrictions, if any</span>\r\n            </li>\r\n            <li>\r\n              <span>go into details, try to be on the same page with your opponent</span>\r\n            </li>\r\n            <li>\r\n              <span>list problems that the solution has to solve</span>\r\n            </li>\r\n            <li>\r\n              <span>\r\n                <a class=\"link\" routerLink=\"/terms\">do not mention</a> any brands unless they are widely known</span>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"deadline-block\">\r\n        <h2>Work Upload Deadline</h2>\r\n        <div class=\"work-deadline-container\">\r\n          <input type=\"button\" value=\"Two Days\" (click)=\"selectDate($event.target, 2)\">\r\n          <input type=\"button\" value=\"Week\" (click)=\"selectDate($event.target, 7)\">\r\n          <input type=\"button\" value=\"Two Weeks\" (click)=\"selectDate($event.target, 14)\">\r\n          <span>or</span>\r\n          <div class=\"inline datepicker-wrapper\">\r\n            <input id=\"deadline-picker\" type=\"date\" min=\"{{getMinDate()}}\" max=\"{{getMaxDate()}}\" (change)=\"onChangeDatePickerValue($event.target)\"\r\n              onkeydown=\"return false\" readonly>\r\n            <div id=\"datepicker\" class=\"datepicker\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"start-duel-block\">\r\n        <a class=\"button primary-button\" (click)=\"startDuel()\">\r\n          <span>Start the Duel</span>\r\n        </a>\r\n        <p class=\"inline\">Keep in mind our Rule Book when starting the duel</p>\r\n      </div>\r\n      <!-- <p>Enter the name below or choose from the handy suggestion list</p>\r\n    <input autocomplete [config]=\"config\" [items]=\"opponents\" [(ngModel)]=\"selectedUser.value\" (inputChangedEvent)=\"onInputChangedEvent($event)\"\r\n      (selectEvent)=\"onSelect($event)\">\r\n    <div *ngIf=\"startDuelInfo.suggestedOpponents.closestMinus || startDuelInfo.suggestedOpponents.closestPlus\" class=\"suggested-opponents-container\">\r\n      <div *ngIf=\"startDuelInfo.suggestedOpponents.furthestMinus\" class=\"suggested-opponent\">\r\n        <div>\r\n          <app-profile-picture fontSize=15 width=36 height=36 userId={{startDuelInfo.suggestedOpponents.furthestMinus.id}} fullname={{startDuelInfo.suggestedOpponents.furthestMinus.fullname}}\r\n            backgroundImage={{startDuelInfo.suggestedOpponents.furthestMinus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.furthestMinus.imageColor}}>\r\n          </app-profile-picture>\r\n          <p>{{startDuelInfo.suggestedOpponents.furthestMinus.skillPoints}} skill</p>\r\n          <label>Furthest-\r\n            <br>Opponent</label>\r\n          <div>\r\n            <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.furthestMinus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.furthestMinus)\"\r\n            />\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"startDuelInfo.suggestedOpponents.middleMinus\" class=\"suggested-opponent\">\r\n        <div>\r\n          <app-profile-picture fontSize=15 width=36 height=36 userId={{startDuelInfo.suggestedOpponents.middleMinus.id}} fullname={{startDuelInfo.suggestedOpponents.middleMinus.fullname}}\r\n            backgroundImage={{startDuelInfo.suggestedOpponents.middleMinus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.middleMinus.imageColor}}>\r\n          </app-profile-picture>\r\n          <p>{{startDuelInfo.suggestedOpponents.middleMinus.skillPoints}} skill</p>\r\n          <label>Middle-\r\n            <br>Opponent</label>\r\n          <div>\r\n            <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.middleMinus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.middleMinus)\"\r\n            />\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"startDuelInfo.suggestedOpponents.closestMinus\" class=\"suggested-opponent\">\r\n        <div>\r\n          <app-profile-picture fontSize=15 width=36 height=36 userId={{startDuelInfo.suggestedOpponents.closestMinus.id}} fullname={{startDuelInfo.suggestedOpponents.closestMinus.fullname}}\r\n            backgroundImage={{startDuelInfo.suggestedOpponents.closestMinus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.closestMinus.imageColor}}>\r\n          </app-profile-picture>\r\n          <p>{{startDuelInfo.suggestedOpponents.closestMinus.skillPoints}} skill</p>\r\n          <label>Closest-\r\n            <br>Opponent</label>\r\n          <div>\r\n            <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.closestMinus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.closestMinus)\"\r\n            />\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"startDuelInfo.currentUser\" class=\"suggested-opponent\">\r\n        <div>\r\n          <app-profile-picture fontSize=15 width=36 height=36 userId={{startDuelInfo.currentUser.id}} fullname={{startDuelInfo.currentUser.fullname}}\r\n            backgroundImage={{startDuelInfo.currentUser.imageUrl}} backgroundColor={{startDuelInfo.currentUser.imageColor}}>\r\n          </app-profile-picture>\r\n          <p>{{startDuelInfo.currentUser.skillPoints}} skill</p>\r\n          <label>That's\r\n            <br>you</label>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"startDuelInfo.suggestedOpponents.closestPlus\" class=\"suggested-opponent\">\r\n        <div>\r\n          <app-profile-picture fontSize=15 width=36 height=36 userId={{startDuelInfo.suggestedOpponents.closestPlus.id}} fullname={{startDuelInfo.suggestedOpponents.closestPlus.fullname}}\r\n            backgroundImage={{startDuelInfo.suggestedOpponents.closestPlus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.closestPlus.imageColor}}>\r\n          </app-profile-picture>\r\n          <p>{{startDuelInfo.suggestedOpponents.closestPlus.skillPoints}} skill</p>\r\n          <label>Closest+\r\n            <br>Opponent</label>\r\n          <div>\r\n            <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.closestPlus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.closestPlus)\"\r\n            />\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"startDuelInfo.suggestedOpponents.middlePlus\" class=\"suggested-opponent\">\r\n        <div>\r\n          <app-profile-picture fontSize=15 width=36 height=36 userId={{startDuelInfo.suggestedOpponents.middlePlus.id}} fullname={{startDuelInfo.suggestedOpponents.middlePlus.fullname}}\r\n            backgroundImage={{startDuelInfo.suggestedOpponents.middlePlus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.middlePlus.imageColor}}>\r\n          </app-profile-picture>\r\n          <p>{{startDuelInfo.suggestedOpponents.middlePlus.skillPoints}} skill</p>\r\n          <label>Middle+\r\n            <br>Opponent</label>\r\n          <div>\r\n            <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.middlePlus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.middlePlus)\"\r\n            />\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"startDuelInfo.suggestedOpponents.furthestPlus\" class=\"suggested-opponent\">\r\n        <div>\r\n          <app-profile-picture fontSize=15 width=36 height=36 userId={{startDuelInfo.suggestedOpponents.furthestPlus.id}} fullname={{startDuelInfo.suggestedOpponents.furthestPlus.fullname}}\r\n            backgroundImage={{startDuelInfo.suggestedOpponents.furthestPlus.imageUrl}} backgroundColor={{startDuelInfo.suggestedOpponents.furthestPlus.imageColor}}>\r\n          </app-profile-picture>\r\n          <p>{{startDuelInfo.suggestedOpponents.furthestPlus.skillPoints}} skill</p>\r\n          <label>Furthest+\r\n            <br>Opponent</label>\r\n          <div>\r\n            <input type=\"radio\" attr.data-user-id=\"{{startDuelInfo.suggestedOpponents.furthestPlus.id}}\" (click)=\"selectByRadio(startDuelInfo.suggestedOpponents.furthestPlus)\"\r\n            />\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <h1>Task</h1>\r\n    <p>Make it as descriptive as possible. Remember, better described tasks get accepted way more!</p>\r\n    <textarea [(ngModel)]=\"selectedTaskText\"></textarea>\r\n    <h1>Work Upload Deadline</h1>\r\n    <div class=\"work-deadline-container\">\r\n      <input type=\"button\" value=\"Two Days\" (click)=\"selectDate($event.target, 2)\">\r\n      <input type=\"button\" value=\"Week\" (click)=\"selectDate($event.target, 7)\">\r\n      <input type=\"button\" value=\"Two Weeks\" (click)=\"selectDate($event.target, 14)\">\r\n      <input id=\"deadline-picker\" type=\"date\" min=\"{{getMinDate()}}\" max=\"{{getMaxDate()}}\" (change)=\"onChangeDatePickerValue($event.target)\"\r\n        onkeydown=\"return false\">\r\n    </div>\r\n    <button (click)=\"startDuel()\">Start the Duel</button> -->\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"!canStartDuel\" class=\"duels-limit-block\">\r\n    <img width=\"386\" height=\"266\" src=\"../../../assets/img/start-duel/duels-limit-illustration.png\">\r\n    <div class=\"inline\">\r\n      <h2>Wow, you’ve used up all 3 duels this week!</h2>\r\n      <p>If you have enough time and creative ideas, you can purchase Duelity Silver to unlock unlimited duels</p>\r\n      <a class=\"button primary-button\" (click)=\"goToPage('silver')\">\r\n        <span>Check Duelity Silver Out</span>\r\n      </a>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/components/start-duel/start-duel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartDuelComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_start_duel_service_start_duel_service__ = __webpack_require__("./src/app/services/start-duel-service/start-duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_date_formatter_date_formatter_service__ = __webpack_require__("./src/app/services/date-formatter/date-formatter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_tooltip_tooltip_service__ = __webpack_require__("./src/app/services/tooltip/tooltip.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__enums_input_type__ = __webpack_require__("./src/app/enums/input-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_category_category_service__ = __webpack_require__("./src/app/services/category/category.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var StartDuelComponent = /** @class */ (function () {
    function StartDuelComponent(startDuelService, authService, validationService, dateFormatter, router, titleService, alertService, tooltipService, categoryService) {
        this.startDuelService = startDuelService;
        this.authService = authService;
        this.validationService = validationService;
        this.dateFormatter = dateFormatter;
        this.router = router;
        this.titleService = titleService;
        this.alertService = alertService;
        this.tooltipService = tooltipService;
        this.categoryService = categoryService;
        this.maxDaysAmount = 14;
        this.minDaysAmount = 2;
        this.canStartDuel = false;
        // Opponent's input fields
        this.config = { 'placeholder': 'test', 'sourceField': ['value'] };
        this.opponents = [];
        this.selectedDate = null;
        this.selectedTaskText = '';
        this.inputType = __WEBPACK_IMPORTED_MODULE_9__enums_input_type__["a" /* InputType */];
    }
    StartDuelComponent.prototype.ngOnInit = function () {
        this.initializeData();
        this.titleService.setTitle("Starting a Duel - Duelity");
    };
    StartDuelComponent.prototype.onInputChangedEvent = function (search) {
        var _this = this;
        if (search.trim() == "") {
            this.selectUser(this.getEmptyUser());
        }
        this.startDuelService.getOpponents(search).subscribe(function (result) {
            if (result.success) {
                _this.opponents = result.data;
            }
        });
    };
    StartDuelComponent.prototype.onSelect = function (item) {
        this.selectUser(item);
    };
    StartDuelComponent.prototype.selectByRadio = function (item) {
        this.selectUser(item);
    };
    StartDuelComponent.prototype.selectDate = function (control, daysAmount) {
        $(".work-deadline-container input[type='button']").removeClass("selected");
        $(".work-deadline-container input[type='date']").val('');
        $(control).addClass("selected");
        this.selectedDate = this.getDeadline(daysAmount);
        var datePicker = document.getElementById("deadline-picker");
        if (this.validationService.isObjectValid(datePicker)) {
            datePicker.valueAsDate = new Date(this.selectedDate);
        }
    };
    StartDuelComponent.prototype.onSelectedUserChange = function (user) {
        this.selectedUser.value = user;
    };
    StartDuelComponent.prototype.onSelectedTaskText = function (text) {
        this.selectedTaskText = text;
    };
    StartDuelComponent.prototype.startDuel = function () {
        var _this = this;
        if (this.validateDuelData()) {
            var selectedCategory = this.startDuelInfo.commonCategories.filter(function (category) { return category.selected; })[0];
            this.startDuelService.startDuel(this.selectedUser.id, selectedCategory._id, this.selectedTaskText, this.selectedDate).subscribe(function (response) {
                if (response.success) {
                    _this.router.navigate(['/myduels']);
                    _this.alertService.success("Duel request has been sent. " + _this.selectedUser.value + " has 24 hours to answer");
                }
                else {
                    console.log(response.error);
                    _this.alertService.error("Duel request could not be sent. Please try again");
                }
            });
        }
    };
    StartDuelComponent.prototype.initializeData = function () {
        var _this = this;
        this.startDuelService.getStartDuelInfo().subscribe(function (response) {
            if (response.success) {
                _this.startDuelInfo = response.data;
                if (!_this.startDuelInfo.isPaidAccount && (_this.startDuelInfo.duelsLeft === 0 || _this.startDuelInfo.currentDuels > 0)) {
                    _this.canStartDuel = false;
                }
                else {
                    _this.canStartDuel = true;
                    var user = _this.startDuelService.selectedUser || _this.getEmptyUser();
                    user.commonCategories = _this.startDuelInfo.commonCategories;
                    _this.selectUser(user);
                    _this.initializeDatePicker();
                    setTimeout(function () {
                        _this.selectDate($(".work-deadline-container input[type='button']").first(), 2);
                    });
                }
                _this.initializeDuelsAmount(_this.startDuelInfo);
            }
        });
    };
    StartDuelComponent.prototype.onDatePickerSelect = function (date) {
        $(".work-deadline-container input[type='button']").removeClass("selected");
        this.selectedDate = date;
        var dateObj = new Date(this.selectedDate);
        dateObj.setDate(dateObj.getDate() + 1);
        var datePicker = document.getElementById("deadline-picker");
        datePicker.valueAsDate = dateObj;
    };
    StartDuelComponent.prototype.initializeDatePicker = function () {
        var _this = this;
        setTimeout(function () {
            $('#datepicker').datepicker({
                inline: true,
                showOtherMonths: true,
                dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                minDate: _this.getMinDate(),
                maxDate: _this.getMaxDate(),
                onSelect: _this.onDatePickerSelect
            });
            $("#datepicker").hide();
            $('#deadline-picker').click(function () {
                setTimeout(function () {
                    $("#datepicker").show();
                });
            });
            $(document).click(function () {
                if ($('#datepicker').is(":visible")) {
                    $("#datepicker").hide();
                }
            });
            $("#datepicker").click(function (event) {
                event.stopPropagation();
            });
        });
    };
    StartDuelComponent.prototype.initializeDuelsAmount = function (startDuelInfo) {
        var _this = this;
        this.duelsLeft = startDuelInfo.isPaidAccount ? '∞' : startDuelInfo.duelsLeft;
        this.currentDuels = startDuelInfo.isPaidAccount ? '∞' : startDuelInfo.currentDuels;
        setTimeout(function () {
            _this.tooltipService.initializeTooltip('.custom-tooltip');
        }, 0);
    };
    StartDuelComponent.prototype.selectUser = function (user) {
        this.selectedUser = user;
        this.selectedUser.commonCategories.forEach(function (category) { return category.selected = false; });
        this.startDuelInfo.commonCategories = this.selectedUser.commonCategories;
        setTimeout(function () {
            $("input:radio").prop('checked', false);
            $("input[data-user-id=" + user.id + "]:radio").prop('checked', true);
        });
    };
    StartDuelComponent.prototype.getEmptyUser = function () {
        return {
            id: null,
            value: null,
            commonCategories: []
        };
    };
    StartDuelComponent.prototype.getDeadline = function (daysAmount) {
        var date = new Date();
        // Adding 1 including one day of duel's waiting status
        date.setDate(date.getDate() + daysAmount + 1);
        //date.setSeconds(date.getSeconds() + 120);
        return date.toISOString();
    };
    StartDuelComponent.prototype.goToPage = function (page) {
        this.router.navigate([page]);
    };
    StartDuelComponent.prototype.validateDuelData = function () {
        if (!this.validationService.isObjectValid(this.selectedUser.id)) {
            alert("Please, select user");
            return false;
        }
        var selectedCategory = this.startDuelInfo.commonCategories.filter(function (category) { return category.selected; })[0];
        if (!this.validationService.isObjectValid(selectedCategory)) {
            alert("Please, select category");
            return false;
        }
        if (!this.validationService.isStringValid(this.selectedTaskText)) {
            alert("Please, put a task description");
            return false;
        }
        if (this.selectedTaskText.length < 100) {
            alert("Please, put a bigger task description");
            return false;
        }
        if (!this.validationService.isObjectValid(this.selectedDate)) {
            alert("Please, select deadline");
            return false;
        }
        return true;
    };
    StartDuelComponent.prototype.getMinDate = function () {
        return new Date(this.getDeadline(this.minDaysAmount));
    };
    StartDuelComponent.prototype.getMaxDate = function () {
        var date = new Date();
        date.setDate(date.getDate() + this.maxDaysAmount);
        return date;
    };
    StartDuelComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-start-duel',
            template: __webpack_require__("./src/app/components/start-duel/start-duel.component.html"),
            styles: [__webpack_require__("./src/app/components/start-duel/start-duel.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_start_duel_service_start_duel_service__["a" /* StartDuelService */],
            __WEBPACK_IMPORTED_MODULE_3__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__services_validation_validation_service__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_5__services_date_formatter_date_formatter_service__["a" /* DateFormatterService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_7__services_alert_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_8__services_tooltip_tooltip_service__["a" /* TooltipService */],
            __WEBPACK_IMPORTED_MODULE_10__services_category_category_service__["a" /* CategoryService */]])
    ], StartDuelComponent);
    return StartDuelComponent;
}());



/***/ }),

/***/ "./src/app/components/user/services/follow.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FollowService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var FollowService = /** @class */ (function () {
    function FollowService(http, headersProviderService, tokenProviderService, authService) {
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.tokenProviderService = tokenProviderService;
        this.authService = authService;
    }
    FollowService.prototype.followUser = function (userId) {
        this.tokenProviderService.loadToken();
        var userFollower = {
            userId: userId,
            followerId: this.authService.getCurrentUser().id
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/user/follow", userFollower, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    FollowService.prototype.unfollowUser = function (userId) {
        this.tokenProviderService.loadToken();
        var userFollower = {
            userId: userId,
            followerId: this.authService.getCurrentUser().id
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/user/unfollow", userFollower, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    FollowService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_3__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_4__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__["a" /* AuthService */]])
    ], FollowService);
    return FollowService;
}());



/***/ }),

/***/ "./src/app/components/user/services/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UserService = /** @class */ (function () {
    function UserService(http, headersProviderService, tokenProviderService, authService) {
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.tokenProviderService = tokenProviderService;
        this.authService = authService;
    }
    UserService.prototype.getUserProfileByUsername = function (username) {
        this.tokenProviderService.loadToken();
        var currentUser = this.authService.getCurrentUser();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/user/profile", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                username: username,
                currentUsername: currentUser ? currentUser.username : null
            }
        })
            .map(function (res) { return res.json(); });
    };
    UserService.prototype.getUserDuels = function (userId, statuses) {
        if (statuses === void 0) { statuses = []; }
        this.tokenProviderService.loadToken();
        var data = {
            userId: userId,
            statuses: statuses,
            currentUserId: this.authService.getCurrentUserId()
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/user/duels", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    UserService.prototype.turnOffHelp = function () {
        this.tokenProviderService.loadToken();
        var data = {
            userId: this.authService.getCurrentUserId()
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/user/turn-off-help", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_3__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_4__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__["a" /* AuthService */]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/components/user/user.component.css":
/***/ (function(module, exports) {

module.exports = ".user-profile {\r\n    padding-bottom: 100px;\r\n}\r\n\r\n.duels-visibility {\r\n    opacity: 0;\r\n}\r\n\r\n.duels-visibility.visible {\r\n    opacity: 1;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.profile-body {\r\n    border: 1px solid #D8E2E9;\r\n    padding: 40px;\r\n    margin-top: 70px;\r\n    margin-bottom: 50px;\r\n}\r\n\r\n.profile-body .title {\r\n    margin-top: 5px;\r\n}\r\n\r\n.profile-body .title img {\r\n    margin-left: 1px;\r\n    margin-top: -2px;\r\n}\r\n\r\n.profile-body .title .rating {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-size: 20px;\r\n    color: #C1C7CB;\r\n    margin-left: 12px;\r\n}\r\n\r\n.profile-body p.info {\r\n    margin:0;\r\n    line-height: 21px;\r\n    padding-left: 25px;\r\n    margin-bottom: 8px;\r\n}\r\n\r\n.profile-body p.username {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2215%22 height%3D%2215%22 viewBox%3D%220 0 15 15%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M7.50176 9.44991C6.42626 9.44991 5.55176 8.57541 5.55176 7.49991C5.55176 6.42516 6.42626 5.55066 7.50176 5.55066C8.57726 5.55066 9.45176 6.42516 9.45176 7.49991C9.45176 8.57541 8.57726 9.44991 7.50176 9.44991ZM7.5 0C3.3645 0 0 3.3645 0 7.5C0 11.6362 3.3645 15 7.5 15C9.02925 15 10.4948 14.5492 11.7397 13.6943L10.8907 12.4575C9.897 13.14 8.72475 13.5 7.5 13.5C4.19175 13.5 1.5 10.809 1.5 7.5C1.5 4.19175 4.19175 1.5 7.5 1.5C10.8083 1.5 13.5 4.19175 13.5 7.5V8.08725C13.5 9.12825 13.0515 9.75 12.3 9.75L12.2985 9.7635C12.2528 9.7545 12.2055 9.75 12.1575 9.75H12C11.5778 9.75 10.95 9.1365 10.95 8.72325V7.5C10.95 5.59875 9.402 4.05075 7.5 4.05075C5.598 4.05075 4.05 5.59875 4.05 7.5C4.05 9.40275 5.598 10.95 7.5 10.95C8.4255 10.95 9.2625 10.5795 9.88275 9.984C10.3695 10.7025 11.1967 11.25 12 11.25L12.0015 11.2365C12.0488 11.2455 12.0967 11.25 12.147 11.25H12.3C13.9147 11.25 15 9.9795 15 8.08725V7.5C15 3.3645 11.6355 0 7.5 0Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0 50% no-repeat;\r\n}\r\n\r\n.profile-body p.location {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2212%22 height%3D%2218%22 viewBox%3D%220 0 12 18%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M6 4.97688C5.52771 4.97688 5.14286 5.34931 5.14286 5.80636C5.14286 6.26423 5.52771 6.63584 6 6.63584C6.47229 6.63584 6.85714 6.26423 6.85714 5.80636C6.85714 5.34931 6.47229 4.97688 6 4.97688ZM6 8.2948C4.58229 8.2948 3.42857 7.17832 3.42857 5.80636C3.42857 4.4344 4.58229 3.31792 6 3.31792C7.41771 3.31792 8.57143 4.4344 8.57143 5.80636C8.57143 7.17832 7.41771 8.2948 6 8.2948Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M6 1.65896C3.63686 1.65896 1.71429 3.51948 1.71429 5.80636C1.71429 7.95554 4.37743 12.0963 6 14.3425C7.62171 12.0955 10.2857 7.95388 10.2857 5.80636C10.2857 3.51948 8.36314 1.65896 6 1.65896ZM6 17.1429C3.978 14.5325 0 9.07119 0 5.80636C0 2.60457 2.69143 0 6 0C9.30857 0 12 2.60457 12 5.80636C12 9.07119 8.022 14.5333 6 17.1429Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0 50% no-repeat;\r\n}\r\n\r\n.profile-body p.categories {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2215%22 viewBox%3D%220 0 16 15%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M14.2951 2.77201L7.38681 9.77656C7.26044 9.55873 7.11622 9.35145 6.93557 9.16829C6.75493 8.98513 6.54678 8.84192 6.32971 8.71755L13.244 1.70621C13.5242 1.42205 14.0141 1.42205 14.2951 1.70621C14.4356 1.84867 14.513 2.03861 14.513 2.23911C14.513 2.44036 14.4356 2.62955 14.2951 2.77201ZM5.88452 12.3664C5.10916 13.1526 3.91082 13.4021 3.01131 13.4699C3.29306 12.8081 3.26555 12.2413 3.24325 11.7808C3.17858 10.4542 4.04834 9.79238 4.83336 9.79238C5.23033 9.79238 5.60352 9.94916 5.88452 10.2341C6.46659 10.8235 6.46734 11.7747 5.88452 12.3664ZM16 2.23909C16 1.63534 15.7681 1.06777 15.3473 0.641155C14.505 -0.213592 13.0354 -0.213593 12.1931 0.640401L4.60382 8.33539C3.91247 8.38664 3.23672 8.65573 2.73122 9.16827C1.67932 10.2341 1.72318 11.1325 1.75812 11.8546C1.7886 12.475 1.8109 12.9227 1.07197 13.6719L0 14.7588C1.35 14.9329 1.66817 15 2.41156 15C3.52367 15 5.59104 14.7965 6.93584 13.4322C7.45026 12.9114 7.74018 12.23 7.78776 11.5019L15.3466 3.83854C15.7681 3.41117 16 2.8436 16 2.23909Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0 50% no-repeat;\r\n}\r\n\r\n.profile-body p.account-creation-date {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2215%22 viewBox%3D%220 0 17 15%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M6.18215 4.49999C5.32985 4.49999 4.63672 5.17199 4.63672 5.99999C4.63672 6.82724 5.32985 7.49999 6.18215 7.49999C7.03446 7.49999 7.72758 6.82724 7.72758 5.99999C7.72758 5.17199 7.03446 4.49999 6.18215 4.49999ZM6.18169 8.99999C4.47707 8.99999 3.09082 7.65449 3.09082 5.99999C3.09082 4.34549 4.47707 2.99999 6.18169 2.99999C7.8863 2.99999 9.27255 4.34549 9.27255 5.99999C9.27255 7.65449 7.8863 8.99999 6.18169 8.99999Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M1.66797 13.5H10.6956C10.2335 12.144 8.50574 11.25 6.18141 11.25C3.85707 11.25 2.13005 12.144 1.66797 13.5ZM12.3635 15H0V14.25C0 11.6003 2.54224 9.75 6.18173 9.75C9.822 9.75 12.3635 11.6003 12.3635 14.25V15Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M17.0001 2.25H14.6819V0H13.1365V2.25H10.8184V3.75H13.1365V6H14.6819V3.75H17.0001V2.25Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0 50% no-repeat;\r\n}\r\n\r\n.profile-body .info-column {\r\n    margin-left: 30px;\r\n}\r\n\r\n.profile-body .description {\r\n    margin-top: -4px;\r\n    margin-bottom: 21px;\r\n}\r\n\r\n.profile-body .button {\r\n    margin-top: 28px;\r\n    margin-right: 20px;\r\n}\r\n\r\n.profile-body .button:last-child {\r\n    margin-right: 29px;\r\n}\r\n\r\n.profile-body .featured {\r\n    display: inline-block;\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2222%22 viewBox%3D%220 0 16 22%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M0 22V0H16V22L8 13.8947L0 22Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0 50% no-repeat;\r\n    height: 22px;\r\n    padding-left: 24px;\r\n    margin-left: -9px;\r\n}\r\n\r\n.profile-body .featured p {\r\n    color: #F55045 !important;\r\n}\r\n\r\n.profile-body .number-block {\r\n    display: inline-block;\r\n    text-align: right;\r\n    margin-left: 72px;\r\n    margin-top: 15px;\r\n}\r\n\r\n.profile-body .number-block:first-child {\r\n    margin-left: 0;\r\n}\r\n\r\n.profile-body .number-block p {\r\n    margin-bottom: 5px;\r\n}\r\n\r\n.profile-body .number-block p:first-child {\r\n    font-size: 25px;\r\n    color: #262626;\r\n}\r\n\r\n.profile-body .social-networks {\r\n    text-align: right;\r\n    margin-top: 185px;\r\n}\r\n\r\n.profile-body .social-networks > div {\r\n    display: inline-block;\r\n    margin-left: 33px;\r\n}\r\n\r\n.profile-body .social-networks > div:first-child {\r\n    margin-left: 0;\r\n}\r\n\r\n.profile-body .featured-points-space {\r\n    height: 50px;\r\n}\r\n\r\n.duel-hover {\r\n    height: 250px;\r\n    background-color: rgba(255, 255, 255, 0.9);\r\n    margin-top: -250px;\r\n    position: relative;\r\n    z-index: 200;\r\n    text-align: center;\r\n    padding-top: 55px;\r\n}\r\n\r\n.duel-hover p.block-paragraph {\r\n    margin-top: 15px;\r\n    width: 410px;\r\n    display: inline-block;\r\n}\r\n\r\n.duel-hover img {\r\n    margin-right: 7px;\r\n}\r\n\r\n.duel-hover .secondary-button {\r\n    padding: 0;\r\n    height: 40px;\r\n    padding-left: 14px;\r\n    padding-right: 14px;\r\n    margin-left: 15px;\r\n}\r\n\r\n.duel-hover p.other-button {\r\n    margin-right: 15px;\r\n}\r\n\r\n.duel-hover p.other-button a {\r\n    opacity: 1;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.duel-hover p.other-button a:hover {\r\n    opacity: .7;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.duel-hover p.other-button > a {\r\n    color: #5D5D5D !important;\r\n    border-bottom: 1px dotted #5D5D5D !important;\r\n}\r\n\r\n.hide-block {\r\n    border: 1px solid #D8E2E9;\r\n    height: 40px;\r\n    top: -250px;\r\n    position: relative;\r\n    z-index: 200;\r\n    background-color: #fff;\r\n    opacity: 0;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n    padding-left: 16px;\r\n    padding-right: 16px;\r\n}\r\n\r\n.hide-block p,\r\n.hide-block span{\r\n    line-height: 40px;\r\n}\r\n\r\n.hide-block span {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    line-height: 17px;\r\n    color: #F55045;\r\n    position: relative;\r\n    top: 11px;\r\n}\r\n\r\n.duel-wrapper:hover .hide-block {\r\n    opacity: 1;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.more-categories {\r\n    color: #5D5D5D;\r\n    opacity: 0.5;\r\n    border-bottom: 1px dotted rgb(174,174,174);\r\n    line-height: 15px;\r\n    cursor: pointer;\r\n    margin-left: 7px;\r\n}\r\n\r\n.can-start-duel-tooltip {\r\n    width: 150px;\r\n    height: 50px;\r\n    position: absolute;\r\n}"

/***/ }),

/***/ "./src/app/components/user/user.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"user-profile\" *ngIf=\"userSuccess === true\">\r\n  <app-user-list></app-user-list>\r\n  <div class=\"profile-body\">\r\n    <div class=\"left\">\r\n      <app-profile-picture elementClass=\"large\" fullname={{user.fullname}} userId={{user.id}} backgroundImage={{user.imageUrl}}\r\n        backgroundColor={{user.imageColor}}>\r\n      </app-profile-picture>\r\n    </div>\r\n    <div class=\"left info-column\">\r\n      <h2 class=\"title\">\r\n        {{user.fullname}}\r\n        <img *ngIf=\"user.isPaidAccount\" width=\"16\" height=\"16\" src=\"../../../assets/img/general/silver-label-icon.svg\">\r\n        <span class=\"rating\">#{{user.rating}}</span>\r\n      </h2>\r\n      <p class=\"description\">{{user.description}}</p>\r\n      <p class=\"info username\">@{{user.username}}</p>\r\n      <p class=\"info location\" *ngIf=\"user.location\">{{user.location}}</p>\r\n      <p class=\"info categories inline\" *ngIf=\"user.categories\">{{firstCategory}}</p>\r\n      <div #categoriesTooltip class=\"inline custom-tooltip\" *ngIf=\"tooltipCategories.length > 0\">\r\n        <span class=\"tooltiptext\">{{tooltipCategories}}</span>\r\n        <p class=\"more-categories\">and {{tooltipCategories.split(',').length}} more</p>\r\n      </div>\r\n      <p class=\"info account-creation-date\" *ngIf=\"user.accountCreationDate\">{{dateFormatter.toShortFormat(user.accountCreationDate)}}</p>\r\n      <div *ngIf=\"isCurrentUser\" class=\"inline\">\r\n        <a class=\"button secondary-button\" routerLink=\"/settings\">\r\n          <span>Settings</span>\r\n        </a>\r\n      </div>\r\n      <div *ngIf=\"!isCurrentUser\" class=\"inline\">\r\n        <div *ngIf=\"authService.getCurrentUserId() !== null\">\r\n          <a class=\"button secondary-button\" *ngIf=\"!isFollowed()\" (click)=\"followUser()\">\r\n            <span>Follow</span>\r\n          </a>\r\n          <a class=\"button secondary-button\" *ngIf=\"isFollowed()\" (click)=\"unfollowUser()\">\r\n            <span>Unfollow</span>\r\n          </a>\r\n          <a id=\"startDuelButton\" class=\"button primary-button inline\" [class.disabled]=\"!canStartDuel.canStartDuel\" (click)=\"startDuel()\">\r\n            <span>Start a duel</span>\r\n          </a>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"user.featuredPoints > 0\" class=\"inline\">\r\n        <div class=\"inline featured-points-space\"></div>\r\n        <div class=\"featured\">\r\n          <p>x{{user.featuredPoints}}</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"right\">\r\n      <div>\r\n        <div class=\"number-block\">\r\n          <p>{{user.skillPoints}}</p>\r\n          <p>Skill</p>\r\n        </div>\r\n        <div class=\"number-block\">\r\n          <p>{{user.duelsWon}}</p>\r\n          <p>Duels Won</p>\r\n        </div>\r\n        <div class=\"number-block clickable\" (click)=\"openUserFollowers()\">\r\n          <p>{{user.followers.length}}</p>\r\n          <p>Followers</p>\r\n        </div>\r\n        <div class=\"number-block clickable\" (click)=\"openUserFollowing()\">\r\n          <p>{{user.following.length}}</p>\r\n          <p>Following</p>\r\n        </div>\r\n      </div>\r\n      <div>\r\n        <div class=\"social-networks\" *ngIf=\"user.socialNetworks\">\r\n          <div *ngIf=\"user.socialNetworks.behance\">\r\n            <a target=\"_blank\" href=\"//{{user.socialNetworks.behance}}\">\r\n              <img src=\"../../../assets/img/profile/behance.svg\">\r\n            </a>\r\n          </div>\r\n          <div *ngIf=\"user.socialNetworks.dribbble\">\r\n            <a target=\"_blank\" href=\"//{{user.socialNetworks.dribbble}}\">\r\n              <img src=\"../../../assets/img/profile/dribbble.svg\">\r\n            </a>\r\n          </div>\r\n          <div *ngIf=\"user.socialNetworks.twitter\">\r\n            <a target=\"_blank\" href=\"//{{user.socialNetworks.twitter}}\">\r\n              <img src=\"../../../assets/img/profile/twitter.svg\">\r\n            </a>\r\n          </div>\r\n          <div *ngIf=\"user.socialNetworks.instagram\">\r\n            <a target=\"_blank\" href=\"//{{user.socialNetworks.instagram}}\">\r\n              <img src=\"../../../assets/img/profile/instagram.svg\">\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <div *ngIf=\"!canStartDuel.canStartDuel && !isCurrentUser\" #canStartDuelTooltip class=\"can-start-duel-tooltip inline custom-tooltip\">\r\n      <span *ngIf=\"canStartDuel.message !== null && canStartDuel.message !== undefined && canStartDuel.message.length > 0\" class=\"tooltiptext\">{{canStartDuel.message}}</span>\r\n    </div>\r\n  </div>\r\n  <div #duelsContainer *ngIf=\"isCurrentUser\" class=\"duels-visibility\">\r\n    <div *ngIf=\"duels.length > 0\" class=\"duels-container\">\r\n      <div *ngFor=\"let duel of duels\" class=\"duel-wrapper\">\r\n        <app-duel [duel]=\"duel\" [duelOwner]=\"user.id\" (notify)=\"onNotify($event)\" (click)=\"openVotePage(duel)\"></app-duel>\r\n        <div *ngIf=\"user.isPaidAccount && ((duel.leftUser.user.id === user.id && !duel.leftUser.isHidden) || (duel.rightUser.user.id === user.id && !duel.rightUser.isHidden))\"\r\n          class=\"hide-block\">\r\n          <p class=\"left\">Duelity Silver allows you to hide duels from your profile</p>\r\n          <span class=\"right\" (click)=\"changeDuelVisibility(duel.id, true)\">Hide duel</span>\r\n          <div class=\"clearfix\"></div>\r\n        </div>\r\n        <div *ngIf=\"user.isPaidAccount && ((duel.leftUser.user.id === user.id && duel.leftUser.isHidden) || (duel.rightUser.user.id === user.id && duel.rightUser.isHidden))\"\r\n          class=\"duel-hover\">\r\n          <div>\r\n            <img class=\"inline\" src=\"../../../assets/img/duel/hidden-duel-icon.svg\">\r\n            <h3 class=\"inline\">This duel is hidden from your profile</h3>\r\n          </div>\r\n          <p class=\"block-paragraph\">Only you can see it here though it’s still visible from your opponent profile and other pages</p>\r\n          <div class=\"clearfix\"></div>\r\n          <p class=\"inline other-button\">\r\n            <a (click)=\"openVotePage(duel)\">Open Duel</a>\r\n          </p>\r\n          <a class=\"button secondary-button\" (click)=\"changeDuelVisibility(duel.id, false)\">\r\n            <span>Make Public</span>\r\n          </a>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div #duelsContainer *ngIf=\"!isCurrentUser\" class=\"duels-visibility\">\r\n    <div *ngIf=\"duels.length > 0\" class=\"duels\">\r\n      <div class=\"duels-container\">\r\n        <div *ngFor=\"let duel of duels\" (click)=\"openVotePage(duel)\" class=\"duel-wrapper\">\r\n          <app-duel *ngIf=\"(duel.leftUser.user.id === user.id && !duel.leftUser.isHidden) || (duel.rightUser.user.id === user.id && !duel.rightUser.isHidden)\"\r\n            [duel]=\"duel\" [duelOwner]=\"user.id\" (notify)=\"onNotify($event)\"></app-duel>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div *ngIf=\"userSuccess === false\">\r\n  <h1>User not found!</h1>\r\n</div>"

/***/ }),

/***/ "./src/app/components/user/user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__enums_duel_status__ = __webpack_require__("./src/app/enums/duel-status.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_user_service__ = __webpack_require__("./src/app/components/user/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_date_formatter_date_formatter_service__ = __webpack_require__("./src/app/services/date-formatter/date-formatter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_follow_service__ = __webpack_require__("./src/app/components/user/services/follow.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_duel_status_message_duel_status_message_service__ = __webpack_require__("./src/app/services/duel-status-message/duel-status-message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_duel_timer_duel_timer_service__ = __webpack_require__("./src/app/services/duel-timer/duel-timer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_start_duel_service_start_duel_service__ = __webpack_require__("./src/app/services/start-duel-service/start-duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_vote_vote_service__ = __webpack_require__("./src/app/services/vote/vote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__child_components_user_list_services_user_list_service__ = __webpack_require__("./src/app/child-components/user-list/services/user-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__enums_user_list_order_criteria__ = __webpack_require__("./src/app/enums/user-list-order-criteria.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_tooltip_tooltip_service__ = __webpack_require__("./src/app/services/tooltip/tooltip.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var UserComponent = /** @class */ (function () {
    function UserComponent(activatedRoute, userService, authService, router, dateFormatter, followService, duelStatusMessageService, duelTimer, startDuelService, voteService, userListService, duelService, titleService, tooltipService, validationService) {
        this.activatedRoute = activatedRoute;
        this.userService = userService;
        this.authService = authService;
        this.router = router;
        this.dateFormatter = dateFormatter;
        this.followService = followService;
        this.duelStatusMessageService = duelStatusMessageService;
        this.duelTimer = duelTimer;
        this.startDuelService = startDuelService;
        this.voteService = voteService;
        this.userListService = userListService;
        this.duelService = duelService;
        this.titleService = titleService;
        this.tooltipService = tooltipService;
        this.validationService = validationService;
        this.userSuccess = undefined;
        this.isCurrentUser = false;
        this.duels = [];
        this.canStartDuel = null;
        this.tooltipCategories = [];
    }
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.activatedRoute.params.subscribe(function (params) {
            _this.username = params['username'];
            _this.getUser();
        });
    };
    UserComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    UserComponent.prototype.onNotify = function (userId) {
        var _this = this;
        this.userService.getUserDuels(userId, [__WEBPACK_IMPORTED_MODULE_2__enums_duel_status__["a" /* DuelStatus */].Voting, __WEBPACK_IMPORTED_MODULE_2__enums_duel_status__["a" /* DuelStatus */].Ended]).subscribe(function (response) {
            if (response.success) {
                _this.duels = response.duels;
            }
            else {
                console.log(response.error);
            }
        });
    };
    UserComponent.prototype.getUser = function () {
        var _this = this;
        this.userService.getUserProfileByUsername(this.username).subscribe(function (response) {
            _this.userSuccess = response.success;
            if (response.success) {
                _this.isCurrentUser = _this.authService.getCurrentUserId() === response.data.user.id;
                _this.user = response.data.user;
                _this.canStartDuel = response.data.canStartDuel;
                _this.initializeCategories(_this.user.categories);
                _this.initializeTooltips();
                _this.titleService.setTitle(_this.user.fullname + " - Duelity");
                _this.userService.getUserDuels(_this.user.id, [__WEBPACK_IMPORTED_MODULE_2__enums_duel_status__["a" /* DuelStatus */].Voting, __WEBPACK_IMPORTED_MODULE_2__enums_duel_status__["a" /* DuelStatus */].Ended]).subscribe(function (data) {
                    if (data.success) {
                        _this.duels = data.duels;
                        _this.showDuels();
                    }
                });
            }
        });
    };
    UserComponent.prototype.followUser = function () {
        var _this = this;
        this.followService.followUser(this.user.id).subscribe(function (data) {
            if (data.success) {
                _this.getUser();
            }
        });
    };
    UserComponent.prototype.unfollowUser = function () {
        var _this = this;
        this.followService.unfollowUser(this.user.id).subscribe(function (data) {
            if (data.success) {
                _this.getUser();
            }
        });
    };
    UserComponent.prototype.isFollowed = function () {
        var currentUserId = this.authService.getCurrentUserId();
        return this.user && this.user.followers.filter(function (follow) { return follow.followerId === currentUserId; })[0];
    };
    UserComponent.prototype.openUserFollowers = function () {
        var followerIds = this.user.followers.map(function (follower) { return follower.followerId; });
        if (followerIds.length > 0) {
            this.userListService.getUsersAndOpenModal("Followers", followerIds, __WEBPACK_IMPORTED_MODULE_12__enums_user_list_order_criteria__["a" /* UserListOrderCriteria */].SkillPoints);
        }
    };
    UserComponent.prototype.openUserFollowing = function () {
        var followingIds = this.user.following.map(function (following) { return following.userId; });
        if (followingIds.length > 0) {
            this.userListService.getUsersAndOpenModal("Following", followingIds, __WEBPACK_IMPORTED_MODULE_12__enums_user_list_order_criteria__["a" /* UserListOrderCriteria */].SkillPoints);
        }
    };
    UserComponent.prototype.changeDuelVisibility = function (duelId, hide) {
        var _this = this;
        this.hideDuels();
        this.duelService.changeDuelVisibility(duelId, hide).subscribe(function (response) {
            if (response.success) {
                _this.duels = response.duels;
                _this.showDuels();
            }
            else {
                console.log("Something goes wrong during changing duel visibility");
            }
        });
    };
    UserComponent.prototype.startDuel = function () {
        this.startDuelService.selectedUser = {
            id: this.user.id,
            value: this.user.fullname,
            username: this.user.username
        };
        this.router.navigate(['start']);
    };
    UserComponent.prototype.openVotePage = function (duel) {
        this.voteService.openVotePage(duel);
    };
    UserComponent.prototype.initializeCategories = function (categories) {
        categories = categories.split(',');
        if (categories.length > 1) {
            this.firstCategory = categories.shift();
            this.tooltipCategories = categories.join(",");
        }
        else {
            this.firstCategory = categories.shift();
        }
    };
    UserComponent.prototype.showDuels = function () {
        var _this = this;
        setTimeout(function () { $(_this.duelsContainer.nativeElement).addClass("visible"); }, 2000);
    };
    UserComponent.prototype.hideDuels = function () {
        $(this.duelsContainer.nativeElement).removeClass("visible");
    };
    UserComponent.prototype.initializeTooltips = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.validationService.isObjectValid(_this.categoriesTooltip)) {
                _this.tooltipService.initializeTooltip(_this.categoriesTooltip.nativeElement);
            }
            if (_this.validationService.isObjectValid(_this.canStartDuelTooltip)) {
                _this.tooltipService.initializeTooltip(_this.canStartDuelTooltip.nativeElement);
                ;
                var position = $("#startDuelButton").offset();
                if (_this.validationService.isObjectValid(position)) {
                    $(_this.canStartDuelTooltip.nativeElement).offset({ top: position.top, left: position.left });
                }
            }
        }, 0);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('categoriesTooltip'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], UserComponent.prototype, "categoriesTooltip", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('canStartDuelTooltip'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], UserComponent.prototype, "canStartDuelTooltip", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('duelsContainer'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], UserComponent.prototype, "duelsContainer", void 0);
    UserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-user',
            template: __webpack_require__("./src/app/components/user/user.component.html"),
            styles: [__webpack_require__("./src/app/components/user/user.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__services_date_formatter_date_formatter_service__["a" /* DateFormatterService */],
            __WEBPACK_IMPORTED_MODULE_6__services_follow_service__["a" /* FollowService */],
            __WEBPACK_IMPORTED_MODULE_7__services_duel_status_message_duel_status_message_service__["a" /* DuelStatusMessageService */],
            __WEBPACK_IMPORTED_MODULE_8__services_duel_timer_duel_timer_service__["a" /* DuelTimerService */],
            __WEBPACK_IMPORTED_MODULE_9__services_start_duel_service_start_duel_service__["a" /* StartDuelService */],
            __WEBPACK_IMPORTED_MODULE_10__services_vote_vote_service__["a" /* VoteService */],
            __WEBPACK_IMPORTED_MODULE_11__child_components_user_list_services_user_list_service__["a" /* UserListService */],
            __WEBPACK_IMPORTED_MODULE_13__services_duel_service_duel_service__["a" /* DuelService */],
            __WEBPACK_IMPORTED_MODULE_14__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_15__services_tooltip_tooltip_service__["a" /* TooltipService */],
            __WEBPACK_IMPORTED_MODULE_16__services_validation_validation_service__["a" /* ValidationService */]])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/app/components/vote/vote.component.css":
/***/ (function(module, exports) {

module.exports = "#vote-page {\r\n    padding-top: 40px;\r\n    padding-bottom: 220px;\r\n}\r\n\r\n#vote-page.unauthorized {\r\n    padding-bottom: 100px;\r\n}\r\n\r\n.main.right {\r\n    width: 440px;\r\n}\r\n\r\n.header button {\r\n    width: 120px;\r\n}\r\n\r\n.task-section {\r\n    min-height: 249px;\r\n}\r\n\r\n.task-section p {\r\n    overflow: hidden;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.task-section #expand-button {\r\n    border: 0;\r\n    outline: 0;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #26AFFF;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n    background-color: transparent;\r\n    padding: 0;\r\n}\r\n\r\n.task-section #expand-button span {\r\n    position: relative;\r\n    top: 2px;\r\n    left: 2px;\r\n}\r\n\r\n.task-section #expand-button:hover,\r\n.task-section #expand-button:focus {\r\n    border: 0;\r\n    outline: 0;\r\n}\r\n\r\n.task-section #expand-button:hover {\r\n    color: #2686BE;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.task-section #expand-button svg.spin {\r\n    -webkit-animation: expand-button-spin .2s linear infinite;\r\n    animation: expand-button-spin .2s linear infinite;\r\n}\r\n\r\n@-webkit-keyframes expand-button-spin \r\n{ \r\n    100% \r\n    { \r\n        -webkit-transform: rotate(-110deg); \r\n    } \r\n}\r\n\r\n@keyframes expand-button-spin \r\n{ \r\n    100% \r\n    { \r\n        -webkit-transform: rotate(-110deg); \r\n        transform:rotate(-110deg); \r\n    } \r\n}\r\n\r\n.task-section #expand-button path {\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.task-section #expand-button:hover path {\r\n    fill: #2686BE;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.task-section .section-header {\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.task-section .section-header span.additional-info {\r\n    margin-left: 6px;\r\n}\r\n\r\n.section-header span.additional-info {\r\n    font-family: Menoe Grotesque Pro;\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 20px;\r\n    line-height: 24px;\r\n    color: #C1C7CB;\r\n}\r\n\r\n.voting-section .winner-label {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    text-align: center;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #5D5D5D;\r\n    background-color: #FFE226;\r\n    padding: 3px;\r\n    margin-left: 5px;\r\n}\r\n\r\n.voting-section .draw-label {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    text-align: center;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #5D5D5D;\r\n    background-color: #D8E2E9;\r\n    padding: 3px;\r\n}\r\n\r\n.voting-section .ended-state p.voting-ended-text {\r\n    margin-top: 29px;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.voting-section .ended-state p.small {\r\n    width: 354px;\r\n    margin-bottom: 25px;\r\n    line-height: 17px;\r\n}\r\n\r\n.voting-section .ended-state h3 {\r\n    margin-top: -4px;\r\n}\r\n\r\n.voting-section .input-wrapper {\r\n    margin-top: 27px;\r\n}\r\n\r\n.voting-section .unauthorized-state {\r\n    margin-top: 40px;\r\n}\r\n\r\n.voting-section .unauthorized-state p {\r\n    width: 150px;\r\n    display: inline-block;\r\n}\r\n\r\n.voting-section .create-account {\r\n    border: 0;\r\n    padding-left: 0;\r\n    padding-right: 0;\r\n    margin-left: 65px;\r\n    margin-right: 20px;\r\n}\r\n\r\n.voting-section .create-account:hover {\r\n    background-color: transparent;\r\n}\r\n\r\n.voting-section .sign-in {\r\n    padding-left: 20px;\r\n    padding-right: 20px;\r\n}\r\n\r\n.voting-section .unauthorized-state .sign-in-buttons {\r\n    position: relative;\r\n    top: -10px;\r\n}\r\n\r\n.voting-section .own-duel-state p {\r\n    margin-top: 40px;\r\n    width: 175px;\r\n}\r\n\r\n.comments-section,\r\n.voting-section {\r\n    margin-top: 70px;\r\n}\r\n\r\n.voting-section .custom-radio-wrapper:first-child {\r\n    margin-right: 55px;\r\n    margin-top: 40px;\r\n}\r\n\r\n.voting-section .voting-results p {\r\n    margin-top: 40px;\r\n    margin-bottom: 3px;\r\n}\r\n\r\n.comments-container {\r\n    margin-top: 40px;\r\n}\r\n\r\n.comments-container .empty-state {\r\n    text-align: center;\r\n}\r\n\r\n.comments-container .empty-state img {\r\n    margin-bottom: 15px;\r\n}\r\n\r\n.comments-container .empty-state h3 {\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.comments-container .empty-state p {\r\n    width: 252px;\r\n    display: inline-block;\r\n}\r\n\r\n.comments-section {\r\n    padding-right: 30px;\r\n}\r\n\r\n.comments-section .comment-block {\r\n    margin-bottom: 50px;\r\n}\r\n\r\n.comments-section .button-block {\r\n    position: relative;\r\n    top: 7px;\r\n}\r\n\r\n.comments-section .button-block button {\r\n    border: 0;\r\n    outline: 0;\r\n    color: #a5a5a5;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    line-height: 17px;\r\n    background-color: transparent;\r\n    -webkit-transition: color .2s ease-in-out;\r\n    transition: color .2s ease-in-out;\r\n    margin-left: 10px;\r\n}\r\n\r\n.comments-section .button-block button:hover {\r\n    border: 0;\r\n    outline: 0;\r\n    color: #5D5D5D;\r\n    -webkit-transition: color .2s ease-in-out;\r\n    transition: color .2s ease-in-out;\r\n}\r\n\r\n.comments-section .button-block button.selected {\r\n    color: #5D5D5D;\r\n}\r\n\r\n.comments-section .button-block button:focus {\r\n    border: 0;\r\n    outline: 0;\r\n}\r\n\r\n.comments-section .inline {\r\n    vertical-align: top;\r\n}\r\n\r\n.comments-section .avatar-wrapper {\r\n    margin-right: 20px;\r\n}\r\n\r\n.comments-section .comments-wrapper {\r\n    width: 575px;\r\n}\r\n\r\n.comments-section .comment-time {\r\n    color: #C1C7CB;\r\n}\r\n\r\n.comments-section .comment-author-name {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    line-height: 17px;\r\n    color: #262626;\r\n    cursor: pointer;\r\n}\r\n\r\n.comments-section .comment-text {\r\n    margin-top: -13px;\r\n}\r\n\r\n.comments-section .comment-footer {\r\n    margin-top: -10px;\r\n}\r\n\r\n.comments-section .votes-circle {\r\n    margin-right: 5px;\r\n    display: inline-block;\r\n}\r\n\r\n.comments-section .votes-circle .circle {\r\n    width: 10px;\r\n    height: 10px;\r\n}\r\n\r\n.comments-section .votes-circle .circle.selected {\r\n    background-color: #C1C7CB;\r\n    border: 2px solid #C1C7CB;\r\n}\r\n\r\n.comments-section .comment-footer span {\r\n    font-family: Menoe Grotesque Pro Italic;\r\n    font-size: 14px;\r\n    line-height: 21px;\r\n    letter-spacing: -0.02em;\r\n    color: #5D5D5D;\r\n    opacity: 0.7;\r\n    margin-top: -5px;\r\n}\r\n\r\n.comments-section .comment-footer .button-block {\r\n    visibility: hidden;\r\n    margin-top: -4px;\r\n}\r\n\r\n.comments-section .comment-block:hover .comment-footer .button-block {\r\n    visibility: visible;\r\n}\r\n\r\n.comments-section .pager-block {\r\n    width: 100%;\r\n}\r\n\r\n.comments-section .comment-footer .button-block button.warning {\r\n    color: #F55045;\r\n    padding-right: 0;\r\n}\r\n\r\n.comments-section .comment-footer .button-block button.warning:hover {\r\n    color: #A04437;\r\n}\r\n\r\n.share-section {\r\n    position: absolute;\r\n    bottom: 350px;\r\n}\r\n\r\n.share-section .share-social-block {\r\n    margin-top: 20px;\r\n}\r\n\r\n.share-section .share-social-block div {\r\n    display: inline-block;  \r\n    margin-right: 30px;\r\n    cursor: pointer;\r\n}\r\n\r\n.share-section .share-social-block div path {\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.share-section .share-social-block div.twitter:hover path {\r\n    fill: #26AFFF;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.share-section .share-social-block div.pinterest:hover path {\r\n    fill: #CB1F24;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.share-section .share-social-block div.facebook:hover path {\r\n    fill: #4867AD;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}"

/***/ }),

/***/ "./src/app/components/vote/vote.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"isPageLoaded\">\r\n  <div *ngIf=\"duel\" id=\"vote-page\" [class.unauthorized]=\"authService.getCurrentUser() === null\">\r\n    <app-report-comment></app-report-comment>\r\n    <div class=\"clearfix\"></div>\r\n    <div>\r\n      <div class=\"main left\">\r\n        <app-duel [duel]=\"duel\" [sizeClass]=\"'opened'\" [duelOwner]=\"currentUserId\" (notify)=\"onNotify($event)\" [leftUserModalId]=\"'leftUserModal'\"\r\n          [rightUserModalId]=\"'rightUserModal'\"></app-duel>\r\n        <div class=\"comments-section\">\r\n          <div>\r\n            <h2 class=\"section-header left\">Comments\r\n              <span class=\"additional-info\">{{commentsData.totalCount}}</span>\r\n            </h2>\r\n            <div *ngIf=\"commentsData.comments.length > 0\" class=\"button-block right\">\r\n              <button [class.selected]=\"selectedOrderDirection === orderDirection.Asc\" (click)=\"changeCommentsOrder(orderDirection.Asc)\">Oldest</button>\r\n              <button [class.selected]=\"selectedOrderDirection === orderDirection.Desc\" (click)=\"changeCommentsOrder(orderDirection.Desc)\">Newest</button>\r\n            </div>\r\n            <div class=\"clearfix\"></div>\r\n          </div>\r\n          <div class=\"comments-container\">\r\n            <div class=\"empty-state\" *ngIf=\"commentsData.comments.length === 0\">\r\n              <img width=\"336\" height=\"273\" src=\"../../../assets/img/vote/comments-empty-state-illustration.png\">\r\n              <h3>No comments for this duel yet</h3>\r\n              <p>\r\n                <span>Be the first to comment your thoughts on this duel</span>\r\n              </p>\r\n            </div>\r\n            <div *ngIf=\"commentsData.comments.length > 0\">\r\n              <div class=\"comment-block clickable\" *ngFor=\"let comment of commentsData.comments\">\r\n                <div class=\"avatar-wrapper inline clickable\">\r\n                  <app-profile-picture elementClass=\"x-medium\" userId={{comment.user.id}} fullname={{comment.user.fullname}} backgroundImage={{comment.user.imageUrl}}\r\n                    backgroundColor={{comment.user.imageColor}} (click)=\"goToPerson(comment.user.username)\">\r\n                  </app-profile-picture>\r\n                </div>\r\n                <div class=\"comments-wrapper inline\">\r\n                  <div>\r\n                    <div class=\"left comment-author-name\" (click)=\"goToPerson(comment.user.username)\">{{comment.user.fullname}}</div>\r\n                    <div class=\"right\">\r\n                      <p class=\"comment-time\">{{dateFormatter.toRelativeFormat(comment.timestamp)}}</p>\r\n                    </div>\r\n                    <div class=\"clearfix\"></div>\r\n                  </div>\r\n                  <p class=\"comment-text\">{{comment.text}}</p>\r\n                  <div class=\"votes-container\">\r\n                    <div class=\"comment-footer\">\r\n                      <div class=\"left\">\r\n                        <div *ngIf=\"comment.user.selectedUserId !== null && comment.user.id !== duel.leftUser.user.id && comment.user.id !== duel.rightUser.user.id\"\r\n                          class=\"votes-circle\">\r\n                          <div class=\"circle comment-circle\" [class.selected]=\"duel.leftUser.user.id === comment.user.selectedUserId\"></div>\r\n                          <div class=\"circle comment-circle\" [class.selected]=\"duel.rightUser.user.id === comment.user.selectedUserId\"></div>\r\n                        </div>\r\n                        <span *ngIf=\"duel.leftUser.user.id === comment.user.selectedUserId\" class=\"additional-info\">Vote for {{duel.leftUser.user.fullname}}</span>\r\n                        <span *ngIf=\"duel.rightUser.user.id === comment.user.selectedUserId\" class=\"additional-info\">Vote for {{duel.rightUser.user.fullname}}</span>\r\n                        <span *ngIf=\"comment.user.selectedUserId === null && comment.user.id !== duel.leftUser.user.id && comment.user.id !== duel.rightUser.user.id\"\r\n                          class=\"additional-info\">Commented after the duel has ended</span>\r\n                        <span *ngIf=\"comment.user.selectedUserId === null && (comment.user.id === duel.leftUser.user.id || comment.user.id === duel.rightUser.user.id)\"\r\n                          class=\"additional-info\">Duelist</span>\r\n                      </div>\r\n                      <div *ngIf=\"currentUserId !== null\" class=\"button-block right\">\r\n                        <!-- <button *ngIf=\"comment.user.id !== currentUserId\" (click)=\"reportService.openReportCommentModal(comment.id)\">Report</button> -->\r\n                        <button class=\"warning\" *ngIf=\"comment.user.id === currentUserId\" (click)=\"deleteComment(comment.id)\">Delete</button>\r\n                      </div>\r\n                      <div class=\"clearfix\"></div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"pager-block\">\r\n                <div *ngFor=\"let page of pages\" [class.active]=\"page.isActive\" [class.selected]=\"page.isCurrentPage\" (click)=\"setPage(page.symbol, page.isActive, page.isCurrentPage)\">\r\n                  <div>{{page.symbol}}</div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"main right\">\r\n        <div class=\"task-section\">\r\n          <h2 class=\"section-header\">Task\r\n            <span class=\"additional-info\">by {{duel.leftUser.user.fullname}}</span>\r\n          </h2>\r\n          <p #duelTaskBlock>{{duel.duelTask}}</p>\r\n          <button #expandButton id=\"expand-button\" (click)=\"toogleTaskExpand()\">\r\n            <svg *ngIf=\"isExpandedTask\" width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n              <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M3.03651 3.03692C0.853177 5.22025 0.853177 8.77318 3.03651 10.9565C5.21984 13.1398 8.77277 13.1398 10.9561 10.9565C13.1394 8.77318 13.1394 5.22025 10.9561 3.03692C8.77277 0.853584 5.21984 0.853584 3.03651 3.03692ZM11.9465 11.9465C9.21717 14.6758 4.77626 14.6758 2.04697 11.9465C-0.682323 9.21717 -0.682323 4.77626 2.04697 2.04697C4.77626 -0.682323 9.21717 -0.682323 11.9465 2.04697C14.6758 4.77626 14.6758 9.21717 11.9465 11.9465Z\"\r\n                fill=\"#26AFFF\" />\r\n              <path d=\"M3.03779 6.29657V7.69635L10.9574 7.69635V6.29657L3.03779 6.29657Z\" fill=\"#26AFFF\" />\r\n            </svg>\r\n            <svg *ngIf=\"!isExpandedTask\" width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n              <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M3.03794 3.03834C0.853578 5.2227 0.853578 8.7773 3.03794 10.9617C5.22229 13.146 8.77689 13.146 10.9612 10.9617C13.1456 8.7773 13.1456 5.2227 10.9612 3.03834C8.77689 0.853985 5.22229 0.853985 3.03794 3.03834ZM11.9521 11.9521C9.2215 14.6826 4.7785 14.6826 2.04793 11.9521C-0.682643 9.2215 -0.682643 4.7785 2.04793 2.04793C4.7785 -0.682643 9.2215 -0.682643 11.9521 2.04793C14.6826 4.7785 14.6826 9.2215 11.9521 11.9521Z\"\r\n                fill=\"#26AFFF\" />\r\n              <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M7.70109 3.03809L6.30064 3.03809V6.29952L3.03921 6.29952V7.69997H6.30064V10.9614H7.70109L7.70159 7.70046L10.9625 7.69997V6.29952L7.70109 6.29952V3.03809Z\"\r\n                fill=\"#26AFFF\" />\r\n            </svg>\r\n            <span>{{expandTaskButtonText}}</span>\r\n          </button>\r\n        </div>\r\n        <div class=\"voting-section\">\r\n          <h2 class=\"section-header\">Voting\r\n            <span class=\"additional-info\">{{duel.leftUser.votesAmount + duel.rightUser.votesAmount}}</span>\r\n          </h2>\r\n          <div *ngIf=\"authService.getCurrentUser() === null\" class=\"unauthorized-state\">\r\n            <p>Only authorised users can vote\r\n              <span>\r\n                <svg width=\"16\" height=\"16\" viewBox=\"0 0 16 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                  <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M7.99972 14.3997C4.47092 14.3997 1.59972 11.5285 1.59972 7.99972C1.59972 4.47092 4.47092 1.59972 7.99972 1.59972C11.5285 1.59972 14.3997 4.47092 14.3997 7.99972C14.3997 11.5285 11.5285 14.3997 7.99972 14.3997ZM8 0C3.5888 0 0 3.5888 0 8C0 12.412 3.5888 16 8 16C12.4112 16 16 12.412 16 8C16 3.5888 12.4112 0 8 0Z\"\r\n                    fill=\"#5D5D5D\" />\r\n                  <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M6.3998 4.80078H4.7998V7.20078H6.3998V4.80078Z\" fill=\"#5D5D5D\" />\r\n                  <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M9.59961 7.20078H11.1996V4.80078H9.59961V7.20078Z\" fill=\"#5D5D5D\" />\r\n                  <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M9.17559 8.37598L7.99959 9.55198L6.82439 8.37598L5.97559 9.22478L7.15159 10.4L5.97559 11.576L6.82439 12.4248L7.99959 11.2488L9.17559 12.4248L10.0244 11.576L8.84839 10.4L10.0244 9.22478L9.17559 8.37598Z\"\r\n                    fill=\"#5D5D5D\" />\r\n                </svg>\r\n              </span>\r\n            </p>\r\n            <div class=\"sign-in-buttons inline\">\r\n              <a class=\"button secondary-button create-account\" (click)=\"registerService.goToRegister()\">\r\n                <span>Create Account</span>\r\n              </a>\r\n              <a class=\"button secondary-button sign-in\" (click)=\"loginService.openModal()\">\r\n                <span>Sign In</span>\r\n              </a>\r\n            </div>\r\n          </div>\r\n          <div *ngIf=\"authService.getCurrentUser() !== null\">\r\n            <div *ngIf=\"duel.status === duelStatus.Voting && (currentUserId === duel.leftUser.user.id || currentUserId === duel.rightUser.user.id)\"\r\n              class=\"own-duel-state\">\r\n              <p>You can’t vote in your own duel\r\n                <span>\r\n                  <svg width=\"16\" height=\"16\" viewBox=\"0 0 16 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                    <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M5.5997 6C5.7101 6 5.7997 6.0896 5.7997 6.2C5.7997 6.42 5.3997 6.42 5.3997 6.2C5.3997 6.0896 5.4893 6 5.5997 6ZM5.59995 7.60078C6.37195 7.60078 6.99995 6.97278 6.99995 6.20078C6.99995 5.42878 6.37195 4.80078 5.59995 4.80078C4.82795 4.80078 4.19995 5.42878 4.19995 6.20078C4.19995 6.97278 4.82795 7.60078 5.59995 7.60078Z\"\r\n                      fill=\"#5D5D5D\" />\r\n                    <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M8.00007 14.3997C4.47127 14.3997 1.60007 11.5285 1.60007 7.99972C1.60007 4.47092 4.47127 1.59972 8.00007 1.59972C11.5289 1.59972 14.4001 4.47092 14.4001 7.99972C14.4001 11.5285 11.5289 14.3997 8.00007 14.3997ZM8 0C3.5888 0 0 3.5888 0 8C0 12.412 3.5888 16 8 16C12.4112 16 16 12.412 16 8C16 3.5888 12.4112 0 8 0Z\"\r\n                      fill=\"#5D5D5D\" />\r\n                    <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M7.99995 11.1993C6.98555 11.1993 6.05275 10.7337 5.44075 9.92086L4.16235 10.8833C5.07995 12.1017 6.47835 12.7993 7.99995 12.7993C9.52395 12.7993 10.924 12.1001 11.84 10.8801L10.56 9.91846C9.94955 10.7329 9.01595 11.1993 7.99995 11.1993Z\"\r\n                      fill=\"#5D5D5D\" />\r\n                    <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M8.7998 7.19961H11.9998V5.59961H8.7998V7.19961Z\" fill=\"#5D5D5D\" />\r\n                  </svg>\r\n                </span>\r\n              </p>\r\n              <div class=\"input-wrapper\">\r\n                <app-input (onValueChange)=\"onCommentTextChange($event)\" [value]=\"commentText\" [placeholder]=\"'Comment'\" [type]=\"inputType.Text\"\r\n                  [error]=\"descriptionErrorMessage\" [counterMinLength]=\"20\"></app-input>\r\n              </div>\r\n              <a class=\"button primary-button\" [class.disabled]=\"!validateComments(commentText)\" (click)=\"addComment()\">\r\n                <span>Comment</span>\r\n              </a>\r\n            </div>\r\n            <div *ngIf=\"duel.status === duelStatus.Voting && currentUserId !== duel.leftUser.user.id && currentUserId !== duel.rightUser.user.id && (!duel.leftUser.isSelected && !duel.rightUser.isSelected)\">\r\n              <div class=\"inline custom-radio-wrapper\">\r\n                <label class=\"custom-radio container\">{{duel.leftUser.user.fullname}}\r\n                  <input type=\"radio\" name=\"votedUser\" [(ngModel)]=\"votedUser\" value=\"{{duel.leftUser.user.id}}\">\r\n                  <span class=\"checkmark\"></span>\r\n                </label>\r\n              </div>\r\n              <div class=\"inline custom-radio-wrapper\">\r\n                <label class=\"custom-radio container\">{{duel.rightUser.user.fullname}}\r\n                  <input type=\"radio\" name=\"votedUser\" [(ngModel)]=\"votedUser\" value=\"{{duel.rightUser.user.id}}\">\r\n                  <span class=\"checkmark\"></span>\r\n                </label>\r\n              </div>\r\n              <div class=\"input-wrapper\">\r\n                <app-input (onValueChange)=\"onCommentTextChange($event)\" [value]=\"commentText\" [placeholder]=\"'Comment'\" [type]=\"inputType.Text\"\r\n                  [error]=\"descriptionErrorMessage\" [counterMinLength]=\"20\"></app-input>\r\n              </div>\r\n              <p>Comment on why are you voting this way. Participants value your feedback.</p>\r\n              <br>\r\n              <a class=\"button primary-button\" [class.disabled]=\"!validateVote(commentText)\" (click)=\"addVote()\">\r\n                <span>Vote</span>\r\n              </a>\r\n            </div>\r\n            <div class=\"voting-results\" *ngIf=\"duel.status === duelStatus.Voting && currentUserId !== duel.leftUser.user.id && currentUserId !== duel.rightUser.user.id && (duel.leftUser.isSelected || duel.rightUser.isSelected)\">\r\n              <p>Voted for</p>\r\n              <div class=\"votes-container\">\r\n                <div class=\"votes-circle inline\">\r\n                  <div class=\"circle\" [class.selected]=\"duel.leftUser.isSelected\"></div>\r\n                  <div class=\"circle\" [class.selected]=\"duel.rightUser.isSelected\"></div>\r\n                </div>\r\n                <h3 class=\"inline\" *ngIf=\"duel.leftUser.isSelected\">{{duel.leftUser.user.fullname}}</h3>\r\n                <h3 class=\"inline\" *ngIf=\"duel.rightUser.isSelected\">{{duel.rightUser.user.fullname}}</h3>\r\n              </div>\r\n              <div class=\"input-wrapper\">\r\n                <app-input (onValueChange)=\"onCommentTextChange($event)\" [value]=\"commentText\" [placeholder]=\"'Comment'\" [type]=\"inputType.Text\"\r\n                  [error]=\"descriptionErrorMessage\" [counterMinLength]=\"20\"></app-input>\r\n              </div>\r\n              <br>\r\n              <a class=\"button primary-button\" [class.disabled]=\"!validateComments(commentText)\" (click)=\"addComment()\">\r\n                <span>Comment</span>\r\n              </a>\r\n            </div>\r\n            <div class=\"ended-state\" *ngIf=\"duel.status === duelStatus.Ended\">\r\n              <p class=\"voting-ended-text\">Voting ended {{dateFormatter.toRelativeFormat(duel.endDate)}}</p>\r\n              <div *ngIf=\"duel.leftUser.votesAmount > duel.rightUser.votesAmount || duel.leftUser.votesAmount < duel.rightUser.votesAmount\">\r\n                <h3 *ngIf=\"duel.leftUser.votesAmount > duel.rightUser.votesAmount\">{{duel.leftUser.user.fullname}}\r\n                  <span class=\"winner-label\">Won</span>\r\n                </h3>\r\n                <h3 *ngIf=\"duel.leftUser.votesAmount < duel.rightUser.votesAmount\">{{duel.rightUser.user.fullname}}\r\n                  <span class=\"winner-label\">Won</span>\r\n                </h3>\r\n              </div>\r\n              <div *ngIf=\"duel.leftUser.votesAmount === duel.rightUser.votesAmount\">\r\n                <span class=\"draw-label\">Draw</span>\r\n              </div>\r\n              <div class=\"input-wrapper\">\r\n                <app-input (onValueChange)=\"onCommentTextChange($event)\" [value]=\"commentText\" [placeholder]=\"'Comment'\" [type]=\"inputType.Text\"\r\n                  [error]=\"descriptionErrorMessage\" [counterMinLength]=\"20\"></app-input>\r\n              </div>\r\n              <p class=\"small\">You cannot vote since the duel has ended, though you can still comment this duel</p>\r\n              <a class=\"button primary-button\" [class.disabled]=\"!validateComments(commentText)\" (click)=\"addComment()\">\r\n                <span>Comment</span>\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div *ngIf=\"authService.getCurrentUser() !== null\" class=\"share-section\">\r\n          <h2 class=\"section-header\">Share</h2>\r\n          <div class=\"share-social-block\">\r\n            <div class=\"twitter\">\r\n              <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M12 24C18.627 24 24 18.6274 24 12C24 5.37258 18.627 0 12 0C5.37305 0 0 5.37258 0 12C0 18.6274 5.37305 24 12 24ZM18.5625 7.89707C19.2094 7.82412 19.8258 7.66072 20.4 7.41979C19.9723 8.02723 19.4285 8.56079 18.7992 8.98548L18.8098 9.37781C18.8098 13.3824 15.5918 18 9.70547 18C7.89844 18 6.2168 17.4978 4.8 16.6365C5.05078 16.6633 5.30508 16.678 5.56289 16.678C7.06289 16.678 8.44336 16.1952 9.53789 15.3829C8.1375 15.3588 6.95625 14.4828 6.54844 13.2781C6.74531 13.315 6.94453 13.3344 7.15078 13.3344C7.44375 13.3344 7.72617 13.2965 7.99453 13.2282C6.53086 12.9494 5.42813 11.7244 5.42813 10.2566V10.2188C5.85938 10.4459 6.35273 10.5825 6.87773 10.5991C6.01992 10.0563 5.45391 9.12854 5.45391 8.07707C5.45391 7.52132 5.61094 7.00069 5.88633 6.55387C7.46484 8.38726 9.82266 9.5938 12.4828 9.72029C12.4277 9.49872 12.3996 9.26701 12.3996 9.02977C12.3996 7.35703 13.8328 6 15.6 6C16.5211 6 17.352 6.36742 17.9355 6.95731C18.6645 6.82068 19.3488 6.56864 19.9688 6.22155C19.7297 6.92776 19.2234 7.52226 18.5625 7.89707Z\"\r\n                  fill=\"#C1C7CB\" />\r\n              </svg>\r\n            </div>\r\n            <div class=\"pinterest\">\r\n              <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path d=\"M12 0C5.37933 0 0 5.37009 0 11.9794C0 16.8951 2.93794 21.1085 7.2 22.9674C7.15861 22.1412 7.2 21.1085 7.40691 20.1997C7.6552 19.2083 8.93794 13.673 8.93794 13.673C8.93794 13.673 8.56552 12.8882 8.56552 11.7728C8.56552 9.99656 9.6 8.67471 10.8827 8.67471C11.9586 8.67471 12.4965 9.50087 12.4965 10.4923C12.4965 11.5663 11.7931 13.2186 11.4207 14.747C11.131 16.0276 12.0413 17.0602 13.3241 17.0602C15.6 17.0602 17.131 14.1273 17.131 10.6988C17.131 8.05501 15.3517 6.11356 12.1241 6.11356C8.48273 6.11356 6.20691 8.8399 6.20691 11.8554C6.20691 12.8881 6.49655 13.6317 6.99309 14.2099C7.2 14.4578 7.24139 14.5817 7.15861 14.8709C7.11722 15.0774 6.9517 15.6144 6.91031 15.8209C6.82753 16.1101 6.57928 16.234 6.28964 16.1101C4.59309 15.4078 3.84825 13.5903 3.84825 11.5249C3.84825 8.13761 6.70341 4.0481 12.4138 4.0481C17.0069 4.0481 20.0276 7.35277 20.0276 10.9053C20.0276 15.6144 17.4207 19.1256 13.5724 19.1256C12.2896 19.1256 11.0483 18.4234 10.6345 17.6385C10.6345 17.6385 9.93103 20.4062 9.80691 20.9432C9.55861 21.852 9.06206 22.8021 8.60691 23.5043C9.68278 23.8348 10.8414 24 12 24C18.6207 24 24 18.6299 24 12.0206C24 5.41131 18.6207 0 12 0Z\"\r\n                  fill=\"#C1C7CB\" />\r\n              </svg>\r\n            </div>\r\n            <div class=\"facebook\">\r\n              <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path d=\"M12 0C5.38323 0 0 5.38323 0 12C0 18.6163 5.38323 24 12 24C18.6163 24 24 18.6163 24 12C24 5.38323 18.6173 0 12 0ZM14.9843 12.4225H13.032C13.032 15.5416 13.032 19.381 13.032 19.381H10.139C10.139 19.381 10.139 15.5788 10.139 12.4225H8.76388V9.9631H10.139V8.37235C10.139 7.23306 10.6804 5.45283 13.0586 5.45283L15.2023 5.46105V7.84838C15.2023 7.84838 13.8996 7.84838 13.6463 7.84838C13.3931 7.84838 13.0329 7.97503 13.0329 8.51833V9.96359H15.2371L14.9843 12.4225Z\"\r\n                  fill=\"#C1C7CB\" />\r\n              </svg>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n  </div>\r\n  <div *ngIf=\"!duel\">\r\n    <h1>Cannot find a duel.</h1>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/vote/vote.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__enums_order_direction__ = __webpack_require__("./src/app/enums/order-direction.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_vote_vote_service__ = __webpack_require__("./src/app/services/vote/vote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_date_formatter_date_formatter_service__ = __webpack_require__("./src/app/services/date-formatter/date-formatter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_pager_pager_service__ = __webpack_require__("./src/app/services/pager/pager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__enums_duel_status__ = __webpack_require__("./src/app/enums/duel-status.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__enums_input_type__ = __webpack_require__("./src/app/enums/input-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_report_report_service__ = __webpack_require__("./src/app/services/report/report.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_login_login_service__ = __webpack_require__("./src/app/services/login/login.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__register_services_register_service__ = __webpack_require__("./src/app/components/register/services/register.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var VoteComponent = /** @class */ (function () {
    function VoteComponent(activatedRoute, voteService, authService, router, validationService, dateFormatter, pager, reportService, titleService, alertService, loginService, registerService) {
        this.activatedRoute = activatedRoute;
        this.voteService = voteService;
        this.authService = authService;
        this.router = router;
        this.validationService = validationService;
        this.dateFormatter = dateFormatter;
        this.pager = pager;
        this.reportService = reportService;
        this.titleService = titleService;
        this.alertService = alertService;
        this.loginService = loginService;
        this.registerService = registerService;
        this.duel = null;
        this.duelId = null;
        this.commentText = "";
        this.votedUser = null;
        this.commentsMinLength = 20;
        this.pages = [];
        this.isPageLoaded = false;
        this.isExpandedTask = false;
        this.expandTaskButtonText = "Expand Task";
        this.duelTaskHeight = 0;
        this.minDuelTaskHeight = 150;
        this.orderDirectionKey = "comments_order_direction";
        this.duelStatus = __WEBPACK_IMPORTED_MODULE_8__enums_duel_status__["a" /* DuelStatus */];
        this.orderDirection = __WEBPACK_IMPORTED_MODULE_2__enums_order_direction__["a" /* OrderDirection */];
        this.inputType = __WEBPACK_IMPORTED_MODULE_9__enums_input_type__["a" /* InputType */];
    }
    VoteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.activatedRoute.params.subscribe(function (params) {
            _this.duelId = params['id'];
            _this.getVoteInfo();
        });
    };
    VoteComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    VoteComponent.prototype.onNotify = function (userId) {
        this.getVoteInfo();
    };
    VoteComponent.prototype.onCommentTextChange = function (commentText) {
        this.commentText = commentText;
    };
    VoteComponent.prototype.addComment = function () {
        var _this = this;
        if (this.currentUserId === null) {
            this.loginService.openModal();
        }
        else {
            this.voteService.addComment(this.duel.id, this.commentText, __WEBPACK_IMPORTED_MODULE_2__enums_order_direction__["a" /* OrderDirection */].Asc).subscribe(function (result) {
                if (result.success) {
                    _this.commentText = "";
                    if (_this.selectedOrderDirection === __WEBPACK_IMPORTED_MODULE_2__enums_order_direction__["a" /* OrderDirection */].Desc) {
                        _this.setPage(1);
                    }
                    else {
                        var lastPage = _this.pager.getLastPage(_this.commentsData.totalCount + 1);
                        _this.setPage(lastPage);
                    }
                }
                else {
                    console.log("Error during adding comment.");
                }
            });
        }
    };
    VoteComponent.prototype.addVote = function () {
        var _this = this;
        if (this.currentUserId === null) {
            this.loginService.openModal();
        }
        else {
            this.voteService.addVote(this.duel.id, this.votedUser, this.commentText, __WEBPACK_IMPORTED_MODULE_2__enums_order_direction__["a" /* OrderDirection */].Asc).subscribe(function (result) {
                if (result.success) {
                    _this.duel = result.duel;
                    _this.commentsData = result.commentsData;
                    _this.commentText = "";
                    _this.alertService.success("Voted for " + _this.getVotedUserFullname() + ". There\u2019s no turning back!");
                }
                else {
                    console.log("Error during voting.");
                    _this.alertService.error("Your vote for " + _this.getVotedUserFullname() + " was not sent. Please try again");
                }
            });
        }
    };
    VoteComponent.prototype.deleteComment = function (commentId) {
        var _this = this;
        this.voteService.deleteComment(commentId, this.duel.id, __WEBPACK_IMPORTED_MODULE_2__enums_order_direction__["a" /* OrderDirection */].Asc).subscribe(function (result) {
            if (result.success) {
                if (_this.commentsData.totalCount === 1) {
                    _this.pages = [];
                    _this.commentsData.comments = [];
                    _this.commentsData.totalCount = 0;
                }
                else {
                    var currentPage = _this.pages.filter(function (page) { return page.isCurrentPage; })[0].symbol, isLastComment = _this.pager.isLastElement(_this.commentsData.totalCount), page = isLastComment ? currentPage - 1 : currentPage;
                    _this.setPage(page);
                }
            }
            else {
                console.log("Error during deleting comment.");
            }
        });
    };
    VoteComponent.prototype.goToPerson = function (username) {
        this.router.navigate(["/user", username]);
    };
    VoteComponent.prototype.validateComments = function (comments) {
        return comments.replace(/ /g, '').length >= this.commentsMinLength;
    };
    VoteComponent.prototype.validateVote = function (comments) {
        return this.validateComments(comments) && this.validationService.isObjectValid(this.votedUser);
    };
    VoteComponent.prototype.setPage = function (page, isActive, isCurrentPage) {
        var _this = this;
        if (isActive === void 0) { isActive = true; }
        if (isCurrentPage === void 0) { isCurrentPage = false; }
        if (isActive && !isCurrentPage) {
            this.voteService.getComments(this.duel.id, this.selectedOrderDirection, page).subscribe(function (result) {
                if (result.success) {
                    _this.commentsData = result.commentsData;
                    _this.pages = _this.pager.getPager(_this.commentsData.totalCount, page);
                }
                else {
                    console.log("Error during fetching comments.");
                }
            });
        }
    };
    VoteComponent.prototype.changeCommentsOrder = function (orderDirection) {
        if (this.selectedOrderDirection !== orderDirection) {
            this.selectedOrderDirection = orderDirection;
            localStorage.setItem(this.orderDirectionKey, JSON.stringify(orderDirection));
            this.setPage(1);
        }
    };
    VoteComponent.prototype.toogleTaskExpand = function () {
        var _this = this;
        var svg = $($(this.expandButton.nativeElement).find("svg"));
        var taskBlock = this.getTaskBlock();
        if (this.isExpandedTask) {
            svg.addClass("spin");
            taskBlock.height(this.minDuelTaskHeight);
            setTimeout(function () {
                svg.removeClass("spin");
                _this.expandTaskButtonText = "Expand Task";
                _this.isExpandedTask = false;
            }, 200);
        }
        else {
            svg.addClass("spin");
            taskBlock.height(this.duelTaskHeight);
            setTimeout(function () {
                svg.removeClass("spin");
                _this.expandTaskButtonText = "Collapse Task";
                _this.isExpandedTask = true;
            }, 200);
        }
    };
    VoteComponent.prototype.initializeExpandableBlock = function () {
        var taskBlock = this.getTaskBlock();
        this.duelTaskHeight = taskBlock.height();
        if (this.duelTaskHeight < this.minDuelTaskHeight) {
            var btn = $(this.expandButton.nativeElement);
            btn.hide();
        }
        taskBlock.height(this.minDuelTaskHeight);
    };
    VoteComponent.prototype.getTaskBlock = function () {
        return $(this.duelTaskBlock.nativeElement);
    };
    VoteComponent.prototype.getVoteInfo = function () {
        var _this = this;
        var commentsOrder = JSON.parse(localStorage.getItem(this.orderDirectionKey));
        this.selectedOrderDirection = commentsOrder;
        this.voteService.getVoteInfo(this.duelId, this.selectedOrderDirection).subscribe(function (response) {
            _this.isPageLoaded = true;
            if (response.success) {
                if (response.duel && (response.duel.status === __WEBPACK_IMPORTED_MODULE_8__enums_duel_status__["a" /* DuelStatus */].Voting || response.duel.status === __WEBPACK_IMPORTED_MODULE_8__enums_duel_status__["a" /* DuelStatus */].Ended)) {
                    _this.duel = response.duel;
                    _this.currentUserId = _this.authService.getCurrentUserId();
                    _this.commentsData = response.commentsData;
                    _this.titleService.setTitle(_this.duel.leftUser.user.fullname + " vs " + _this.duel.rightUser.user.fullname + " - Duelity");
                    _this.pages = _this.pager.getPager(_this.commentsData.totalCount, 1);
                    setTimeout(function () { _this.initializeExpandableBlock(); }, 1000);
                }
            }
            else {
                console.log(response.error);
            }
        });
    };
    VoteComponent.prototype.getVotedUserFullname = function () {
        return this.votedUser === this.duel.leftUser.user.id ?
            this.duel.leftUser.user.fullname :
            this.duel.right.user.fullname;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('expandButton'),
        __metadata("design:type", Object)
    ], VoteComponent.prototype, "expandButton", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('duelTaskBlock'),
        __metadata("design:type", Object)
    ], VoteComponent.prototype, "duelTaskBlock", void 0);
    VoteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-vote',
            template: __webpack_require__("./src/app/components/vote/vote.component.html"),
            styles: [__webpack_require__("./src/app/components/vote/vote.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__services_vote_vote_service__["a" /* VoteService */],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__services_validation_validation_service__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_6__services_date_formatter_date_formatter_service__["a" /* DateFormatterService */],
            __WEBPACK_IMPORTED_MODULE_7__services_pager_pager_service__["a" /* PagerService */],
            __WEBPACK_IMPORTED_MODULE_10__services_report_report_service__["a" /* ReportService */],
            __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_12__services_alert_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_13__services_login_login_service__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_14__register_services_register_service__["a" /* RegisterService */]])
    ], VoteComponent);
    return VoteComponent;
}());



/***/ }),

/***/ "./src/app/components/work/constants/save-button-tooltip-messages.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return WorkUploadMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PreviewImageUploadMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DescriptionUploadMessage; });
var WorkUploadMessage = "You should upload work images!";
var PreviewImageUploadMessage = "You should upload preview image!";
var DescriptionUploadMessage = "You shoud write a description!";


/***/ }),

/***/ "./src/app/components/work/services/work.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorkService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var WorkService = /** @class */ (function () {
    function WorkService(tokenProviderService, http, headersProviderService, authService) {
        this.tokenProviderService = tokenProviderService;
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.authService = authService;
    }
    WorkService.prototype.getWork = function (duelId) {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/duel/work", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                duelId: duelId,
                userId: this.authService.getCurrentUser().id
            }
        })
            .map(function (res) { return res.json(); });
    };
    WorkService.prototype.uploadWork = function (data) {
        this.tokenProviderService.loadToken();
        data.userId = this.authService.getCurrentUser().id;
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/duel/upload", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    WorkService.prototype.deleteTemporaryImage = function (imagesForDelete) {
        this.tokenProviderService.loadToken();
        var data = { imagesForDelete: imagesForDelete };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/duel/delete-temp-images", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    WorkService.prototype.clearResourcesInCrashWay = function (imagesForDelete) {
        this.tokenProviderService.loadToken();
        var data = { imagesForDelete: imagesForDelete };
        $.ajax({
            async: false,
            url: __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/duel/delete-temp-images",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': this.tokenProviderService.authToken,
            },
            method: 'POST',
            data: JSON.stringify(data),
            success: function (data) {
                console.log('success: ' + data);
            }
        });
    };
    WorkService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_3__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__["a" /* AuthService */]])
    ], WorkService);
    return WorkService;
}());



/***/ }),

/***/ "./src/app/components/work/work.component.css":
/***/ (function(module, exports) {

module.exports = "#work-edit {\r\n    padding-top: 100px;\r\n    padding-bottom: 100px;\r\n}\r\n\r\n.top-header p {\r\n    margin-top: 20px;\r\n}\r\n\r\n.users-info div {\r\n    display: inline-block;\r\n}\r\n\r\n.user-image {\r\n    border-radius: 50%;\r\n    display: inline-block;\r\n    margin-right: 3px;\r\n}\r\n\r\n.users-info-block {\r\n    padding-right: 25px;\r\n    padding-left: 15px;\r\n    background-color: rgba(216, 226, 233, .3);\r\n}\r\n\r\n.users-info-block:first-child {\r\n    margin-right: 20px;\r\n}\r\n\r\n.users-info-block .user-avatar {\r\n    display: inline-block;\r\n    vertical-align: top;\r\n    margin-top: 18px;\r\n    margin-bottom: 18px;\r\n    margin-right: 10px;\r\n}\r\n\r\n.users-info-block .users-info-metadata {\r\n    position: relative; \r\n    top: 13px;\r\n}\r\n\r\n.users-info-block .users-info-metadata h3 {\r\n    color: #5D5D5D;\r\n}\r\n\r\n.users-info-block .users-info-metadata h3 svg {\r\n    position: relative;\r\n    top: -1px;\r\n    margin-left: 5px;\r\n}\r\n\r\n.users-info-block.uploaded .users-info-metadata h3 {\r\n    color: #31D431;\r\n}\r\n\r\n.users-info-block p {\r\n    margin: 0;\r\n}\r\n\r\n.users-info-block.uploaded {\r\n    background-color: rgba(49, 212, 49, .1);\r\n}\r\n\r\n.right > div {\r\n    width: 400px;\r\n}\r\n\r\n.uploaded-image {\r\n    display: block;\r\n}\r\n\r\n.upload-work-container {\r\n    width: 1200px;\r\n    margin: 0 auto;\r\n    margin-top: 20px;\r\n}\r\n\r\n.top {\r\n    width: 1200px;\r\n    margin: 0 auto;\r\n}\r\n\r\n.bottom {\r\n    width: 795px;\r\n    margin: 0 auto;\r\n}\r\n\r\n.work-preview-block {\r\n    width: 220px;\r\n    height: 200px;\r\n    border: 1px solid #D8E2E9;\r\n}\r\n\r\n.preview-image {\r\n    width: 220px;\r\n    height: 200px;\r\n    margin-top: -245px;\r\n    position: relative;\r\n    z-index: 1000;\r\n}\r\n\r\n.uploaded-image {\r\n    cursor: pointer;\r\n}\r\n\r\n.task-section {\r\n    min-height: 249px;\r\n    margin-top: 30px;\r\n}\r\n\r\n.task-section p {\r\n    overflow: hidden;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.task-section #expand-button {\r\n    border: 0;\r\n    outline: 0;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #26AFFF;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n    background-color: transparent;\r\n    padding: 0;\r\n    padding-bottom: 20px;\r\n}\r\n\r\n.task-section #expand-button span {\r\n    position: relative;\r\n    top: 2px;\r\n    left: 2px;\r\n}\r\n\r\n.task-section #expand-button:hover,\r\n.task-section #expand-button:focus {\r\n    border: 0;\r\n    outline: 0;\r\n}\r\n\r\n.task-section #expand-button:hover {\r\n    color: #2686BE;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.task-section #expand-button svg.spin {\r\n    -webkit-animation: expand-button-spin .2s linear infinite;\r\n    animation: expand-button-spin .2s linear infinite;\r\n}\r\n\r\n.work-info-block .block-header {\r\n    margin-bottom: 17px;\r\n}\r\n\r\n.work-info-block .work-description {\r\n    width: 534px;\r\n}\r\n\r\n.save-block .button {\r\n    margin-right: 20px;\r\n}\r\n\r\n.save-block .save-label {\r\n    width: 414px;\r\n    position: relative;\r\n    top: 24px;\r\n    left: 7px;\r\n}\r\n\r\n@-webkit-keyframes expand-button-spin \r\n{ \r\n    100% \r\n    { \r\n        -webkit-transform: rotate(-110deg); \r\n    } \r\n}\r\n\r\n@keyframes expand-button-spin \r\n{ \r\n    100% \r\n    { \r\n        -webkit-transform: rotate(-110deg); \r\n        transform:rotate(-110deg); \r\n    } \r\n}\r\n\r\n.task-section #expand-button path {\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.task-section #expand-button:hover path {\r\n    fill: #2686BE;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.task-section .section-header {\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.task-section .section-header span.additional-info {\r\n    margin-left: 6px;\r\n}\r\n\r\n.upload-container {\r\n    width: 1200px;\r\n    height: 600px;\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%221200%22 height%3D%22600%22 viewBox%3D%220 0 1200 600%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Crect x%3D%220.5%22 y%3D%220.5%22 width%3D%221199%22 height%3D%22599%22 stroke%3D%22%23D8E2E9%22 stroke-dasharray%3D%228 10%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 50% 50% no-repeat;\r\n    position: relative;\r\n    text-align: center;\r\n}\r\n\r\n.upload-container .width-label {\r\n    width: 120px;\r\n    position: absolute;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    left: 0;\r\n    right: 0;\r\n    font-family: Menoe Grotesque Pro;\r\n    font-style: normal;\r\n    font-weight: normal;\r\n    font-size: 14px;\r\n    line-height: 25px;\r\n    text-align: center;\r\n    letter-spacing: -0.02em;\r\n    color: #5D5D5D;\r\n    background-color: #fff;\r\n    color: #a9a9a9;\r\n}\r\n\r\n.upload-container .width-label svg:first-child {\r\n    position: relative;\r\n    left: -3px;\r\n    top: -1px;\r\n}\r\n\r\n.upload-container .width-label svg:last-child {\r\n    position: relative;\r\n    right: -3px;\r\n    top: -1px;\r\n}\r\n\r\n.upload-container .width-label.top-label {\r\n    top: -11px;\r\n}\r\n\r\n.upload-container .width-label.bottom-label {\r\n    bottom: -11px;\r\n}\r\n\r\n.upload-container .upload-content img {\r\n    margin-top: 70px;\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.upload-container .upload-content .delimiter {\r\n    opacity: .5;\r\n    margin-top: 60px;\r\n    margin-bottom: 60px;\r\n}\r\n\r\n.upload-container .upload-content p.small {\r\n    opacity: .5;\r\n    margin-top: 10px;\r\n}\r\n\r\n.inputfile {\r\n\twidth: 0.1px;\r\n\theight: 0.1px;\r\n\topacity: 0;\r\n\toverflow: hidden;\r\n\tposition: absolute;\r\n\tz-index: -1;\r\n}\r\n\r\n.inputfile + label {\r\n    width: 220px;\r\n    height: 200px;\r\n    text-align: center;\r\n}\r\n\r\n.inputfile + label svg {\r\n    margin-top: 53px;\r\n    margin-bottom: 17px;\r\n}\r\n\r\n.inputfile + label .main-label {\r\n    margin-top: 5px;\r\n    margin-bottom: 20px;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    line-height: 17px;\r\n    text-align: center;\r\n    color: #262626;\r\n}\r\n\r\n.inputfile + label p.small {\r\n    opacity: .5;\r\n    width: 137px;\r\n    margin: 0 auto;\r\n    line-height: 16px;\r\n}\r\n\r\n.work-info-block {\r\n    margin-top: 67px;\r\n}\r\n\r\n/* Test styles */\r\n\r\n.manipulation-video {\r\n    margin: 10px 0;\r\n}\r\n\r\n.actions { margin: 20px 0; }\r\n\r\n.upload_link { color: #000; border: 1px solid #aaa; background-color: #e0e0e0;\r\n    font-size: 18px; padding: 5px 10px; width: 250px; margin: 10px 0 20px 0;\r\n    font-weight: bold; text-align: center; text-decoration: none; margin: 5px; }\r\n\r\n.introducing-cloudinary {\r\n    max-width: 80%;\r\n}\r\n\r\n.static-photo img {\r\n    max-width: 20%;\r\n    position: absolute;\r\n    right: 0;\r\n}\r\n\r\n.photo { margin: 10px; padding: 10px; border-top: 2px solid #ccc; }\r\n\r\n.photo .thumbnail { margin-top: 10px; display: block; max-width: 200px; border: none; }\r\n\r\n.toggle_info { margin-top: 10px; font-weight: bold; color: #e62401; display: block; }\r\n\r\n.thumbnail_holder { height: 182px; margin-bottom: 5px; margin-right: 10px; }\r\n\r\n.info td, .uploaded_info td { font-size: 12px }\r\n\r\n.note { margin: 20px 0}\r\n\r\n.inline { display: inline-block; }\r\n\r\ntd { vertical-align: top; padding-right: 5px; }\r\n\r\n#direct_upload_jquery, #direct_upload {\r\n    -webkit-box-sizing: content-box;\r\n            box-sizing: content-box;\r\n}\r\n\r\n#direct_upload_jquery, #direct_upload { \r\n    padding: 20px 20px;\r\n    border-top: 1px solid #ccc; \r\n    border-bottom: 1px solid #ccc; \r\n}\r\n\r\n#direct_upload_jquery h1, #direct_upload h1 { margin: 0 0 15px 0; }\r\n\r\n.back_link { font-weight: bold; display: block; font-size: 16px; margin: 10px 0; }\r\n\r\nform { border: 1px solid #ddd; margin: 15px 0; padding: 15px 0; border-radius: 4px; }\r\n\r\nform .form_line { margin-bottom: 20px; }\r\n\r\nform .form_controls { margin-left: 180px; }\r\n\r\nform label { float: left; width: 160px; padding-top: 3px; text-align: right; }\r\n\r\nform .error { color: #c55; margin: 0 10px; }\r\n\r\n#direct_upload, #direct_upload_jquery { border: 4px dashed #ccc; }\r\n\r\n#direct_upload.dragover { border-color: #0c0; }\r\n\r\n#direct_upload.dragover-err { border-color: #c00; }\r\n\r\n.upload_details { font-size: 12px; margin: 20px; border-top: 1px solid #ccc; word-wrap: break-word; }\r\n\r\n.upload_button_holder {\r\n    position: relative;\r\n    overflow: hidden;\r\n    display: inline-block;\r\n}\r\n\r\n.upload_button {\r\n    display: inline-block;\r\n    position: relative;\r\n    font-weight: bold;\r\n    font-size: 14px;\r\n    background-color: rgb(15, 97, 172);\r\n    color: #fff;\r\n    padding: 5px 0;\r\n    border: 1px solid #000;\r\n    border-radius: 4px;\r\n    width: 100px;\r\n    height: 18px;\r\n    text-decoration: none;\r\n    text-align: center;\r\n    cursor: pointer;\r\n}\r\n\r\n.upload_button_holder .upload_button {\r\n    display: block;\r\n}\r\n\r\n.upload_button:hover {\r\n    background-color: rgb(17, 133, 240);\r\n}\r\n\r\n.upload_button_holder .cloudinary_fileupload {\r\n    opacity: 0;\r\n    filter: alpha(opacity=0);\r\n    cursor: pointer;\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    width: 100%;\r\n    height: 100%;\r\n    margin: 0;\r\n    padding: 0;\r\n    border: none;\r\n}\r\n\r\n.progress-bar{\r\n  width: 100px;\r\n  position: relative;\r\n  height: 4px;\r\n}\r\n\r\n.progress-bar .progress{\r\n  height: 4px;\r\n  background-color: #ff0000;\r\n  width: 0;\r\n}\r\n\r\n.preview img, .preview audio, .preview video {\r\n    max-width: 300px;\r\n    max-height: 150px;\r\n}\r\n\r\n.photo-listing.ng-enter,\r\n.photo-listing.ng-leave,\r\n.photo-listing.ng-move {\r\n  -webkit-transition: 0.5s linear all;\r\n  transition: 0.5s linear all;\r\n}\r\n\r\n.photo-listing.ng-enter,\r\n.photo-listing.ng-move {\r\n  opacity: 0;\r\n  height: 0;\r\n  overflow: hidden;\r\n}\r\n\r\n.photo-listing.ng-move.ng-move-active,\r\n.photo-listing.ng-enter.ng-enter-active {\r\n  opacity: 1;\r\n  height: 120px;\r\n}\r\n\r\n.photo-listing.ng-leave {\r\n  opacity: 1;\r\n  overflow: hidden;\r\n}\r\n\r\n.photo-listing.ng-leave.ng-leave-active {\r\n  opacity: 0;\r\n  height: 0;\r\n  padding-top: 0;\r\n  padding-bottom: 0;\r\n}\r\n\r\nimg.media-object {\r\n    display: block;\r\n    margin: 0 auto;\r\n    width: 1200px;\r\n    cursor: pointer;\r\n}\r\n\r\n/* cross fading between routes with ngView */\r\n\r\n.view-container {\r\n  position: relative;\r\n}\r\n\r\n.view-frame.ng-enter,\r\n.view-frame.ng-leave {\r\n  background: white;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n}\r\n\r\n.view-frame.ng-enter {\r\n  -webkit-animation: 0.5s fade-in;\r\n  animation: 0.5s fade-in;\r\n  z-index: 100;\r\n}\r\n\r\n.view-frame.ng-leave {\r\n  -webkit-animation: 0.5s fade-out;\r\n  animation: 0.5s fade-out;\r\n  z-index: 99;\r\n}\r\n\r\n@keyframes fade-in {\r\n  from { opacity: 0; }\r\n  to { opacity: 1; }\r\n}\r\n\r\n@-webkit-keyframes fade-in {\r\n  from { opacity: 0; }\r\n  to { opacity: 1; }\r\n}\r\n\r\n@keyframes fade-out {\r\n  from { opacity: 1; }\r\n  to { opacity: 0; }\r\n}\r\n\r\n@-webkit-keyframes fade-out {\r\n  from { opacity: 1; }\r\n  to { opacity: 0; }\r\n}\r\n\r\n/* End of test styles */"

/***/ }),

/***/ "./src/app/components/work/work.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"work-edit\" *ngIf=\"success !== null\">\r\n  <div *ngIf=\"!success\">\r\n    <h1>{{message}}</h1>\r\n  </div>\r\n  <div *ngIf=\"success\">\r\n    <div>\r\n      <div class=\"top\">\r\n        <div class=\"left\">\r\n          <div class=\"top-header\" *ngIf=\"currentUserInfo.imageUrls.length === 0\">\r\n            <h1>Uploading work</h1>\r\n            <p>You are uploading work for a duel with {{opponentUserInfo.user.fullname}}</p>\r\n          </div>\r\n          <div class=\"top-header\" *ngIf=\"currentUserInfo.imageUrls.length > 0\">\r\n            <h1>Editing</h1>\r\n            <p>You are editing work for a duel with {{opponentUserInfo.user.fullname}}</p>\r\n          </div>\r\n          <div class=\"users-info\">\r\n            <div class=\"users-info-block\" [class.uploaded]=\"currentUserInfo.imageUrls.length > 0\">\r\n              <app-profile-picture class=\"user-avatar\" elementClass=\"x-small\" userId={{currentUserInfo.user.id}} fullname={{currentUserInfo.user.fullname}}\r\n                backgroundImage={{currentUserInfo.user.imageUrl}} backgroundColor={{currentUserInfo.user.imageColor}}>\r\n              </app-profile-picture>\r\n              <div class=\"users-info-metadata\">\r\n                <h3>You\r\n                  <svg *ngIf=\"currentUserInfo.imageUrls.length === 0\" width=\"16\" height=\"16\" viewBox=\"0 0 16 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                    <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M8 16C12.418 16 16 12.4183 16 8C16 3.58173 12.418 0 8 0C3.58203 0 0 3.58173 0 8C0 12.4183 3.58203 16 8 16ZM6 4H8V8H11V10H6V4Z\"\r\n                      fill=\"#C1C7CB\" />\r\n                  </svg>\r\n                  <svg *ngIf=\"currentUserInfo.imageUrls.length > 0\" width=\"18\" height=\"16\" viewBox=\"0 0 18 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                    <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M5.89827 16L0 9.87659L1.6 8L5.67784 12.221L15.6 0L17.6 1.14802L5.89827 16Z\"\r\n                      fill=\"#31D431\" />\r\n                  </svg>\r\n                </h3>\r\n                <p *ngIf=\"currentUserInfo.imageUrls.length === 0\">Not uploaded yet</p>\r\n                <p *ngIf=\"currentUserInfo.imageUrls.length > 0\">Work uploaded</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"users-info-block\" [class.uploaded]=\"opponentUserInfo.imageUrls.length >  0\">\r\n              <app-profile-picture class=\"user-avatar\" elementClass=\"x-small\" userId={{opponentUserInfo.user.id}} fullname={{opponentUserInfo.user.fullname}}\r\n                backgroundImage={{opponentUserInfo.user.imageUrl}} backgroundColor={{opponentUserInfo.user.imageColor}}>\r\n              </app-profile-picture>\r\n              <div class=\"users-info-metadata\">\r\n                <h3>{{opponentUserInfo.user.fullname}}\r\n                  <svg *ngIf=\"opponentUserInfo.imageUrls.length === 0\" width=\"16\" height=\"16\" viewBox=\"0 0 16 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                    <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M8 16C12.418 16 16 12.4183 16 8C16 3.58173 12.418 0 8 0C3.58203 0 0 3.58173 0 8C0 12.4183 3.58203 16 8 16ZM6 4H8V8H11V10H6V4Z\"\r\n                      fill=\"#C1C7CB\" />\r\n                  </svg>\r\n                  <svg *ngIf=\"opponentUserInfo.imageUrls.length > 0\" width=\"18\" height=\"16\" viewBox=\"0 0 18 16\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                    <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M5.89827 16L0 9.87659L1.6 8L5.67784 12.221L15.6 0L17.6 1.14802L5.89827 16Z\"\r\n                      fill=\"#31D431\" />\r\n                  </svg>\r\n                </h3>\r\n                <p *ngIf=\"opponentUserInfo.imageUrls.length === 0\">Not uploaded yet</p>\r\n                <p *ngIf=\"opponentUserInfo.imageUrls.length > 0\">Work uploaded</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"right\">\r\n          <div class=\"task-section\">\r\n            <h3 class=\"section-header\">Duel Task</h3>\r\n            <p #duelTaskBlock>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\r\n              magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n            <button #expandButton id=\"expand-button\" (click)=\"toogleTaskExpand()\">\r\n              <svg *ngIf=\"isExpandedTask\" width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M3.03651 3.03692C0.853177 5.22025 0.853177 8.77318 3.03651 10.9565C5.21984 13.1398 8.77277 13.1398 10.9561 10.9565C13.1394 8.77318 13.1394 5.22025 10.9561 3.03692C8.77277 0.853584 5.21984 0.853584 3.03651 3.03692ZM11.9465 11.9465C9.21717 14.6758 4.77626 14.6758 2.04697 11.9465C-0.682323 9.21717 -0.682323 4.77626 2.04697 2.04697C4.77626 -0.682323 9.21717 -0.682323 11.9465 2.04697C14.6758 4.77626 14.6758 9.21717 11.9465 11.9465Z\"\r\n                  fill=\"#26AFFF\" />\r\n                <path d=\"M3.03779 6.29657V7.69635L10.9574 7.69635V6.29657L3.03779 6.29657Z\" fill=\"#26AFFF\" />\r\n              </svg>\r\n              <svg *ngIf=\"!isExpandedTask\" width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M3.03794 3.03834C0.853578 5.2227 0.853578 8.7773 3.03794 10.9617C5.22229 13.146 8.77689 13.146 10.9612 10.9617C13.1456 8.7773 13.1456 5.2227 10.9612 3.03834C8.77689 0.853985 5.22229 0.853985 3.03794 3.03834ZM11.9521 11.9521C9.2215 14.6826 4.7785 14.6826 2.04793 11.9521C-0.682643 9.2215 -0.682643 4.7785 2.04793 2.04793C4.7785 -0.682643 9.2215 -0.682643 11.9521 2.04793C14.6826 4.7785 14.6826 9.2215 11.9521 11.9521Z\"\r\n                  fill=\"#26AFFF\" />\r\n                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M7.70109 3.03809L6.30064 3.03809V6.29952L3.03921 6.29952V7.69997H6.30064V10.9614H7.70109L7.70159 7.70046L10.9625 7.69997V6.29952L7.70109 6.29952V3.03809Z\"\r\n                  fill=\"#26AFFF\" />\r\n              </svg>\r\n              <span>{{expandTaskButtonText}}</span>\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"upload-container\">\r\n        <div class=\"width-label top-label\">\r\n          <svg width=\"13\" height=\"14\" viewBox=\"0 0 13 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n            <path d=\"M6.76 1L1 7M1 7L6.76 13M1 7H13\" stroke=\"#C1C7CB\" />\r\n          </svg>\r\n          1200px\r\n          <svg width=\"13\" height=\"14\" viewBox=\"0 0 13 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n            <path d=\"M6.24 1L12 7M12 7L6.24 13M12 7H0\" stroke=\"#C1C7CB\" />\r\n          </svg>\r\n        </div>\r\n        <div class=\"upload-content\">\r\n          <img width=\"339\" height=\"166\" src=\"../../../assets/img/upload-work/upload-container-illustration.png\">\r\n          <h2>Nothing here... yet!</h2>\r\n          <p>Drop files at me to upload them</p>\r\n          <p class=\"delimiter\">or</p>\r\n          <a class=\"button primary-button\">\r\n            <span>Upload Files</span>\r\n          </a>\r\n          <p class=\"small\">JPG, PNG or GIF, 10MB Max</p>\r\n        </div>\r\n        <div class=\"width-label bottom-label\">\r\n          <svg width=\"13\" height=\"14\" viewBox=\"0 0 13 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n            <path d=\"M6.76 1L1 7M1 7L6.76 13M1 7H13\" stroke=\"#C1C7CB\" />\r\n          </svg>\r\n          1200px\r\n          <svg width=\"13\" height=\"14\" viewBox=\"0 0 13 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n            <path d=\"M6.24 1L12 7M12 7L6.24 13M12 7H0\" stroke=\"#C1C7CB\" />\r\n          </svg>\r\n        </div>\r\n      </div>\r\n      <div class=\"work-info-block\">\r\n        <div class=\"bottom\">\r\n          <div class=\"left\">\r\n            <h3 class=\"block-header\">Work Preview</h3>\r\n            <div class=\"work-preview-block\">\r\n              <div>\r\n                <input type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" (change)=\"onPreviewImageUpload($event)\" />\r\n                <label for=\"file\">\r\n                  <div class=\"inline\">\r\n                    <svg width=\"22\" height=\"24\" viewBox=\"0 0 22 24\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                      <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M15.4393 4.707L10.7323 0L6.02527 4.707L7.43927 6.121L9.73227 3.828V12.414H11.7323V3.828L14.0253 6.121L15.4393 4.707Z\"\r\n                        fill=\"black\" />\r\n                      <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M2.12 22L3.453 18H18.012L19.345 22H2.12ZM21.3649 21.735L19.7319 16.837V8C19.7319 7.448 19.2849 7 18.7319 7H15.7319V9H17.7319V16H3.73195V9H5.73195V7H2.73195C2.17995 7 1.73195 7.448 1.73195 8V16.837L0.0999475 21.734C-0.0860525 22.292 -0.0100525 22.864 0.307948 23.306C0.624948 23.747 1.14395 24 1.73195 24H19.7319C20.3199 24 20.8389 23.747 21.1569 23.306C21.4749 22.864 21.5509 22.292 21.3649 21.735Z\"\r\n                        fill=\"black\" />\r\n                      <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M12.7323 21H8.7323V19H12.7323V21Z\" fill=\"black\" />\r\n                    </svg>\r\n                  </div>\r\n                  <div class=\"main-label\">Upload Work</div>\r\n                  <p class=\"small\">JPG or PNG, 5MB Max at least 220x200</p>\r\n                </label>\r\n              </div>\r\n              <!-- <input type=\"file\" value=\"Upload Preview\" (change)=\"onPreviewImageUpload($event)\" /> -->\r\n              <img *ngIf=\"cloudinaryService.previewImageFile\" src=\"\" imgPreview [image]=\"cloudinaryService.previewImageFile\" class=\"preview-image\"\r\n                (click)=\"removeTemporaryImages([], true)\" />\r\n              <img *ngIf=\"!cloudinaryService.previewImageFile && currentUserInfo.previewImageUrl.url\" src=\"{{currentUserInfo.previewImageUrl.url}}\"\r\n                class=\"preview-image\" (click)=\"removePreviewImage()\" />\r\n            </div>\r\n          </div>\r\n          <div class=\"right\">\r\n            <h3 class=\"block-header\">Work Description</h3>\r\n            <div class=\"input-wrapper work-description\">\r\n              <app-input (onValueChange)=\"onSelectedTaskText($event)\" [value]=\"currentUserInfo.workDescription\" [placeholder]=\"'Work Description'\"\r\n                [type]=\"inputType.Textarea\" [elementClass]=\"'big-textarea'\"></app-input>\r\n            </div>\r\n          </div>\r\n          <div class=\"clearfix\"></div>\r\n          <br>\r\n          <div class=\"save-block\">\r\n            <a class=\"button secondary-button\" (click)=\"cancelUploading()\">\r\n              <span>Cancel</span>\r\n            </a>\r\n            <a class=\"button primary-button\" title=\"{{buttonTooltip}}\" (click)=\"uploadWork()\" [class.disabled]=\"canSave()\">\r\n              <span *ngIf=\"currentUserInfo.imageUrls.length === 0\">Upload My Work</span>\r\n              <span *ngIf=\"currentUserInfo.imageUrls.length > 0\">Save Edits</span>\r\n            </a>\r\n            <p class=\"inline save-label\" *ngIf=\"opponentUserInfo.imageUrls.length === 0\">Your opponent didn’t upload his work yet. This means after uploading you will still be able to edit it until\r\n              your opponent is ready.</p>\r\n            <p class=\"inline save-label\" *ngIf=\"opponentUserInfo.imageUrls.length > 0\">Your opponent already uploaded his work. This means after uploading duel will go public and voting will automatically\r\n              start.\r\n            </p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/work/work.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorkComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__ = __webpack_require__("./src/app/components/work/constants/save-button-tooltip-messages.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_work_service__ = __webpack_require__("./src/app/components/work/services/work.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_cloudinary_service_cloudinary_service__ = __webpack_require__("./src/app/services/cloudinary-service/cloudinary.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__enums_input_type__ = __webpack_require__("./src/app/enums/input-type.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var WorkComponent = /** @class */ (function () {
    function WorkComponent(cloudinaryService, activatedRoute, workService, authService, validationService, router, titleService, alertService) {
        this.cloudinaryService = cloudinaryService;
        this.activatedRoute = activatedRoute;
        this.workService = workService;
        this.authService = authService;
        this.validationService = validationService;
        this.router = router;
        this.titleService = titleService;
        this.alertService = alertService;
        this.success = null;
        this.message = null;
        this.buttonTooltip = "";
        this.imagesForDelete = [];
        this.wasSaved = false;
        this.isExpandedTask = false;
        this.expandTaskButtonText = "Expand Task";
        this.duelTaskHeight = 0;
        this.minDuelTaskHeight = 90;
        this.inputType = __WEBPACK_IMPORTED_MODULE_9__enums_input_type__["a" /* InputType */];
    }
    WorkComponent.prototype.beforeUnloadHandler = function ($event) {
        if (!this.wasSaved) {
            var publicIds = [];
            publicIds.push(this.cloudinaryService.responses.map(function (response) { return response.data.public_id; }));
            if (this.cloudinaryService.previewImageResponse) {
                publicIds.push(this.cloudinaryService.previewImageResponse.public_id);
            }
            this.cloudinaryService.destroyUploader();
            this.workService.clearResourcesInCrashWay(publicIds);
        }
    };
    WorkComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.activatedRoute.params.subscribe(function (params) {
            var duelId = params['id'];
            _this.getWork(duelId);
        });
    };
    WorkComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
        if (this.cloudinaryService.uploader.queue.length > 0) {
            this.cloudinaryService.uploader.onCompleteAll = this.clearResources.bind(this);
        }
        else {
            this.clearResources();
        }
    };
    WorkComponent.prototype.toogleTaskExpand = function () {
        var _this = this;
        var svg = $($(this.expandButton.nativeElement).find("svg"));
        var taskBlock = this.getTaskBlock();
        if (this.isExpandedTask) {
            svg.addClass("spin");
            taskBlock.height(this.minDuelTaskHeight);
            setTimeout(function () {
                svg.removeClass("spin");
                _this.expandTaskButtonText = "Expand Task";
                _this.isExpandedTask = false;
            }, 200);
        }
        else {
            svg.addClass("spin");
            taskBlock.height(this.duelTaskHeight);
            setTimeout(function () {
                svg.removeClass("spin");
                _this.expandTaskButtonText = "Collapse Task";
                _this.isExpandedTask = true;
            }, 200);
        }
    };
    WorkComponent.prototype.initializeExpandableBlock = function () {
        var taskBlock = this.getTaskBlock();
        this.duelTaskHeight = taskBlock.height();
        if (this.duelTaskHeight < this.minDuelTaskHeight) {
            var btn = $(this.expandButton.nativeElement);
            btn.hide();
        }
        taskBlock.height(this.minDuelTaskHeight);
    };
    WorkComponent.prototype.getTaskBlock = function () {
        return $(this.duelTaskBlock.nativeElement);
    };
    WorkComponent.prototype.onSelectedTaskText = function (taskText) {
        this.currentUserInfo.workDescription = taskText;
        this.updateSaveButtonTooltip();
    };
    WorkComponent.prototype.clearResources = function () {
        if (!this.wasSaved) {
            this.removeTemporaryImages(this.cloudinaryService.files, true);
        }
        this.cloudinaryService.destroyUploader();
    };
    WorkComponent.prototype.uploadWork = function () {
        var _this = this;
        var data = {
            duelId: this.duelInfo.id,
            imagesForDelete: this.imagesForDelete,
            workDescription: this.currentUserInfo.workDescription,
            previewImage: {
                publicId: this.cloudinaryService.previewImageResponse &&
                    this.cloudinaryService.previewImageResponse.public_id || this.currentUserInfo.previewImageUrl.publicId,
                url: this.cloudinaryService.previewImageResponse &&
                    this.cloudinaryService.previewImageResponse.secure_url || this.currentUserInfo.previewImageUrl.url
            },
            imageUrls: this.cloudinaryService.responses.map(function (response) {
                return {
                    publicId: response.data.public_id,
                    url: response.data.secure_url
                };
            })
        };
        this.workService.uploadWork(data).subscribe(function (response) {
            if (response.success) {
                _this.wasSaved = true;
                _this.router.navigate(["/myduels"]);
                _this.alertService.success(_this.getUploadSuccessMessage());
            }
            else {
                _this.alertService.error(_this.getUploadErrorMessage());
            }
        });
    };
    WorkComponent.prototype.removeImage = function (imagePublicId) {
        this.currentUserInfo.imageUrls = this.currentUserInfo.imageUrls.filter(function (image) { return image.publicId !== imagePublicId; });
        this.imagesForDelete.push(imagePublicId);
        this.updateSaveButtonTooltip();
    };
    WorkComponent.prototype.removePreviewImage = function () {
        this.currentUserInfo.previewImageUrl = { publicId: null, url: null };
        this.updateSaveButtonTooltip();
    };
    WorkComponent.prototype.removeTemporaryImages = function (items, clearPreviewImage) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var targetResponses = _this.cloudinaryService.responses.filter(function (response) { return items.indexOf(response.file) >= 0; });
            var publicIds = [];
            if (targetResponses && targetResponses.length > 0) {
                publicIds.push(targetResponses.map(function (response) { return response.data.public_id; }));
                _this.cloudinaryService.responses = _this.cloudinaryService.responses.filter(function (response) { return targetResponses.indexOf(response) === -1; });
                _this.cloudinaryService.files = _this.cloudinaryService.files.filter(function (file) { return targetResponses.map(function (response) { return response.file; }).indexOf(file) === -1; });
            }
            if (_this.cloudinaryService.previewImageResponse && clearPreviewImage) {
                publicIds.push(_this.cloudinaryService.previewImageResponse.public_id);
                _this.cloudinaryService.previewImageFile = null;
                _this.cloudinaryService.previewImageResponse = null;
            }
            _this.updateSaveButtonTooltip();
            _this.workService.deleteTemporaryImage(publicIds).subscribe(function (data) {
                if (data.success) {
                    resolve();
                }
                else {
                    reject();
                }
            });
        });
    };
    WorkComponent.prototype.updateSaveButtonTooltip = function () {
        var pageState = this.getPageState();
        if (!pageState.stagingImagesExist && !pageState.uploadedImagesExist) {
            if (this.buttonTooltip.indexOf(__WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__["c" /* WorkUploadMessage */]) === -1) {
                this.buttonTooltip += __WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__["c" /* WorkUploadMessage */];
            }
        }
        else {
            this.buttonTooltip = this.buttonTooltip.replace(__WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__["c" /* WorkUploadMessage */], "");
        }
        if (!pageState.stagingPreviewImageExists && !pageState.uploadedPreviewImageExists) {
            if (this.buttonTooltip.indexOf(__WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__["b" /* PreviewImageUploadMessage */]) === -1) {
                this.buttonTooltip += __WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__["b" /* PreviewImageUploadMessage */];
            }
        }
        else {
            this.buttonTooltip = this.buttonTooltip.replace(__WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__["b" /* PreviewImageUploadMessage */], "");
        }
        if (!pageState.descriptionExists) {
            if (this.buttonTooltip.indexOf(__WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__["a" /* DescriptionUploadMessage */]) === -1) {
                this.buttonTooltip += __WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__["a" /* DescriptionUploadMessage */];
            }
        }
        else {
            this.buttonTooltip = this.buttonTooltip.replace(__WEBPACK_IMPORTED_MODULE_2__constants_save_button_tooltip_messages__["a" /* DescriptionUploadMessage */], "");
        }
        if (pageState.allItemsExists || pageState.noItemsAtAll) {
            this.buttonTooltip = "";
        }
    };
    WorkComponent.prototype.canSave = function () {
        var pageState = this.getPageState();
        return pageState.noItemsInQueue && (pageState.allItemsExists || pageState.noItemsAtAll);
    };
    WorkComponent.prototype.getPageState = function () {
        var noItemsInQueue = this.cloudinaryService.uploader &&
            this.cloudinaryService.uploader.queue &&
            this.cloudinaryService.uploader.queue.length === 0;
        var stagingImagesExist = this.cloudinaryService.responses.length > 0, stagingPreviewImageExists = this.validationService.isObjectValid(this.cloudinaryService.previewImageResponse) &&
            this.validationService.isObjectValid(this.cloudinaryService.previewImageResponse.public_id), uploadedImagesExist = this.currentUserInfo.imageUrls.length > 0, uploadedPreviewImageExists = this.validationService.isObjectValid(this.currentUserInfo.previewImageUrl) &&
            this.validationService.isObjectValid(this.currentUserInfo.previewImageUrl.url), descriptionExists = this.validationService.isStringValid(this.currentUserInfo.workDescription);
        var allItemsExists = descriptionExists &&
            ((stagingImagesExist && stagingPreviewImageExists) ||
                (stagingImagesExist && uploadedPreviewImageExists) ||
                (uploadedImagesExist && stagingPreviewImageExists) ||
                (uploadedImagesExist && uploadedPreviewImageExists));
        var noItemsAtAll = !descriptionExists &&
            !stagingImagesExist &&
            !stagingPreviewImageExists &&
            !uploadedImagesExist &&
            !uploadedPreviewImageExists;
        return {
            noItemsInQueue: noItemsInQueue,
            stagingImagesExist: stagingImagesExist,
            stagingPreviewImageExists: stagingPreviewImageExists,
            uploadedImagesExist: uploadedImagesExist,
            uploadedPreviewImageExists: uploadedPreviewImageExists,
            descriptionExists: descriptionExists,
            allItemsExists: allItemsExists,
            noItemsAtAll: noItemsAtAll
        };
    };
    WorkComponent.prototype.onPreviewImageUpload = function (e) {
        var file = e.target.files[0];
        if (file) {
            if (this.validationService.isProImageValid(file.type, this.currentUserInfo.user.isPaidAccount)) {
                this.cloudinaryService.previewImageUploader.addToQueue(e.target.files);
            }
            else {
                alert("Image format is not valid");
            }
        }
    };
    WorkComponent.prototype.cancelUploading = function () {
        this.wasSaved = false;
        this.router.navigate(["/myduels"]);
    };
    WorkComponent.prototype.getWork = function (duelId) {
        var _this = this;
        this.workService.getWork(duelId).subscribe(function (response) {
            if (response.success) {
                if (response.duel !== null) {
                    _this.success = true;
                    _this.duelInfo = response.duel;
                    _this.currentUserInfo = _this.getCurrentUserInfo(_this.duelInfo);
                    _this.opponentUserInfo = _this.getOpponentUserInfo(_this.duelInfo);
                    _this.titleService.setTitle("" + (_this.isUploadMode() ? "Uploading Work - Duelity" : "Editing Work - Duelity"));
                    _this.cloudinaryService.initWorkUploaders(_this.duelInfo.id, _this.currentUserInfo.user.id, _this.updateSaveButtonTooltip.bind(_this));
                    setTimeout(function () { _this.initializeExpandableBlock(); }, 1000);
                }
                else {
                    _this.success = false;
                    _this.message = response.message;
                }
            }
            else {
                console.log(response.error);
            }
        });
    };
    WorkComponent.prototype.getCurrentUserInfo = function (duel) {
        return duel.leftUser.user.id === this.authService.getCurrentUser().id ?
            duel.leftUser : duel.rightUser;
    };
    WorkComponent.prototype.getOpponentUserInfo = function (duel) {
        return duel.leftUser.user.id === this.authService.getCurrentUser().id ?
            duel.rightUser : duel.leftUser;
    };
    WorkComponent.prototype.isUploadMode = function () {
        return this.currentUserInfo.imageUrls.length === 0;
    };
    WorkComponent.prototype.isOpponentReady = function () {
        return this.opponentUserInfo.imageUrls.length !== 0;
    };
    WorkComponent.prototype.getUploadSuccessMessage = function () {
        return this.isOpponentReady() ? "Work has been uploaded. Voting started!" :
            this.isUploadMode() ? "Work has been uploaded. Waiting for " + this.opponentUserInfo.user.fullname + " now"
                : "Work edits has been saved";
    };
    WorkComponent.prototype.getUploadErrorMessage = function () {
        return this.isUploadMode() ? "Work could not be uploaded. Try reuploading again" :
            "Works edits could not be saved. Please try again";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('expandButton'),
        __metadata("design:type", Object)
    ], WorkComponent.prototype, "expandButton", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('duelTaskBlock'),
        __metadata("design:type", Object)
    ], WorkComponent.prototype, "duelTaskBlock", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:beforeunload', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], WorkComponent.prototype, "beforeUnloadHandler", null);
    WorkComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-work',
            template: __webpack_require__("./src/app/components/work/work.component.html"),
            styles: [__webpack_require__("./src/app/components/work/work.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__services_cloudinary_service_cloudinary_service__["a" /* CloudinaryService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__services_work_service__["a" /* WorkService */],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_6__services_validation_validation_service__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["c" /* Title */],
            __WEBPACK_IMPORTED_MODULE_8__services_alert_alert_service__["a" /* AlertService */]])
    ], WorkComponent);
    return WorkComponent;
}());



/***/ }),

/***/ "./src/app/directives/block-background.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlockBackgroundDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BlockBackgroundDirective = /** @class */ (function () {
    function BlockBackgroundDirective(el) {
        this.el = el;
    }
    BlockBackgroundDirective.prototype.ngOnChanges = function (changes) {
        var el = this.el;
        if (this.background && this.background.url) {
            el.nativeElement.style.backgroundImage = "url(" + this.background.url + ")";
            el.nativeElement.style.backgroundSize = "cover";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], BlockBackgroundDirective.prototype, "background", void 0);
    BlockBackgroundDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: 'div[blockBackground]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], BlockBackgroundDirective);
    return BlockBackgroundDirective;
}());



/***/ }),

/***/ "./src/app/directives/image-preview.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImagePreviewDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ImagePreviewDirective = /** @class */ (function () {
    function ImagePreviewDirective(el) {
        this.el = el;
    }
    ImagePreviewDirective.prototype.ngOnChanges = function (changes) {
        var reader = new FileReader();
        var el = this.el;
        reader.onloadend = function (e) {
            el.nativeElement.src = reader.result;
        };
        if (this.image && this.image.rawFile) {
            return reader.readAsDataURL(this.image.rawFile);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ImagePreviewDirective.prototype, "image", void 0);
    ImagePreviewDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: 'img[imgPreview]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], ImagePreviewDirective);
    return ImagePreviewDirective;
}());



/***/ }),

/***/ "./src/app/enums/duel-status.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DuelStatus; });
var DuelStatus;
(function (DuelStatus) {
    DuelStatus[DuelStatus["Waiting"] = 0] = "Waiting";
    DuelStatus[DuelStatus["InProgress"] = 1] = "InProgress";
    DuelStatus[DuelStatus["Voting"] = 2] = "Voting";
    DuelStatus[DuelStatus["Ended"] = 3] = "Ended";
})(DuelStatus || (DuelStatus = {}));


/***/ }),

/***/ "./src/app/enums/duel-type.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DuelType; });
var DuelType;
(function (DuelType) {
    DuelType[DuelType["Following"] = 0] = "Following";
    DuelType[DuelType["New"] = 1] = "New";
    DuelType[DuelType["Popular"] = 2] = "Popular";
    DuelType[DuelType["Featured"] = 3] = "Featured";
})(DuelType || (DuelType = {}));


/***/ }),

/***/ "./src/app/enums/input-type.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputType; });
var InputType;
(function (InputType) {
    InputType[InputType["Text"] = 0] = "Text";
    InputType[InputType["Password"] = 1] = "Password";
    InputType[InputType["Textarea"] = 2] = "Textarea";
})(InputType || (InputType = {}));


/***/ }),

/***/ "./src/app/enums/notification-type.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationType; });
var NotificationType;
(function (NotificationType) {
    NotificationType[NotificationType["CreatedDuel"] = 0] = "CreatedDuel";
    NotificationType[NotificationType["AcceptedDuel"] = 1] = "AcceptedDuel";
    NotificationType[NotificationType["DeclinedDuel"] = 2] = "DeclinedDuel";
    NotificationType[NotificationType["OpponentUploadedWork"] = 3] = "OpponentUploadedWork";
    NotificationType[NotificationType["NewVote"] = 4] = "NewVote";
    NotificationType[NotificationType["WonDuel"] = 5] = "WonDuel";
    NotificationType[NotificationType["LostDuel"] = 6] = "LostDuel";
    NotificationType[NotificationType["DrawDuel"] = 7] = "DrawDuel";
    NotificationType[NotificationType["NewFollow"] = 8] = "NewFollow";
    NotificationType[NotificationType["FeaturedDuel"] = 9] = "FeaturedDuel";
    NotificationType[NotificationType["VotedDuelWon"] = 10] = "VotedDuelWon";
    NotificationType[NotificationType["VotedDuelLost"] = 11] = "VotedDuelLost";
    NotificationType[NotificationType["PopularDuel"] = 12] = "PopularDuel";
    NotificationType[NotificationType["EndedSubscription"] = 13] = "EndedSubscription";
    NotificationType[NotificationType["StartedVoting"] = 14] = "StartedVoting";
})(NotificationType || (NotificationType = {}));


/***/ }),

/***/ "./src/app/enums/order-direction.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderDirection; });
var OrderDirection;
(function (OrderDirection) {
    OrderDirection[OrderDirection["Asc"] = 0] = "Asc";
    OrderDirection[OrderDirection["Desc"] = 1] = "Desc";
})(OrderDirection || (OrderDirection = {}));


/***/ }),

/***/ "./src/app/enums/search-type.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchType; });
var SearchType;
(function (SearchType) {
    SearchType[SearchType["Users"] = 0] = "Users";
    SearchType[SearchType["Duels"] = 1] = "Duels";
})(SearchType || (SearchType = {}));


/***/ }),

/***/ "./src/app/enums/settings-type.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsType; });
var SettingsType;
(function (SettingsType) {
    SettingsType[SettingsType["General"] = 0] = "General";
    SettingsType[SettingsType["DuelCategories"] = 1] = "DuelCategories";
    SettingsType[SettingsType["Notifications"] = 2] = "Notifications";
    SettingsType[SettingsType["Billing"] = 3] = "Billing";
})(SettingsType || (SettingsType = {}));


/***/ }),

/***/ "./src/app/enums/user-list-order-criteria.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserListOrderCriteria; });
var UserListOrderCriteria;
(function (UserListOrderCriteria) {
    UserListOrderCriteria[UserListOrderCriteria["SkillPoints"] = 0] = "SkillPoints";
    UserListOrderCriteria[UserListOrderCriteria["FeaturedPoints"] = 1] = "FeaturedPoints";
})(UserListOrderCriteria || (UserListOrderCriteria = {}));


/***/ }),

/***/ "./src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.authService.loggedIn()) {
            return true;
        }
        else {
            this.router.navigate(["/popular"]);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_auth_service__["a" /* AuthService */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/guards/settings-deactivate.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsDeactivateGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SettingsDeactivateGuard = /** @class */ (function () {
    function SettingsDeactivateGuard() {
    }
    SettingsDeactivateGuard.prototype.canDeactivate = function (target) {
        if (target.accountSettings.hasChanges(target.previousSettings)) {
            target.openNotSavedModal();
            return target.deactivateSubject;
        }
        return true;
    };
    SettingsDeactivateGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], SettingsDeactivateGuard);
    return SettingsDeactivateGuard;
}());



/***/ }),

/***/ "./src/app/guards/work-deactivate.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorkDeactivateGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var WorkDeactivateGuard = /** @class */ (function () {
    function WorkDeactivateGuard() {
    }
    WorkDeactivateGuard.prototype.canDeactivate = function (target) {
        if (!target.wasSaved) {
            return window.confirm('You have unsaved changes on this page. Do you want to leave this page before saving?');
        }
        return true;
    };
    WorkDeactivateGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], WorkDeactivateGuard);
    return WorkDeactivateGuard;
}());



/***/ }),

/***/ "./src/app/pipes/safe-html.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SafeHtmlPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SafeHtmlPipe = /** @class */ (function () {
    function SafeHtmlPipe(sanitized) {
        this.sanitized = sanitized;
    }
    SafeHtmlPipe.prototype.transform = function (value, args) {
        return this.sanitized.bypassSecurityTrustHtml(value);
    };
    SafeHtmlPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'safeHtml'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* DomSanitizer */]])
    ], SafeHtmlPipe);
    return SafeHtmlPipe;
}());



/***/ }),

/***/ "./src/app/root-components/body/body.component.css":
/***/ (function(module, exports) {

module.exports = ".container {\r\n    margin-bottom: 250px;\r\n}"

/***/ }),

/***/ "./src/app/root-components/body/body.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <router-outlet></router-outlet>\r\n</div>"

/***/ }),

/***/ "./src/app/root-components/body/body.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BodyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BodyComponent = /** @class */ (function () {
    function BodyComponent() {
    }
    BodyComponent.prototype.ngOnInit = function () {
    };
    BodyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-body',
            template: __webpack_require__("./src/app/root-components/body/body.component.html"),
            styles: [__webpack_require__("./src/app/root-components/body/body.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BodyComponent);
    return BodyComponent;
}());



/***/ }),

/***/ "./src/app/root-components/footer/footer.component.css":
/***/ (function(module, exports) {

module.exports = "footer {\r\n    background-color: rgba(220, 220, 220, 0.3);\r\n    height: 250px;\r\n    position: absolute;\r\n    bottom: 0;\r\n    width: 100%;\r\n}\r\n\r\nfooter .row-1 {\r\n    margin-top: 60px;\r\n}\r\n\r\nfooter .row-1 img {\r\n    margin-right: 155px;\r\n    opacity: 1;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n    cursor: pointer;\r\n    border: 0;\r\n    outline: 0;\r\n}\r\n\r\nfooter .row-1 img:hover { \r\n    opacity: 0.8;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n    cursor: pointer;\r\n}\r\n\r\nfooter .row-1 a {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 17px;\r\n    line-height: 23px;\r\n    color: #262626;\r\n    letter-spacing: -0.2px;\r\n    margin-right: 115px;\r\n    text-decoration: none;\r\n    opacity: 1;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\nfooter .row-1 a:hover {\r\n    color: #5D5D5D;\r\n    opacity: 0.7;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\nfooter .row-1 a:last-child {\r\n    margin-right: 0;\r\n}\r\n\r\nfooter .row-2 {\r\n    margin-top: 70px;\r\n}\r\n\r\nfooter .row-2 p {\r\n    display: inline-block;\r\n    margin-left: 210px;\r\n}\r\n\r\n.social {\r\n    display: inline-block;\r\n}\r\n\r\n.social > a {\r\n    vertical-align: middle;\r\n    display: inline-block;\r\n    margin-left: 25px;\r\n}\r\n\r\n.medium path,\r\n.twitter path {\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.medium:hover path,\r\n.twitter:hover path {\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n    fill: #262626;\r\n}"

/***/ }),

/***/ "./src/app/root-components/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer>\r\n    <div class=\"container\">\r\n        <div class=\"row-1\">\r\n            <img src=\"../../../assets/img/footer/graphic-logo.svg\" routerLink=\"/\">\r\n            <a routerLink=\"/about\">About</a>\r\n            <a routerLink=\"/silver\">Duelity Silver</a>\r\n            <a routerLink=\"/about\">Terms Of Use</a>\r\n            <a routerLink=\"/about\">Privacy Policy</a>\r\n            <a routerLink=\"/about\">Support</a>\r\n        </div>\r\n        <div class=\"row-2\">\r\n            <p>© Duelity 2019 — All Rights Reserved</p>\r\n            <div class=\"social right\">\r\n                <a class=\"medium\" href=\"https://medium.com/\" target=\"_blank\">\r\n                    <svg width=\"22\" height=\"22\" viewBox=\"0 0 22 22\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                        <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M0 0H22V22H0V0ZM5.06374 6.87473C5.19922 6.99684 5.2684 7.17616 5.25004 7.35758V13.8899C5.29024 14.1255 5.21596 14.3663 5.04994 14.5383L3.49748 16.4214V16.6698H7.89958V16.4214L6.34712 14.5383C6.17989 14.3665 6.10101 14.1274 6.13322 13.8899V8.24051L9.99714 16.6698H10.4456L13.7645 8.24051V14.9591C13.7645 15.1384 13.7645 15.1729 13.6472 15.2902L12.4535 16.449V16.6973H18.2494V16.449L17.0971 15.3178C16.9954 15.2402 16.9449 15.1128 16.966 14.9867V6.67469C16.9449 6.54856 16.9954 6.42113 17.0971 6.34359L18.277 5.21233V4.96401H14.1923L11.2805 12.2275L7.96858 4.96401H3.68377V5.21233L5.06374 6.87473Z\"\r\n                            fill=\"#5D5D5D\" />\r\n                    </svg>\r\n                </a>\r\n                <a class=\"twitter\" href=\"https://twitter.com/\" target=\"_blank\">\r\n                    <svg width=\"24\" height=\"19\" viewBox=\"0 0 24 19\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                        <path d=\"M24 2.248C23.1166 2.62947 22.1683 2.88819 21.1731 3.00369C22.1899 2.41025 22.9687 1.46896 23.3365 0.350785C22.3828 0.900352 21.3299 1.29941 20.2085 1.51574C19.3107 0.581748 18.0325 0 16.6154 0C13.8966 0 11.6917 2.14863 11.6917 4.79713C11.6917 5.17277 11.735 5.53964 11.8197 5.89046C7.72716 5.69019 4.09976 3.77983 1.67127 0.876963C1.2476 1.58442 1.00601 2.40875 1.00601 3.2887C1.00601 4.95353 1.8768 6.42249 3.19651 7.28191C2.38882 7.25562 1.62981 7.03929 0.966346 6.67971V6.73964C0.966346 9.06366 2.66286 11.0033 4.91466 11.4447C4.5018 11.5529 4.06731 11.6128 3.61659 11.6128C3.29928 11.6128 2.99279 11.5821 2.6899 11.5236C3.31731 13.4311 5.13462 14.8182 7.28906 14.8562C5.60517 16.1425 3.48137 16.9069 1.17368 16.9069C0.777044 16.9069 0.385817 16.8835 0 16.8411C2.17969 18.2049 4.76683 19 7.54688 19C16.6028 19 21.5535 11.6888 21.5535 5.34819L21.5373 4.727C22.5054 4.05459 23.3419 3.20977 24 2.248Z\"\r\n                            fill=\"#5D5D5D\" />\r\n                    </svg>\r\n                </a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</footer>"

/***/ }),

/***/ "./src/app/root-components/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("./src/app/root-components/footer/footer.component.html"),
            styles: [__webpack_require__("./src/app/root-components/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/root-components/header/header.component.css":
/***/ (function(module, exports) {

module.exports = ".logo-header {\r\n    margin-top: 50px;\r\n    height: 50px;\r\n}\r\n\r\n.logo-header > div {\r\n    display: inline-block;\r\n}\r\n\r\n.logo-header > div:first-child {\r\n    opacity: 1;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n    position: relative;\r\n    top: 5px;\r\n}\r\n\r\n.logo-header > div:first-child:hover {\r\n    opacity: 0.8;\r\n    -webkit-transition: all .3s ease-in-out;\r\n    transition: all .3s ease-in-out;\r\n}\r\n\r\n.logo-header > div:first-child img {\r\n    vertical-align: top;\r\n}\r\n\r\n.logo-header .beta-label {\r\n    position: relative;\r\n    top: 4px;\r\n}\r\n\r\n.logo-header .menu-panel > a {\r\n    display: inline-block;\r\n    margin-left: 13px;\r\n}\r\n\r\n.logo-header .menu-panel .notification-indicator {\r\n    height: 25px;\r\n    display: inline-block;\r\n    border-radius: 50px;\r\n    -webkit-box-shadow: 0px 4px 14px rgba(165, 180, 209, 0.3);\r\n            box-shadow: 0px 4px 14px rgba(165, 180, 209, 0.3);\r\n    text-align: center;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    color: rgb(93, 93, 93, 0.5);\r\n    line-height: 25px;\r\n    padding-top: 1px;\r\n    padding-left: 9px;\r\n    padding-right: 9px;\r\n}\r\n\r\n.logo-header .menu-panel .notification-indicator.new-notification {\r\n    color: #F55045;\r\n}\r\n\r\n.logo-header .menu-panel .notification-indicator:hover,\r\n.logo-header .menu-panel .notification-indicator:focus {\r\n    text-decoration: none;\r\n}\r\n\r\n.search-icon {\r\n    vertical-align: middle;\r\n    display: inline-block;\r\n    width: 18px;\r\n    height: 18px;\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2218%22 height%3D%2218%22 viewBox%3D%220 0 18 18%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M7.30705 12.7873C5.84381 12.7873 4.46735 12.2174 3.43249 11.1825C2.39671 10.1477 1.82676 8.7712 1.82676 7.30705C1.82676 5.84381 2.39671 4.46735 3.43249 3.43249C4.46735 2.39671 5.84381 1.82676 7.30705 1.82676C8.77028 1.82676 10.1467 2.39671 11.1816 3.43249C12.2174 4.46735 12.7873 5.84381 12.7873 7.30705C12.7873 8.7712 12.2174 10.1477 11.1816 11.1825C10.1467 12.2174 8.77028 12.7873 7.30705 12.7873ZM18 16.7085L13.075 11.7826C14.0688 10.5075 14.6141 8.95022 14.6141 7.30705C14.6141 5.35515 13.8532 3.52017 12.4731 2.14097C11.0939 0.759933 9.25894 0 7.30705 0C5.35515 0 3.52017 0.759933 2.14097 2.14097C0.760846 3.52017 0 5.35515 0 7.30705C0 9.25894 0.760846 11.0939 2.14097 12.4731C3.52017 13.8542 5.35515 14.6141 7.30705 14.6141C8.95022 14.6141 10.5075 14.0697 11.7826 13.0741L16.7085 18L18 16.7085Z%22 fill%3D%22%235D5D5D%22%2F%3E%0D%3C%2Fsvg%3E%0D\");\r\n    position: relative;\r\n    left: 8px;\r\n    opacity: .5;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n    margin-right: 7px;\r\n}\r\n\r\n.search-icon:hover {\r\n    opacity: 1;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\n.logo-header .button {\r\n    margin-left: 20px;\r\n}\r\n\r\nnav {\r\n    margin-top: 55px;\r\n}\r\n\r\nnav a {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 17px;\r\n    line-height: 23px;\r\n    color: #262626;\r\n    opacity: 0.6;\r\n    text-decoration: none;\r\n    letter-spacing: -0.2px;\r\n    margin-right: 50px;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\nnav a:hover {\r\n    opacity: 1;\r\n    -webkit-transition: all .2s ease-in-out;\r\n    transition: all .2s ease-in-out;\r\n}\r\n\r\nnav a.active {\r\n    color: #F55045;\r\n    opacity: 1;\r\n}\r\n\r\n.menu-wrapper.notifications-block .menu {\r\n    padding-top: 0;\r\n    padding-bottom: 13px;\r\n}\r\n\r\n.menu-wrapper .menu {\r\n    left: -131px;\r\n    margin-top: 10px;\r\n    width: 243px;\r\n    padding-top: 20px;\r\n    padding-bottom: 20px;\r\n}\r\n\r\n.menu-wrapper .menu::after {\r\n    left: 210px;\r\n}\r\n\r\n.menu-wrapper .menu > a {\r\n    display: block;\r\n    margin: 0 auto;\r\n    padding-left: 53px;\r\n    padding-top: 13px;\r\n    padding-bottom: 5px;\r\n}\r\n\r\n.menu-wrapper .menu > a:last-child {\r\n    padding-bottom: 12px;\r\n    margin-top: -5px;\r\n}\r\n\r\n.menu-wrapper .menu > a:hover {\r\n    text-decoration: none;\r\n    background-color: rgb(216, 226, 233, .4);\r\n}\r\n\r\n.menu-wrapper .menu h3 {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 14px;\r\n    line-height: 17px;\r\n    color: #262626;\r\n}\r\n\r\n.menu-wrapper .menu p {\r\n    font-size: 12px;\r\n    color: #5D5D5D;\r\n    margin-bottom: 0;\r\n}\r\n\r\n.menu-wrapper .menu .start-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2218%22 viewBox%3D%220 0 17 18%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M14.4472 15.0372C14.7456 15.3449 14.7542 15.8516 14.4472 16.1672C14.155 16.4693 13.6449 16.4693 13.3511 16.1672L10.0318 12.745L11.1279 11.6149L14.4472 15.0372ZM6.49711 14.1293L4.00493 15.4136L5.25063 12.8441L12.2264 5.65284L13.4721 6.93797L6.49711 14.1293ZM5.37227 7.94098C5.5963 7.80512 5.81024 7.65007 5.99861 7.45586C6.1862 7.26165 6.33659 7.04267 6.46836 6.8117L7.68926 8.06966L6.59317 9.19974L5.37227 7.94098ZM1.57457 3.24082C2.24199 3.53973 2.79004 3.51255 3.31173 3.48618C4.68534 3.41585 5.35586 4.36371 5.35586 5.19569C5.35586 5.62247 5.19462 6.02367 4.90161 6.32657C4.31558 6.93078 3.29468 6.93078 2.70865 6.32657C1.90479 5.49619 1.64433 4.22385 1.57457 3.24082ZM15.1905 3.8794C15.5347 4.23425 15.5347 4.81128 15.1913 5.16533L14.568 5.80789L13.3223 4.52276L13.9456 3.8802C14.2781 3.53654 14.8595 3.53733 15.1905 3.8794ZM13.3511 10.2419L14.4472 11.372L16.7727 8.97436C17.0758 8.66187 17.0758 8.15677 16.7727 7.84428L15.7789 6.81969L16.2882 6.29541C17.2347 5.31718 17.2347 3.72755 16.2874 2.74931C15.3696 1.80305 13.7689 1.80305 12.8488 2.74931L8.78531 6.93957L6.88613 4.98071C6.83575 4.20947 6.5342 3.48778 5.99856 2.93553C4.89316 1.79586 4.02342 1.8486 3.23507 1.88936C2.72811 1.91574 2.24285 2.0548 1.36613 1.15169L0.249106 3.05176e-05L0.0491117 1.61603C0.0305076 1.76868 -0.390411 5.3907 1.61263 7.45586C2.14905 8.00812 2.84903 8.32061 3.59785 8.37176L5.49702 10.3298L4.06528 11.806C4.00637 11.8659 3.95753 11.9362 3.92032 12.013L1.57775 16.8434C1.42892 17.1511 1.48783 17.5227 1.72348 17.7657C1.96146 18.0118 2.32347 18.0678 2.61803 17.9159L7.3024 15.5007C7.37682 15.4624 7.44503 15.412 7.50394 15.3513L8.93569 13.8751L12.255 17.2973C12.6945 17.7505 13.2782 17.9998 13.8991 17.9998C14.52 17.9998 15.1045 17.7505 15.5433 17.2981C15.982 16.8458 16.2246 16.2432 16.2246 15.6022C16.2246 14.962 15.9828 14.3594 15.5433 13.9071L12.224 10.4849L14.6828 7.94977L15.1285 8.40932L13.3511 10.2419Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 24px 11px no-repeat;\r\n}\r\n\r\n.menu-wrapper .menu .settings {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2217%22 viewBox%3D%220 0 17 17%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M11.9001 5.52503C12.8386 5.52503 13.6001 4.76428 13.6001 3.82503C13.6001 2.88663 12.8386 2.12503 11.9001 2.12503C10.9619 2.12503 10.2002 2.88663 10.2002 3.82503C10.2002 4.76428 10.9619 5.52503 11.9001 5.52503Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M3.82495 7.65003H13.175C15.2839 7.65003 17 5.93473 17 3.82503C17 1.71618 15.2839 3.05176e-05 13.175 3.05176e-05H3.82495C1.71606 3.05176e-05 0 1.71618 0 3.82503C0 5.93473 1.71606 7.65003 3.82495 7.65003ZM1.7002 3.82503C1.7002 2.65372 2.65381 1.70003 3.8252 1.70003H13.1753C14.3464 1.70003 15.3003 2.65372 15.3003 3.82503C15.3003 4.99633 14.3464 5.95003 13.1753 5.95003H3.8252C2.65381 5.95003 1.7002 4.99633 1.7002 3.82503Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath d%3D%22M5.09985 14.875C6.03833 14.875 6.7998 14.1143 6.7998 13.175C6.7998 12.2366 6.03833 11.475 5.09985 11.475C4.16162 11.475 3.3999 12.2366 3.3999 13.175C3.3999 14.1143 4.16162 14.875 5.09985 14.875Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M13.175 17H3.82495C1.71606 17 0 15.2847 0 13.175C0 11.0662 1.71606 9.35004 3.82495 9.35004H13.175C15.2839 9.35004 17 11.0662 17 13.175C17 15.2847 15.2839 17 13.175 17ZM3.8252 11.05C2.65381 11.05 1.7002 12.0037 1.7002 13.175C1.7002 14.3463 2.65381 15.3 3.8252 15.3H13.1753C14.3464 15.3 15.3003 14.3463 15.3003 13.175C15.3003 12.0037 14.3464 11.05 13.1753 11.05H3.8252Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 24px 13px no-repeat;\r\n}\r\n\r\n.menu-wrapper .menu .silver {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2216%22 viewBox%3D%220 0 16 16%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M6.34766 11.802C6.8457 12.0074 7.44336 12.11 8.14062 12.11C8.78516 12.11 9.35742 12.0147 9.85547 11.824C10.3633 11.626 10.7539 11.3474 11.0332 10.988C11.3203 10.6214 11.4629 10.1924 11.4629 9.70103C11.4629 9.2537 11.3672 8.88337 11.1758 8.59003C10.9863 8.28937 10.6855 8.04736 10.2734 7.86404C9.87109 7.67337 9.32812 7.5157 8.64648 7.39104C8.22852 7.30303 7.90625 7.2187 7.67773 7.13803C7.45898 7.05003 7.29688 6.9547 7.19531 6.85204C7.09961 6.74937 7.05078 6.62103 7.05078 6.46703C7.05078 6.2397 7.13867 6.06737 7.31445 5.95004C7.40625 5.88931 7.51758 5.8443 7.65039 5.815C7.77344 5.78769 7.91406 5.77403 8.07422 5.77403C8.36133 5.77403 8.59961 5.8327 8.78906 5.95004C8.98828 6.06737 9.10742 6.21037 9.15234 6.37904C9.19727 6.44503 9.24414 6.4927 9.29492 6.52203C9.35352 6.55137 9.42773 6.56603 9.51562 6.56603H10.9668C11.0078 6.56603 11.0449 6.55737 11.0781 6.54005C11.0977 6.52976 11.1152 6.51643 11.1328 6.50003C11.1758 6.45603 11.1992 6.40103 11.1992 6.33503C11.1895 6.12654 11.1309 5.91653 11.0234 5.70498C10.9648 5.58913 10.8906 5.47281 10.8027 5.35603C10.5605 5.0187 10.2051 4.74004 9.73633 4.52003C9.26562 4.30003 8.71289 4.19003 8.07422 4.19003C7.45898 4.19003 6.91992 4.28903 6.45703 4.48703C5.99609 4.68504 5.64062 4.96003 5.39062 5.31203C5.14062 5.66403 5.01562 6.0637 5.01562 6.51103C5.01562 7.17104 5.23242 7.6807 5.66602 8.04003C6.10547 8.39204 6.76562 8.6597 7.64453 8.84303C8.12109 8.9457 8.48047 9.03737 8.72266 9.11803C8.8418 9.15787 8.94531 9.20038 9.0332 9.24557C9.12305 9.29191 9.19531 9.34106 9.25195 9.39304C9.36914 9.4957 9.42773 9.6277 9.42773 9.78903C9.42773 10.0164 9.31641 10.196 9.09766 10.328C8.87695 10.46 8.55859 10.526 8.14062 10.526C7.78906 10.526 7.50586 10.4637 7.29297 10.339C7.08008 10.2144 6.92578 10.0567 6.83203 9.86604C6.77344 9.80003 6.71484 9.75237 6.65625 9.72303C6.60352 9.68636 6.53516 9.66803 6.44727 9.66803H5.06055C4.99414 9.66803 4.93555 9.6937 4.88477 9.74503C4.83984 9.78903 4.81836 9.84037 4.81836 9.89903C4.83398 10.295 4.96484 10.6617 5.21484 10.999C5.4707 11.329 5.84961 11.5967 6.34766 11.802Z%22 fill%3D%22%2326AFFF%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M0 8.00003C0 3.58176 3.58203 3.05176e-05 8 3.05176e-05C12.418 3.05176e-05 16 3.58176 16 8.00003C16 12.4183 12.418 16 8 16C3.58203 16 0 12.4183 0 8.00003ZM1.5 8.00003C1.5 4.68633 4.68555 1.50003 8 1.50003C11.3145 1.50003 14.5 4.68633 14.5 8.00003C14.5 11.3137 11.3145 14.5 8 14.5C4.68555 14.5 1.5 11.3137 1.5 8.00003Z%22 fill%3D%22%2326AFFF%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 24px 13px no-repeat;\r\n}\r\n\r\n.menu-wrapper .menu .sign-out {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2217%22 viewBox%3D%220 0 17 17%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M11.2991 4.49866L12.501 5.70056L9.7019 8.4996L12.501 11.2987L11.2991 12.5005L8.5 9.70236L5.70093 12.5005L4.49902 11.2987L7.2981 8.4996L4.49902 5.70056L5.70093 4.49866L8.5 7.29771L11.2991 4.49866Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M0 8.50003C0 13.1869 3.81299 17 8.5 17C13.187 17 17 13.1869 17 8.50003C17 3.81313 13.187 3.05176e-05 8.5 3.05176e-05C3.81299 3.05176e-05 0 3.81313 0 8.50003ZM1.69995 8.50003C1.69995 4.75067 4.75049 1.70003 8.5 1.70003C12.2493 1.70003 15.3 4.75067 15.3 8.50003C15.3 12.2494 12.2493 15.3 8.5 15.3C4.75049 15.3 1.69995 12.2494 1.69995 8.50003Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 24px 13px no-repeat;\r\n}\r\n\r\n.menu-wrapper .menu .delimiter {\r\n    height: 1px;\r\n    background-color: #D8E2E9;\r\n    width: 188px;\r\n    margin: 0 auto;\r\n    margin-top: 10px;\r\n    margin-bottom: 15px;\r\n}\r\n\r\n.modal .modal-dialog {\r\n\t-webkit-transform: scale(1);\r\n\ttransform: scale(1);\r\n\topacity: 0;\r\n\t-webkit-transition: all 0.3s;\r\n    transition: all 0.3s;\r\n}\r\n\r\n.modal .modal-content {\r\n    border: 0;\r\n}\r\n\r\n.modal.show .modal-dialog {\r\n\t-webkit-transform: scale(1);\r\n\ttransform: scale(1);\r\n    opacity: 1;\r\n    border: 0;\r\n}\r\n\r\n#searchModal .search-wrapper {\r\n    width: 1200px;\r\n    border: 1px solid #D8E2E9;\r\n}\r\n\r\n#searchModal input {\r\n    height: 80px;\r\n    width: 1100px;\r\n    padding-left: 40px;\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 17px;\r\n    outline: 0;\r\n    border: 0;\r\n}\r\n\r\n#searchModal input:focus,\r\n#searchModal input:hover {\r\n    outline: 0;\r\n}\r\n\r\n#searchModal input::-webkit-input-placeholder {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 17px;\r\n    color: #C1C7CB;\r\n}\r\n\r\n#searchModal input:-ms-input-placeholder {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 17px;\r\n    color: #C1C7CB;\r\n}\r\n\r\n#searchModal input::-ms-input-placeholder {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 17px;\r\n    color: #C1C7CB;\r\n}\r\n\r\n#searchModal input::placeholder {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 17px;\r\n    color: #C1C7CB;\r\n}\r\n\r\n#searchModal input::-ms-input-placeholder {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 17px;\r\n    color: #C1C7CB;\r\n}\r\n\r\n#searchModal input:-ms-input-placeholder {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 17px;\r\n    color: #C1C7CB;\r\n}\r\n\r\n#searchModal button {\r\n    width: 90px;\r\n    height: 79px;\r\n    background-color: #fff;\r\n}\r\n\r\n#searchModal button path {\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n#searchModal button:hover path {\r\n    fill: #5D5D5D;\r\n    -webkit-transition: all .1s ease-in-out;\r\n    transition: all .1s ease-in-out;\r\n}\r\n\r\n.modal-dialog {\r\n    max-width: 1200px !important;\r\n    margin-top: 160px;\r\n    margin-left: 15px;\r\n}\r\n\r\n.modal:before {\r\n    content: none;\r\n}\r\n\r\n.notifications-block .menu {\r\n    width: 400px;\r\n    left: -305px;\r\n}\r\n\r\n.notifications-block .inline {\r\n    vertical-align: top;\r\n}\r\n\r\n.notifications-block .all {\r\n    text-align: center;\r\n    margin-top: 8px;\r\n}\r\n\r\n.notifications-block .all span {\r\n    font-family: Rubik;\r\n    font-style: normal;\r\n    font-weight: 500;\r\n    font-size: 11px;\r\n    line-height: 13px;\r\n    letter-spacing: 0.08em;\r\n    text-transform: uppercase;\r\n    color: #5D5D5D;\r\n    border: 0;\r\n    outline: 0;\r\n}\r\n\r\n.notifications-block .all span:hover,\r\n.notifications-block .all span:focus {\r\n    border: 0;\r\n    outline: 0;\r\n}\r\n\r\n.notification-container {\r\n    margin: 0 auto;\r\n    border-bottom: 1px solid #ecf1f4;\r\n    padding-top: 15px;\r\n    padding-bottom: 15px;\r\n    padding-left: 18px;\r\n    padding-right: 18px;\r\n}\r\n\r\n.notification-container .timestamp-block {\r\n    margin-top: -18px;\r\n}\r\n\r\n.notification-container .timestamp-block p.small {\r\n    opacity: 0.5; \r\n}\r\n\r\n.logo-header .menu-panel .notifications-block .menu::after {\r\n    left: 380px;\r\n}\r\n\r\n.notification-container .notification-text-block p.created-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2218%22 viewBox%3D%220 0 17 18%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M14.4472 15.0371C14.7456 15.3448 14.7542 15.8515 14.4472 16.1672C14.155 16.4693 13.6449 16.4693 13.3511 16.1672L10.0318 12.745L11.1279 11.6149L14.4472 15.0371ZM6.49711 14.1292L4.00493 15.4136L5.25063 12.8441L12.2264 5.65281L13.4721 6.93794L6.49711 14.1292ZM5.37227 7.94095C5.5963 7.80509 5.81024 7.65004 5.99861 7.45583C6.1862 7.26162 6.33659 7.04264 6.46836 6.81167L7.68926 8.06962L6.59317 9.19971L5.37227 7.94095ZM1.57457 3.24079C2.24199 3.5397 2.79004 3.51252 3.31173 3.48615C4.68534 3.41582 5.35586 4.36368 5.35586 5.19566C5.35586 5.62244 5.19462 6.02364 4.90161 6.32654C4.31558 6.93075 3.29468 6.93075 2.70865 6.32654C1.90479 5.49616 1.64433 4.22382 1.57457 3.24079ZM15.1905 3.87937C15.5347 4.23422 15.5347 4.81124 15.1913 5.1653L14.568 5.80786L13.3223 4.52273L13.9456 3.88016C14.2781 3.5365 14.8595 3.5373 15.1905 3.87937ZM13.3511 10.2419L14.4472 11.372L16.7727 8.97433C17.0758 8.66184 17.0758 8.15674 16.7727 7.84425L15.7789 6.81966L16.2882 6.29538C17.2347 5.31715 17.2347 3.72752 16.2874 2.74928C15.3696 1.80302 13.7689 1.80302 12.8488 2.74928L8.78531 6.93954L6.88613 4.98068C6.83575 4.20944 6.5342 3.48775 5.99856 2.9355C4.89316 1.79583 4.02342 1.84857 3.23507 1.88933C2.72811 1.91571 2.24285 2.05477 1.36613 1.15166L0.249106 0L0.0491117 1.616C0.0305076 1.76865 -0.390411 5.39067 1.61263 7.45583C2.14905 8.00809 2.84903 8.32058 3.59785 8.37173L5.49702 10.3298L4.06528 11.8059C4.00637 11.8659 3.95753 11.9362 3.92032 12.0129L1.57775 16.8434C1.42892 17.1511 1.48783 17.5227 1.72348 17.7656C1.96146 18.0118 2.32347 18.0677 2.61803 17.9159L7.3024 15.5007C7.37682 15.4623 7.44503 15.412 7.50394 15.3512L8.93569 13.8751L12.255 17.2973C12.6945 17.7505 13.2782 17.9998 13.8991 17.9998C14.52 17.9998 15.1045 17.7505 15.5433 17.2981C15.982 16.8458 16.2246 16.2431 16.2246 15.6022C16.2246 14.962 15.9828 14.3594 15.5433 13.9071L12.224 10.4848L14.6828 7.94974L15.1285 8.40929L13.3511 10.2419Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.accepted-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2212%22 viewBox%3D%220 0 16 12%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M1 5.66667L5.66667 10.3333L15 1%22 stroke%3D%22%2331D431%22 stroke-width%3D%222%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.declined-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2214%22 height%3D%2214%22 viewBox%3D%220 0 14 14%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M14 1.13679L12.8632 0L7 5.86321L1.13679 0L0 1.13679L5.86321 7L0 12.8632L1.13679 14L7 8.13759L12.8632 14L14 12.8632L8.13679 7L14 1.13679Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.uploaded-work {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2218%22 height%3D%2220%22 viewBox%3D%220 0 18 20%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M12.947 3.9225L8.99985 0L5.05266 3.9225L6.23841 5.10083L8.16127 3.19V10.345H9.83843V3.19L11.7613 5.10083L12.947 3.9225Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M1.77778 18.3333L2.89561 15H15.1045L16.2223 18.3333H1.77778ZM17.9162 18.1125L16.5468 14.0308V6.66667C16.5468 6.20667 16.1719 5.83333 15.7082 5.83333H13.1925V7.5H14.8696V13.3333H3.12953V7.5H4.80669V5.83333H2.29095C1.82806 5.83333 1.45237 6.20667 1.45237 6.66667V14.0308L0.0838138 18.1117C-0.0721618 18.5767 -0.00842979 19.0533 0.258238 19.4217C0.524068 19.7892 0.95929 20 1.45237 20H16.5468C17.0399 20 17.4751 19.7892 17.7418 19.4217C18.0084 19.0533 18.0722 18.5767 17.9162 18.1125Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M10.677 17.5H7.32272V15.8333H10.677V17.5Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.new-vote {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%227%22 viewBox%3D%220 0 16 7%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Crect width%3D%227%22 height%3D%227%22 rx%3D%223.5%22 fill%3D%22%23F55045%22%2F%3E%0D%3Crect x%3D%2210%22 y%3D%221%22 width%3D%225%22 height%3D%225%22 rx%3D%222.5%22 stroke%3D%22%23C1C7CB%22 stroke-width%3D%222%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 15px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.won-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2217%22 viewBox%3D%220 0 17 17%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M15.2989 4.28317C15.2989 4.54242 15.1824 4.78552 14.9801 4.94702L13.5938 6.05627C13.5946 6.02057 13.5989 5.98572 13.5989 5.95002V2.55002H15.2989V4.28317ZM8.49375 9.34998C6.61865 9.34998 5.09375 7.82508 5.09375 5.94998V1.69998H11.8937V5.94998C11.8937 7.82508 10.3688 9.34998 8.49375 9.34998ZM2.02077 4.94787C1.81762 4.78552 1.70117 4.54242 1.70117 4.28317V2.55002H3.40117V5.95002C3.40117 5.98572 3.40542 6.02057 3.40627 6.05627L2.02077 4.94787ZM10.1988 15.3H6.79883V14.45H10.1988V15.3ZM16.15 0.85H13.3637C13.0688 0.34425 12.5264 0 11.9 0H5.1C4.47355 0 3.93125 0.34425 3.6363 0.85H0.85C0.3808 0.85 0 1.2308 0 1.7V4.28315C0 5.06175 0.3485 5.78765 0.9571 6.2747L4.5084 9.1154C5.27595 10.081 6.3835 10.7601 7.65 10.9735V12.75H5.95C5.4808 12.75 5.1 13.1308 5.1 13.6V15.3H4.25V17H12.75V15.3H11.9V13.6C11.9 13.1308 11.5192 12.75 11.05 12.75H9.35V10.9735C10.6165 10.7601 11.7241 10.081 12.4916 9.1154L16.0438 6.2747C16.6515 5.78765 17 5.06175 17 4.28315V1.7C17 1.2308 16.6192 0.85 16.15 0.85Z%22 fill%3D%22%2331D431%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.lost-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2217%22 height%3D%2217%22 viewBox%3D%220 0 17 17%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M8.50074 15.3001C4.75139 15.3001 1.70074 12.2494 1.70074 8.50007C1.70074 4.75072 4.75139 1.70007 8.50074 1.70007C12.2501 1.70007 15.3007 4.75072 15.3007 8.50007C15.3007 12.2494 12.2501 15.3001 8.50074 15.3001ZM8.5 0C3.8131 0 0 3.8131 0 8.5C0 13.1877 3.8131 17 8.5 17C13.1869 17 17 13.1877 17 8.5C17 3.8131 13.1869 0 8.5 0ZM12.8586 6.79993C12.8586 6.79993 11.9023 7.86583 11.9023 8.39368C11.9023 8.92153 12.3307 9.34993 12.8586 9.34993C13.3864 9.34993 13.8148 8.92153 13.8148 8.39368C13.8148 7.86583 12.8586 6.79993 12.8586 6.79993ZM4.25 5.10004H7.65V6.80004H4.25V5.10004ZM12.7477 5.10004H9.34766V6.80004H12.7477V5.10004ZM4.94051 12.4987L3.75391 11.2807C5.03146 10.0354 6.71701 9.35034 8.50116 9.35034C10.3185 9.35034 12.0261 10.0575 13.3088 11.341L12.106 12.5429C11.1438 11.5807 9.86371 11.0503 8.50116 11.0503C7.16326 11.0503 5.89846 11.5646 4.94051 12.4987Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 9px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.draw-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%227%22 viewBox%3D%220 0 16 7%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Crect width%3D%227%22 height%3D%227%22 rx%3D%223.5%22 fill%3D%22%23F55045%22%2F%3E%0D%3Crect x%3D%229%22 width%3D%227%22 height%3D%227%22 rx%3D%223.5%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 15px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.new-follow {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2219%22 height%3D%2218%22 viewBox%3D%220 0 19 18%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M6.90681 5.1818C5.95421 5.1818 5.17952 5.95562 5.17952 6.90908C5.17952 7.86168 5.95421 8.63637 6.90681 8.63637C7.8594 8.63637 8.63409 7.86168 8.63409 6.90908C8.63409 5.95562 7.8594 5.1818 6.90681 5.1818ZM6.90948 10.3637C5.00429 10.3637 3.45491 8.81433 3.45491 6.90914C3.45491 5.00394 5.00429 3.45456 6.90948 3.45456C8.81468 3.45456 10.3641 5.00394 10.3641 6.90914C10.3641 8.81433 8.81468 10.3637 6.90948 10.3637Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M1.86523 15.5456H11.9552C11.4387 13.9841 9.50761 12.9547 6.90977 12.9547C4.31194 12.9547 2.38169 13.9841 1.86523 15.5456ZM13.8183 17.2729H0V16.4092C0 13.358 2.84139 11.2274 6.90915 11.2274C10.9778 11.2274 13.8183 13.358 13.8183 16.4092V17.2729Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M18.9995 2.59093H16.4085V0H14.6813V2.59093H12.0903V4.31822H14.6813V6.90915H16.4085V4.31822H18.9995V2.59093Z%22 fill%3D%22%23C1C7CB%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.featured-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2213%22 height%3D%2216%22 viewBox%3D%220 0 13 16%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M0 16V0H13V16L6.5 10.1053L0 16Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.popular-duel {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2219%22 height%3D%2211%22 viewBox%3D%220 0 19 11%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M12.2511 0V1.87922H15.7085L10.3229 7.12787L6.46638 3.36944L0 9.67139L1.36327 11L6.46638 6.02665L10.3229 9.78509L17.0718 3.20782V6.57726H19V0H12.2511Z%22 fill%3D%22%2331D431%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.ended-subscription {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%2216%22 viewBox%3D%220 0 16 16%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Cpath d%3D%22M6.34766 11.802C6.8457 12.0073 7.44336 12.11 8.14062 12.11C8.78516 12.11 9.35742 12.0147 9.85547 11.824C10.3633 11.626 10.7539 11.3473 11.0332 10.988C11.3203 10.6213 11.4629 10.1923 11.4629 9.701C11.4629 9.25367 11.3672 8.88334 11.1758 8.59C10.9863 8.28934 10.6855 8.04733 10.2734 7.86401C9.87109 7.67334 9.32812 7.51567 8.64648 7.39101C8.22852 7.303 7.90625 7.21867 7.67773 7.138C7.45898 7.05 7.29688 6.95467 7.19531 6.85201C7.09961 6.74934 7.05078 6.621 7.05078 6.467C7.05078 6.23967 7.13867 6.06734 7.31445 5.95C7.40625 5.88928 7.51758 5.84427 7.65039 5.81497C7.77344 5.78766 7.91406 5.774 8.07422 5.774C8.36133 5.774 8.59961 5.83267 8.78906 5.95C8.98828 6.06734 9.10742 6.21033 9.15234 6.37901C9.19727 6.445 9.24414 6.49267 9.29492 6.522C9.35352 6.55134 9.42773 6.566 9.51562 6.566H10.9668C11.0078 6.566 11.0449 6.55734 11.0781 6.54002C11.0977 6.52973 11.1152 6.5164 11.1328 6.5C11.1758 6.456 11.1992 6.401 11.1992 6.335C11.1895 6.12651 11.1309 5.9165 11.0234 5.70495C10.9648 5.5891 10.8906 5.47278 10.8027 5.356C10.5605 5.01867 10.2051 4.74001 9.73633 4.52C9.26562 4.3 8.71289 4.19 8.07422 4.19C7.45898 4.19 6.91992 4.289 6.45703 4.487C5.99609 4.68501 5.64062 4.96 5.39062 5.312C5.14062 5.664 5.01562 6.06367 5.01562 6.511C5.01562 7.17101 5.23242 7.68067 5.66602 8.04C6.10547 8.39201 6.76562 8.65967 7.64453 8.843C8.12109 8.94567 8.48047 9.03734 8.72266 9.118C8.8418 9.15784 8.94531 9.20035 9.0332 9.24554C9.12305 9.29188 9.19531 9.34103 9.25195 9.39301C9.36914 9.49567 9.42773 9.62767 9.42773 9.789C9.42773 10.0163 9.31641 10.196 9.09766 10.328C8.87695 10.46 8.55859 10.526 8.14062 10.526C7.78906 10.526 7.50586 10.4637 7.29297 10.339C7.08008 10.2143 6.92578 10.0567 6.83203 9.866C6.77344 9.8 6.71484 9.75233 6.65625 9.723C6.60352 9.68633 6.53516 9.668 6.44727 9.668H5.06055C4.99414 9.668 4.93555 9.69367 4.88477 9.745C4.83984 9.789 4.81836 9.84034 4.81836 9.899C4.83398 10.295 4.96484 10.6617 5.21484 10.999C5.4707 11.329 5.84961 11.5967 6.34766 11.802Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3Cpath fill-rule%3D%22evenodd%22 clip-rule%3D%22evenodd%22 d%3D%22M0 8C0 3.58173 3.58203 0 8 0C12.418 0 16 3.58173 16 8C16 12.4183 12.418 16 8 16C3.58203 16 0 12.4183 0 8ZM1.5 8C1.5 4.68629 4.68555 1.5 8 1.5C11.3145 1.5 14.5 4.68629 14.5 8C14.5 11.3137 11.3145 14.5 8 14.5C4.68555 14.5 1.5 11.3137 1.5 8Z%22 fill%3D%22%23F55045%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 8px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p.started-voting {\r\n    background: url(\"data:image/svg+xml,%3Csvg width%3D%2216%22 height%3D%227%22 viewBox%3D%220 0 16 7%22 fill%3D%22none%22 xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0D%3Crect x%3D%221%22 y%3D%221%22 width%3D%225%22 height%3D%225%22 rx%3D%222.5%22 stroke%3D%22%23C1C7CB%22 stroke-width%3D%222%22%2F%3E%0D%3Crect x%3D%2210%22 y%3D%221%22 width%3D%225%22 height%3D%225%22 rx%3D%222.5%22 stroke%3D%22%23C1C7CB%22 stroke-width%3D%222%22%2F%3E%0D%3C%2Fsvg%3E%0D\") 0% 15px no-repeat;\r\n}\r\n\r\n.notification-container .notification-text-block p {\r\n    padding-left: 28px;\r\n    margin-left: 5px;\r\n    margin-top: 5px;\r\n}\r\n\r\n.notification-container .notification-text-block .notification-text {\r\n    padding-bottom: 15px;\r\n    width: 305px;\r\n}\r\n\r\n.notification-container .is-new-block {\r\n    position: relative;\r\n    top: 22px;\r\n    right: 11px; \r\n}\r\n\r\n.notification-container .is-new-block > div {\r\n    width: 6px;\r\n    height: 6px;\r\n    border-radius: 50%;\r\n    background-color: #F55045;\r\n    opacity: 1;\r\n}\r\n\r\n.notification-container .is-new-block > div.hidden {\r\n    opacity: 0;\r\n}\r\n\r\n.notification-container.new-notification {\r\n    background-color: #fffef4;\r\n}"

/***/ }),

/***/ "./src/app/root-components/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<header>\r\n  <!-- Modals -->\r\n  <div class=\"modal fade\" id=\"{{searchModalId}}\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-body\">\r\n          <div class=\"search-wrapper\">\r\n            <input [(ngModel)]=\"searchQuery\" class=\"left\" placeholder=\"Search for duels, users and more\" (keyup.enter)=\"makeSearch()\"\r\n            />\r\n            <button class=\"right\" (click)=\"makeSearch()\">\r\n              <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M9.74273 17.0498C7.79175 17.0498 5.95646 16.2898 4.57665 14.91C3.19562 13.5302 2.43568 11.6949 2.43568 9.74273C2.43568 7.79175 3.19562 5.95646 4.57665 4.57665C5.95646 3.19562 7.79175 2.43568 9.74273 2.43568C11.6937 2.43568 13.529 3.19562 14.9088 4.57665C16.2898 5.95646 17.0498 7.79175 17.0498 9.74273C17.0498 11.6949 16.2898 13.5302 14.9088 14.91C13.529 16.2898 11.6937 17.0498 9.74273 17.0498ZM24 22.278L17.4334 15.7102C18.7584 14.01 19.4855 11.9336 19.4855 9.74273C19.4855 7.1402 18.471 4.69356 16.6308 2.85462C14.7919 1.01324 12.3453 0 9.74273 0C7.1402 0 4.69356 1.01324 2.85462 2.85462C1.01446 4.69356 0 7.1402 0 9.74273C0 12.3453 1.01446 14.7919 2.85462 16.6308C4.69356 18.4722 7.1402 19.4855 9.74273 19.4855C11.9336 19.4855 14.01 18.7596 15.7102 17.4322L22.278 24L24 22.278Z\"\r\n                  fill=\"#D8E2E9\" />\r\n              </svg>\r\n            </button>\r\n            <div class=\"clearfix\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- End Modals -->\r\n  <div class=\"container\">\r\n    <app-login *ngIf=\"authService.getCurrentUser() === null\"></app-login>\r\n    <div class=\"logo-header\">\r\n      <div class=\"left clickable\" routerLink=\"/\">\r\n        <img class=\"logo\" src=\"../../../assets/img/header/main-logo.svg\">\r\n        <img class=\"beta-label\" src=\"../../../assets/img/header/beta-label.svg\">\r\n      </div>\r\n      <div class=\"right\">\r\n        <div class=\"menu-panel\" *ngIf=\"authService.getCurrentUser() !== null\">\r\n          <button class=\"search-icon\" (click)=\"openSearchModal()\"></button>\r\n          <a id=\"{{notificationsBlockId}}\" class=\"menu-wrapper notifications-block\">\r\n            <a class=\"notification-indicator\" [class.new-notification]=\"notificationService.headerNotificationsAmount > 0\" routerLink=\"/notifications\">{{notificationService.headerNotificationsAmount}}</a>\r\n            <span *ngIf=\"notificationService.headerNotifications.length > 0\" class=\"menu\">\r\n              <div class=\"notification-container\" [class.new-notification]=\"notification.isNewNotification\" *ngFor=\"let notification of notificationService.headerNotifications\">\r\n                <div>\r\n                  <div>\r\n                    <div class=\"image-block inline\">\r\n                      <div class=\"is-new-block\">\r\n                        <div [class.hidden]=\"!notification.isNewNotification\"></div>\r\n                      </div>\r\n                      <app-profile-picture class=\"avatar\" elementClass=\"x-small\" userId={{notification.userId}} fullname={{notification.fullname}}\r\n                        backgroundImage={{notification.imageUrl}} backgroundColor={{notification.imageColor}}>\r\n                      </app-profile-picture>\r\n                    </div>\r\n                    <div class=\"notification-text-block inline\">\r\n                      <p class=\"notification-text {{notificationService.notificationIcons[notification.type]}}\" [innerHTML]=\"notification.text | safeHtml\"></p>\r\n                      <div class=\"timestamp-block\">\r\n                        <p class=\"small\">{{dateFormatter.toRelativeFormat(notification.timestamp)}}</p>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"clearfix\"></div>\r\n                </div>\r\n                <div class=\"clearfix\"></div>\r\n              </div>\r\n              <div class=\"all\">\r\n                <span routerLink=\"/notifications\">See all notifications</span>\r\n              </div>\r\n            </span>\r\n          </a>\r\n          <a class=\"menu-wrapper\">\r\n            <app-profile-picture (click)=\"goToPerson()\" elementClass=\"x-small\" fullname={{authService.getCurrentUser().fullname}} userId={{authService.getCurrentUser().id}}\r\n              backgroundImage={{authService.getCurrentUser().imageUrl}} backgroundColor={{authService.getCurrentUser().imageColor}}>\r\n            </app-profile-picture>\r\n            <span class=\"menu\">\r\n              <a class=\"start-duel\" routerLink=\"/start\">\r\n                <h3>Start a Duel</h3>\r\n                <p>{{getDuelsLeft()}} duels left this week</p>\r\n              </a>\r\n              <a class=\"settings\" routerLink=\"/settings\">\r\n                <h3>Account Settings</h3>\r\n                <p>Change account info</p>\r\n              </a>\r\n              <a *ngIf=\"!authService.getCurrentUser().isPaidAccount\" class=\"silver\" routerLink=\"/silver\">\r\n                <h3>Get Duelity Silver</h3>\r\n                <p>Unleash new features</p>\r\n              </a>\r\n              <div class=\"delimiter\"></div>\r\n              <a class=\"sign-out\" (click)=\"logout()\">\r\n                <h3>Sign Out</h3>\r\n              </a>\r\n            </span>\r\n          </a>\r\n        </div>\r\n        <div *ngIf=\"authService.getCurrentUser() === null\">\r\n          <button class=\"search-icon\" (click)=\"openSearchModal()\"></button>\r\n          <a class=\"button secondary-button\" (click)=\"registerService.goToRegister()\">\r\n            <span>Create Account</span>\r\n          </a>\r\n          <a class=\"button primary-button\" (click)=\"loginService.openModal()\">\r\n            <span>Sign In</span>\r\n          </a>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"clearfix\"></div>\r\n    <nav *ngIf=\"authService.getCurrentUser() !== null\">\r\n      <a routerLink=\"/following\" routerLinkActive=\"active\">Following</a>\r\n      <a routerLink=\"/popular\" routerLinkActive=\"active\">Popular</a>\r\n      <a routerLink=\"/featured\" routerLinkActive=\"active\">Featured</a>\r\n      <a routerLink=\"/new\" routerLinkActive=\"active\">New</a>\r\n      <a routerLink=\"/myduels\" routerLinkActive=\"active\">My Duels</a>\r\n    </nav>\r\n    <nav *ngIf=\"authService.getCurrentUser() === null\">\r\n      <a routerLink=\"/popular\" routerLinkActive=\"active\">Popular</a>\r\n      <a routerLink=\"/featured\" routerLinkActive=\"active\">Featured</a>\r\n      <a routerLink=\"/new\" routerLinkActive=\"active\">New</a>\r\n    </nav>\r\n  </div>\r\n</header>"

/***/ }),

/***/ "./src/app/root-components/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_register_services_register_service__ = __webpack_require__("./src/app/components/register/services/register.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_login_login_service__ = __webpack_require__("./src/app/services/login/login.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_header_service__ = __webpack_require__("./src/app/root-components/header/services/header.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_notification_notification_service__ = __webpack_require__("./src/app/services/notification/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_date_formatter_date_formatter_service__ = __webpack_require__("./src/app/services/date-formatter/date-formatter.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router, authService, socketService, alertService, registerService, loginService, validationService, headerService, notificationService, dateFormatter) {
        this.router = router;
        this.authService = authService;
        this.socketService = socketService;
        this.alertService = alertService;
        this.registerService = registerService;
        this.loginService = loginService;
        this.validationService = validationService;
        this.headerService = headerService;
        this.notificationService = notificationService;
        this.dateFormatter = dateFormatter;
        this.searchModalId = "searchModal";
        this.searchUrl = "search";
        this.notificationsBlockId = "notificationsBlock";
        this.searchQuery = "";
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.authService.loggedIn()) {
            this.notificationService.getNotifications(1);
            setTimeout(function () { _this.initializeNotificationsBlock(); });
        }
    };
    HeaderComponent.prototype.goToPerson = function () {
        var username = this.authService.getCurrentUser().username;
        this.router.navigate(["/user", username]);
    };
    HeaderComponent.prototype.logout = function () {
        this.socketService.removeOnlineUser();
        this.authService.logout();
        this.alertService.success("Signed out. See you soon!");
        window.location.reload();
    };
    HeaderComponent.prototype.openSearchModal = function () {
        var _this = this;
        if (this.router.url.indexOf(this.searchUrl) === -1) {
            this.searchQuery = "";
            $('#' + this.searchModalId).modal();
            setTimeout(function () {
                $("#" + _this.searchModalId + " input").focus();
            }, 300);
        }
        else {
            $('#searchMainInput').focus();
        }
    };
    HeaderComponent.prototype.makeSearch = function () {
        if (this.validationService.isStringValid(this.searchQuery)) {
            this.router.navigate(["/search", this.searchQuery]);
            $('#' + this.searchModalId).modal('hide');
        }
    };
    HeaderComponent.prototype.getDuelsLeft = function () {
        var user = this.authService.getCurrentUser();
        return user.isPaidAccount ? '∞' : user.duelsLeft;
    };
    HeaderComponent.prototype.initializeNotificationsBlock = function () {
        var _this = this;
        $('#' + this.notificationsBlockId)
            .on('mouseenter', function () {
            _this.notificationService.isHeaderOpened = true;
            var unreadNotificationIds = _this.notificationService.headerNotifications.filter(function (notification) { return notification.isNewNotification; })
                .map(function (notification) { return notification.id; });
            _this.notificationService.markNotificationsAsRead(unreadNotificationIds);
        })
            .on('mouseleave', function () {
            _this.notificationService.isHeaderOpened = false;
        });
    };
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-header',
            template: __webpack_require__("./src/app/root-components/header/header.component.html"),
            styles: [__webpack_require__("./src/app/root-components/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__services_socket_socket_service__["a" /* SocketService */],
            __WEBPACK_IMPORTED_MODULE_4__services_alert_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_5__components_register_services_register_service__["a" /* RegisterService */],
            __WEBPACK_IMPORTED_MODULE_6__services_login_login_service__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_7__services_validation_validation_service__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_8__services_header_service__["a" /* HeaderService */],
            __WEBPACK_IMPORTED_MODULE_9__services_notification_notification_service__["a" /* NotificationService */],
            __WEBPACK_IMPORTED_MODULE_10__services_date_formatter_date_formatter_service__["a" /* DateFormatterService */]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/root-components/header/services/header.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_notification_notification_service__ = __webpack_require__("./src/app/services/notification/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeaderService = /** @class */ (function () {
    function HeaderService(notificationService, duelService, router) {
        this.notificationService = notificationService;
        this.duelService = duelService;
        this.router = router;
    }
    HeaderService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_notification_notification_service__["a" /* NotificationService */],
            __WEBPACK_IMPORTED_MODULE_2__services_duel_service_duel_service__["a" /* DuelService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]])
    ], HeaderService);
    return HeaderService;
}());



/***/ }),

/***/ "./src/app/services/alert/alert.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AlertService = /** @class */ (function () {
    function AlertService() {
        this.alertContainerId = "alertContainer";
        this.alertWrapperId = "alertWrapper";
        this.currentMessage = "";
    }
    AlertService.prototype.success = function (message) {
        this.isSuccess = true;
        this.currentMessage = message;
        this.animateAlert();
    };
    AlertService.prototype.error = function (message) {
        this.isSuccess = false;
        this.currentMessage = message;
        this.animateAlert();
    };
    AlertService.prototype.animateAlert = function () {
        var _this = this;
        $('#' + this.alertContainerId).animate({ top: '+=137px' });
        $('#' + this.alertWrapperId).animate({ opacity: 1 });
        setTimeout(function () {
            $('#' + _this.alertContainerId).animate({ top: '-=137px' });
            $('#' + _this.alertWrapperId).animate({ opacity: 0 });
        }, 3000);
    };
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], AlertService);
    return AlertService;
}());



/***/ }),

/***/ "./src/app/services/auth/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthService = /** @class */ (function () {
    function AuthService(http, headersProviderService, tokenProviderService) {
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.tokenProviderService = tokenProviderService;
        this.user = null;
    }
    AuthService.prototype.registerUser = function (user) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/api/register", user, { headers: this.headersProviderService.getJSONHeaders() })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.authenticateUser = function (user) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/api/authenticate", user, { headers: this.headersProviderService.getJSONHeaders() })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.loggedIn = function () {
        return this.tokenProviderService.isTokenValid();
    };
    AuthService.prototype.getUserProfile = function () {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/api/profile", { headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken) })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.checkUserEmail = function (email) {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/api/check-user-email", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                email: email
            }
        })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.checkUserPassword = function (password) {
        this.tokenProviderService.loadToken();
        var data = {
            currentUserId: this.getCurrentUser().id,
            password: password
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/user/check-password", data, { headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken) })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.logout = function () {
        this.tokenProviderService.authToken = null;
        this.user = null;
        localStorage.clear();
    };
    AuthService.prototype.storeUserData = function (token, user) {
        this.tokenProviderService.saveToken(token);
        this.tokenProviderService.authToken = token;
        this.updateUserData(user);
    };
    AuthService.prototype.updateUserData = function (user) {
        localStorage.setItem("user", JSON.stringify(user));
        this.user = user;
    };
    AuthService.prototype.getCurrentUser = function () {
        return this.user || JSON.parse(localStorage.getItem("user"));
    };
    AuthService.prototype.getCurrentUserId = function () {
        var currentUser = this.getCurrentUser();
        return currentUser ? currentUser.id : null;
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_4__request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_5__token_provider_token_provider_service__["a" /* TokenProviderService */]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/category/category.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CategoryService = /** @class */ (function () {
    function CategoryService(http, headersProviderService, tokenProviderService) {
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.tokenProviderService = tokenProviderService;
    }
    CategoryService.prototype.pickCategory = function (category) {
        category.selected = !category.selected;
    };
    CategoryService.prototype.pickOneCategory = function (category, categories) {
        categories.forEach(function (c) { return c.selected = false; });
        this.pickCategory(category);
    };
    CategoryService.prototype.collectCategories = function (categories) {
        var result = [];
        categories.forEach(function (categoryGroup) {
            var selectedCategories = categoryGroup.filter(function (category) { return category.selected; }) || [];
            selectedCategories.forEach(function (category) {
                result.push(category.id);
            });
        });
        return result;
    };
    CategoryService.prototype.getCategories = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/category/get-categories")
            .map(function (res) { return res.json(); });
    };
    CategoryService.prototype.getUserCategories = function (userId) {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/category/get-user-categories", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                userId: userId
            }
        })
            .map(function (res) { return res.json(); });
    };
    CategoryService.prototype.saveUserCategories = function (userId, categories) {
        this.tokenProviderService.loadToken();
        var data = {
            userId: userId,
            categories: categories
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/category/save-user-categories", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    CategoryService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_3__request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_4__token_provider_token_provider_service__["a" /* TokenProviderService */]])
    ], CategoryService);
    return CategoryService;
}());



/***/ }),

/***/ "./src/app/services/cloudinary-service/cloudinary.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CloudinaryService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cloudinary_angular_5_x__ = __webpack_require__("./node_modules/@cloudinary/angular-5.x/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cloudinary_angular_5_x___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__cloudinary_angular_5_x__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__validation_validation_service__ = __webpack_require__("./src/app/services/validation/validation.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CloudinaryService = /** @class */ (function () {
    function CloudinaryService(cloudinary, zone, validationService) {
        var _this = this;
        this.cloudinary = cloudinary;
        this.zone = zone;
        this.validationService = validationService;
        this.responses = [];
        this.files = [];
        this.hasBaseDropZoneOver = false;
        this.uploaderOptions = {
            url: "https://api.cloudinary.com/v1_1/" + this.cloudinary.config().cloud_name + "/upload",
            autoUpload: true,
            isHTML5: true,
            removeAfterUpload: true,
            headers: [
                {
                    name: 'X-Requested-With',
                    value: 'XMLHttpRequest'
                }
            ]
        };
        this.upsertResponse = function (fileItem) {
            _this.zone.run(function () {
                var existingId = _this.responses.reduce(function (prev, current, index) {
                    if (current.file.name === fileItem.file.name && !current.status) {
                        return index;
                    }
                    return prev;
                }, -1);
                if (existingId > -1) {
                    _this.responses[existingId] = Object.assign(_this.responses[existingId], fileItem);
                }
                else {
                    _this.responses.push(fileItem);
                }
            });
        };
        this.responses = [];
    }
    CloudinaryService.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    CloudinaryService.prototype.getFileProperties = function (fileProperties) {
        if (!fileProperties) {
            return null;
        }
        return Object.keys(fileProperties).map(function (key) { return ({ 'key': key, 'value': fileProperties[key] }); });
    };
    CloudinaryService.prototype.destroyUploader = function () {
        this.hasBaseDropZoneOver = false;
        this.responses = [];
        this.files = [];
        this.previewImageFile = null;
        this.previewImageResponse = null;
    };
    CloudinaryService.prototype.initWorkUploaders = function (duelId, userId, tooltipUpdate) {
        var self = this;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"](this.uploaderOptions);
        this.previewImageUploader = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"](this.uploaderOptions);
        this.previewImageFile = null;
        this.previewImageResponse = null;
        this.workUploaderInitHandlers(duelId, userId, tooltipUpdate);
        this.previewImageUploaderInitHandlers(duelId, userId, tooltipUpdate);
    };
    CloudinaryService.prototype.initAvatarUploader = function (userId, uploadCallback) {
        var self = this;
        this.avatarUploader = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"](this.uploaderOptions);
        this.avatarFile = null;
        this.avatarResponse = null;
        this.avatarUploaderInitHandlers(userId, uploadCallback);
    };
    CloudinaryService.prototype.workUploaderInitHandlers = function (duelId, userId, tooltipUpdate) {
        var _this = this;
        this.uploader.onBuildItemForm = function (fileItem, form) {
            form.append('upload_preset', _this.cloudinary.config().upload_preset);
            form.append('folder', "duels/duel-" + duelId + "/user-" + userId + "/work-images");
            form.append('file', fileItem);
            fileItem.withCredentials = false;
            return { fileItem: fileItem, form: form };
        };
        this.uploader.onAfterAddingFile = function (fileItem) {
            if (!_this.validationService.isProImageValid(fileItem.file.type, true)) {
                _this.uploader.removeFromQueue(fileItem);
            }
        };
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            _this.files.push(item.file);
            if (_this.validationService.isStringValid(response)) {
                _this.upsertResponse({
                    file: item.file,
                    status: status,
                    data: JSON.parse(response)
                });
                tooltipUpdate();
            }
        };
        this.uploader.onProgressItem = function (fileItem, progress) {
            _this.upsertResponse({
                file: fileItem.file,
                progress: progress,
                data: {}
            });
        };
        this.uploader.onCompleteAll = function () {
            return void 0;
        };
    };
    CloudinaryService.prototype.previewImageUploaderInitHandlers = function (duelId, userId, tooltipUpdate) {
        var _this = this;
        this.previewImageUploader.onBuildItemForm = function (fileItem, form) {
            form.append('upload_preset', _this.cloudinary.config().upload_preset);
            form.append('folder', "duels/duel-" + duelId + "/user-" + userId + "/preview-image");
            form.append('file', fileItem);
            fileItem.withCredentials = false;
            return { fileItem: fileItem, form: form };
        };
        this.previewImageUploader.onCompleteItem = function (item, response, status, headers) {
            _this.previewImageFile = item.file;
            if (_this.validationService.isStringValid(response)) {
                _this.previewImageResponse = JSON.parse(response);
                tooltipUpdate();
            }
        };
    };
    CloudinaryService.prototype.avatarUploaderInitHandlers = function (userId, callback) {
        var _this = this;
        this.avatarUploader.onBuildItemForm = function (fileItem, form) {
            form.append('upload_preset', _this.cloudinary.config().upload_preset);
            form.append('folder', "avatars/" + userId);
            form.append('file', fileItem);
            fileItem.withCredentials = false;
            return { fileItem: fileItem, form: form };
        };
        this.avatarUploader.onCompleteItem = function (item, response, status, headers) {
            _this.avatarFile = item.file;
            if (_this.validationService.isStringValid(response)) {
                _this.avatarResponse = JSON.parse(response);
                callback(_this.avatarResponse.secure_url, _this.avatarResponse.public_id, "");
            }
        };
    };
    CloudinaryService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__cloudinary_angular_5_x__["Cloudinary"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_3__validation_validation_service__["a" /* ValidationService */]])
    ], CloudinaryService);
    return CloudinaryService;
}());



/***/ }),

/***/ "./src/app/services/date-formatter/date-formatter.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateFormatterService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DateFormatterService = /** @class */ (function () {
    function DateFormatterService() {
    }
    DateFormatterService.prototype.toShortFormat = function (date) {
        date = this.getDateObject(date);
        return date.day + " " + date.month + ", " + date.year;
    };
    DateFormatterService.prototype.toShortFormatWithoutYear = function (date) {
        date = this.getDateObject(date);
        return date.month + " " + date.day;
    };
    DateFormatterService.prototype.toShortFormatWithDashes = function (date) {
        date = this.getNumericDateObject(date);
        return date.year + "-" + this.formatDigits(date.month) + "-" + this.formatDigits(date.day);
    };
    DateFormatterService.prototype.toRelativeFormat = function (date) {
        var now = new Date(), dateObject = this.getDateObject(date), nowObject = this.getDateObject(now);
        date = new Date(date);
        var differenceInMinutes = this.getDateDifferenceInMinutes(date, now);
        if (differenceInMinutes < 1) {
            return "just now";
        }
        if (differenceInMinutes < 60) {
            return differenceInMinutes + " " + (differenceInMinutes === 1 ? "minute" : "minutes") + " ago";
        }
        var differenceInHours = this.getDateDifferenceInHours(date, now);
        if (differenceInHours < 24) {
            return differenceInHours + " " + (differenceInHours === 1 ? "hour" : "hours") + " ago";
        }
        if (differenceInHours > 24 && nowObject.year === dateObject.year) {
            return dateObject.day + " " + dateObject.month;
        }
        else {
            return this.toShortFormat(date);
        }
    };
    DateFormatterService.prototype.getDateDifferenceInDays = function (firstDate, secondDate) {
        firstDate = new Date(firstDate);
        secondDate = new Date(secondDate);
        var diffDays = Math.ceil(Math.abs(this.getDateDifference(firstDate, secondDate)) / (1000 * 60 * 60 * 24));
        return diffDays;
    };
    DateFormatterService.prototype.getDateDifferenceInHours = function (firstDate, secondDate) {
        var oneHour = 1000 * 60 * 60;
        return Math.floor(this.getDateDifference(firstDate, secondDate) / oneHour);
    };
    DateFormatterService.prototype.getDateDifferenceInMinutes = function (firstDate, secondDate) {
        var oneMinute = 1000 * 60;
        return Math.floor(this.getDateDifference(firstDate, secondDate) / oneMinute);
    };
    DateFormatterService.prototype.getDateDifference = function (firstDate, secondDate) {
        return secondDate.getTime() - firstDate.getTime();
    };
    DateFormatterService.prototype.getDateObject = function (date) {
        date = new Date(date);
        return {
            day: date.getDate(),
            month: date.toLocaleString("en-US", { month: "long" }),
            year: date.getFullYear()
        };
    };
    DateFormatterService.prototype.getNumericDateObject = function (date) {
        date = new Date(date);
        return {
            day: date.getDate(),
            month: date.getMonth() + 1,
            year: date.getFullYear()
        };
    };
    DateFormatterService.prototype.formatDigits = function (digit) {
        return digit < 10 ? "0" + digit : digit;
    };
    DateFormatterService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], DateFormatterService);
    return DateFormatterService;
}());



/***/ }),

/***/ "./src/app/services/dribbble/dribbble.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DribbbleService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_EmptyObservable__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/EmptyObservable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DribbbleService = /** @class */ (function () {
    function DribbbleService(http, headersProviderService, tokenProviderService) {
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.tokenProviderService = tokenProviderService;
    }
    DribbbleService.prototype.authorize = function () {
        var config = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].dribbble;
        window.location.href = this.getDribbbleAuthorizeUrl(config.clientId, config.stateKey);
    };
    DribbbleService.prototype.getToken = function (code, state) {
        this.tokenProviderService.loadToken();
        var config = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].dribbble;
        if (!code || state !== config.stateKey) {
            return new __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_EmptyObservable__["a" /* EmptyObservable */]();
        }
        var data = {
            clientId: config.clientId,
            clientSecret: config.clientSecret,
            code: code
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].serverEndpoint + "/api/get-dribbble-token", data)
            .map(function (res) { return res.json(); });
    };
    DribbbleService.prototype.getUserData = function (token) {
        return this.http.get(this.getDribbbleUserLink(), {
            headers: this.headersProviderService.getProtectedHeaders("Bearer " + token)
        })
            .map(function (res) { return res.json(); });
    };
    DribbbleService.prototype.getDribbbleAuthorizeUrl = function (clientId, state) {
        return "https://dribbble.com/oauth/authorize?client_id=" + clientId + "&state=" + state;
    };
    DribbbleService.prototype.getDribbbleUserLink = function () {
        return "https://api.dribbble.com/v2/user";
    };
    DribbbleService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_5__request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_6__token_provider_token_provider_service__["a" /* TokenProviderService */]])
    ], DribbbleService);
    return DribbbleService;
}());



/***/ }),

/***/ "./src/app/services/duel-service/duel.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DuelService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__enums_duel_status__ = __webpack_require__("./src/app/enums/duel-status.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_my_duels_services_my_duels_service__ = __webpack_require__("./src/app/components/my-duels/services/my-duels.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var DuelService = /** @class */ (function () {
    function DuelService(tokenProviderService, authService, headersProviderService, myDuelsService, router, http) {
        this.tokenProviderService = tokenProviderService;
        this.authService = authService;
        this.headersProviderService = headersProviderService;
        this.myDuelsService = myDuelsService;
        this.router = router;
        this.http = http;
        this.duelPage = "duel";
    }
    DuelService.prototype.removeDuel = function (duelId) {
        this.tokenProviderService.loadToken();
        var duelInfo = {
            duelId: duelId,
            currentUserId: this.authService.getCurrentUser().id
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].serverEndpoint + "/duel/remove", duelInfo, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    DuelService.prototype.acceptDuel = function (duelId) {
        this.tokenProviderService.loadToken();
        var duelInfo = {
            duelId: duelId,
            currentUserId: this.authService.getCurrentUser().id
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].serverEndpoint + "/duel/accept", duelInfo, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    DuelService.prototype.getDuelStatus = function (duelId) {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].serverEndpoint + "/duel/get-status", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                duelId: duelId
            }
        })
            .map(function (res) { return res.json(); });
    };
    DuelService.prototype.getDuelPage = function () {
        return this.duelPage;
    };
    DuelService.prototype.goToDuel = function (duelId) {
        var _this = this;
        this.getDuelStatus(duelId).subscribe(function (response) {
            if (response.success) {
                if (response.status === __WEBPACK_IMPORTED_MODULE_4__enums_duel_status__["a" /* DuelStatus */].Waiting || response.status === __WEBPACK_IMPORTED_MODULE_4__enums_duel_status__["a" /* DuelStatus */].InProgress) {
                    _this.myDuelsService.currentStatus = response.status;
                    _this.router.navigate([_this.myDuelsService.getMyDuelsPage()]);
                }
                else {
                    _this.router.navigate([_this.duelPage, duelId]);
                }
            }
            else {
                console.log(response.error);
            }
        });
    };
    DuelService.prototype.getDuelByType = function (duelType, page, pageSize) {
        if (pageSize === void 0) { pageSize = 40; }
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].serverEndpoint + "/duel/get-duels-by-type", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                duelType: duelType,
                currentUserId: this.authService.getCurrentUserId(),
                page: page,
                pageSize: pageSize
            }
        })
            .map(function (res) { return res.json(); });
    };
    DuelService.prototype.changeDuelVisibility = function (duelId, hide) {
        this.tokenProviderService.loadToken();
        var duelInfo = {
            duelId: duelId,
            currentUserId: this.authService.getCurrentUserId(),
            hide: hide
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].serverEndpoint + "/duel/change-visibility", duelInfo, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    DuelService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_7__services_auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_8__components_my_duels_services_my_duels_service__["a" /* MyDuelsService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], DuelService);
    return DuelService;
}());



/***/ }),

/***/ "./src/app/services/duel-status-message/duel-status-message.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DuelStatusMessageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__ = __webpack_require__("./src/app/enums/duel-status.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DuelStatusMessageService = /** @class */ (function () {
    function DuelStatusMessageService(authService) {
        this.authService = authService;
    }
    DuelStatusMessageService.prototype.getStatusMessage = function (duel) {
        switch (duel.status) {
            case __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].Waiting: return this.getWaitingStatusMessage(duel);
            case __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].InProgress: return "left to upload works";
            case __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].Voting: return "until voting ends";
            case __WEBPACK_IMPORTED_MODULE_1__enums_duel_status__["a" /* DuelStatus */].Ended: return "this duel has ended";
        }
    };
    DuelStatusMessageService.prototype.getWaitingStatusMessage = function (duel) {
        return duel.leftUser.user.id === this.authService.getCurrentUser().id ?
            "left for " + duel.rightUser.user.fullname + " to decide" :
            "left to accept or decline duel";
    };
    DuelStatusMessageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_auth_service__["a" /* AuthService */]])
    ], DuelStatusMessageService);
    return DuelStatusMessageService;
}());



/***/ }),

/***/ "./src/app/services/duel-timer/duel-timer.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DuelTimerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DuelTimerService = /** @class */ (function () {
    function DuelTimerService() {
        this.timerInterval = 1000;
    }
    DuelTimerService.prototype.timer = function (endDate) {
        endDate = new Date(endDate).getTime();
        var startDate = new Date().getTime();
        if (isNaN(endDate) || endDate < startDate) {
            return null;
        }
        var timeRemaining = (endDate - startDate) / 1000;
        if (timeRemaining >= 0) {
            var days = timeRemaining / 86400;
            timeRemaining = timeRemaining % 86400;
            var hours = timeRemaining / 3600;
            timeRemaining = timeRemaining % 3600;
            var minutes = timeRemaining / 60;
            timeRemaining = timeRemaining % 60;
            var seconds = timeRemaining;
        }
        return {
            days: Math.floor(days).toString(),
            hours: this.getTimeNumber(Math.floor(hours)).toString(),
            minutes: this.getTimeNumber(Math.floor(minutes)).toString(),
            seconds: this.getTimeNumber(Math.floor(seconds)).toString()
        };
    };
    DuelTimerService.prototype.getTimeNumber = function (digit) {
        return digit % 10 !== digit ? digit : "0" + digit;
    };
    DuelTimerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], DuelTimerService);
    return DuelTimerService;
}());



/***/ }),

/***/ "./src/app/services/google/google.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__socket_socket_service__ = __webpack_require__("./src/app/services/socket/socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_register_services_register_service__ = __webpack_require__("./src/app/components/register/services/register.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login_service__ = __webpack_require__("./src/app/services/login/login.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var GoogleService = /** @class */ (function () {
    function GoogleService(http, authService, socketService, registerService, router, loginService) {
        this.http = http;
        this.authService = authService;
        this.socketService = socketService;
        this.registerService = registerService;
        this.router = router;
        this.loginService = loginService;
    }
    GoogleService.prototype.initGoogleAuth = function (element) {
        var _this = this;
        setTimeout(function () {
            gapi.load('auth2', function () {
                _this.auth2 = gapi.auth2.init({
                    client_id: __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].google.clientId,
                    cookiepolicy: 'single_host_origin',
                    scope: 'profile email'
                });
                _this.attachSignIn(element);
            });
        });
    };
    GoogleService.prototype.attachSignIn = function (element) {
        var _this = this;
        var self = this;
        this.auth2.attachClickHandler(element, {}, function (googleUser) {
            var profile = googleUser.getBasicProfile(), token = googleUser.getAuthResponse().id_token;
            _this.verifyTokenAndAuthorize(token).subscribe(function (response) {
                if (response.success) {
                    if (response.newUser) {
                        _this.registerService.initializeAuthRegister(response.user.email, response.user.fullname);
                        _this.router.navigate(["/register"]);
                    }
                    else {
                        _this.authService.storeUserData(response.token, response.user);
                        _this.socketService.addOnlineUser();
                        _this.router.navigate(["/following"]);
                    }
                    _this.loginService.closeModal();
                }
                else {
                    console.log(response.error);
                }
            });
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    GoogleService.prototype.verifyTokenAndAuthorize = function (token) {
        var data = { token: token };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].serverEndpoint + "/api/verify-google-token", data)
            .map(function (res) { return res.json(); });
    };
    GoogleService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_3__auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__socket_socket_service__["a" /* SocketService */],
            __WEBPACK_IMPORTED_MODULE_6__components_register_services_register_service__["a" /* RegisterService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_7__login_login_service__["a" /* LoginService */]])
    ], GoogleService);
    return GoogleService;
}());



/***/ }),

/***/ "./src/app/services/login/login.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoginService = /** @class */ (function () {
    function LoginService() {
        this.modalId = "loginModal";
    }
    LoginService.prototype.openModal = function () {
        this.getModal().modal();
    };
    LoginService.prototype.closeModal = function () {
        this.getModal().modal('hide');
    };
    LoginService.prototype.getModal = function () {
        return $('#' + this.modalId);
    };
    LoginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/services/notification/notification.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__enums_notification_type__ = __webpack_require__("./src/app/enums/notification-type.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__duel_service_duel_service__ = __webpack_require__("./src/app/services/duel-service/duel.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pager_pager_service__ = __webpack_require__("./src/app/services/pager/pager.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var NotificationService = /** @class */ (function () {
    function NotificationService(tokenProviderService, http, headersProviderService, router, authService, duelService, pager) {
        this.tokenProviderService = tokenProviderService;
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.router = router;
        this.authService = authService;
        this.duelService = duelService;
        this.pager = pager;
        this.notificationIcons = (_a = {},
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].CreatedDuel] = "created-duel",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].AcceptedDuel] = "accepted-duel",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].DeclinedDuel] = "declined-duel",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].OpponentUploadedWork] = "uploaded-work",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].NewVote] = "new-vote",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].WonDuel] = "won-duel",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].LostDuel] = "lost-duel",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].DrawDuel] = "draw-duel",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].NewFollow] = "new-follow",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].FeaturedDuel] = "featured-duel",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].VotedDuelWon] = "new-vote",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].VotedDuelLost] = "new-vote",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].PopularDuel] = "popular-duel",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].EndedSubscription] = "ended-subscription",
            _a[__WEBPACK_IMPORTED_MODULE_7__enums_notification_type__["a" /* NotificationType */].StartedVoting] = "started-voting",
            _a);
        this.notifications = [];
        this.headerNotifications = [];
        this.pages = [];
        this.headerNotificationsAmount = 0;
        this.isHeaderOpened = false;
        this.headerPageSize = 4;
        this.pageSize = 20;
        this.notificationsRoute = "notifications";
        var _a;
    }
    NotificationService.prototype.getNotifications = function (page, isActive, isCurrentPage) {
        var _this = this;
        if (isActive === void 0) { isActive = true; }
        if (isCurrentPage === void 0) { isCurrentPage = false; }
        if (isActive && !isCurrentPage) {
            this.getNotificationsRequest(page).subscribe(function (response) {
                if (response.success) {
                    _this.notifications = response.notifications;
                    _this.headerNotifications = _this.notifications.slice(0, _this.headerPageSize);
                    _this.headerNotificationsAmount = response.newNotificationCount;
                    _this.initializeNotifications(_this.goToPage.bind(_this));
                    _this.pages = _this.pager.getPager(response.totalCount, page, _this.pageSize);
                    var unreadNotificationIds = _this.notifications.filter(function (notification) { return notification.isNewNotification; })
                        .map(function (notification) { return notification.id; });
                    if (_this.shouldMarkAsRead()) {
                        _this.markNotificationsAsRead(unreadNotificationIds);
                    }
                }
                else {
                    console.log(response.error);
                }
            });
        }
        ;
    };
    NotificationService.prototype.markNotificationsAsRead = function (notificationIds) {
        var _this = this;
        this.markNotificationsAsReadRequest(notificationIds).subscribe(function (response) {
            if (response.success) {
                setTimeout(function () {
                    _this.headerNotifications.filter(function (notification) { return notificationIds.indexOf(notification.id) !== -1; }).map(function (notification) {
                        notification.isNewNotification = false;
                    });
                    _this.notifications.filter(function (notification) { return notificationIds.indexOf(notification.id) !== -1; }).map(function (notification) {
                        notification.isNewNotification = false;
                    });
                    _this.headerNotificationsAmount = response.newNotificationCount;
                    _this.initializeNotifications(_this.goToPage.bind(_this));
                }, 1000);
            }
            else {
                console.log("Something went wrong during marking notifications as read");
            }
        });
    };
    //#region API Requests
    NotificationService.prototype.getNotificationsRequest = function (page, pageSize) {
        if (pageSize === void 0) { pageSize = 20; }
        this.tokenProviderService.loadToken();
        var data = {
            userId: this.authService.getCurrentUser().id
        };
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/notification/get-notifications", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                userId: this.authService.getCurrentUser().id,
                page: page,
                pageSize: pageSize
            }
        })
            .map(function (res) { return res.json(); });
    };
    NotificationService.prototype.markNotificationsAsReadRequest = function (notificationIds) {
        this.tokenProviderService.loadToken();
        var notificationInfo = {
            notificationIds: notificationIds,
            currentUserId: this.authService.getCurrentUser().id
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/notification/mark-as-read", notificationInfo, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    //#endregion
    NotificationService.prototype.initializeNotifications = function (goToPage) {
        setTimeout(function () {
            $(".notification-container .notification-text-block span").click(function (e) {
                var link = e.target;
                goToPage($(link).data("page"), $(link).data("identifier"));
            });
        });
    };
    NotificationService.prototype.goToPage = function (page, parameter) {
        if (this.duelService.getDuelPage() === page) {
            this.duelService.goToDuel(parameter);
        }
        else {
            this.router.navigate([page, parameter]);
        }
    };
    NotificationService.prototype.shouldMarkAsRead = function () {
        return this.router.url.includes(this.notificationsRoute) || this.isHeaderOpened;
    };
    NotificationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_9__duel_service_duel_service__["a" /* DuelService */],
            __WEBPACK_IMPORTED_MODULE_10__pager_pager_service__["a" /* PagerService */]])
    ], NotificationService);
    return NotificationService;
}());



/***/ }),

/***/ "./src/app/services/pager/pager.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PagerService = /** @class */ (function () {
    function PagerService() {
        this.pageSize = 10;
        this.pageMaxSize = 5;
        this.pageMinSize = 3;
        this.pageStartFlag = 7;
        this.pageDelimiter = "...";
    }
    PagerService.prototype.isLastElement = function (totalItems) {
        return totalItems % this.pageSize === 1;
    };
    PagerService.prototype.getLastPage = function (totalItems) {
        return Math.ceil(totalItems / this.pageSize);
    };
    PagerService.prototype.getPager = function (totalItems, currentPage, pageSize) {
        if (currentPage === void 0) { currentPage = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        this.pageSize = pageSize;
        var totalPages = Math.ceil(totalItems / this.pageSize), startPage = 1, result = [];
        if (totalPages <= this.pageStartFlag) {
            return this.getPagingArray(startPage, totalPages, currentPage);
        }
        else {
            if (currentPage < this.pageMinSize) {
                result = this.getPagingArray(startPage, this.pageMinSize, currentPage);
                result.push(this.getDelimiterObject());
                result.push(this.getPageObject(totalPages, currentPage));
                return result;
            }
            if (currentPage >= this.pageMaxSize - 2 && currentPage < this.pageMaxSize && totalPages > this.pageMaxSize + 2) {
                result = this.getPagingArray(startPage, this.pageMaxSize, currentPage);
                result.push(this.getDelimiterObject());
                result.push(this.getPageObject(totalPages, currentPage));
                return result;
            }
            if (currentPage >= this.pageMinSize && currentPage < this.pageMaxSize - 2) {
                result = this.getPagingArray(startPage, currentPage + 2, currentPage);
                result.push(this.getDelimiterObject());
                result.push(this.getPageObject(totalPages, currentPage));
                return result;
            }
            if (currentPage >= this.pageMaxSize && currentPage < totalPages - this.pageMaxSize + 2) {
                result = this.getPagingArray(currentPage - 1, currentPage + 1, currentPage);
                result.unshift(this.getDelimiterObject());
                result.unshift(this.getPageObject(startPage, currentPage));
                result.push(this.getDelimiterObject());
                result.push(this.getPageObject(totalPages, currentPage));
                return result;
            }
            if (currentPage > totalPages - this.pageMaxSize - 2 && currentPage <= totalPages - this.pageMinSize + 1) {
                result = this.getPagingArray(currentPage - 1, totalPages, currentPage);
                result.unshift(this.getDelimiterObject());
                result.unshift(this.getPageObject(startPage, currentPage));
                return result;
            }
            if (currentPage >= totalPages - this.pageMaxSize && currentPage < totalPages - this.pageMinSize + 1) {
                result = this.getPagingArray(totalPages - this.pageMaxSize + 1, totalPages, currentPage);
                result.unshift(this.getDelimiterObject());
                result.unshift(this.getPageObject(startPage, currentPage));
                return result;
            }
            if (currentPage >= totalPages - this.pageMinSize) {
                result = this.getPagingArray(totalPages - this.pageMinSize + 1, totalPages, currentPage);
                result.unshift(this.getDelimiterObject());
                result.unshift(this.getPageObject(startPage, currentPage));
                return result;
            }
        }
    };
    PagerService.prototype.getPagingArray = function (startPage, endPage, currentPage) {
        var _this = this;
        return Array.from(Array((endPage + 1) - startPage).keys()).map(function (i) { return _this.getPageObject(startPage + i, currentPage); });
    };
    PagerService.prototype.getPageObject = function (page, currentPage) {
        return {
            symbol: page,
            isActive: true,
            isCurrentPage: currentPage === page
        };
    };
    PagerService.prototype.getDelimiterObject = function () {
        return {
            symbol: this.pageDelimiter,
            isActive: false
        };
    };
    PagerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], PagerService);
    return PagerService;
}());



/***/ }),

/***/ "./src/app/services/report/report.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ReportService = /** @class */ (function () {
    function ReportService(tokenProviderService, http, headersProviderService, authService) {
        this.tokenProviderService = tokenProviderService;
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.authService = authService;
        this.reportCommentModalId = "#reportCommentModal";
        this.selectedCommentId = null;
    }
    ReportService.prototype.openReportCommentModal = function (commentId) {
        this.selectedCommentId = commentId;
        $(this.reportCommentModalId).modal();
    };
    ReportService.prototype.closeReportCommentModal = function () {
        $(this.reportCommentModalId).modal("hide");
    };
    ReportService.prototype.reportComment = function () {
        if (this.selectedCommentId) {
            this.tokenProviderService.loadToken();
            var data = {
                commentId: this.selectedCommentId,
                currentUserId: this.authService.getCurrentUser().id,
            };
            this.selectedCommentId = null;
            return this.http.post(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].serverEndpoint + "/duel/report-comment", data, {
                headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            })
                .map(function (res) { return res.json(); });
        }
    };
    ReportService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_4__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__["a" /* AuthService */]])
    ], ReportService);
    return ReportService;
}());



/***/ }),

/***/ "./src/app/services/request-headers-provider/request-headers-provider.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestHeadersProviderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RequestHeadersProviderService = /** @class */ (function () {
    function RequestHeadersProviderService() {
    }
    RequestHeadersProviderService.prototype.getJSONHeaders = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append("Content-Type", "application/json");
        return headers;
    };
    RequestHeadersProviderService.prototype.getProtectedHeaders = function (token) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append("Authorization", token);
        return headers;
    };
    RequestHeadersProviderService.prototype.getJSONProtectedHeaders = function (token) {
        var headers = this.getJSONHeaders();
        headers.append("Authorization", token);
        return headers;
    };
    RequestHeadersProviderService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], RequestHeadersProviderService);
    return RequestHeadersProviderService;
}());



/***/ }),

/***/ "./src/app/services/socket/socket.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocketService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client__ = __webpack_require__("./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__alert_alert_service__ = __webpack_require__("./src/app/services/alert/alert.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__notification_notification_service__ = __webpack_require__("./src/app/services/notification/notification.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SocketService = /** @class */ (function () {
    function SocketService(authService, router, alertService, notificationService) {
        this.authService = authService;
        this.router = router;
        this.alertService = alertService;
        this.notificationService = notificationService;
        this.onlineUsers = [];
        this.usersAmount = 0;
    }
    SocketService.prototype.initializeSocket = function () {
        this.socket = __WEBPACK_IMPORTED_MODULE_3_socket_io_client__(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].serverEndpoint);
        this.initHandlers();
        this.addOnlineUser();
    };
    SocketService.prototype.addOnlineUser = function () {
        var user = this.authService.getCurrentUser();
        if (user !== null) {
            this.socket.emit('user-connect', user.id);
        }
    };
    SocketService.prototype.removeOnlineUser = function () {
        var user = this.authService.getCurrentUser();
        if (user !== null) {
            this.socket.emit('user-disconnect', user.id);
        }
    };
    SocketService.prototype.initHandlers = function () {
        var _this = this;
        this.socket.on('user-connect', function (users) {
            _this.onlineUsers = users;
        });
        this.socket.on('disconnect', function (users) {
            _this.onlineUsers = users;
        });
        this.socket.on('user-disconnect', function (users) {
            _this.onlineUsers = users;
        });
        this.socket.on('account-created', function (usersAmount) {
            _this.usersAmount = usersAmount;
        });
        this.socket.on('new-notification', function (userId) {
            var user = _this.authService.getCurrentUser();
            if (user && user.id === userId) {
                _this.notificationService.getNotifications(1);
            }
        });
        this.socket.on('user-banned', function (userId) {
            var user = _this.authService.getCurrentUser();
            if (user && user.id === userId) {
                _this.alertService.error("Unfortunately, you are banned. Please, contact administrators!");
                setTimeout(function () {
                    _this.authService.logout();
                    window.location.reload();
                }, 2000);
            }
        });
    };
    SocketService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__auth_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__alert_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_6__notification_notification_service__["a" /* NotificationService */]])
    ], SocketService);
    return SocketService;
}());



/***/ }),

/***/ "./src/app/services/start-duel-service/start-duel.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartDuelService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var StartDuelService = /** @class */ (function () {
    function StartDuelService(http, headersProviderService, tokenProviderService, authService) {
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.tokenProviderService = tokenProviderService;
        this.authService = authService;
    }
    StartDuelService.prototype.getStartDuelInfo = function () {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/start-duel/get-info", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                username: this.selectedUser && this.selectedUser.username,
                currentUsername: this.authService.getCurrentUser().username
            }
        })
            .map(function (res) { return res.json(); });
    };
    StartDuelService.prototype.getOpponents = function (search) {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/start-duel/get-opponents", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                currentUsername: this.authService.getCurrentUser().username,
                search: search
            }
        })
            .map(function (res) { return res.json(); });
    };
    StartDuelService.prototype.startDuel = function (rightUser, duelCategory, duelTask, deadline) {
        this.tokenProviderService.loadToken();
        var date = new Date();
        var endDate = date.setDate(date.getDate() + 1);
        //var endDate = date.setSeconds(date.getSeconds() + 30);
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/start-duel/create-duel", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                leftUser: this.authService.getCurrentUser().id,
                rightUser: rightUser,
                duelCategory: duelCategory,
                duelTask: duelTask,
                deadline: deadline,
                endDate: endDate
            }
        })
            .map(function (res) { return res.json(); });
    };
    StartDuelService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_3__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_4__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_auth_service__["a" /* AuthService */]])
    ], StartDuelService);
    return StartDuelService;
}());



/***/ }),

/***/ "./src/app/services/token-provider/token-provider.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TokenProviderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__ = __webpack_require__("./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular2_jwt__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TokenProviderService = /** @class */ (function () {
    function TokenProviderService() {
        this.authToken = null;
        this.tokenKey = "id_token_duelity";
    }
    TokenProviderService.prototype.loadToken = function () {
        this.authToken = this.getToken();
    };
    TokenProviderService.prototype.getToken = function () {
        return localStorage.getItem(this.tokenKey);
    };
    TokenProviderService.prototype.saveToken = function (token) {
        return localStorage.setItem(this.tokenKey, token);
    };
    TokenProviderService.prototype.isTokenValid = function () {
        return Object(__WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["tokenNotExpired"])(this.tokenKey);
    };
    TokenProviderService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], TokenProviderService);
    return TokenProviderService;
}());



/***/ }),

/***/ "./src/app/services/tooltip/tooltip.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TooltipService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TooltipService = /** @class */ (function () {
    function TooltipService() {
    }
    TooltipService.prototype.initializeTooltip = function (container) {
        $(container).off("mouseenter mouseleave");
        $(container).hover(function (e) {
            var currentTooltip = $($(e.currentTarget).children()[0]);
            var offset = 25;
            var tooltipWidth = currentTooltip.width();
            if (tooltipWidth > 400) {
                currentTooltip.css("width", 400 + "px");
                currentTooltip.css("white-space", "normal");
                tooltipWidth = currentTooltip.width();
            }
            currentTooltip.css("margin-left", -(tooltipWidth + offset) / 2 + "px");
        });
    };
    TooltipService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], TooltipService);
    return TooltipService;
}());



/***/ }),

/***/ "./src/app/services/twitter/twitter.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TwitterService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TwitterService = /** @class */ (function () {
    function TwitterService(http) {
        this.http = http;
    }
    TwitterService.prototype.getTwitterUrl = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].serverEndpoint + "/api/get-twitter-url")
            .map(function (res) { return res.json(); });
    };
    TwitterService.prototype.verifyUser = function (token, verifier) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].serverEndpoint + "/api/verify-twitter-user", {
            params: {
                token: token,
                verifier: verifier
            }
        })
            .map(function (res) { return res.json(); });
    };
    TwitterService.prototype.onTwitterSignIn = function () {
        this.getTwitterUrl().subscribe(function (response) {
            if (response.success) {
                window.location.href = response.url;
            }
            else {
                console.log(response.error);
            }
        });
    };
    TwitterService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]])
    ], TwitterService);
    return TwitterService;
}());



/***/ }),

/***/ "./src/app/services/validation/validation.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ValidationService = /** @class */ (function () {
    function ValidationService() {
    }
    ValidationService_1 = ValidationService;
    ValidationService.prototype.getEmptyFieldMessage = function () {
        return "This field can't be empty";
    };
    ValidationService.prototype.getTooShortMessage = function (fieldName, symbolAmount) {
        return "Your " + fieldName + " can't be shorter than " + symbolAmount + " symbols";
    };
    ValidationService.prototype.getTooShortAltMessage = function (fieldName, symbolAmount) {
        return "Your " + fieldName + " needs to be at least " + symbolAmount + " symbols long";
    };
    ValidationService.prototype.getTooLongMessage = function (fieldName, symbolAmount) {
        return "Your " + fieldName + " can't be longer than " + symbolAmount + " symbols";
    };
    ValidationService.prototype.getForbiddenCharactersMessage = function (fieldName, allowedCharacters) {
        return "Your " + fieldName + " can only have " + allowedCharacters;
    };
    ValidationService.prototype.getIsNotAnEmailMessage = function () {
        return "This is not an email";
    };
    ValidationService.prototype.isObjectValid = function (object) {
        return object !== null && object !== undefined;
    };
    ValidationService.prototype.isStringValid = function (string) {
        return this.isObjectValid(string) && string.replace(/ /g, '') !== "";
    };
    ValidationService.prototype.isEmailValid = function (email) {
        if (!this.isStringValid(email)) {
            return false;
        }
        ;
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    ValidationService.prototype.isEnglishString = function (string) {
        var re = /^[a-zA-Z ]+$/;
        return this.isStringValid(string) && re.test(String(string));
    };
    ValidationService.prototype.isUsernameValid = function (string) {
        var re = /^[a-zA-Z0-9-_]+$/;
        return re.test(String(string));
    };
    ValidationService.prototype.isSocialNetworkValid = function (string) {
        var re = /^[a-zA-Z0-9-_.]+$/;
        return re.test(String(string));
    };
    ValidationService.prototype.isImageValid = function (imageType) {
        return ValidationService_1.imageFormats.indexOf(imageType) >= 0;
    };
    ValidationService.prototype.isProImageValid = function (imageType, isPro) {
        return this.isImageValid(imageType) || (isPro && imageType === ValidationService_1.gifImageFormat);
    };
    //#region Validators
    ValidationService.prototype.validateFullname = function (fullname, isRequired) {
        var min = 3;
        var max = 32;
        var fieldName = "Full Name";
        fullname = fullname == null || fullname == undefined ? "" : fullname;
        var modifiedFullname = fullname.replace(/ /g, '');
        if (isRequired) {
            if (!this.isStringValid(modifiedFullname)) {
                return this.getEmptyFieldMessage();
            }
        }
        if (modifiedFullname.length < min) {
            return this.getTooShortMessage(fieldName, min);
        }
        if (modifiedFullname.length > max) {
            return this.getTooLongMessage(fieldName, max);
        }
        if (!this.isEnglishString(modifiedFullname)) {
            return this.getForbiddenCharactersMessage(fieldName, 'A-Z, a-z, " "');
        }
        return "";
    };
    ValidationService.prototype.validateEmail = function (email, isRequired) {
        var min = 6;
        var max = 64;
        var fieldName = "Email";
        email = email == null || email == undefined ? "" : email;
        var modifiedEmail = email.replace(/ /g, '');
        if (isRequired) {
            if (!this.isStringValid(modifiedEmail)) {
                return this.getEmptyFieldMessage();
            }
        }
        if (email.length < min) {
            return this.getTooShortMessage(fieldName, min);
        }
        if (email.length > max) {
            return this.getTooLongMessage(fieldName, max);
        }
        if (!this.isEmailValid(email)) {
            return this.getIsNotAnEmailMessage();
        }
    };
    ValidationService.prototype.validatePassword = function (password, isRequired) {
        var min = 6;
        var max = 64;
        var fieldName = "Password";
        password = password == null || password == undefined ? "" : password;
        if (isRequired) {
            if (!this.isStringValid(password)) {
                return this.getEmptyFieldMessage();
            }
        }
        if (password.length < min) {
            return this.getTooShortAltMessage(fieldName, min);
        }
        if (password.length > max) {
            return this.getTooLongMessage(fieldName, max);
        }
    };
    ValidationService.prototype.validateUsername = function (username, isRequired) {
        var min = 3;
        var max = 24;
        var fieldName = "Username";
        username = username == null || username == undefined ? "" : username;
        var modifiedUsername = username.replace(/ /g, '');
        if (isRequired) {
            if (!this.isStringValid(username)) {
                return this.getEmptyFieldMessage();
            }
        }
        if (username.length < min) {
            return this.getTooShortAltMessage(fieldName, min);
        }
        if (username.length > max) {
            return this.getTooLongMessage(fieldName, max);
        }
        if (!this.isUsernameValid(username)) {
            return this.getForbiddenCharactersMessage(fieldName, 'A-Z, a-z, 0-9, "-", "_"');
        }
    };
    ValidationService.prototype.validateDescription = function (description, isRequired) {
        var max = 50;
        var fieldName = "Description";
        description = description == null || description == undefined ? "" : description;
        var modifiedDescription = description.replace(/ /g, '');
        if (isRequired) {
            if (!this.isStringValid(modifiedDescription)) {
                return this.getEmptyFieldMessage();
            }
        }
        if (modifiedDescription.length > max) {
            return this.getTooLongMessage(fieldName, max);
        }
    };
    ValidationService.prototype.validateSocialNetwork = function (fieldName, socialNetwork, isRequired) {
        socialNetwork = socialNetwork == null || socialNetwork == undefined ? "" : socialNetwork;
        var modifiedSocialNetwork = socialNetwork.replace(/ /g, '');
        if (isRequired) {
            if (!this.isStringValid(socialNetwork)) {
                return this.getEmptyFieldMessage();
            }
        }
        if (!this.isSocialNetworkValid(socialNetwork)) {
            return this.getForbiddenCharactersMessage(fieldName, 'A-Z, a-z, 0-9, "-", "_" "."');
        }
    };
    ValidationService.imageFormats = ["image/jpeg", "image/png"];
    ValidationService.gifImageFormat = "image/gif";
    ValidationService = ValidationService_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ValidationService);
    return ValidationService;
    var ValidationService_1;
}());



/***/ }),

/***/ "./src/app/services/vote/vote.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoteService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__enums_duel_status__ = __webpack_require__("./src/app/enums/duel-status.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_request_headers_provider_request_headers_provider_service__ = __webpack_require__("./src/app/services/request-headers-provider/request-headers-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_token_provider_token_provider_service__ = __webpack_require__("./src/app/services/token-provider/token-provider.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__auth_auth_service__ = __webpack_require__("./src/app/services/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var VoteService = /** @class */ (function () {
    function VoteService(tokenProviderService, http, headersProviderService, router, authService) {
        this.tokenProviderService = tokenProviderService;
        this.http = http;
        this.headersProviderService = headersProviderService;
        this.router = router;
        this.authService = authService;
    }
    VoteService.prototype.getVoteInfo = function (duelId, orderDirection) {
        this.tokenProviderService.loadToken();
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/duel/vote-info", {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
            params: {
                duelId: duelId,
                orderDirection: orderDirection,
                currentUserId: this.authService.getCurrentUserId()
            }
        })
            .map(function (res) { return res.json(); });
    };
    VoteService.prototype.addComment = function (duelId, text, orderDirection) {
        this.tokenProviderService.loadToken();
        var data = {
            duelId: duelId,
            currentUserId: this.authService.getCurrentUser().id,
            text: text,
            orderDirection: orderDirection
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/duel/add-comment", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    VoteService.prototype.deleteComment = function (commentId, duelId, orderDirection) {
        this.tokenProviderService.loadToken();
        var data = {
            commentId: commentId,
            duelId: duelId,
            currentUserId: this.authService.getCurrentUser().id,
            orderDirection: orderDirection
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/duel/delete-comment", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    VoteService.prototype.getComments = function (duelId, orderDirection, page) {
        this.tokenProviderService.loadToken();
        var data = {
            duelId: duelId,
            orderDirection: orderDirection,
            page: page
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/duel/get-comments", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    VoteService.prototype.addVote = function (duelId, selectedUserId, text, orderDirection) {
        this.tokenProviderService.loadToken();
        var data = {
            duelId: duelId,
            selectedUserId: selectedUserId,
            currentUserId: this.authService.getCurrentUser().id,
            text: text,
            orderDirection: orderDirection
        };
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].serverEndpoint + "/duel/add-vote", data, {
            headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
            .map(function (res) { return res.json(); });
    };
    VoteService.prototype.openVotePage = function (duel) {
        if (duel.status === __WEBPACK_IMPORTED_MODULE_3__enums_duel_status__["a" /* DuelStatus */].Voting || duel.status === __WEBPACK_IMPORTED_MODULE_3__enums_duel_status__["a" /* DuelStatus */].Ended) {
            this.router.navigate(["/duel", duel.numericId]);
        }
    };
    VoteService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__services_token_provider_token_provider_service__["a" /* TokenProviderService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_5__services_request_headers_provider_request_headers_provider_service__["a" /* RequestHeadersProviderService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_7__auth_auth_service__["a" /* AuthService */]])
    ], VoteService);
    return VoteService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    serverEndpoint: "",
    cloudinary: {
        cloud_name: "duelity",
        upload_preset: 'nriiui3e'
    },
    dribbble: {
        clientId: "8a27fe63f7283032ee00d2d3c49db7729a5b9caf54196914a7895eb14c65895a",
        clientSecret: "7eeb96eed6115c11e9e46332356b8653ae06129af738bf2e01e65b3a1d881271",
        stateKey: "96424428d541f16e5d323d3f6bc2f092"
    },
    google: {
        clientId: '609878851118-v2akj5dggsmf2or4hfhpicbornpjmhlu.apps.googleusercontent.com'
    }
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map