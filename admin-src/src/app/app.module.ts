import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Guards
import { AuthGuard } from './guards/auth.guard';

// Global Services
import { AuthService } from './services/auth/auth.service';
import { TokenProviderService } from './services/token-provider/token-provider.service';
import { RequestHeadersProviderService } from './services/request-headers-provider/request-headers-provider.service';
import { SocketService } from './services/socket/socket.service';
import { AdminService } from './services/admin/admin.service';
import { DateFormatterService } from './services/date-formatter/date-formatter.service';
import { ModalService } from './services/modal/modal.service'; 

// Root Components
import { HeaderComponent } from './root-components/header/header.component';
import { BodyComponent } from './root-components/body/body.component';

// Child Components
import { DefaultPictureComponent } from './child-components/default-picture/default-picture.component';
import { ConfirmationModalComponent } from './child-components/confirmation-modal/confirmation-modal.component';

// Screen Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { OverviewComponent } from './components/overview/overview.component';
import { DuelsComponent } from './components/duels/duels.component';
import { UsersComponent } from './components/users/users.component';
import { ReportsComponent } from './components/reports/reports.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'overview', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'overview', component: OverviewComponent, canActivate: [AuthGuard] },
  { path: 'duels', component: DuelsComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'reports', component: ReportsComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OverviewComponent,
    UsersComponent,
    ReportsComponent,
    HeaderComponent,
    BodyComponent,
    DuelsComponent,
    DefaultPictureComponent,
    ConfirmationModalComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    FormsModule
  ],
  providers: [
    AuthService,
    TokenProviderService,
    RequestHeadersProviderService,
    AuthGuard,
    SocketService,
    AdminService,
    DateFormatterService,
    ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
