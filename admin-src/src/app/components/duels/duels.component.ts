import { Component, OnInit } from '@angular/core';
import { DuelEntity } from '../../enums/duel-entity';

@Component({
  selector: 'app-duels',
  templateUrl: './duels.component.html',
  styleUrls: ['./duels.component.css']
})
export class DuelsComponent implements OnInit {

  private link: string = "";

  private readonly entityMapping = {
    "duel": DuelEntity.Duel,
    "user": DuelEntity.User
  }

  constructor() { }

  ngOnInit() {
  }

  getDuelEntity() {
    var entityData = this.parseLink();

    if(!entityData || !entityData.id || entityData.entity === undefined) {
      alert("Invalid link!");
      return;
    }
    
    console.log(entityData);
  }

  private parseLink() {
    var minLinkParts = 2;

    if(!this.link) {
      return null;
    }

    var arr = this.link.split('/');

    if(arr.length < minLinkParts) {
      return null;
    }

    return { 
      id: arr[arr.length - 1],
      entity: this.entityMapping[arr[arr.length - 2]]
    };
  }
}
