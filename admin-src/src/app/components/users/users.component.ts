import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin/admin.service';
import { DateFormatterService } from '../../services/date-formatter/date-formatter.service';
import { ModalService } from '../../services/modal/modal.service';
import { ConfirmationModalSettings } from '../../child-components/confirmation-modal/models/confirmation-modal-settings';
import { OrderDirection } from '../../enums/order-direction';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  private searchString: string = "";
  private users: Array<any> = null;
  private selectedUser: any = null;
  private selectedOrderDirection: OrderDirection = OrderDirection.Desc;

  public orderDirection = OrderDirection;

  constructor(
    private adminService: AdminService,
    private dateFormatter: DateFormatterService,
    private modalService: ModalService
  ) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.adminService.getUsersData(this.searchString, this.selectedOrderDirection).subscribe(response => {
      if(response.success) {
        this.users = response.data;
      } else {
        console.log("Something goes wrong during fetching users data");
      }
    })
  }

  onBanUser(user) {
    var settings = this.modalService.getBanModalSettings(user, this.changeUserBanStatus.bind(this));
    this.selectedUser = user;
    this.modalService.openConfirmationModal(settings);
  }

  onUnbanUser(user) {
    var settings = this.modalService.getUnbanModalSettings(user, this.changeUserBanStatus.bind(this));
    this.selectedUser = user;
    this.modalService.openConfirmationModal(settings);
  }

  private changeUserBanStatus() {
    this.adminService.changeUserBanStatus(this.selectedUser.id, this.searchString).subscribe(response => {
      if(response.success) {
        this.users = response.data;
      } else {
        console.log(response.error);
      }

      this.modalService.closeConfirmationModal();
    });
  }

  private changeOrderDirection(orderDirection) {
    this.selectedOrderDirection = orderDirection;
    this.getUsers();
  }
}
