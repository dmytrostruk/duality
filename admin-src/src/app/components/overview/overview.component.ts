import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../services/socket/socket.service';
import { AdminService } from '../../services/admin/admin.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  private isDataInitialized: boolean = false;
  
  constructor(
    private socketService: SocketService,
    private adminService: AdminService
  ) { }

  ngOnInit() {
    this.initializeData();
  }

  getSubscriptionPercentage() {
    if(this.socketService.usersAmount === 0) {
      return 0;
    }

    var subscriptionPercentage = this.socketService.paidAccountsAmount * 100 / this.socketService.usersAmount;

    return Math.round(subscriptionPercentage);
  }

  private initializeData() {
    this.adminService.getOverviewData().subscribe(response => {
      if(response.success) {
        this.socketService.duelsAmount = response.data.duelsAmount;
        this.socketService.paidAccountsAmount = response.data.paidAccountsAmount;
        this.socketService.usersAmount = response.data.usersAmount;

        this.isDataInitialized = true;
      } else {
        console.log(response.error);
      }
    });
  }
}
