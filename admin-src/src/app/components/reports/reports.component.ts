import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin/admin.service';
import { ReportEntityType } from '../../enums/report-entity-type';
import { DateFormatterService } from '../../services/date-formatter/date-formatter.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  private reportType: ReportEntityType = ReportEntityType.Comment;
  private reportEntityType = ReportEntityType;

  private reports: Array<any> = null;

  constructor(
    private adminService: AdminService,
    public dateFormatter: DateFormatterService
  ) { }

  ngOnInit() {
    this.getReports();
  }

  changeReportType(reportType) {
    this.reportType = reportType;
    this.getReports();
  }

  getReports() {
    this.adminService.getReports(this.reportType).subscribe(response => {
      if(response.success) {
        this.reports = response.reports;
        console.log(this.reports);
      } else {
        console.log(response.error);
      }
    });
  }
}
