export class ConfirmationModalSettings {
    title: string;
    text: string;
    cancelButtonText: string;
    confirmButtonText: string;
    confirmAction: Function
}