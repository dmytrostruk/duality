import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-picture',
  templateUrl: './default-picture.component.html',
  styleUrls: ['./default-picture.component.css']
})
export class DefaultPictureComponent implements OnInit {
  @Input() userId: string;
  @Input() fullname: string;
  @Input() width: number;
  @Input() height: number;
  @Input() fontSize: number;
  @Input() backgroundColor: string;
  @Input() backgroundImage: string;

  constructor() { }

  ngOnInit() { 

  }

  getPictureText() {
    if(!this.fullname) {
      return "";
    }

    this.fullname = this.fullname.trim();

    var words = this.fullname.split(' '),
        firstLetter = words[0].split('')[0],
        lastLetter = words[words.length - 1] ? words[words.length - 1].split('')[0] : "";
    
    return firstLetter + (words.length > 1 ? lastLetter : "");
  }
}
