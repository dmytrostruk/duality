import { Injectable } from '@angular/core';

@Injectable()
export class TokenProviderService {
  authToken = null;

  private tokenKey = "id_token_admin_duelity";

  constructor() { }
  
  loadToken() {
    this.authToken = this.getToken();
  }

  getToken() {
    return localStorage.getItem(this.tokenKey);
  }

  saveToken(token) {
    return localStorage.setItem(this.tokenKey, token);
  }

  isTokenValid() {
    return this.authToken !== null;
  }
}
