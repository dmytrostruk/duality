import { Injectable } from '@angular/core';
import { ConfirmationModalSettings } from '../../child-components/confirmation-modal/models/confirmation-modal-settings';

declare var $: any;

@Injectable()
export class ModalService {

  private readonly confirmationModalId: string = "#confirmationModal";
  public confirmationModalSettings: ConfirmationModalSettings;

  constructor() { }

  openConfirmationModal(settings: ConfirmationModalSettings) {
    $(this.confirmationModalId).modal();
    this.confirmationModalSettings = settings;
  }

  closeConfirmationModal() {
    $(this.confirmationModalId).modal("hide");
    this.confirmationModalSettings = null;
  }

  getBanModalSettings(user, action) : ConfirmationModalSettings {
    var settings = new ConfirmationModalSettings();

    settings.title = `Ban ${user.fullname}?`;
    settings.text = "They won’t be able to access their account anymore";
    settings.cancelButtonText = "No, Cancel";
    settings.confirmButtonText = "Yes, Ban";
    settings.confirmAction = action;

    return settings;
  }

  getUnbanModalSettings(user, action) : ConfirmationModalSettings {
    var settings = new ConfirmationModalSettings();

    settings.title = `Unban ${user.fullname}?`;
    settings.text = "They will be able to access their account again";
    settings.cancelButtonText = "No, Cancel";
    settings.confirmButtonText = "Yes, Unban";
    settings.confirmAction = action;

    return settings;
  }
}
