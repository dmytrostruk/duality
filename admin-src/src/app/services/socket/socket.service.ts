import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as io from 'socket.io-client';

@Injectable()
export class SocketService {

  private socket: any;

  public onlineUsers: Array<any> = [];
  public duelsAmount: number = 0;
  public paidAccountsAmount: number = 0;
  public usersAmount: number = 0;

  constructor() { }

  initializeSocket() {
    this.socket = io(environment.serverEndpoint);
    this.initHandlers();
  }

  initHandlers() {
    this.socket.on('user-connect', users => {
      this.onlineUsers = this.getUniqueValues(users);
    });

    this.socket.on('disconnect', users => {
      this.onlineUsers = this.getUniqueValues(users);
    });

    this.socket.on('user-disconnect', users => {
      this.onlineUsers = this.getUniqueValues(users);
    });

    this.socket.on('duel-created', duelsAmount => {
      this.duelsAmount = duelsAmount;
    });

    this.socket.on('account-created', usersAmount => {
      this.usersAmount = usersAmount;
    });
  }

  private getUniqueValues(arr) {
    if(!arr) return [];

    return typeof arr.filter === "function" ? arr.filter((value, index, self) => self.indexOf(value) === index) : [];
  }
}
