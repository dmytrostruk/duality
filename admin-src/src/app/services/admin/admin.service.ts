import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import { TokenProviderService } from '../token-provider/token-provider.service';
import { RequestHeadersProviderService } from '../request-headers-provider/request-headers-provider.service';
import { OrderDirection } from '../../enums/order-direction';
import { ReportEntityType } from '../../enums/report-entity-type';

@Injectable()
export class AdminService {

  constructor(
    private tokenProviderService: TokenProviderService,
    private headersProviderService: RequestHeadersProviderService,
    private http: Http
  ) { }

  getOverviewData() {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/admin/overview-data",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken)
      })
      .map(res => res.json());
  }

  getUsersData(searchString: string, orderDirection: OrderDirection = OrderDirection.Desc) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/admin/users-data",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          searchString: searchString,
          orderDirection: orderDirection
        } 
      })
      .map(res => res.json());
  }

  changeUserBanStatus(userId, searchString) {
    var data = { 
      userId: userId,
      searchString: searchString
    };

    return this.http.post(environment.serverEndpoint + "/admin/change-user-ban-status", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }
  
  getReports(reportEntityType: ReportEntityType) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/admin/reports",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          reportEntityType: reportEntityType
        } 
      })
      .map(res => res.json());
  }
}
