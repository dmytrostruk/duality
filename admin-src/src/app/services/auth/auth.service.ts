import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/map';

import { RequestHeadersProviderService } from '../request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../token-provider/token-provider.service';

@Injectable()
export class AuthService {

  constructor(
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private tokenProviderService: TokenProviderService
  ) { }

  authenticateUser(user) {
    return this.http.post(environment.serverEndpoint + "/admin/authenticate", user, 
      { headers: this.headersProviderService.getJSONHeaders()})
      .map(res => res.json());
  }

  loggedIn() {
    return this.tokenProviderService.isTokenValid();
  }

  logout() {
    this.tokenProviderService.authToken = null;

    localStorage.clear();
  }

  storeUserData(token) {
    this.tokenProviderService.saveToken(token);
    this.tokenProviderService.authToken = token;
  }
}
