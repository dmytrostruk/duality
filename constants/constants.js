const StartDuelValidationMessages = {
    SkillDifference: "skill difference",
    UserActiveDuel: "existence of your active duel",
    UserDuelsLimit: "your duels amount limitation",
    OpponentActiveDuel: "existence of opponent's active duel",
    OpponentDuelsLimit: "opponent's duels amount limitation",
    CategoriesInCommon: "absence of common categories"
};

const SkillCalculation = {
    MaximumSkillDifference: 50,
    MaximumWinPoints: 100,
    ProAccountMultiplier: 1.5
};

const DuelLimitations = {
    MaxDuelsPerWeek: 3,
};

module.exports = {
    StartDuelValidationMessages,
    SkillCalculation,
    DuelLimitations
};