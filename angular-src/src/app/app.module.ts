import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { FileDropModule } from 'ngx-file-drop';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { FileUploadModule } from 'ng2-file-upload';
import { environment } from '../environments/environment';
import * as  Cloudinary from 'cloudinary-core';

// Guards
import { AuthGuard } from './guards/auth.guard';
import { WorkDeactivateGuard } from './guards/work-deactivate.guard';
import { SettingsDeactivateGuard } from './guards/settings-deactivate.guard';

// Directives
import { ImagePreviewDirective } from './directives/image-preview.directive';
import { BlockBackgroundDirective } from './directives/block-background.directive';

// Pipes
import { SafeHtmlPipe } from './pipes/safe-html.pipe';

// Global Services
import { ValidationService } from './services/validation/validation.service';
import { RequestHeadersProviderService } from './services/request-headers-provider/request-headers-provider.service';
import { AuthService } from './services/auth/auth.service';
import { TokenProviderService } from './services/token-provider/token-provider.service';
import { DateFormatterService } from './services/date-formatter/date-formatter.service';
import { DuelStatusMessageService } from './services/duel-status-message/duel-status-message.service';
import { DuelTimerService } from './services/duel-timer/duel-timer.service';
import { StartDuelService } from './services/start-duel-service/start-duel.service';
import { DuelService } from './services/duel-service/duel.service';
import { CloudinaryService } from './services/cloudinary-service/cloudinary.service';
import { PagerService } from './services/pager/pager.service';
import { NotificationService } from './services/notification/notification.service';
import { VoteService } from './services/vote/vote.service';
import { SocketService } from './services/socket/socket.service';
import { ReportService } from './services/report/report.service';
import { DribbbleService } from './services/dribbble/dribbble.service';
import { GoogleService } from './services/google/google.service';
import { TwitterService } from './services/twitter/twitter.service';
import { CategoryService } from './services/category/category.service';
import { AlertService } from './services/alert/alert.service';
import { LoginService } from './services/login/login.service';
import { TooltipService } from './services/tooltip/tooltip.service';

// Component Services
import { UserService } from './components/user/services/user.service';
import { FollowService } from './components/user/services/follow.service';
import { WorkService } from './components/work/services/work.service';
import { MyDuelsService } from './components/my-duels/services/my-duels.service';
import { AccountSettingsService } from './components/account-settings/services/account-settings.service';
import { DefaultPictureService } from './child-components/default-picture/services/default-picture.service';
import { RegisterService } from './components/register/services/register.service';
import { UserListService } from './child-components/user-list/services/user-list.service';
import { SearchService } from './components/search/services/search.service';
import { PasswordRestoreService } from './components/password-restore/services/password-restore.service';
import { HeaderService } from './root-components/header/services/header.service';

// Child Components
import { DuelComponent } from './child-components/duel/duel.component';
import { WorkModalComponent } from './child-components/work-modal/work-modal.component';
import { DefaultPictureComponent } from './child-components/default-picture/default-picture.component';
import { ReportCommentComponent } from './child-components/report-comment/report-comment.component';
import { UserListComponent } from './child-components/user-list/user-list.component';
import { AlertComponent } from './child-components/alert/alert.component';
import { InputComponent } from './child-components/input/input.component';
import { BackToTopComponent } from './child-components/back-to-top/back-to-top/back-to-top.component';

// Screen Components 
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';
import { StartDuelComponent } from './components/start-duel/start-duel.component';
import { MyDuelsComponent } from './components/my-duels/my-duels.component';
import { HeaderComponent } from './root-components/header/header.component';
import { BodyComponent } from './root-components/body/body.component';
import { FooterComponent } from './root-components/footer/footer.component';
import { FollowingDuelsComponent } from './components/following-duels/following-duels.component';
import { NewDuelsComponent } from './components/new-duels/new-duels.component';
import { PopularDuelsComponent } from './components/popular-duels/popular-duels.component';
import { FeaturedDuelsComponent } from './components/featured-duels/featured-duels.component';
import { WorkComponent } from './components/work/work.component';
import { VoteComponent } from './components/vote/vote.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { AccountSettingsComponent } from './components/account-settings/account-settings.component';
import { ProAccountComponent } from './components/pro-account/pro-account.component';
import { AuthCallbackComponent } from './components/auth-callback/auth-callback.component';
import { SearchComponent } from './components/search/search.component';
import { PasswordRestoreComponent } from './components/password-restore/password-restore.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'following', pathMatch: 'full' },
  { path: 'register', component: RegisterComponent },
  { path: 'following', component: FollowingDuelsComponent, canActivate: [AuthGuard] }, 
  { path: 'new', component: NewDuelsComponent },
  { path: 'popular', component: PopularDuelsComponent },  
  { path: 'featured', component: FeaturedDuelsComponent },
  { path: 'myduels', component: MyDuelsComponent, canActivate: [AuthGuard] }, 
  { path: 'user/:username', component: UserComponent },
  { path: 'start', component: StartDuelComponent, canActivate: [AuthGuard] },
  { path: 'work/:id', component: WorkComponent, canActivate: [AuthGuard], canDeactivate: [WorkDeactivateGuard] },
  { path: 'duel/:id', component: VoteComponent },
  { path: 'notifications', component: NotificationsComponent, canActivate: [AuthGuard] },
  { path: 'settings', component: AccountSettingsComponent, canActivate: [AuthGuard], canDeactivate: [SettingsDeactivateGuard] },
  { path: 'silver', component: ProAccountComponent },
  { path: 'search', component: SearchComponent },
  { path: 'search/:query', component: SearchComponent },
  { path: 'authcallback', component: AuthCallbackComponent },
  { path: 'restore', component: PasswordRestoreComponent },
  { path: '**', redirectTo: 'following', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    DuelComponent,
    StartDuelComponent,
    MyDuelsComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    FollowingDuelsComponent,
    NewDuelsComponent,
    PopularDuelsComponent,
    FeaturedDuelsComponent,
    WorkComponent,
    ImagePreviewDirective,
    BlockBackgroundDirective,
    VoteComponent,
    WorkModalComponent,
    NotificationsComponent,
    SafeHtmlPipe,
    AccountSettingsComponent,
    DefaultPictureComponent,
    ProAccountComponent,
    ReportCommentComponent,
    AuthCallbackComponent,
    UserListComponent,
    SearchComponent,
    PasswordRestoreComponent,
    AlertComponent,
    InputComponent,
    BackToTopComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpModule,
    AutocompleteModule.forRoot(),
    FileDropModule,
    CloudinaryModule.forRoot(Cloudinary, environment.cloudinary),
    FileUploadModule
  ],
  providers: [
    ValidationService,
    RequestHeadersProviderService,
    AuthService,
    AuthGuard,
    WorkDeactivateGuard,
    UserService,
    TokenProviderService,
    DateFormatterService,
    FollowService,
    DuelStatusMessageService,
    DuelTimerService,
    StartDuelService,
    DuelService,
    WorkService,
    CloudinaryService,
    VoteService,
    PagerService,
    NotificationService,
    MyDuelsService,
    AccountSettingsService,
    DefaultPictureService,
    SocketService,
    ReportService,
    DribbbleService,
    GoogleService,
    TwitterService,
    CategoryService,
    RegisterService,
    UserListService,
    SearchService,
    PasswordRestoreService,
    AlertService,
    LoginService,
    TooltipService,
    SettingsDeactivateGuard,
    HeaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
