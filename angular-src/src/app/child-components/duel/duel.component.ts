import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { DuelStatus } from '../../enums/duel-status';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { DuelStatusMessageService } from '../../services/duel-status-message/duel-status-message.service';
import { DuelTimerService } from '../../services/duel-timer/duel-timer.service';
import { DateFormatterService } from '../../services/date-formatter/date-formatter.service';
import { AuthService } from '../../services/auth/auth.service';
import { DuelService } from '../../services/duel-service/duel.service';
import { UserService } from '../../components/user/services/user.service';
import { ValidationService } from '../../services/validation/validation.service';
import { AlertService } from '../../services/alert/alert.service';
import { TooltipService } from '../../services/tooltip/tooltip.service';

declare var $: any;

@Component({
  selector: 'app-duel',
  templateUrl: './duel.component.html',
  styleUrls: ['./duel.component.css']
})
export class DuelComponent implements OnInit, OnChanges {
  @Input() duel: any;
  @Input() duelOwner: any;

  @Input() leftUserModalId: string;
  @Input() rightUserModalId: string;
  @Input() sizeClass: string;

  @Output() notify: EventEmitter<any> = new EventEmitter();

  @ViewChild('lostVotingBar') lostVotingBar;
  @ViewChild('winVotingBar') winVotingBar;
  @ViewChild('duelBlock') duelBlock;
  @ViewChild('votingWrapper') votingWrapper;

  duelStatus = DuelStatus;
  private showComponent: boolean = false;

  constructor(
    private duelStatusMessageService: DuelStatusMessageService,
    private duelTimer: DuelTimerService,
    private dateFormatter: DateFormatterService,
    private authService: AuthService,
    private duelService: DuelService,
    private userService: UserService,
    private validationService: ValidationService,
    private alertService: AlertService,
    private router: Router,
    private tooltipService: TooltipService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.showComponent = true;
      setTimeout(() => { 
        this.tooltipService.initializeTooltip('.duel-tooltip');
       }, 0);
    }, this.duelTimer.timerInterval);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setDuelInformation(changes.duel.currentValue);
  }

  getPercentageValue(value, totalValue) {
    if (totalValue === 0) {
      return 0;
    }

    return Math.round(value * 100 / totalValue);
  }

  private setDuelInformation(duel) {
    duel.statusString = this.duelStatusMessageService.getStatusMessage(duel);

    if (duel.status === DuelStatus.Waiting) {
      duel.deadlineInDays = this.dateFormatter.getDateDifferenceInDays(duel.deadline, duel.createdAt) - 1;
    }

    if (duel.status !== DuelStatus.Ended) {
      if (duel.status === DuelStatus.Voting) {
        setTimeout(() => {
          this.setVotingBarStyles(duel.leftUser.votesAmount, duel.rightUser.votesAmount);
        }, 1005);
      }

      var timerIntervalId = setInterval(() => {
        var timerDate = this.duelTimer.timer(duel.endDate);
        if (timerDate !== null) {
          duel.date = this.getTimerString(timerDate);
        } else {
          switch (duel.status) {
            case DuelStatus.Waiting:
            case DuelStatus.InProgress:
            case DuelStatus.Voting:
              this.notify.emit(this.duelOwner);
              break;
          }

          clearInterval(timerIntervalId);
        }
      }, this.duelTimer.timerInterval);
    } else {
      duel.date = this.dateFormatter.toShortFormatWithoutYear(duel.endDate);
      setTimeout(() => {
        this.setVotingBarStyles(duel.leftUser.votesAmount, duel.rightUser.votesAmount);
      }, 1005);
    }
  }

  isCurrentUser(userId) {
    return this.authService.getCurrentUser().id === userId;
  }

  isWorkUploaded(previewImage) {
    return this.validationService.isObjectValid(previewImage.url);
  }

  removeDuel(duelId, messageState) {
    this.duelService.removeDuel(duelId).subscribe(response => {
      if (response.success) {
        this.notify.emit(response.duels);
        this.alertService.success(`Duel has been ${messageState} and deleted`);
      } else {
        console.log(response.error);
        this.alertService.error(`Duel could not be ${messageState}. Please try again`);
      }
    });
  }

  acceptDuel(duelId) {
    this.duelService.acceptDuel(duelId).subscribe(response => {
      if (response.success) {
        this.notify.emit(response.duels);
        this.alertService.success("Duel has been accepted. Get working!");
      } else {
        console.log(response.error);
        this.alertService.error("Duel could not be accepted. Please try again");
      }
    })
  }

  openWorkPage(duelId) {
    this.router.navigate(["/work", duelId]);
  }

  openModal(modalId) {
    if (modalId) {
      $("#" + modalId).modal('show');
    }
  }

  private setVotingBarStyles(leftVotesAmount, rightVotesAmount) {
    const votingBarSpace = 416;
    
    if (this.winVotingBar !== undefined && this.lostVotingBar !== undefined) {
      var winVotingBar = $(this.winVotingBar.nativeElement),
        lostVotingBar = $(this.lostVotingBar.nativeElement),
        votingWrapper = $(this.votingWrapper.nativeElement);

      var votingBarWidth = Math.round(this.duelBlock.nativeElement.offsetWidth - votingBarSpace);

      winVotingBar.css("width", votingBarWidth);
      lostVotingBar.css("width", votingBarWidth);
      votingWrapper.css("width", votingBarWidth);

      if (leftVotesAmount > rightVotesAmount) {
        winVotingBar.css("z-index", "10");
        lostVotingBar.css("z-index", "1");

        winVotingBar.css("width", votingBarWidth * this.getPercentageValue(leftVotesAmount, leftVotesAmount + rightVotesAmount) / 100);
      } else if (leftVotesAmount < rightVotesAmount) {
        lostVotingBar.css("z-index", "10");
        winVotingBar.css("z-index", "1");

        lostVotingBar.css("width", votingBarWidth * this.getPercentageValue(leftVotesAmount, leftVotesAmount + rightVotesAmount) / 100);
      } else {
        winVotingBar.css("z-index", "10");
        lostVotingBar.css("z-index", "1");

        winVotingBar.css("width", votingBarWidth);
      }
    }
  }

  private getTimerString(timer) {
    var timerString = timer.hours + ":" + timer.minutes;

    var daysNumber = parseInt(timer.days);
    if (daysNumber > 0) {
      timerString = timer.days + " day" + (daysNumber > 1 ? "s" : "");
    }

    return timerString;
  }
}
