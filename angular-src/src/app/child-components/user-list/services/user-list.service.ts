import { Injectable } from '@angular/core';
import { TokenProviderService } from '../../../services/token-provider/token-provider.service';
import { RequestHeadersProviderService } from '../../../services/request-headers-provider/request-headers-provider.service';
import { environment } from '../../../../environments/environment';
import { Http } from '@angular/http';
import { PagerService } from '../../../services/pager/pager.service';
import { UserListOrderCriteria } from '../../../enums/user-list-order-criteria';

declare var $: any;

@Injectable()
export class UserListService {

  private readonly modalId: string = "userList";

  private userIds: Array<any> = [];
  private orderCriteria: UserListOrderCriteria;

  public totalUserCount: number = 0;

  public heading: string = null;
  public users: Array<any> = [];

  private readonly pageSize: number = 6;
  public pages: Array<any> = [];

  constructor(
    private tokenProviderService: TokenProviderService,
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private pager: PagerService
  ) { }

  closeUserListModal() {
    $('#' + this.modalId).modal('hide');
    this.heading = null;
    this.users = [];
  }

  getUsersAndOpenModal(heading: string, userIds: Array<any>, orderCriteria: UserListOrderCriteria) {
    this.userIds = userIds;
    this.heading = heading;
    this.orderCriteria = orderCriteria;

    this.getUserList(this.userIds, orderCriteria, 1, this.pageSize).subscribe(response => {
      if(response.success) {
        $('#' + this.modalId).modal();
        this.users = response.data.users;
        this.totalUserCount = response.data.totalCount;
        this.pages = this.pager.getPager(response.data.totalCount, 1, this.pageSize);
      } else {
        console.log(response.error);
      }
    });
  }

  setPage(page, isActive = true, isCurrentPage = false) {
    if(isActive && !isCurrentPage) {
      this.getUserList(this.userIds, this.orderCriteria, page, this.pageSize).subscribe(response => {
        if(response.success) {
          this.users = response.data.users;
          this.totalUserCount = response.data.totalCount;
          this.pages = this.pager.getPager(response.data.totalCount, page, this.pageSize);
        } else {
          console.log(response.error);
        }
      });
    }
  }

  getUserList(userIds, orderCriteria: UserListOrderCriteria, page: number, pageSize: number) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/user/get-user-list",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          userIds: userIds,
          page: page,
          pageSize: pageSize,
          orderCriteria: orderCriteria
        }
      })
      .map(res => res.json());
  }
}
