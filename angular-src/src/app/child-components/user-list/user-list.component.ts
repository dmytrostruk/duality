import { Component, OnInit, Input } from '@angular/core';
import { UserListService } from './services/user-list.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  constructor(
    public userListService: UserListService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  goToPerson(username) {
    this.userListService.closeUserListModal();
    this.router.navigate(["/user", username]);
  }
}
