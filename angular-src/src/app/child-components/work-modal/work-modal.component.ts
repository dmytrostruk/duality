import { Component, OnInit, Input, HostListener } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-work-modal',
  templateUrl: './work-modal.component.html',
  styleUrls: ['./work-modal.component.css']
})
export class WorkModalComponent implements OnInit {
  @Input() current: any;
  @Input() opponent: any;

  @Input() modalId: string;

  isFirstUserSelected: boolean = true;
  isModalOpened: boolean = false;
  isHeaderFixed: boolean = false;
  currentImages: Array<any> = [];

  constructor() { }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    const left = 37;
    const right = 39;

    var keycode = event.keyCode;
  
    if(keycode == left && this.isModalOpened) {
      this.selectUser(true);
    }

    if(keycode == right && this.isModalOpened) {
      this.selectUser(false);
    }
  }

  ngOnInit() {
    this.selectUser(true);
    setTimeout(() => {
      this.initModal();
    }, 100);
  }

  initModal() {
    var self = this;

    $("#" + this.modalId).off("shown.bs.modal");
    $('#' + this.modalId).on('shown.bs.modal', function () {
      self.isFirstUserSelected = true;
      self.isModalOpened = true;

      var blockHeight = $("#" + self.modalId).find(".user-block").map((index, element) => $(element).height());
      var maxHeight = Math.max(...blockHeight);
      $("#" + self.modalId + " .user-block").height(maxHeight);

      // var headerPosition = $("#" + self.modalId + " .modal-header").offset().top;
      // var previousCounter = 0;

      // $("#" + self.modalId).scroll((e) => {
      //   var scrollPosition =  $("#" + self.modalId).scrollTop();
      //   if(Math.abs(scrollPosition) > 10) {
      //     self.isHeaderFixed = true;
      //   } else {
      //     self.isHeaderFixed = false;
      //   }
      // });
    });

    $("#" + this.modalId).off("hidden.bs.modal");
    $("#" + this.modalId).on('hidden.bs.modal', () => {
      self.isModalOpened = false;
    });
  }

  selectUser(isFirstUser) {
    this.isFirstUserSelected = isFirstUser;
    this.currentImages = isFirstUser ? this.current.imageUrls : this.opponent.imageUrls;
  }
}
