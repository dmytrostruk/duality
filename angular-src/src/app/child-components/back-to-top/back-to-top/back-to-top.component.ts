import { Component, OnInit, ViewChild } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-back-to-top',
  templateUrl: './back-to-top.component.html',
  styleUrls: ['./back-to-top.component.css']
})
export class BackToTopComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.initializeAnimation();
  }

  scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  private initializeAnimation() {
    var buttonElement = $("#backToTop");

    $(window).scroll(() => {
      var scroll = $(window).scrollTop();

      if (scroll >= 500) {
        buttonElement.removeClass("hidden");
      } else {
        buttonElement.addClass("hidden");
      }
    });
  }
}
