import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { InputType } from '../../enums/input-type';
import { ValidationService } from '../../services/validation/validation.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  private errorMessage: string
  private showError: boolean = false;

  @Input() placeholder: string;
  @Input() value: string;
  @Input() type: InputType;
  @Input() counterMinLength: number;
  @Input() elementClass: string = '';
  @Output() onValueChange = new EventEmitter<string>();

  @Input()
  set error(error: string) {
    this.showError = this.validationService.isStringValid(error);
    
    if(this.showError) {
      this.errorMessage = error;
    } else {
      setTimeout(() => {this.errorMessage = error;}, 300);
    }
  }

  inputType = InputType;

  constructor(
    private validationService: ValidationService
  ) { }

  ngOnInit() {
  }

  onChange(e) {
    this.onValueChange.emit(this.value);
  }

  getCounterValue() {
    return this.counterMinLength - this.value.replace(/ /g,'').length;
  }
}
