import { Component, OnInit } from '@angular/core';
import { ReportService } from '../../services/report/report.service';

declare var $: any;

@Component({
  selector: 'app-report-comment',
  templateUrl: './report-comment.component.html',
  styleUrls: ['./report-comment.component.css']
})
export class ReportCommentComponent implements OnInit {

  constructor(private reportService: ReportService) { }

  ngOnInit() {
  }

  onConfirmReport() {
    this.reportService.reportComment().subscribe(response => {
      if(response.success) {
        alert("Report has been successfully sent!");
      } else {
        console.log(response.error);
      }

      this.reportService.closeReportCommentModal();
    })
  }
}
