import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DefaultPictureService } from './services/default-picture.service';
import { SocketService } from '../../services/socket/socket.service';

@Component({
  selector: 'app-profile-picture',
  templateUrl: './default-picture.component.html',
  styleUrls: ['./default-picture.component.css']
})
export class DefaultPictureComponent implements OnInit  {
  @Input() userId: string;
  @Input() fullname: string;
  @Input() elementClass: string;
  @Input() backgroundColor: string;
  @Input() backgroundImage: string;
  @Input() hasBorder: boolean;

  constructor(
    private defaultPictureService: DefaultPictureService,
    public socketService: SocketService
  ) { }

  ngOnInit() { 
    if(!this.backgroundColor) {
      this.backgroundColor = this.defaultPictureService.generateRandomColor();
    }
  }

  getPictureText() {
    if(!this.fullname) {
      return "";
    }

    this.fullname = this.fullname.trim();

    var words = this.fullname.split(' '),
        firstLetter = words[0].split('')[0],
        lastLetter = words[words.length - 1] ? words[words.length - 1].split('')[0] : "";
    
    return firstLetter + (words.length > 1 ? lastLetter : "");
  }
}
