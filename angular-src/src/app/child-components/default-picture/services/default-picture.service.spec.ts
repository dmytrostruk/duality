import { TestBed, inject } from '@angular/core/testing';

import { DefaultPictureService } from './default-picture.service';

describe('DefaultPictureService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DefaultPictureService]
    });
  });

  it('should be created', inject([DefaultPictureService], (service: DefaultPictureService) => {
    expect(service).toBeTruthy();
  }));
});
