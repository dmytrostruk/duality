import { Injectable } from '@angular/core';

@Injectable()
export class DefaultPictureService {

  static readonly backgroundColorArray: Array<string> = ['#FFE226', '#F55045', '#26AFFF'];

  constructor() { }

  generateRandomColor() {
    var arr = DefaultPictureService.backgroundColorArray;
    return arr[Math.floor(Math.random() * arr.length)];
  }
}
