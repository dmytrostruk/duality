import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultPictureComponent } from './default-picture.component';

describe('DefaultPictureComponent', () => {
  let component: DefaultPictureComponent;
  let fixture: ComponentFixture<DefaultPictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultPictureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
