import { Directive, Input, ElementRef, SimpleChanges } from '@angular/core';

@Directive({
  selector: 'img[imgPreview]'
})
export class ImagePreviewDirective {

    @Input() image: any;

    constructor(private el: ElementRef) { }

    ngOnChanges(changes: SimpleChanges) {
        let reader = new FileReader();
        let el = this.el;

        reader.onloadend = function (e) {
            el.nativeElement.src = reader.result;
        };

        if (this.image && this.image.rawFile) {
            return reader.readAsDataURL(this.image.rawFile);
        }
    }
}
