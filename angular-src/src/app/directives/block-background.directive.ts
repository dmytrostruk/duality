import { Directive, SimpleChanges, ElementRef, Input } from '@angular/core';

@Directive({
  selector: 'div[blockBackground]'
})
export class BlockBackgroundDirective {

  @Input() background: any;
  
  constructor(private el: ElementRef) { }

  ngOnChanges(changes: SimpleChanges) {
    let el = this.el;

    if (this.background && this.background.url) {
        el.nativeElement.style.backgroundImage = "url(" + this.background.url + ")";
        el.nativeElement.style.backgroundSize = "cover";
    }
  }
}