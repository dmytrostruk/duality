import { Injectable, Inject } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { AccountSettingsComponent } from '../components/account-settings/account-settings.component';

@Injectable()
export class SettingsDeactivateGuard implements CanDeactivate<AccountSettingsComponent> {
    
    canDeactivate(target: AccountSettingsComponent) {
        if (target.accountSettings.hasChanges(target.previousSettings)) {
            target.openNotSavedModal();
            return target.deactivateSubject;
        }

        return true;
    }
}