import { Injectable, Inject } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { WorkComponent } from '../components/work/work.component';

@Injectable()
export class WorkDeactivateGuard implements CanDeactivate<WorkComponent> {
    
    canDeactivate(target: WorkComponent) {
        if (!target.wasSaved) {
            return window.confirm('You have unsaved changes on this page. Do you want to leave this page before saving?');
        }

        return true;
    }
}