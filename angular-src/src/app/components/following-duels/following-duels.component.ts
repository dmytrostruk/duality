import { Component, OnInit } from '@angular/core';
import { DuelService } from '../../services/duel-service/duel.service';
import { VoteService } from '../../services/vote/vote.service';
import { DuelType } from '../../enums/duel-type';
import { AuthService } from '../../services/auth/auth.service';
import { UserListService } from '../../child-components/user-list/services/user-list.service';
import { Router } from '@angular/router';
import { UserListOrderCriteria } from '../../enums/user-list-order-criteria';
import { Title } from '@angular/platform-browser';
import { TooltipService } from '../../services/tooltip/tooltip.service';

declare var $: any;

@Component({
  selector: 'app-following-duels',
  templateUrl: './following-duels.component.html',
  styleUrls: ['./following-duels.component.css']
})
export class FollowingDuelsComponent implements OnInit {

  private readonly previewUserAmount: number = 5;
  private totalUserCount: number = 0;
  private duelsPage: number = 1;
  private totalDuelsAmount: number = 0;
  private isPopularDuels: boolean = false;

  duels: Array<any> = [];
  following: Array<any> = [];
  previewUsers: Array<any> = [];

  constructor(
    private duelService: DuelService,
    private voteService: VoteService,
    private authService: AuthService,
    private userListService: UserListService,
    private router: Router,
    private titleService: Title,
    private tooltipService: TooltipService
  ) { }

  ngOnInit() {
    this.initializeData();
    this.titleService.setTitle("Duelity - one on one design duels with equal opponents");
  }

  onNotify() {
    this.initializeData();
  }

  openVotePage(duel) {
    this.voteService.openVotePage(duel);
  }

  goToPerson(username) {
    this.router.navigate(["/user", username]);
  }

  openUserFollowing() {
    var followingIds = this.following.map(following => following.userId);
    this.userListService.getUsersAndOpenModal("Following", followingIds, UserListOrderCriteria.SkillPoints);
  }

  loadNextPage() {
    this.duelsPage++;
    this.loadDuels();
  }

  private initializeData() {
    this.duelService.getDuelByType(DuelType.Following, this.duelsPage).subscribe(response => {
      if (response.success) {
        this.duels.push(...response.duels);
        this.totalDuelsAmount = response.totalAmount;
        this.isPopularDuels = response.isPopularDuels;

        if (this.duels.length > 0) {
          this.authService.getUserProfile().subscribe(profile => {
            this.following = profile.user.following;
            var followingIds = this.following.map(following => following.userId);

            this.userListService.getUserList(followingIds, UserListOrderCriteria.SkillPoints, 1, this.previewUserAmount).subscribe(response => {
              if (response.success) {
                this.previewUsers = response.data.users;
                this.totalUserCount = response.data.totalCount;
                this.initializePreviewUsers();
              } else {
                console.log(response.error);
              }
            });
          });
        }
      } else {
        console.log("Something goes wrong during fetching following duels.");
      }
    });
  }

  private loadDuels() {
    this.duelService.getDuelByType(DuelType.Popular, this.duelsPage).subscribe(response => {
      if (response.success) {
        this.duels.push(...response.duels);
        this.totalDuelsAmount = response.totalAmount;
      } else {
        console.log("Something goes wrong during fetching popular duels.");
      }
    });
  }

  private initializePreviewUsers() {
    setTimeout(() => {
      var avatars = $(".avatar");
      var rightPosition = this.totalUserCount > this.previewUserAmount ? 10 : 0;
      var zIndex = 6;

      if (avatars.length > 0) {
        for (var i = avatars.length - 1; i >= 0; i--) {
          var avatar = $(avatars[i]);
          avatar.css("right", `-${rightPosition}px`);
          avatar.css("z-index", zIndex);

          rightPosition += 5;
          zIndex += 1;
        }

        $(".avatars-label").css("right", `${16 - (rightPosition - 5)}px`);
        this.tooltipService.initializeTooltip('.custom-tooltip');
      }
    }, 0);
  }
}
