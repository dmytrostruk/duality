import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowingDuelsComponent } from './following-duels.component';

describe('FollowingDuelsComponent', () => {
  let component: FollowingDuelsComponent;
  let fixture: ComponentFixture<FollowingDuelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowingDuelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowingDuelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
