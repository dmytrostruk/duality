import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderDirection } from '../../enums/order-direction';
import { VoteService } from '../../services/vote/vote.service';
import { AuthService } from '../../services/auth/auth.service';
import { ValidationService } from '../../services/validation/validation.service';
import { DateFormatterService } from '../../services/date-formatter/date-formatter.service';
import { PagerService } from '../../services/pager/pager.service';
import { DuelStatus } from '../../enums/duel-status';
import { InputType } from '../../enums/input-type';
import { ReportService } from '../../services/report/report.service';
import { Title } from '@angular/platform-browser';
import { AlertService } from '../../services/alert/alert.service';
import { LoginService } from '../../services/login/login.service';
import { RegisterService } from '../register/services/register.service';

declare var $: any;

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})
export class VoteComponent implements OnInit, OnDestroy {

  private subscription: ISubscription;
  private duel: any = null;
  private duelId: string = null;
  private currentUserId: any;
  private commentsData: any;
  private commentText: string = "";
  private votedUser: any = null;
  private commentsMinLength: number = 20;
  private pages: Array<any> = [];
  private isPageLoaded: boolean = false;

  private isExpandedTask: boolean = false;
  private expandTaskButtonText: string = "Expand Task";
  private duelTaskHeight: number = 0;
  private minDuelTaskHeight: number = 150;

  private readonly orderDirectionKey = "comments_order_direction";
  private selectedOrderDirection: OrderDirection;

  duelStatus = DuelStatus;
  orderDirection = OrderDirection;
  inputType = InputType;

  @ViewChild('expandButton') expandButton;
  @ViewChild('duelTaskBlock') duelTaskBlock;

  constructor(
    private activatedRoute: ActivatedRoute,
    private voteService: VoteService,
    private authService: AuthService,
    private router: Router,
    private validationService: ValidationService,
    public dateFormatter: DateFormatterService,
    private pager: PagerService,
    public reportService: ReportService,
    private titleService: Title,
    private alertService: AlertService,
    private loginService: LoginService,
    private registerService: RegisterService
  ) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.duelId = params['id'];
      this.getVoteInfo();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onNotify(userId) {
    this.getVoteInfo();  
  }

  onCommentTextChange(commentText) {
    this.commentText = commentText;
  }

  addComment() {
    if(this.currentUserId === null) {
      this.loginService.openModal();
    } else {
      this.voteService.addComment(this.duel.id, this.commentText, OrderDirection.Asc).subscribe(result => {
        if(result.success) {
          this.commentText = "";
  
          if(this.selectedOrderDirection === OrderDirection.Desc) {
            this.setPage(1);
          } else {
            var lastPage = this.pager.getLastPage(this.commentsData.totalCount + 1);
            this.setPage(lastPage);
          }
        } else {
          console.log("Error during adding comment.");
        }
      });
    }
  }

  addVote() {
    if(this.currentUserId === null) {
      this.loginService.openModal();
    } else {
      this.voteService.addVote(this.duel.id, this.votedUser, this.commentText, OrderDirection.Asc).subscribe(result => {
        if(result.success) {
          this.duel = result.duel;
          this.commentsData = result.commentsData;
          this.commentText = "";
          this.alertService.success(`Voted for ${this.getVotedUserFullname()}. There’s no turning back!`);
        } else {
          console.log("Error during voting.");
          this.alertService.error(`Your vote for ${this.getVotedUserFullname()} was not sent. Please try again`)
        }
      });
    }
  }

  deleteComment(commentId) {
    this.voteService.deleteComment(commentId, this.duel.id, OrderDirection.Asc).subscribe(result => {
      if(result.success) {
        if(this.commentsData.totalCount === 1) {
          this.pages = [];
          this.commentsData.comments = [];
          this.commentsData.totalCount = 0;
        } else {
          var currentPage = this.pages.filter(page => page.isCurrentPage)[0].symbol,
          isLastComment = this.pager.isLastElement(this.commentsData.totalCount),
          page = isLastComment ? currentPage - 1 : currentPage;

          this.setPage(page);
        }
      } else {
        console.log("Error during deleting comment.");
      }
    });
  }

  goToPerson(username) {
    this.router.navigate(["/user", username]);
  }

  validateComments(comments) {
    return comments.replace(/ /g,'').length >= this.commentsMinLength;
  }

  validateVote(comments) {
    return this.validateComments(comments) && this.validationService.isObjectValid(this.votedUser);
  }

  setPage(page, isActive = true, isCurrentPage = false) {
    if(isActive && !isCurrentPage) {
      this.voteService.getComments(this.duel.id, this.selectedOrderDirection, page).subscribe(result => {
        if(result.success) {
          this.commentsData = result.commentsData;
          this.pages = this.pager.getPager(this.commentsData.totalCount, page);
        } else {
          console.log("Error during fetching comments.");
        }
      });
    }
  }

  changeCommentsOrder(orderDirection: OrderDirection) {
    if(this.selectedOrderDirection !== orderDirection) {
      this.selectedOrderDirection = orderDirection;
      localStorage.setItem(this.orderDirectionKey, JSON.stringify(orderDirection));

      this.setPage(1);
    }
  }

  toogleTaskExpand() {
    var svg = $($(this.expandButton.nativeElement).find("svg"));
    var taskBlock = this.getTaskBlock();

    if(this.isExpandedTask) {
      svg.addClass("spin");
      taskBlock.height(this.minDuelTaskHeight);
      setTimeout(() =>{ 
        svg.removeClass("spin");
        this.expandTaskButtonText = "Expand Task";
        this.isExpandedTask = false;
      }, 200);
    } else {
      svg.addClass("spin");
      taskBlock.height(this.duelTaskHeight);
      setTimeout(() => { 
        svg.removeClass("spin");
        this.expandTaskButtonText = "Collapse Task";
        this.isExpandedTask = true;
      }, 200);
    }
  }

  initializeExpandableBlock() {
    var taskBlock = this.getTaskBlock();
    this.duelTaskHeight = taskBlock.height();

    if(this.duelTaskHeight < this.minDuelTaskHeight) {
      var btn = $(this.expandButton.nativeElement);
      btn.hide();
    }

    taskBlock.height(this.minDuelTaskHeight);
  }

  getTaskBlock() {
    return $(this.duelTaskBlock.nativeElement);
  }

  private getVoteInfo() {
    var commentsOrder = JSON.parse(localStorage.getItem(this.orderDirectionKey));
    this.selectedOrderDirection = commentsOrder;

    this.voteService.getVoteInfo(this.duelId, this.selectedOrderDirection).subscribe(response => {
      this.isPageLoaded = true;
      
      if(response.success) {
        if(response.duel && (response.duel.status === DuelStatus.Voting || response.duel.status === DuelStatus.Ended)) {
          this.duel = response.duel;
          this.currentUserId = this.authService.getCurrentUserId();
          this.commentsData = response.commentsData;

          this.titleService.setTitle(`${this.duel.leftUser.user.fullname} vs ${this.duel.rightUser.user.fullname} - Duelity`)
        
          this.pages = this.pager.getPager(this.commentsData.totalCount, 1);

          setTimeout(() => {this.initializeExpandableBlock()}, 1000);
        }
      } else {
        console.log(response.error);
      }
    })
  }

  private getVotedUserFullname() {
    return this.votedUser === this.duel.leftUser.user.id ? 
           this.duel.leftUser.user.fullname : 
           this.duel.right.user.fullname;
  }
}
