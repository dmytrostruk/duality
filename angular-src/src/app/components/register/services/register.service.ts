import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TokenProviderService } from '../../../services/token-provider/token-provider.service';
import { Http } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { RequestHeadersProviderService } from '../../../services/request-headers-provider/request-headers-provider.service';

@Injectable()
export class RegisterService {

  public chooseCategoryFlag: boolean = false;
  public isViaAuthService: boolean = false;
  public userEmail: string = null;
  public userFullname: string = null;

  constructor(
    private router: Router,
    private tokenProviderService: TokenProviderService,
    private http: Http,
    private headersProviderService: RequestHeadersProviderService
  ) { }

  goToRegister() {
    this.initializeManualRegister();
    this.router.navigate(['/register']);
  }

  initializeAuthRegister(email: string, fullname: string) {
    this.chooseCategoryFlag = true;
    this.isViaAuthService = true;
    this.userEmail = email;
    this.userFullname = fullname;
  }

  initializeManualRegister() {
    this.chooseCategoryFlag = false;
    this.isViaAuthService = false;
    this.userEmail = null;
    this.userFullname = null;
  }

  getUsersAmount() {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/admin/users-amount",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken)
      })
      .map(res => res.json());
  }
}
