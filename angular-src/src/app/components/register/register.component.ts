import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ValidationService } from '../../services/validation/validation.service';
import { AuthService } from '../../services/auth/auth.service';
import { DribbbleService } from '../../services/dribbble/dribbble.service';
import { CategoryService } from '../../services/category/category.service';
import { SocketService } from '../../services/socket/socket.service';
import { RegisterService } from './services/register.service';
import { Title } from '@angular/platform-browser';
import { LoginService } from '../../services/login/login.service';
import { TwitterService } from '../../services/twitter/twitter.service';
import { GoogleService } from '../../services/google/google.service';
import { InputType } from '../../enums/input-type';
import { AlertService } from '../../services/alert/alert.service';

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @ViewChild('googleButton')
  public googleButton: ElementRef;

  name: string = "";
  email: string = "";
  password: string = "";
  isTermsAccepted: boolean = false;

  nameErrorMessage: string = "";
  emailErrorMessage: string = "";
  passwordErrorMessage: string = "";
  showTermsError: boolean = false;

  private categories: Array<any> = [];

  inputType = InputType;

  constructor(
    private authService: AuthService,
    private validationService: ValidationService,
    private socketService: SocketService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private googleService: GoogleService,
    public loginService: LoginService,
    public twitterService: TwitterService,
    public registerService: RegisterService,
    public categoryService: CategoryService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.initializeCategories();
    this.initializeData();
    this.titleService.setTitle("Register at Duelity");

    setTimeout(() => {
      this.googleService.initGoogleAuth(this.googleButton.nativeElement);
    }, 0);
  }

  onRegisterSubmit() {
    const user = {
      name: this.registerService.isViaAuthService ? this.registerService.userFullname : this.name,
      email: this.registerService.isViaAuthService ? this.registerService.userEmail : this.email,
      password: this.password,
      categories: this.categoryService.collectCategories(this.categories)
    };

    if(user.categories.length === 0) {
      this.alertService.error("Pick at least one category to proceed");
      return;
    }

    this.authService.registerUser(user).subscribe(data => {
      if(data.success) {
        this.authService.storeUserData(data.token, data.user);
        this.socketService.addOnlineUser();
        this.router.navigate(["/following"]);
      } else {
        alert(data.message);
      }
    });
  }

  onFullNameChange(fullname: string) {
    this.name = fullname;
  }

  onEmailChange(email: string) {
    this.email = email;
  }

  onPasswordChange(password: string) {
    this.password = password;
  }

  goToCategories() {
    let success = true;

    this.nameErrorMessage = "";
    this.emailErrorMessage = "";
    this.passwordErrorMessage = "";

    const user = {
      name: this.name,
      email: this.email,
      password: this.password
    }

    if(!this.validationService.isStringValid(user.name)) {
      this.nameErrorMessage = "This field can't be empty";
      success = false;
    } else if(!this.validationService.isEnglishString(user.name)) {
      this.nameErrorMessage = "Full name should contains only English letters without numbers and characters";
      success = false;
    }

    if(!this.validationService.isStringValid(user.email)) {
      this.emailErrorMessage = "This field can't be empty";
      success = false;
    } else if(!this.validationService.isEmailValid(user.email)) {
      this.emailErrorMessage = "Please, write a correct email";
      success = false;
    }

    if(!this.validationService.isStringValid(user.password)) {
      this.passwordErrorMessage = "This field can't be empty";
      success = false;
    }

    if(!this.isTermsAccepted) {
      this.showTermsError = true;
      success = false;
    } else {
      this.showTermsError = false;
    }

    if(!success) { 
      return;
    }

    this.authService.checkUserEmail(user.email).subscribe(response => {
      if(response.success) {
        if(response.exists) {
          this.emailErrorMessage = "User with that email already exists";
        } else {
          this.registerService.chooseCategoryFlag = true;
        }
      } else {
        console.log(response.error);
      }
    });
  }

  private initializeData() {
    this.registerService.getUsersAmount().subscribe(response => { 
      if(response.success) {
        this.socketService.usersAmount = response.data;
      }
    });
  }

  private initializeCategories() {
    this.categoryService.getCategories().subscribe(response => {
      if(response.success) { 
        this.categories = response.categories;
      } else {
        console.log(response.error);
      }
    });
  }
}
