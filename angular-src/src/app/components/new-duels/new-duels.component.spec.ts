import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDuelsComponent } from './new-duels.component';

describe('NewDuelsComponent', () => {
  let component: NewDuelsComponent;
  let fixture: ComponentFixture<NewDuelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDuelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDuelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
