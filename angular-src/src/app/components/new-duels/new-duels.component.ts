import { Component, OnInit } from '@angular/core';
import { DuelService } from '../../services/duel-service/duel.service';
import { VoteService } from '../../services/vote/vote.service';
import { DuelType } from '../../enums/duel-type';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-new-duels',
  templateUrl: './new-duels.component.html',
  styleUrls: ['./new-duels.component.css']
})
export class NewDuelsComponent implements OnInit {

  private duelsPage: number = 1;
  private totalDuelsAmount: number = 0;

  duels: Array<any> = [];

  constructor(
    private duelService: DuelService,
    private voteService: VoteService,
    private titleService: Title
  ) { }

  ngOnInit() {
    this.initializeData();
    this.titleService.setTitle("New Duels - Duelity");
  }

  onNotify() {
    this.initializeData();
  }

  openVotePage(duel) {
    this.voteService.openVotePage(duel);
  }

  private initializeData() {
    this.duelService.getDuelByType(DuelType.New, this.duelsPage).subscribe(response => {
      if(response.success) {
        this.duels.push(...response.duels);
        this.totalDuelsAmount = response.totalAmount;
      } else {
        console.log("Something goes wrong during fetching new duels.");
      }
    })
  }

  loadNextPage() {
    this.duelsPage++;
    this.initializeData();
  }
}
