import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ISubscription } from 'rxjs/Subscription';
import { DuelStatus } from '../../enums/duel-status';

import { UserService } from './services/user.service';
import { AuthService } from '../../services/auth/auth.service';
import { DateFormatterService } from '../../services/date-formatter/date-formatter.service';
import { FollowService } from './services/follow.service';
import { DuelStatusMessageService } from '../../services/duel-status-message/duel-status-message.service';
import { DuelTimerService } from '../../services/duel-timer/duel-timer.service';
import { StartDuelService } from '../../services/start-duel-service/start-duel.service';
import { VoteService } from '../../services/vote/vote.service';
import { UserListService } from '../../child-components/user-list/services/user-list.service';
import { UserListOrderCriteria } from '../../enums/user-list-order-criteria';
import { DuelService } from '../../services/duel-service/duel.service';
import { Title } from '@angular/platform-browser';
import { TooltipService } from '../../services/tooltip/tooltip.service';
import { ValidationService } from '../../services/validation/validation.service';

declare var $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  username: string;
  userSuccess: boolean = undefined;
  isCurrentUser: boolean = false;

  user: any;

  duels: Array<any> = [];
  canStartDuel: any = null;
  tooltipCategories: Array<any> = [];
  firstCategory: any;

  @ViewChild('categoriesTooltip')
  categoriesTooltip: ElementRef;

  @ViewChild('canStartDuelTooltip')
  canStartDuelTooltip: ElementRef;

  @ViewChild('duelsContainer')
  duelsContainer: ElementRef;

  private subscription: ISubscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private dateFormatter: DateFormatterService,
    private followService: FollowService,
    private duelStatusMessageService: DuelStatusMessageService,
    private duelTimer: DuelTimerService,
    private startDuelService: StartDuelService,
    private voteService: VoteService,
    private userListService: UserListService,
    private duelService: DuelService,
    private titleService: Title,
    private tooltipService: TooltipService,
    private validationService: ValidationService
  ) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.username = params['username'];
      this.getUser();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onNotify(userId) {
    this.userService.getUserDuels(userId, [DuelStatus.Voting, DuelStatus.Ended]).subscribe(response => {
      if (response.success) {
        this.duels = response.duels;
      } else {
        console.log(response.error);
      }
    })
  }

  getUser() {
    this.userService.getUserProfileByUsername(this.username).subscribe(response => {
      this.userSuccess = response.success;

      if (response.success) {
        this.isCurrentUser = this.authService.getCurrentUserId() === response.data.user.id;

        this.user = response.data.user;
        this.canStartDuel = response.data.canStartDuel;
        this.initializeCategories(this.user.categories);
        this.initializeTooltips();

        this.titleService.setTitle(`${this.user.fullname} - Duelity`)

        this.userService.getUserDuels(this.user.id, [DuelStatus.Voting, DuelStatus.Ended]).subscribe(data => {
          if (data.success) {
            this.duels = data.duels;
            this.showDuels();
          }
        });
      }
    });
  }

  followUser() {
    this.followService.followUser(this.user.id).subscribe(data => {
      if (data.success) {
        this.getUser();
      }
    });
  }

  unfollowUser() {
    this.followService.unfollowUser(this.user.id).subscribe(data => {
      if (data.success) {
        this.getUser();
      }
    })
  }

  isFollowed() {
    var currentUserId = this.authService.getCurrentUserId();
    return this.user && this.user.followers.filter((follow) => follow.followerId === currentUserId)[0];
  }

  openUserFollowers() {
    var followerIds = this.user.followers.map(follower => follower.followerId);

    if (followerIds.length > 0) {
      this.userListService.getUsersAndOpenModal("Followers", followerIds, UserListOrderCriteria.SkillPoints);
    }
  }

  openUserFollowing() {
    var followingIds = this.user.following.map(following => following.userId);

    if (followingIds.length > 0) {
      this.userListService.getUsersAndOpenModal("Following", followingIds, UserListOrderCriteria.SkillPoints);
    }
  }

  changeDuelVisibility(duelId, hide) {
    this.hideDuels();
    this.duelService.changeDuelVisibility(duelId, hide).subscribe(response => {
      if (response.success) {
        this.duels = response.duels;
        this.showDuels();
      } else {
        console.log("Something goes wrong during changing duel visibility");
      }
    });
  }

  startDuel() {
    this.startDuelService.selectedUser = {
      id: this.user.id,
      value: this.user.fullname,
      username: this.user.username
    };

    this.router.navigate(['start']);
  }

  openVotePage(duel) {
    this.voteService.openVotePage(duel);
  }

  private initializeCategories(categories) {
    categories = categories.split(',');
    if (categories.length > 1) {
      this.firstCategory = categories.shift();
      this.tooltipCategories = categories.join(",");
    } else {
      this.firstCategory = categories.shift();
    }
  }

  showDuels() {
    setTimeout(() => { $(this.duelsContainer.nativeElement).addClass("visible"); }, 2000);
  }

  hideDuels() {
    $(this.duelsContainer.nativeElement).removeClass("visible");
  }

  private initializeTooltips() {
    setTimeout(() => {
      if (this.validationService.isObjectValid(this.categoriesTooltip)) {
        this.tooltipService.initializeTooltip(this.categoriesTooltip.nativeElement);
      }

      if (this.validationService.isObjectValid(this.canStartDuelTooltip)) {
        this.tooltipService.initializeTooltip(this.canStartDuelTooltip.nativeElement);;
        var position = $("#startDuelButton").offset();

        if (this.validationService.isObjectValid(position)) {
          $(this.canStartDuelTooltip.nativeElement).offset({ top: position.top, left: position.left });
        }
      }
    }, 0);
  }
}
