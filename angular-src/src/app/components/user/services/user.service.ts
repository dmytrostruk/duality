import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { RequestHeadersProviderService } from '../../../services/request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../../../services/token-provider/token-provider.service';
import { AuthService } from '../../../services/auth/auth.service';
import 'rxjs/add/operator/map';
import { DuelStatus } from '../../../enums/duel-status';

@Injectable()
export class UserService {

  constructor(
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private tokenProviderService: TokenProviderService,
    private authService: AuthService
  ) { }

  getUserProfileByUsername(username) {
    this.tokenProviderService.loadToken();

    var currentUser = this.authService.getCurrentUser();

    return this.http.get(environment.serverEndpoint + "/user/profile",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          username: username,
          currentUsername: currentUser ? currentUser.username : null
        } 
      })
      .map(res => res.json());
  }

  getUserDuels(userId, statuses: Array<DuelStatus> = []) {
    this.tokenProviderService.loadToken();

    var data = {
      userId: userId,
      statuses: statuses,
      currentUserId: this.authService.getCurrentUserId()
    };

    return this.http.post(environment.serverEndpoint + "/user/duels", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  turnOffHelp() {
    this.tokenProviderService.loadToken();

    var data = {
      userId: this.authService.getCurrentUserId()
    }

    return this.http.post(environment.serverEndpoint + "/user/turn-off-help", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }
}