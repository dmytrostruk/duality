import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { RequestHeadersProviderService } from '../../../services/request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../../../services/token-provider/token-provider.service';
import { AuthService } from '../../../services/auth/auth.service';
import 'rxjs/add/operator/map';

@Injectable()
export class FollowService {

  constructor(
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private tokenProviderService: TokenProviderService,
    private authService: AuthService
  ) { }

  followUser(userId) {
    this.tokenProviderService.loadToken();

    var userFollower = {
      userId: userId,
      followerId: this.authService.getCurrentUser().id
    };

    return this.http.post(environment.serverEndpoint + "/user/follow", userFollower,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  unfollowUser(userId) {
    this.tokenProviderService.loadToken();

    var userFollower = {
      userId: userId,
      followerId: this.authService.getCurrentUser().id
    };

    return this.http.post(environment.serverEndpoint + "/user/unfollow", userFollower,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }
}
