import { Component, OnInit } from '@angular/core';
import { SearchService } from './services/search.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchType } from '../../enums/search-type';
import { VoteService } from '../../services/vote/vote.service';
import { AuthService } from '../../services/auth/auth.service';
import { Title } from '@angular/platform-browser';
import { FollowService } from '../user/services/follow.service';
import { ISubscription } from 'rxjs/Subscription';
import { ValidationService } from '../../services/validation/validation.service';

declare var $: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  private searchString: string = "";
  private searchStringOutput: string = "";
  
  private users: Array<any> = [];
  private duels: Array<any> = [];
  private usersCount: number = 0;
  private duelsCount: number = 0;

  private currentMode: SearchType;
  private currentUserId: string;

  private page: number = 1;

  searchType = SearchType;

  private subscription: ISubscription;

  constructor(
    private searchService: SearchService,
    private voteService: VoteService,
    private authService: AuthService,
    private followService: FollowService,
    private router: Router,
    private titleService: Title,
    private activatedRoute: ActivatedRoute,
    private validationService: ValidationService
  ) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.currentUserId = this.authService.getCurrentUserId();
      this.searchString = params['query'];

      if(!this.validationService.isObjectValid(this.searchString)) {
        this.searchString = "";
        this.searchStringOutput = this.searchString;
      }

      this.setSearchMode(SearchType.Users);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  goToPerson(username) {
    this.router.navigate(["/user", username]);
  }

  openVotePage(duel) {
    this.voteService.openVotePage(duel);
  }

  setSearchMode(searchType: SearchType) {
    this.initializeModeButtons();
    this.searchStringOutput = this.searchString;

    switch(searchType) {
      case SearchType.Users: 
        this.setUsersMode();
        break;
      case SearchType.Duels:
        this.setDuelsMode();
        break;
    }
  }

  followUser(userId) {
    this.followService.followUser(userId).subscribe(data => {
      if(data.success) {
        this.getUsers(true);
      }
    });
  }

  unfollowUser(userId) {
    this.followService.unfollowUser(userId).subscribe(data => {
      if(data.success) {
        this.getUsers(true);
      }
    })
  }

  isFollower(followers) {
    return followers.map(follower => follower.followerId).indexOf(this.currentUserId) >= 0;
  }

  loadMore() {
    this.page++;

    switch(this.currentMode) {
      case SearchType.Users: 
        this.getUsers();
        break;
      case SearchType.Duels:
        this.getDuels();
        break;
    }
  }

  private setUsersMode() {
    $("#users-mode").addClass("active");

    this.users = [];
    this.duels = [];
    this.page = 1;

    this.currentMode = SearchType.Users;
    this.titleService.setTitle("Users Search");
    this.getUsers();
  }

  private setDuelsMode() {
    $("#duels-mode").addClass("active");

    this.users = []
    this.duels = [];
    this.page = 1;

    this.currentMode = SearchType.Duels;
    this.titleService.setTitle("Duels Search");
    this.getDuels();
  }

  private initializeModeButtons() {
    $(".switch-mode-button").removeClass("active");
  }

  private getUsers(refreshData: boolean = false) {
    this.searchService.getUsers(this.searchString, this.page).subscribe(response => {
      if(response) {
        if(refreshData) {
          this.users = this.users.slice(0, -6);
        } 

        this.users.push(...response.data.users);

        this.usersCount = response.data.usersCount;
        this.duelsCount = response.data.duelsCount;
      } else {
        console.log(response.error);
      }
    });
  }

  private getDuels() {
    this.searchService.getDuels(this.searchString, this.page).subscribe(response => {
      if(response) {
        this.duels.push(...response.data.duels);

        this.usersCount = response.data.usersCount;
        this.duelsCount = response.data.duelsCount;
      } else {
        console.log(response.error);
      }
    });
  }
}
