import { Injectable } from '@angular/core';
import { TokenProviderService } from '../../../services/token-provider/token-provider.service';
import { environment } from '../../../../environments/environment';
import { Http } from '@angular/http';
import { RequestHeadersProviderService } from '../../../services/request-headers-provider/request-headers-provider.service';

@Injectable()
export class SearchService {

  constructor(
    private tokenProviderService: TokenProviderService,
    private headersProviderService: RequestHeadersProviderService,
    private http: Http
  ) { }

  getUsers(searchString: string, page: number) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/search/get-users",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          searchString: searchString,
          page: page
        }
      })
      .map(res => res.json());
  }

  getDuels(searchString: string, page: number) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/search/get-duels",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          searchString: searchString,
          page: page
        }
      })
      .map(res => res.json());
  }
}
