export class AccountSettings {
    id: string;
    fullname: string;
    description: string;
    username: string;
    email: string;
    behance: string;
    dribbble: string;
    twitter: string;
    instagram: string;
    oldPassword: string;
    newPassword: string;
    imageUrl: string;
    imageColor: string;
    imagePublicId: string;

    static readonly behanceLink: string = "be.net/";
    static readonly dribbbleLink: string = "dribbble.com/";
    static readonly twitterLink: string = "twitter.com/";
    static readonly instagramLink: string = "instagram.com/";

    static map(source) {
        var settings = new AccountSettings();

        settings.id = source.id;
        settings.fullname = source.fullname;
        settings.description = source.description;
        settings.username = source.username;
        settings.email = source.email;
        settings.behance = source.behance;
        settings.dribbble = source.dribbble;
        settings.twitter = source.twitter;
        settings.instagram = source.instagram;
        settings.oldPassword = source.oldPassword;
        settings.newPassword = source.newPassword;
        settings.imageUrl = source.imageUrl;
        settings.imageColor = source.imageColor;
        settings.imagePublicId = source.imagePublicId;

        return settings;
    }

    normalizeSocialNetworkLinks() {
        this.behance = this.behance ? AccountSettings.behanceLink + this.behance : "";
        this.dribbble = this.dribbble ? AccountSettings.dribbbleLink + this.dribbble : "";
        this.twitter = this.twitter ? AccountSettings.twitterLink + this.twitter : "";
        this.instagram = this.instagram ? AccountSettings.instagramLink + this.instagram : "";
    }

    denormalizeSocialNetworkLinks() {
        this.behance = this.behance ? this.behance.replace(AccountSettings.behanceLink, "") : "";
        this.dribbble = this.dribbble ? this.dribbble.replace(AccountSettings.dribbbleLink, "") : "";
        this.twitter = this.twitter ? this.twitter.replace(AccountSettings.twitterLink, "") : "";
        this.instagram = this.instagram ? this.instagram.replace(AccountSettings.instagramLink, "") : "";
    }

    hasChanges(previousSettings: AccountSettings) {
        return !(this.fullname === previousSettings.fullname &&
            this.description === previousSettings.description &&
            this.username === previousSettings.username &&
            this.email === previousSettings.email &&
            this.behance === previousSettings.behance &&
            this.dribbble === previousSettings.dribbble &&
            this.twitter === previousSettings.twitter &&
            this.instagram === previousSettings.instagram &&
            this.newPassword === previousSettings.newPassword);
    }
}