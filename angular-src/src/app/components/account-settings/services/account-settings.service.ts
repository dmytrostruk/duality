import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { RequestHeadersProviderService } from '../../../services/request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../../../services/token-provider/token-provider.service';
import { AuthService } from '../../../services/auth/auth.service';
import 'rxjs/add/operator/map';
import { AccountSettings } from '../models/account-settings';

@Injectable()
export class AccountSettingsService {

  constructor(
    private tokenProviderService: TokenProviderService,
    private authService: AuthService,
    private headersProviderService: RequestHeadersProviderService,
    private router: Router,
    private http: Http
  ) { }

  getAccountSettings() {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/user/account-settings",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          currentUserId: this.authService.getCurrentUser().id
        } 
      })
      .map(res => res.json());
  }

  saveAccountSettings(settings: AccountSettings) {
    this.tokenProviderService.loadToken();

    return this.http.post(environment.serverEndpoint + "/user/save-account-settings", settings,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  saveAvatar(imageUrl, imagePublicId, imageColor) {
    this.tokenProviderService.loadToken();

    var data = {
      currentUserId: this.authService.getCurrentUser().id,
      imageUrl: imageUrl,
      imagePublicId: imagePublicId,
      imageColor: imageColor
    };

    return this.http.post(environment.serverEndpoint + "/user/save-avatar", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }
}
