import { Component, OnInit, OnDestroy } from '@angular/core';
import { SettingsType } from '../../enums/settings-type';
import { AccountSettingsService } from './services/account-settings.service';
import { AccountSettings } from './models/account-settings';
import { ValidationService } from '../../services/validation/validation.service';
import { AuthService } from '../../services/auth/auth.service';
import { DefaultPictureService } from '../../child-components/default-picture/services/default-picture.service';
import { CloudinaryService } from '../../services/cloudinary-service/cloudinary.service';
import { CategoryService } from '../../services/category/category.service';
import { Title } from '@angular/platform-browser';
import { AlertService } from '../../services/alert/alert.service';
import { InputType } from '../../enums/input-type';
import { Subject } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.css']
})
export class AccountSettingsComponent implements OnInit {

  emailModalId: string = "changeEmailModal";
  notSavedModalId: string = "notSavedModal";
  settingsType: SettingsType = SettingsType.General;

  settingsTypes = SettingsType;
  accountSettingsType = AccountSettings;
  inputType = InputType;

  previousSettings: AccountSettings;
  accountSettings: AccountSettings = null;

  emailVerificationPassword: string = "";

  private categories: Array<any> = [];

  nameErrorMessage: string = "";
  descriptionErrorMessage: string = "";
  usernameErrorMessage: string = "";
  emailErrorMessage: string = "";
  behanceErrorMessage: string = "";
  dribbbleErrorMessage: string = "";
  twitterErrorMessage: string = "";
  instagramErrorMessage: string = "";
  oldPasswordErrorMessage: string = "";
  newPasswordErrorMessage: string = "";

  deactivateSubject: Subject<boolean> = new Subject<boolean>();

  constructor(
    private accountSettingsService: AccountSettingsService,
    private validationService: ValidationService,
    private authService: AuthService,
    private defaultPictureService: DefaultPictureService,
    private cloudinaryService: CloudinaryService,
    public categoryService: CategoryService,
    private titleService: Title,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.initializeModelCheck();
    this.changeSettingsTypes(SettingsType.General);
    this.titleService.setTitle(`Settings / ${this.authService.getCurrentUser().fullname} - Duelity`);
    this.initializeFileUploadInput();
    setTimeout(this.initializeNotSavedModal.bind(this), 0);
  }

  changeSettingsTypes(settingsType: SettingsType) {
    this.settingsType = settingsType;

    switch (this.settingsType) {
      case SettingsType.General:
        this.accountSettingsService.getAccountSettings().subscribe(response => {
          if (response.success) {
            this.setAccountSettings(response.data);

            if (settingsType === SettingsType.General) {
              this.cloudinaryService.initAvatarUploader(response.data.id, this.saveAvatar.bind(this));
            }
          } else {
            console.log("Something goes wrong during fetching account info.");
          }
        });
        break;
      case SettingsType.DuelCategories:
        this.initializeCategories();
        break;
    }
  }

  onFullNameChange(fullname) {
    this.accountSettings.fullname = fullname;
  }

  onDescriptionChange(description) {
    this.accountSettings.description = description;
  }

  onEmailChange(email) {
    this.accountSettings.email = email;
  }

  onBehanceChange(behance) {
    this.accountSettings.behance = behance;
  }

  onDribbbleChange(dribbble) {
    this.accountSettings.dribbble = dribbble;
  }

  onTwitterChange(twitter) {
    this.accountSettings.twitter = twitter;
  }

  onInstagramChange(instagram) {
    this.accountSettings.instagram = instagram;
  }

  onOldPasswordChange(oldPassword) {
    this.accountSettings.oldPassword = oldPassword;
  }

  onNewPasswordChange(newPassword) {
    this.accountSettings.newPassword = newPassword;
  }

  onEmailVerificationPasswordChange(emailVerificationPassword) {
    this.emailVerificationPassword = emailVerificationPassword;
  }

  onSaveSettings() {
    this.initializeModelCheck();

    if (this.validationService.isEmailValid(this.accountSettings.email) && this.accountSettings.email !== this.previousSettings.email) {
      this.openEmailModal();
      return;
    }

    if (this.validationService.isStringValid(this.accountSettings.newPassword)) {
      this.newPasswordErrorMessage = this.validationService.validatePassword(this.accountSettings.newPassword, true);
      this.oldPasswordErrorMessage = this.validationService.validatePassword(this.accountSettings.oldPassword, false);

      this.authService.checkUserPassword(this.accountSettings.oldPassword).subscribe(response => {
        if (response.success) {
          this.saveSettings();
        } else {
          this.alertService.error("Wrong password!");
          this.setAccountSettings(this.previousSettings);
        }
      });

      return;
    }

    this.saveSettings();
  }

  onSaveCategories() {
    var userId = this.authService.getCurrentUser().id,
      categories = this.categoryService.collectCategories(this.categories);

    if (categories.length === 0) {
      return;
    }

    this.categoryService.saveUserCategories(userId, categories).subscribe(response => {
      if (response.success) {
        this.categories = response.categories;
        this.alertService.success("New settings saved");
      } else {
        this.alertService.error("Your new settings could not be saved. Please try again");
      }
    });
  }

  setAccountSettings(settings) {
    this.accountSettings = AccountSettings.map(settings);
    this.accountSettings.denormalizeSocialNetworkLinks();

    this.previousSettings = AccountSettings.map(this.accountSettings);
  }

  openEmailModal() {
    $(`#${this.emailModalId}`).modal();
  }

  closeEmailModal() {
    $(`#${this.emailModalId}`).modal('hide');
  }

  openNotSavedModal() {
    $(`#${this.notSavedModalId}`).modal();
  }

  closeNotSavedModal() {
    $(`#${this.notSavedModalId}`).modal('hide');
    $('.modal-backdrop').remove();
  }

  notSavedModalChoose(result: boolean) {
    this.deactivateSubject.next(result);
    this.closeNotSavedModal();
  }

  initializeNotSavedModal() {
    var self = this;
    $(`#${this.notSavedModalId}`).on('hidden.bs.modal', function () {
      self.deactivateSubject.next(false);
    });
  }

  checkPassword() {
    if (!this.validationService.isStringValid(this.emailVerificationPassword)) {
      alert("Password cannot be empty!");
      return;
    }

    this.authService.checkUserPassword(this.emailVerificationPassword).subscribe(response => {
      if (response.success) {
        this.saveSettings();
      } else {
        this.alertService.error("Wrong password!");
        this.setAccountSettings(this.previousSettings);
      }

      this.emailVerificationPassword = "";
      this.closeEmailModal();
    });
  }

  onDeletePicture() {
    this.accountSettings.imageUrl = "";
    this.accountSettings.imagePublicId = "";

    if (!this.accountSettings.imageColor) {
      this.accountSettings.imageColor = this.defaultPictureService.generateRandomColor();
      this.saveAvatar(this.accountSettings.imageUrl, this.accountSettings.imagePublicId, this.accountSettings.imageColor);
    }
  }

  validateCategories() {
    return this.categories.filter(categoryGroup => categoryGroup.filter(category => category.selected).length > 0).length > 0;
  };

  private saveAvatar(imageUrl, imagePublicId, imageColor) {
    this.accountSettingsService.saveAvatar(imageUrl, imagePublicId, imageColor)
      .subscribe(response => {
        if (response.success) {
          this.setAccountSettings(response.data);
          this.authService.updateUserData(response.userData);
          if (this.validationService.isStringValid(response.data.imageUrl)) {
            this.alertService.success("New picture has been uploaded");
          } else {
            this.alertService.success("Picture has been deleted");
          }
        } else {
          this.alertService.error("Error occured during picture saving. Please try again");
        }
      });
  }

  private onAvatarUpload(e) {
    var file = e.target.files[0];

    if (file) {
      if (this.validationService.isImageValid(file.type)) {
        this.cloudinaryService.avatarUploader.addToQueue(e.target.files);
      } else {
        this.alertService.error("Image format is not valid");
      }
    }
  }

  private saveSettings() {
    if (this.isSettingsValid()) {
      var settings = AccountSettings.map(this.accountSettings);
      settings.normalizeSocialNetworkLinks();

      this.accountSettingsService.saveAccountSettings(settings).subscribe(response => {
        if (response.success) {
          this.setAccountSettings(response.data);
          this.alertService.success("New settings saved");
          this.authService.updateUserData(response.userData);
        } else {
          this.alertService.error("Your new settings could not be saved. Please try again");
          this.setAccountSettings(this.previousSettings);
        }
      });
    } else {
      setTimeout(this.scrollToError, 500);
    }
  }

  private scrollToError() {
    const offset = 105;
    var firstError = $("p.error").filter((index, element) => $(element).text() !== "")[0];
    var top = $(firstError).offset().top - offset;

    window.scrollTo({ top: top, behavior: 'smooth' });
  }

  private isSettingsValid(): boolean {
    this.nameErrorMessage = this.validationService.validateFullname(this.accountSettings.fullname, true);
    this.emailErrorMessage = this.validationService.validateEmail(this.accountSettings.email, true);
    this.descriptionErrorMessage = this.validationService.validateDescription(this.accountSettings.description, false);
    this.usernameErrorMessage = this.validationService.validateUsername(this.accountSettings.username, true);

    if (this.validationService.isStringValid(this.accountSettings.behance)) {
      this.behanceErrorMessage = this.validationService.validateSocialNetwork("Behance", this.accountSettings.behance, false);
    }

    if (this.validationService.isStringValid(this.accountSettings.dribbble)) {
      this.dribbbleErrorMessage = this.validationService.validateSocialNetwork("Dribbble", this.accountSettings.dribbble, false);
    }

    if (this.validationService.isStringValid(this.accountSettings.twitter)) {
      this.twitterErrorMessage = this.validationService.validateSocialNetwork("Twitter", this.accountSettings.twitter, false);
    }

    if (this.validationService.isStringValid(this.accountSettings.instagram)) {
      this.instagramErrorMessage = this.validationService.validateSocialNetwork("Instagram", this.accountSettings.instagram, false);
    }

    return !(this.validationService.isStringValid(this.nameErrorMessage) ||
      this.validationService.isStringValid(this.usernameErrorMessage) ||
      this.validationService.isStringValid(this.emailErrorMessage) ||
      this.validationService.isStringValid(this.descriptionErrorMessage) ||
      this.validationService.isStringValid(this.behanceErrorMessage) ||
      this.validationService.isStringValid(this.dribbbleErrorMessage) ||
      this.validationService.isStringValid(this.twitterErrorMessage) ||
      this.validationService.isStringValid(this.instagramErrorMessage));
  }

  private initializeCategories() {
    var userId = this.authService.getCurrentUser().id;

    this.categoryService.getUserCategories(userId).subscribe(response => {
      if (response.success) {
        this.categories = response.categories;
      } else {
        console.log(response.error);
      }
    });
  }

  private initializeModelCheck() {
    this.nameErrorMessage = "";
    this.descriptionErrorMessage = "";
    this.usernameErrorMessage = "";
    this.emailErrorMessage = "";
    this.behanceErrorMessage = "";
    this.dribbbleErrorMessage = "";
    this.twitterErrorMessage = "";
    this.instagramErrorMessage = "";
    this.oldPasswordErrorMessage = "";
    this.newPasswordErrorMessage = "";
  }

  initializeFileUploadInput() {
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
      var label = input.nextElementSibling,
        labelVal = label.innerHTML;

      input.addEventListener('change', function (e) {
        var fileName = '';
        if (this.files && this.files.length > 1)
          fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
        else
          fileName = e.target.value.split('\\').pop();

        if (fileName)
          label.querySelector('span').innerHTML = fileName;
        else
          label.innerHTML = labelVal;
      });

      // Firefox bug fix
      input.addEventListener('focus', function () { input.classList.add('has-focus'); });
      input.addEventListener('blur', function () { input.classList.remove('has-focus'); });
    });
  }
}
