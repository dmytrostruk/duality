import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NotificationService } from '../../services/notification/notification.service';
import { DateFormatterService } from '../../services/date-formatter/date-formatter.service';
import { DuelService } from '../../services/duel-service/duel.service';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../../services/auth/auth.service';
import { NotificationType } from '../../enums/notification-type';
import { HeaderService } from '../../root-components/header/services/header.service';

declare var $: any;

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  constructor(
    private notificationService: NotificationService,
    public dateFormatter: DateFormatterService,
    private router: Router,
    private titleService: Title,
    private authService: AuthService,
    private duelService: DuelService
  ) { }

  ngOnInit() {
    this.notificationService.getNotifications(1);
    this.titleService.setTitle(`Notifications / ${this.authService.getCurrentUser().fullname} - Duelity`)
  }
}
