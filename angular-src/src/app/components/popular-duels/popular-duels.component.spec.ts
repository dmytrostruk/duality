import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularDuelsComponent } from './popular-duels.component';

describe('PopularDuelsComponent', () => {
  let component: PopularDuelsComponent;
  let fixture: ComponentFixture<PopularDuelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopularDuelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopularDuelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
