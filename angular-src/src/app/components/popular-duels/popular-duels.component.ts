import { Component, OnInit } from '@angular/core';
import { DuelService } from '../../services/duel-service/duel.service';
import { VoteService } from '../../services/vote/vote.service';
import { DuelType } from '../../enums/duel-type';
import { UserListService } from '../../child-components/user-list/services/user-list.service';
import { UserListOrderCriteria } from '../../enums/user-list-order-criteria';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../../services/auth/auth.service';
import { RegisterService } from '../register/services/register.service';
import { SocketService } from '../../services/socket/socket.service';
import { TooltipService } from '../../services/tooltip/tooltip.service';

declare var $: any;

@Component({
  selector: 'app-popular-duels',
  templateUrl: './popular-duels.component.html',
  styleUrls: ['./popular-duels.component.css']
})
export class PopularDuelsComponent implements OnInit {

  private readonly previewUserAmount: number = 5;
  private totalUserCount: number = 0;
  private duelsPage: number = 1;
  private totalDuelsAmount: number = 0;

  duels: Array<any> = [];
  previewUsers: Array<any> = [];

  constructor(
    private duelService: DuelService,
    private voteService: VoteService,
    private userListService: UserListService,
    private router: Router,
    private titleService: Title,
    private authService: AuthService,
    private registerService: RegisterService,
    private socketService: SocketService,
    private tooltipService: TooltipService
  ) { }

  ngOnInit() {
    this.duels = [];
    this.previewUsers = [];
    this.duelsPage = 1;
    this.totalDuelsAmount = 0;
    this.totalUserCount = 0;

    this.initializeData();
    this.initializeAd();
    this.titleService.setTitle("Popular Duels - Duelity");
  }

  onNotify() {
    this.initializeData();
  }

  openVotePage(duel) {
    this.voteService.openVotePage(duel);
  }

  goToPerson(username) {
    this.router.navigate(["/user", username]);
  }

  openUserList() {
    this.userListService.getUsersAndOpenModal("Popular", [], UserListOrderCriteria.SkillPoints);
  }

  getRoundedUserAmount() {
    return Math.floor(this.socketService.usersAmount / 10) * 10;
  }

  loadNextPage() {
    this.duelsPage++;
    this.loadDuels();
  }

  private initializeData() {
    this.duelService.getDuelByType(DuelType.Popular, this.duelsPage).subscribe(response => {
      if(response.success) {
        this.duels.push(...response.duels);
        this.totalDuelsAmount = response.totalAmount;

        if(this.duels.length > 0) {
          this.userListService.getUserList([], UserListOrderCriteria.SkillPoints, 1, this.previewUserAmount).subscribe(response => {
            if(response.success) {
              this.previewUsers = response.data.users;
              this.totalUserCount = response.data.totalCount;
              this.initializePreviewUsers();
              setTimeout(() => {this.tooltipService.initializeTooltip('.custom-tooltip');}, 0);
            } else {
              console.log(response.error);
            }
          });
        }
      } else {
        console.log("Something goes wrong during fetching popular duels.");
      }
    });
  }

  private loadDuels() {
    this.duelService.getDuelByType(DuelType.Popular, this.duelsPage).subscribe(response => {
      if(response.success) {
        this.duels.push(...response.duels);
        this.totalDuelsAmount = response.totalAmount;
      } else {
        console.log("Something goes wrong during fetching popular duels.");
      }
    });
  }

  private initializeAd() {
    if(this.authService.getCurrentUser() === null) {
      this.registerService.getUsersAmount().subscribe(response => { 
        if(response.success) {
          this.socketService.usersAmount = response.data;
        }
      });
    }
  }

  private initializePreviewUsers() {
    setTimeout(() => {
      var avatars = $(".avatar");
      var rightPosition = this.totalUserCount > this.previewUserAmount ? 10 : 0;
      var zIndex = 6;
  
      if(avatars.length > 0) {
        for(var i = avatars.length - 1; i >= 0; i--) {
          var avatar = $(avatars[i]);
          avatar.css("right", `-${rightPosition}px`);
          avatar.css("z-index", zIndex);
    
          rightPosition += 5;
          zIndex += 1;
        }

        $(".avatars-label").css("right", `${16 - (rightPosition - 5)}px`);
      }
    }, 0);
  }
}
