import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedDuelsComponent } from './featured-duels.component';

describe('FeaturedDuelsComponent', () => {
  let component: FeaturedDuelsComponent;
  let fixture: ComponentFixture<FeaturedDuelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedDuelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedDuelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
