import { Component, OnInit } from '@angular/core';
import { DuelService } from '../../services/duel-service/duel.service';
import { VoteService } from '../../services/vote/vote.service';
import { DuelType } from '../../enums/duel-type';
import { UserListService } from '../../child-components/user-list/services/user-list.service';
import { Router } from '@angular/router';
import { UserListOrderCriteria } from '../../enums/user-list-order-criteria';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../../services/auth/auth.service';
import { TooltipService } from '../../services/tooltip/tooltip.service';

declare var $: any;

@Component({
  selector: 'app-featured-duels',
  templateUrl: './featured-duels.component.html',
  styleUrls: ['./featured-duels.component.css']
})
export class FeaturedDuelsComponent implements OnInit {

  private readonly previewUserAmount: number = 5;
  private totalUserCount: number = 0;
  private duelsPage: number = 1;
  private totalDuelsAmount: number = 0;

  duels: Array<any> = [];
  previewUsers: Array<any> = [];

  constructor(
    private duelService: DuelService,
    private voteService: VoteService,
    private userListService: UserListService,
    private router: Router,
    private titleService: Title,
    private authService: AuthService,
    private tooltipService: TooltipService
  ) { }

  ngOnInit() {
    this.initializeData();
    this.titleService.setTitle("Featured Duels - Duelity");
  }

  onNotify() {
    this.initializeData();
  }

  openVotePage(duel) {
    this.voteService.openVotePage(duel);
  }

  goToPerson(username) {
    this.router.navigate(["/user", username]);
  }

  openUserList() {
    this.userListService.getUsersAndOpenModal("Featured", [], UserListOrderCriteria.FeaturedPoints);
  }

  loadNextPage() {
    this.duelsPage++;
    this.loadDuels();
  }

  private initializeData() {
    this.duelService.getDuelByType(DuelType.Featured, this.duelsPage).subscribe(response => {
      if(response.success) {
        this.duels.push(...response.duels);
        this.totalDuelsAmount = response.totalAmount;

        if(this.duels.length > 0) {
          this.userListService.getUserList([], UserListOrderCriteria.FeaturedPoints, 1, this.previewUserAmount).subscribe(response => {
            if(response.success) {
              this.previewUsers = response.data.users;
              this.totalUserCount = response.data.totalCount;
              this.initializePreviewUsers();
              setTimeout(() => {this.tooltipService.initializeTooltip('.custom-tooltip');}, 0);
            } else {
              console.log(response.error);
            }
          });
        }
      } else {
        console.log("Something goes wrong during fetching featured duels.");
      }
    });
  }

  private loadDuels() {
    this.duelService.getDuelByType(DuelType.Featured, this.duelsPage).subscribe(response => {
      if(response.success) {
        this.duels.push(...response.duels);
        this.totalDuelsAmount = response.totalAmount;
      } else {
        console.log("Something goes wrong during fetching popular duels.");
      }
    });
  }

  private initializePreviewUsers() {
    setTimeout(() => {
      var avatars = $(".avatar");
      var rightPosition = this.totalUserCount > this.previewUserAmount ? 10 : 0;
      var zIndex = 6;
  
      if(avatars.length > 0) {
        for(var i = avatars.length - 1; i >= 0; i--) {
          var avatar = $(avatars[i]);
          avatar.css("right", `-${rightPosition}px`);
          avatar.css("z-index", zIndex);
    
          rightPosition += 5;
          zIndex += 1;
        }

        $(".avatars-label").css("right", `${16 - (rightPosition - 5)}px`);
      }
    }, 0);
  }
}
