import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { StartDuelService } from '../../services/start-duel-service/start-duel.service';
import { AuthService } from '../../services/auth/auth.service';
import { ValidationService } from '../../services/validation/validation.service';
import { DateFormatterService } from '../../services/date-formatter/date-formatter.service';
import { Title } from '@angular/platform-browser';
import { AlertService } from '../../services/alert/alert.service';
import { TooltipService } from '../../services/tooltip/tooltip.service';

import { InputType } from '../../enums/input-type';
import { CategoryService } from '../../services/category/category.service';

declare var $: any;

@Component({
  selector: 'app-start-duel',
  templateUrl: './start-duel.component.html',
  styleUrls: ['./start-duel.component.css']
})
export class StartDuelComponent implements OnInit {

  private readonly maxDaysAmount: number = 14;
  private readonly minDaysAmount: number = 2;

  startDuelInfo: any;
  duelsLeft: string;
  currentDuels: string;
  canStartDuel: boolean = false;

  // Opponent's input fields
  config: any = {'placeholder': 'test', 'sourceField': ['value']};
  opponents: any[] = [];

  // Form fields
  selectedUser: any;
  selectedDate: string = null;
  selectedTaskText: string = '';

  inputType = InputType;

  constructor(
    private startDuelService: StartDuelService,
    private authService: AuthService,
    private validationService: ValidationService,
    private dateFormatter: DateFormatterService,
    private router: Router,
    private titleService: Title,
    private alertService: AlertService,
    private tooltipService: TooltipService,
    private categoryService: CategoryService
  ) { }

  ngOnInit() {
    this.initializeData();
    this.titleService.setTitle("Starting a Duel - Duelity");
  }

  onInputChangedEvent(search: string) {
    if(search.trim() == "") {
      this.selectUser(this.getEmptyUser());
    }

    this.startDuelService.getOpponents(search).subscribe(result => {
      if(result.success) {
        this.opponents = result.data;
      }
    });
  }

  onSelect(item: any) {
    this.selectUser(item);
  }

  selectByRadio(item: any) {
    this.selectUser(item);
  }

  selectDate(control, daysAmount) {
    $(".work-deadline-container input[type='button']").removeClass("selected");
    $(".work-deadline-container input[type='date']").val('');
    $(control).addClass("selected");   

    this.selectedDate = this.getDeadline(daysAmount);
    var datePicker = <any>document.getElementById("deadline-picker");

    if(this.validationService.isObjectValid(datePicker)) {
      datePicker.valueAsDate =  new Date(this.selectedDate);
    }
  }

  onSelectedUserChange(user) {
    this.selectedUser.value = user;
  }

  onSelectedTaskText(text) {
    this.selectedTaskText = text;
  }

  startDuel() {
    if(this.validateDuelData()) {
      var selectedCategory = this.startDuelInfo.commonCategories.filter(category => category.selected)[0];
      this.startDuelService.startDuel(this.selectedUser.id, selectedCategory._id, this.selectedTaskText, this.selectedDate).subscribe(response => {
        if(response.success) {
          this.router.navigate(['/myduels']);
          this.alertService.success(`Duel request has been sent. ${this.selectedUser.value} has 24 hours to answer`);
        } else {
          console.log(response.error);
          this.alertService.error("Duel request could not be sent. Please try again");
        }
      });
    }
  }

  private initializeData() {
    this.startDuelService.getStartDuelInfo().subscribe(response => {
      if(response.success) {
        this.startDuelInfo = response.data;

        if(!this.startDuelInfo.isPaidAccount && (this. startDuelInfo.duelsLeft === 0 || this.startDuelInfo.currentDuels > 0)) {
          this.canStartDuel = false;
        }
        else {
          this.canStartDuel = true;

          var user = this.startDuelService.selectedUser || this.getEmptyUser();
          user.commonCategories = this.startDuelInfo.commonCategories;

          this.selectUser(user);
          this.initializeDatePicker();
          
          setTimeout(() => {
            this.selectDate($(".work-deadline-container input[type='button']").first(), 2);
          });
        }

        this.initializeDuelsAmount(this.startDuelInfo);
      }
    });
  }
  
  private onDatePickerSelect(date) {
    $(".work-deadline-container input[type='button']").removeClass("selected");
    this.selectedDate = date;

    var dateObj = new Date(this.selectedDate);
    dateObj.setDate(dateObj.getDate() + 1);

    var datePicker = <any>document.getElementById("deadline-picker");
    datePicker.valueAsDate =  dateObj;
  }

  private initializeDatePicker() {
    setTimeout(() => {
      $('#datepicker').datepicker({
        inline: true,
        showOtherMonths: true,
        dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        minDate: this.getMinDate(),
        maxDate: this.getMaxDate(),
        onSelect: this.onDatePickerSelect
      });

      $("#datepicker").hide();

      $('#deadline-picker').click(() => {
        setTimeout(() => {
          $("#datepicker").show();
        });
      })

      $(document).click(function() {
        if($('#datepicker').is(":visible")) {
          $("#datepicker").hide();
        }
      });

      $("#datepicker").click((event) => {
        event.stopPropagation();
    });
    });
  }

  private initializeDuelsAmount(startDuelInfo) {    
    this.duelsLeft = startDuelInfo.isPaidAccount ? '∞' : startDuelInfo.duelsLeft;
    this.currentDuels = startDuelInfo.isPaidAccount ? '∞' : startDuelInfo.currentDuels;
    setTimeout(() => {
      this.tooltipService.initializeTooltip('.custom-tooltip');
    }, 0);
  }

  private selectUser(user) {
    this.selectedUser = user;

    this.selectedUser.commonCategories.forEach(category => category.selected = false);
    this.startDuelInfo.commonCategories = this.selectedUser.commonCategories;

    setTimeout(() => {
      $( "input:radio" ).prop('checked', false);
      $( "input[data-user-id=" + user.id +"]:radio" ).prop('checked', true);
    });
  }

  private getEmptyUser() {
    return {
      id: null, 
      value: null, 
      commonCategories: []
    };
  }

  private getDeadline(daysAmount) {
    var date = new Date();

    // Adding 1 including one day of duel's waiting status
    date.setDate(date.getDate() + daysAmount + 1);

    //date.setSeconds(date.getSeconds() + 120);

    return date.toISOString();
  }

  private goToPage(page) {
    this.router.navigate([page]);
  }

  private validateDuelData() : boolean {
    if (!this.validationService.isObjectValid(this.selectedUser.id)) {
      alert("Please, select user");
      return false;
    }

    var selectedCategory = this.startDuelInfo.commonCategories.filter(category => category.selected)[0];
    if (!this.validationService.isObjectValid(selectedCategory)) {
      alert("Please, select category");
      return false;
    }

    if (!this.validationService.isStringValid(this.selectedTaskText)) {
      alert("Please, put a task description");
      return false;
    }

    if(this.selectedTaskText.length < 100) {
      alert("Please, put a bigger task description");
      return false;
    }

    if (!this.validationService.isObjectValid(this.selectedDate)) {
      alert("Please, select deadline");
      return false;
    }

    return true;
  }

  private getMinDate() {
    return new Date(this.getDeadline(this.minDaysAmount));
  }

  private getMaxDate() {
    var date = new Date();

    date.setDate(date.getDate() + this.maxDaysAmount);

    return date;
  }
}
