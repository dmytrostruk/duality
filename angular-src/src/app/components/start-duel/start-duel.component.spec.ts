import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartDuelComponent } from './start-duel.component';

describe('StartDuelComponent', () => {
  let component: StartDuelComponent;
  let fixture: ComponentFixture<StartDuelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartDuelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartDuelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
