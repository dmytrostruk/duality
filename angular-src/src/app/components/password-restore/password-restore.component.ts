import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ValidationService } from '../../services/validation/validation.service';
import { PasswordRestoreService } from './services/password-restore.service';
import { AuthService } from '../../services/auth/auth.service';
import { SocketService } from '../../services/socket/socket.service';
import { ISubscription } from 'rxjs/Subscription';
import { AlertService } from '../../services/alert/alert.service';
import { LoginService } from '../../services/login/login.service';
import { InputType } from '../../enums/input-type';

@Component({
  selector: 'app-password-restore',
  templateUrl: './password-restore.component.html',
  styleUrls: ['./password-restore.component.css']
})
export class PasswordRestoreComponent implements OnInit, OnDestroy {

  private subscription: ISubscription;

  private email: string = "";
  private password: string = "";
  private secondPassword: string = "";
  private restoreEmail: string = "";

  private emailErrorMessage: string = "";
  private passwordErrorMessage: string = "";
  private secondPasswordErrorMessage: string = "";

  private code: string = "";
  private isCodeValid: boolean = null;

  inputType = InputType;

  constructor(
    public loginService: LoginService,
    private validationService: ValidationService,
    private passwordRestoreService: PasswordRestoreService,
    private authService: AuthService,
    private socketService: SocketService,
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.initComponent();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onEmailChange(email: string) {
    this.email = email;
  }

  onPasswordChange(password: string) {
    this.password = password;
  }

  onSecondPasswordChange(secondPassword: string) {
    this.secondPassword = secondPassword;
  }

  sendRestoreLink() {
    this.emailErrorMessage = "";

    if(!this.validationService.isStringValid(this.email)) {
      this.emailErrorMessage = "This field can't be empty";
      return;
    }

    if(!this.validationService.isEmailValid(this.email)) {
      this.emailErrorMessage = "This is not an email";
      return;
    }

    this.passwordRestoreService.sendRestoreLink(this.email).subscribe(response => {
      if(response.success) {
        this.alertService.success("Instructions have been sent, please check your email");
        this.router.navigate(["/login"]);
      } else {
        this.alertService.error("Oops, we encountered a problem. Please try again");
        console.log(response.error);
      }
    })
  }

  savePassword() {
    var success = true;
    this.passwordErrorMessage = "";
    this.secondPasswordErrorMessage = "";

    if(!this.validationService.isStringValid(this.password)) {
      this.passwordErrorMessage = "This field can't be empty";
      success = false;
    } 

    if(!this.validationService.isStringValid(this.secondPassword)) {
      this.secondPasswordErrorMessage = "This field can't be empty";
      success = false;
    } 

    if(this.password !== this.secondPassword) {
      this.secondPasswordErrorMessage = "Passwords do not match";
      success = false;
    }

    if(!success) return;

    this.passwordRestoreService.saveNewPassword(this.restoreEmail, this.password, this.code).subscribe(response => {
      if(response.success) {
        this.authService.storeUserData(response.token, response.user);
        this.socketService.addOnlineUser();
        this.router.navigate(["/following"]);
        this.alertService.success("Password successfully restored. Welcome back!");
      } else {
        this.alertService.error("There was a problem setting your new password. Please try again");
        console.log(response.error);
      }
    })
  }

  private initComponent() {
    this.subscription = this.activatedRoute.queryParams.subscribe(params => {
      var code = params['code'];
      this.code = code;
      
      if(this.validationService.isObjectValid(code)) {
        this.passwordRestoreService.validateRestoreCode(code).subscribe(response => {
          if(response.success) {
            this.restoreEmail = response.email;
            this.isCodeValid = true; 
          } else {
            this.isCodeValid = false;
            this.router.navigate(["/following"]);
            console.log(response.error);
          }
        })
      } else {
        this.isCodeValid = false;
      }
    });
  }
}
