import { Injectable } from '@angular/core';
import { TokenProviderService } from '../../../services/token-provider/token-provider.service';
import { environment } from '../../../../environments/environment';
import { Http } from '@angular/http';
import { RequestHeadersProviderService } from '../../../services/request-headers-provider/request-headers-provider.service';

@Injectable()
export class PasswordRestoreService {

  constructor(
    private tokenProviderService: TokenProviderService,
    private http: Http,
    private headersProviderService: RequestHeadersProviderService
  ) { }

  sendRestoreLink(email: string) {
    return this.http.get(environment.serverEndpoint + "/api/restore-password",
      {
        params: { 
          email: email
        }
      })
      .map(res => res.json());
  }

  validateRestoreCode(code: string) {
    return this.http.get(environment.serverEndpoint + "/api/validate-restore-code",
    {
      params: { 
        code: code
      }
    })
    .map(res => res.json());
  }

  saveNewPassword(email: string, password: string, code: string) {

    var data = {
      email: email,
      password: password,
      code: code
    };

    return this.http.post(environment.serverEndpoint + "/api/save-new-password", data)
      .map(res => res.json());
  }
}
