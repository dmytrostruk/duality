import { TestBed, inject } from '@angular/core/testing';

import { PasswordRestoreService } from './password-restore.service';

describe('PasswordRestoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PasswordRestoreService]
    });
  });

  it('should be created', inject([PasswordRestoreService], (service: PasswordRestoreService) => {
    expect(service).toBeTruthy();
  }));
});
