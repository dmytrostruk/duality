import { Component, OnInit } from '@angular/core';
import { DuelStatus } from '../../enums/duel-status';

import { UserService } from '../user/services/user.service';
import { AuthService } from '../../services/auth/auth.service';
import { VoteService } from '../../services/vote/vote.service';
import { MyDuelsService } from './services/my-duels.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-my-duels',
  templateUrl: './my-duels.component.html',
  styleUrls: ['./my-duels.component.css']
})
export class MyDuelsComponent implements OnInit {

  duels: Array<any> = [];
  currentUser: any;
  duelsAmount = {};
  
  duelStatus = DuelStatus;

  private showPage: boolean = false;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private voteService: VoteService,
    private myDuelsService: MyDuelsService,
    private titleService: Title
  ) { }

  ngOnInit() {
    this.initializeData();
    this.titleService.setTitle("My Duels - Duelity");
  }
  
  onNotify(userId) {
    this.getUserDuels(this.currentUser.id);
  }

  changeCurrentStatus(status) {
    this.myDuelsService.currentStatus = status;
    this.getUserDuels(this.currentUser.id);
  }

  openVotePage(duel) {
    this.voteService.openVotePage(duel);
  }

  turnOffHelp() {
    this.userService.turnOffHelp().subscribe(response => {
      if(response.success) {
        this.authService.updateUserData(response.user);
      } else {
        console.log(response.error);
      }
    })
  }

  private initializeData() {
    this.currentUser = this.authService.getCurrentUser();

    this.getUserDuels(this.currentUser.id);
  }

  private getUserDuels(userId) {
    this.userService.getUserDuels(userId, [this.myDuelsService.currentStatus]).subscribe(response => {
      if(response.success) {
        this.duels = response.duels;
        this.duelsAmount = response.duelsAmount;
        this.showPage = true;
      } else {
        console.log(response.error);
      }
    });
  }
}
