import { TestBed, inject } from '@angular/core/testing';

import { MyDuelsService } from './my-duels.service';

describe('MyDuelsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyDuelsService]
    });
  });

  it('should be created', inject([MyDuelsService], (service: MyDuelsService) => {
    expect(service).toBeTruthy();
  }));
});
