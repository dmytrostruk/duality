import { Injectable } from '@angular/core';
import { DuelStatus } from '../../../enums/duel-status';

@Injectable()
export class MyDuelsService {
  public currentStatus: DuelStatus = DuelStatus.Waiting;
  private myDuelsPage: string = "myduels"; 
  
  constructor() { }

  getMyDuelsPage() {
    return this.myDuelsPage;
  }
}
