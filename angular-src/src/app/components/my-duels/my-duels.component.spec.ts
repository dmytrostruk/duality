import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDuelsComponent } from './my-duels.component';

describe('MyDuelsComponent', () => {
  let component: MyDuelsComponent;
  let fixture: ComponentFixture<MyDuelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDuelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDuelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
