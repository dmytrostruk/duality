import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-pro-account',
  templateUrl: './pro-account.component.html',
  styleUrls: ['./pro-account.component.css']
})
export class ProAccountComponent implements OnInit {

  private currentUser : any = null;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();
  }
}
