import { Component, OnInit, OnDestroy, HostListener, ViewChild } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { UploadEvent, UploadFile, FileSystemFileEntry } from 'ngx-file-drop';
import { WorkUploadMessage, PreviewImageUploadMessage, DescriptionUploadMessage } from './constants/save-button-tooltip-messages';

import { WorkService } from './services/work.service';
import { AuthService } from '../../services/auth/auth.service';
import { CloudinaryService } from '../../services/cloudinary-service/cloudinary.service';
import { ValidationService } from '../../services/validation/validation.service';
import { Title } from '@angular/platform-browser';
import { AlertService } from '../../services/alert/alert.service';
import { InputType } from '../../enums/input-type';

declare var $: any;

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.css']
})
export class WorkComponent implements OnInit, OnDestroy {

  private duelInfo: any;
  private currentUserInfo: any;
  private opponentUserInfo: any;
  private subscription: ISubscription;

  private success: boolean = null;
  private message: string = null;
  private buttonTooltip: string = "";

  public imagesForDelete: Array<any> = [];
  public wasSaved: boolean = false;

  private isExpandedTask: boolean = false;
  private expandTaskButtonText: string = "Expand Task";
  private duelTaskHeight: number = 0;
  private minDuelTaskHeight: number = 90;

  @ViewChild('expandButton') expandButton;
  @ViewChild('duelTaskBlock') duelTaskBlock;
  
  inputType = InputType;

  constructor(
    public cloudinaryService: CloudinaryService,
    private activatedRoute: ActivatedRoute,
    private workService: WorkService,
    private authService: AuthService,
    private validationService: ValidationService,
    private router: Router,
    private titleService: Title,
    private alertService: AlertService
  ) { }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHandler($event: any) {
    if(!this.wasSaved) {
      var publicIds = [];

      publicIds.push(this.cloudinaryService.responses.map(response => response.data.public_id));

      if(this.cloudinaryService.previewImageResponse) {
        publicIds.push(this.cloudinaryService.previewImageResponse.public_id);
      }

      this.cloudinaryService.destroyUploader();
      this.workService.clearResourcesInCrashWay(publicIds);
    }
  }

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      var duelId = params['id'];
      this.getWork(duelId);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();

    if(this.cloudinaryService.uploader.queue.length > 0) {
      this.cloudinaryService.uploader.onCompleteAll = this.clearResources.bind(this);
    } else {
      this.clearResources();
    }
  }

  toogleTaskExpand() {
    var svg = $($(this.expandButton.nativeElement).find("svg"));
    var taskBlock = this.getTaskBlock();

    if(this.isExpandedTask) {
      svg.addClass("spin");
      taskBlock.height(this.minDuelTaskHeight);
      setTimeout(() =>{ 
        svg.removeClass("spin");
        this.expandTaskButtonText = "Expand Task";
        this.isExpandedTask = false;
      }, 200);
    } else {
      svg.addClass("spin");
      taskBlock.height(this.duelTaskHeight);
      setTimeout(() => { 
        svg.removeClass("spin");
        this.expandTaskButtonText = "Collapse Task";
        this.isExpandedTask = true;
      }, 200);
    }
  }

  initializeExpandableBlock() {
    var taskBlock = this.getTaskBlock();
    this.duelTaskHeight = taskBlock.height();

    if(this.duelTaskHeight < this.minDuelTaskHeight) {
      var btn = $(this.expandButton.nativeElement);
      btn.hide();
    }

    taskBlock.height(this.minDuelTaskHeight);
  }

  getTaskBlock() {
    return $(this.duelTaskBlock.nativeElement);
  }

  private onSelectedTaskText(taskText) {
    this.currentUserInfo.workDescription = taskText;
    this.updateSaveButtonTooltip();
  }

  private clearResources() {
    if(!this.wasSaved) {
      this.removeTemporaryImages(this.cloudinaryService.files, true);
    }

    this.cloudinaryService.destroyUploader();
  }

  private uploadWork() {
    var data = {
      duelId: this.duelInfo.id,
      imagesForDelete: this.imagesForDelete,
      workDescription: this.currentUserInfo.workDescription,
      previewImage: {
        publicId: this.cloudinaryService.previewImageResponse && 
                  this.cloudinaryService.previewImageResponse.public_id || this.currentUserInfo.previewImageUrl.publicId,
        url: this.cloudinaryService.previewImageResponse && 
             this.cloudinaryService.previewImageResponse.secure_url || this.currentUserInfo.previewImageUrl.url
      },
      imageUrls: this.cloudinaryService.responses.map(response => {
        return {
          publicId: response.data.public_id,
          url: response.data.secure_url
        };
      })
    };

    this.workService.uploadWork(data).subscribe(response => {
      if(response.success) {
        this.wasSaved = true;
        this.router.navigate(["/myduels"]);
        this.alertService.success(this.getUploadSuccessMessage());
      } else {
        this.alertService.error(this.getUploadErrorMessage());
      }
    });
  }

  private removeImage(imagePublicId) {
    this.currentUserInfo.imageUrls = this.currentUserInfo.imageUrls.filter(image => image.publicId !== imagePublicId);
    this.imagesForDelete.push(imagePublicId);

    this.updateSaveButtonTooltip();
  }

  private removePreviewImage() {
    this.currentUserInfo.previewImageUrl = { publicId: null, url: null};

    this.updateSaveButtonTooltip();
  }

  private removeTemporaryImages(items: Array<any>, clearPreviewImage: boolean) {
    return new Promise((resolve, reject) => {
      var targetResponses = this.cloudinaryService.responses.filter(response => items.indexOf(response.file) >= 0);
      var publicIds = [];
      
      if(targetResponses && targetResponses.length > 0) {
        publicIds.push(targetResponses.map(response => response.data.public_id));
        this.cloudinaryService.responses = this.cloudinaryService.responses.filter(response => targetResponses.indexOf(response) === -1);
        this.cloudinaryService.files = this.cloudinaryService.files.filter(file => targetResponses.map(response => response.file).indexOf(file) === -1);
      }

      if(this.cloudinaryService.previewImageResponse && clearPreviewImage) {
        publicIds.push(this.cloudinaryService.previewImageResponse.public_id);
        this.cloudinaryService.previewImageFile = null;
        this.cloudinaryService.previewImageResponse = null;
      }

      this.updateSaveButtonTooltip();

      this.workService.deleteTemporaryImage(publicIds).subscribe(data => {
        if(data.success) {
          resolve();
        } else {
          reject();
        }
      });
    });
  }

  private updateSaveButtonTooltip() {
    var pageState = this.getPageState();

    if(!pageState.stagingImagesExist && !pageState.uploadedImagesExist) {
      if(this.buttonTooltip.indexOf(WorkUploadMessage) === -1) {
        this.buttonTooltip += WorkUploadMessage;
      }
    } else {
      this.buttonTooltip = this.buttonTooltip.replace(WorkUploadMessage, "");
    }

    if(!pageState.stagingPreviewImageExists && !pageState.uploadedPreviewImageExists) {
      if(this.buttonTooltip.indexOf(PreviewImageUploadMessage) === -1) {
        this.buttonTooltip += PreviewImageUploadMessage;
      }
    } else {
      this.buttonTooltip = this.buttonTooltip.replace(PreviewImageUploadMessage, "");
    }

    if(!pageState.descriptionExists) {
      if(this.buttonTooltip.indexOf(DescriptionUploadMessage) === -1) {
        this.buttonTooltip += DescriptionUploadMessage;
      }
    } else {
      this.buttonTooltip = this.buttonTooltip.replace(DescriptionUploadMessage, "");
    }

    if(pageState.allItemsExists || pageState.noItemsAtAll) {
      this.buttonTooltip = "";
    }
  }

  private canSave() {
    var pageState = this.getPageState();

    return pageState.noItemsInQueue && (pageState.allItemsExists || pageState.noItemsAtAll);
  }

  private getPageState() {
    var noItemsInQueue = this.cloudinaryService.uploader && 
                         this.cloudinaryService.uploader.queue && 
                         this.cloudinaryService.uploader.queue.length === 0;

    var stagingImagesExist         = this.cloudinaryService.responses.length > 0,
        stagingPreviewImageExists  = this.validationService.isObjectValid(this.cloudinaryService.previewImageResponse) &&
                                     this.validationService.isObjectValid(this.cloudinaryService.previewImageResponse.public_id),
        uploadedImagesExist        = this.currentUserInfo.imageUrls.length > 0,
        uploadedPreviewImageExists = this.validationService.isObjectValid(this.currentUserInfo.previewImageUrl) && 
                                     this.validationService.isObjectValid(this.currentUserInfo.previewImageUrl.url),
        descriptionExists          = this.validationService.isStringValid(this.currentUserInfo.workDescription);

    var allItemsExists = descriptionExists && 
                        ((stagingImagesExist && stagingPreviewImageExists) || 
                        (stagingImagesExist && uploadedPreviewImageExists) ||
                        (uploadedImagesExist && stagingPreviewImageExists) ||
                        (uploadedImagesExist && uploadedPreviewImageExists));

    var noItemsAtAll = !descriptionExists && 
                       !stagingImagesExist && 
                       !stagingPreviewImageExists && 
                       !uploadedImagesExist &&
                       !uploadedPreviewImageExists;

    return { 
      noItemsInQueue, 
      stagingImagesExist, 
      stagingPreviewImageExists, 
      uploadedImagesExist, 
      uploadedPreviewImageExists,
      descriptionExists,
      allItemsExists,
      noItemsAtAll
    }
  }

  private onPreviewImageUpload(e) {
    var file = e.target.files[0];

    if(file) {
      if(this.validationService.isProImageValid(file.type, this.currentUserInfo.user.isPaidAccount)) {
        this.cloudinaryService.previewImageUploader.addToQueue(e.target.files);
      } else {
        alert("Image format is not valid");
      }
    }
  }

  private cancelUploading() {
    this.wasSaved = false;
    this.router.navigate(["/myduels"]);
  }
  
  private getWork(duelId) {
    this.workService.getWork(duelId).subscribe(response => {
      if(response.success) {
        if(response.duel !== null) {
          this.success = true;
          this.duelInfo = response.duel;
          this.currentUserInfo = this.getCurrentUserInfo(this.duelInfo);
          this.opponentUserInfo = this.getOpponentUserInfo(this.duelInfo);

          this.titleService.setTitle(`${this.isUploadMode() ? "Uploading Work - Duelity" : "Editing Work - Duelity"}`)

          this.cloudinaryService.initWorkUploaders(this.duelInfo.id, this.currentUserInfo.user.id, this.updateSaveButtonTooltip.bind(this));

          setTimeout(() => {this.initializeExpandableBlock()}, 1000);
        } else {
          this.success = false;
          this.message = response.message;
        }
      } else {
        console.log(response.error);
      }
    })
  }

  private getCurrentUserInfo(duel) {
    return duel.leftUser.user.id === this.authService.getCurrentUser().id ? 
           duel.leftUser : duel.rightUser;
  }

  private getOpponentUserInfo(duel) {
    return duel.leftUser.user.id === this.authService.getCurrentUser().id ? 
           duel.rightUser : duel.leftUser;
  }

  private isUploadMode() {
    return this.currentUserInfo.imageUrls.length === 0;
  }

  private isOpponentReady() {
    return this.opponentUserInfo.imageUrls.length !== 0;
  }

  private getUploadSuccessMessage() : string {
    return this.isOpponentReady() ? "Work has been uploaded. Voting started!" : 
           this.isUploadMode() ? `Work has been uploaded. Waiting for ${this.opponentUserInfo.user.fullname} now`
           : "Work edits has been saved";
  }

  private getUploadErrorMessage() : string {
    return this.isUploadMode() ? "Work could not be uploaded. Try reuploading again" : 
           "Works edits could not be saved. Please try again";
  }
}
