import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { RequestHeadersProviderService } from '../../../services/request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../../../services/token-provider/token-provider.service';
import { AuthService } from '../../../services/auth/auth.service';
import 'rxjs/add/operator/map';
declare var $: any;

@Injectable()
export class WorkService {

  constructor(
    private tokenProviderService: TokenProviderService,
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private authService: AuthService
  ) { }

  getWork(duelId) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/duel/work",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          duelId: duelId,
          userId: this.authService.getCurrentUser().id
        } 
      })
      .map(res => res.json());
  }

  uploadWork(data) {
    this.tokenProviderService.loadToken();

    data.userId = this.authService.getCurrentUser().id;

    return this.http.post(environment.serverEndpoint + "/duel/upload", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  deleteTemporaryImage(imagesForDelete) {
    this.tokenProviderService.loadToken();

    var data = { imagesForDelete: imagesForDelete };

    return this.http.post(environment.serverEndpoint + "/duel/delete-temp-images", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  clearResourcesInCrashWay(imagesForDelete) {
    this.tokenProviderService.loadToken();

    var data = { imagesForDelete: imagesForDelete };

    $.ajax({
      async: false,
      url: environment.serverEndpoint + "/duel/delete-temp-images",
      headers: {
          'Content-Type':'application/json',
          'Authorization':this.tokenProviderService.authToken,
      },
      method: 'POST',
      data: JSON.stringify(data),
      success: function(data){
        console.log('success: '+ data);
      }
    });
  }
}
