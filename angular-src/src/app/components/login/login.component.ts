import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ValidationService } from '../../services/validation/validation.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { SocketService } from '../../services/socket/socket.service';
import { GoogleService } from '../../services/google/google.service';
import { TwitterService } from '../../services/twitter/twitter.service';
import { RegisterService } from '../register/services/register.service';
import { AlertService } from '../../services/alert/alert.service';
import { LoginService } from '../../services/login/login.service';
import { InputType } from '../../enums/input-type';
import { NotificationService } from '../../services/notification/notification.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;

  usernameErrorMessage: string = "";
  passwordErrorMessage: string = "";

  @ViewChild('googleButton')
  public googleButton: ElementRef;

  inputType = InputType;

  constructor(
    private router: Router,
    private authService: AuthService,
    private validationService: ValidationService,
    private socketService: SocketService,
    private googleService: GoogleService,
    public twitterService: TwitterService,
    public registerService: RegisterService,
    private alertService: AlertService,
    private loginService: LoginService,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.googleService.initGoogleAuth(this.googleButton.nativeElement);
  }

  goToRegister() {
    this.loginService.closeModal();
    this.registerService.goToRegister();
  }

  goToPasswordRestore() {
    this.loginService.closeModal();
    this.router.navigate(["/restore"]);
  }

  onUsernameChange(username: string) {
    this.username = username;
  }

  onPasswordChange(password: string) {
    this.password = password;
  }

  onLoginSubmit() {
    var success = true;

    this.usernameErrorMessage = "";
    this.passwordErrorMessage = "";

    const user = {
      username: this.username,
      password: this.password
    };

    if (!this.validationService.isStringValid(user.username)) {
      this.usernameErrorMessage = "This field can't be empty";
      success = false;
    }

    if (!this.validationService.isStringValid(user.password)) {
      this.passwordErrorMessage = "This field can't be empty";
      success = false;
    }

    if (!success) return;

    this.authService.authenticateUser(user).subscribe(data => {
      if (data.success) {
        this.authService.storeUserData(data.token, data.user);
        this.socketService.addOnlineUser();
        this.loginService.closeModal();
        this.router.navigate([this.router.url]);
        window.location.reload();
      } else {
        this.alertService.error(data.message);
      }
    });
  }

  private isUserValid(user) {
    return this.validationService.isObjectValid(user) &&
      this.validationService.isStringValid(user.username) &&
      this.validationService.isStringValid(user.password);
  }
}
