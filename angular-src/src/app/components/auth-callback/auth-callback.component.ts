import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { TwitterService } from '../../services/twitter/twitter.service';
import { AuthService } from '../../services/auth/auth.service';
import { SocketService } from '../../services/socket/socket.service';
import { RegisterService } from '../register/services/register.service';

@Component({
  selector: 'app-auth-callback',
  templateUrl: './auth-callback.component.html',
  styleUrls: ['./auth-callback.component.css']
})
export class AuthCallbackComponent implements OnInit, OnDestroy {

  private subscription: ISubscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private twitterService: TwitterService,
    private authService: AuthService,
    private socketService: SocketService,
    private registerService: RegisterService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initTwitterCallback();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  
  private initTwitterCallback() {
    this.subscription = this.activatedRoute.queryParams.subscribe(params => {
      var token = params['oauth_token'],
          verifier = params['oauth_verifier'],
          denied = params['denied'];

      if(denied) {
        this.router.navigate(["/register"]);
        return;
      }

      this.twitterService.verifyUser(token, verifier).subscribe(response => {
        if(response.success) {
          if(response.newUser) {
            this.registerService.initializeAuthRegister(response.user.email, response.user.fullname);
            this.router.navigate(["/register"]);
          } else {
            this.authService.storeUserData(response.token, response.user);
            this.socketService.addOnlineUser();
            this.router.navigate(["/following"]);
          }
        } else {
          console.log(response.error);
        }
      })
    });
  }
}
