import { TestBed, inject } from '@angular/core/testing';

import { DuelStatusMessageService } from './duel-status-message.service';

describe('DuelStatusMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DuelStatusMessageService]
    });
  });

  it('should be created', inject([DuelStatusMessageService], (service: DuelStatusMessageService) => {
    expect(service).toBeTruthy();
  }));
});
