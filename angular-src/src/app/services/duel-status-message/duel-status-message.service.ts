import { Injectable } from '@angular/core';
import { DuelStatus } from '../../enums/duel-status';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class DuelStatusMessageService {

  constructor(
    private authService: AuthService
  ) { }

  getStatusMessage(duel) {
    switch(duel.status) {
      case DuelStatus.Waiting: return this.getWaitingStatusMessage(duel);
      case DuelStatus.InProgress: return "left to upload works";
      case DuelStatus.Voting: return "until voting ends";
      case DuelStatus.Ended: return "this duel has ended";
    }
  }

  private getWaitingStatusMessage(duel) {
    return duel.leftUser.user.id === this.authService.getCurrentUser().id ? 
    `left for ${duel.rightUser.user.fullname} to decide` : 
    "left to accept or decline duel";
  }
}
