import { Injectable } from '@angular/core';

@Injectable()
export class ValidationService {

  static readonly imageFormats = ["image/jpeg", "image/png"];
  static readonly gifImageFormat = "image/gif";

  constructor() { }

  getEmptyFieldMessage() : string {
    return "This field can't be empty";
  }

  getTooShortMessage(fieldName: string, symbolAmount: number) : string {
    return `Your ${fieldName} can't be shorter than ${symbolAmount} symbols`;
  }

  getTooShortAltMessage(fieldName: string, symbolAmount: number) : string {
    return `Your ${fieldName} needs to be at least ${symbolAmount} symbols long`;
  } 

  getTooLongMessage(fieldName: string, symbolAmount: number) : string {
    return `Your ${fieldName} can't be longer than ${symbolAmount} symbols`;
  }

  getForbiddenCharactersMessage(fieldName: string, allowedCharacters: string) : string {
    return `Your ${fieldName} can only have ${allowedCharacters}`;
  }

  getIsNotAnEmailMessage() {
    return "This is not an email";
  }

  isObjectValid(object) {
    return object !== null && object !== undefined;
  }

  isStringValid(string) {
    return this.isObjectValid(string) && string.replace(/ /g,'') !== "";
  }

  isEmailValid(email) {
    if(!this.isStringValid(email)) { 
      return false;
    };

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  isEnglishString(string) {
    var re = /^[a-zA-Z ]+$/;
    return this.isStringValid(string) && re.test(String(string));
  }

  isUsernameValid(string) {
    var re = /^[a-zA-Z0-9-_]+$/;
    return re.test(String(string));
  }

  isSocialNetworkValid(string) {
    var re = /^[a-zA-Z0-9-_.]+$/;
    return re.test(String(string));
  }

  isImageValid(imageType) {
    return ValidationService.imageFormats.indexOf(imageType) >= 0;
  }

  isProImageValid(imageType, isPro) {
    return this.isImageValid(imageType) || (isPro && imageType === ValidationService.gifImageFormat);
  }

  //#region Validators

  validateFullname(fullname: string, isRequired: boolean) : string {
    const min = 3;
    const max = 32;
    const fieldName = "Full Name";

    fullname = fullname == null || fullname == undefined ? "" : fullname;

    var modifiedFullname = fullname.replace(/ /g,'');

    if(isRequired) {
      if(!this.isStringValid(modifiedFullname)) {
        return this.getEmptyFieldMessage();
      }
    }

    if(modifiedFullname.length < min) {
      return this.getTooShortMessage(fieldName, min);
    }

    if(modifiedFullname.length > max) {
      return this.getTooLongMessage(fieldName, max);
    }

    if(!this.isEnglishString(modifiedFullname)) {
      return this.getForbiddenCharactersMessage(fieldName, 'A-Z, a-z, " "');
    }
    
    return "";
  }

  validateEmail(email: string, isRequired: boolean) : string {
    const min = 6;
    const max = 64;
    const fieldName = "Email";

    email = email == null || email == undefined ? "" : email;

    var modifiedEmail = email.replace(/ /g,'');

    if(isRequired) {
      if(!this.isStringValid(modifiedEmail)) {
        return this.getEmptyFieldMessage();
      }
    }

    if(email.length < min) {
      return this.getTooShortMessage(fieldName, min);
    }

    if(email.length > max) {
      return this.getTooLongMessage(fieldName, max);
    }

    if(!this.isEmailValid(email)) {
      return this.getIsNotAnEmailMessage();
    }
  }

  validatePassword(password: string, isRequired: boolean) : string {
    const min = 6;
    const max = 64;
    const fieldName = "Password";

    password = password == null || password == undefined ? "" : password;

    if(isRequired) {
      if(!this.isStringValid(password)) {
        return this.getEmptyFieldMessage();
      }
    }

    if(password.length < min) {
      return this.getTooShortAltMessage(fieldName, min);
    }

    if(password.length > max) {
      return this.getTooLongMessage(fieldName, max);
    }
  }

  validateUsername(username: string, isRequired: boolean) : string {
    const min = 3;
    const max = 24;
    const fieldName = "Username";

    username = username == null || username == undefined ? "" : username;

    var modifiedUsername = username.replace(/ /g,'');

    if(isRequired) {
      if(!this.isStringValid(username)) {
        return this.getEmptyFieldMessage();
      }
    }

    if(username.length < min) {
      return this.getTooShortAltMessage(fieldName, min);
    }

    if(username.length > max) {
      return this.getTooLongMessage(fieldName, max);
    }

    if(!this.isUsernameValid(username)) {
      return this.getForbiddenCharactersMessage(fieldName, 'A-Z, a-z, 0-9, "-", "_"');
    }
  }

  validateDescription(description: string, isRequired: boolean) : string {
    const max = 50;
    const fieldName = "Description";

    description = description == null || description == undefined ? "" : description;

    var modifiedDescription = description.replace(/ /g,'');

    if(isRequired) {
      if(!this.isStringValid(modifiedDescription)) {
        return this.getEmptyFieldMessage();
      }
    }

    if(modifiedDescription.length > max) {
      return this.getTooLongMessage(fieldName, max);
    }
  }

  validateSocialNetwork(fieldName: string, socialNetwork: string, isRequired: boolean) : string {
    socialNetwork = socialNetwork == null || socialNetwork == undefined ? "" : socialNetwork;

    var modifiedSocialNetwork = socialNetwork.replace(/ /g,'');

    if(isRequired) {
      if(!this.isStringValid(socialNetwork)) {
        return this.getEmptyFieldMessage();
      }
    }
    
    if(!this.isSocialNetworkValid(socialNetwork)) {
      return this.getForbiddenCharactersMessage(fieldName, 'A-Z, a-z, 0-9, "-", "_" "."');
    }
  }

  //endregion
}
