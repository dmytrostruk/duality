import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';

@Injectable()
export class RequestHeadersProviderService {

  constructor() { }

  getJSONHeaders() {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    
    return headers;
  }

  getProtectedHeaders(token) {
    let headers = new Headers();
    headers.append("Authorization", token);
    
    return headers;
  }

  getJSONProtectedHeaders(token) {
    let headers = this.getJSONHeaders();
    headers.append("Authorization", token);
    
    return headers;
  }
}
