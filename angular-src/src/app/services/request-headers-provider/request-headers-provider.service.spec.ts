import { TestBed, inject } from '@angular/core/testing';

import { RequestHeadersProviderService } from './request-headers-provider.service';

describe('RequestHeadersProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequestHeadersProviderService]
    });
  });

  it('should be created', inject([RequestHeadersProviderService], (service: RequestHeadersProviderService) => {
    expect(service).toBeTruthy();
  }));
});
