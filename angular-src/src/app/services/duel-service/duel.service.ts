import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { DuelStatus } from '../../enums/duel-status';
import { RequestHeadersProviderService } from '../../services/request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../../services/token-provider/token-provider.service';
import { AuthService } from '../../services/auth/auth.service';
import { MyDuelsService } from '../../components/my-duels/services/my-duels.service';
import { DuelType } from '../../enums/duel-type';
import 'rxjs/add/operator/map';

@Injectable()
export class DuelService {

  private duelPage: string = "duel";

  constructor(
    private tokenProviderService: TokenProviderService,
    private authService: AuthService,
    private headersProviderService: RequestHeadersProviderService,
    private myDuelsService: MyDuelsService,
    private router: Router,
    private http: Http
  ) { }

  removeDuel(duelId) {
    this.tokenProviderService.loadToken();

    var duelInfo = {
      duelId: duelId,
      currentUserId: this.authService.getCurrentUser().id
    };

    return this.http.post(environment.serverEndpoint + "/duel/remove", duelInfo,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  acceptDuel(duelId) {
    this.tokenProviderService.loadToken();

    var duelInfo = {
      duelId: duelId,
      currentUserId: this.authService.getCurrentUser().id
    };

    return this.http.post(environment.serverEndpoint + "/duel/accept", duelInfo,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  getDuelStatus(duelId) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/duel/get-status",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          duelId: duelId
        } 
      })
      .map(res => res.json());
  }

  getDuelPage() {
    return this.duelPage;
  }

  goToDuel(duelId) {
    this.getDuelStatus(duelId).subscribe(response => {
      if(response.success) {
        if(response.status === DuelStatus.Waiting || response.status === DuelStatus.InProgress) {
          this.myDuelsService.currentStatus = response.status;
          this.router.navigate([this.myDuelsService.getMyDuelsPage()]);
        } else {
          this.router.navigate([this.duelPage, duelId]);
        }
      } else {
        console.log(response.error);
      }
    })
  }

  getDuelByType(duelType: DuelType, page: number, pageSize: number = 40) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/duel/get-duels-by-type",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          duelType: duelType,
          currentUserId: this.authService.getCurrentUserId(),
          page: page,
          pageSize: pageSize
        } 
      })
      .map(res => res.json());
  }

  changeDuelVisibility(duelId, hide) {
    this.tokenProviderService.loadToken();

    var duelInfo = {
      duelId: duelId,
      currentUserId: this.authService.getCurrentUserId(),
      hide: hide
    };

    return this.http.post(environment.serverEndpoint + "/duel/change-visibility", duelInfo,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }
}
