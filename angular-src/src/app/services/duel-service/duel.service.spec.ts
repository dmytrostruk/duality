import { TestBed, inject } from '@angular/core/testing';

import { DuelService } from './duel.service';

describe('DuelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DuelService]
    });
  });

  it('should be created', inject([DuelService], (service: DuelService) => {
    expect(service).toBeTruthy();
  }));
});
