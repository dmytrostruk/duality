import { Injectable, OnInit, Input, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FileUploader, FileUploaderOptions, ParsedResponseHeaders, FileItem } from 'ng2-file-upload';
import { Cloudinary } from '@cloudinary/angular-5.x';

import { ValidationService } from '../validation/validation.service';

@Injectable()
export class CloudinaryService {

  private queue: any;

  public responses: Array<any> = [];
  public files: Array<any> = [];
  public uploader: FileUploader;

  public previewImageResponse: any;
  public previewImageFile: any;
  public previewImageUploader: FileUploader;

  public avatarResponse: any;
  public avatarFile: any;
  public avatarUploader: FileUploader;

  public hasBaseDropZoneOver: boolean = false;

  private uploaderOptions: FileUploaderOptions = {
    url: `https://api.cloudinary.com/v1_1/${this.cloudinary.config().cloud_name}/upload`,
    autoUpload: true,
    isHTML5: true,
    removeAfterUpload: true,
    headers: [
      {
        name: 'X-Requested-With',
        value: 'XMLHttpRequest'
      }
    ]
  };

  constructor(
    private cloudinary: Cloudinary,
    private zone: NgZone,
    private validationService: ValidationService
  ) { 
    this.responses = [];
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  getFileProperties(fileProperties: any) {
    if (!fileProperties) {
      return null;
    }
    return Object.keys(fileProperties).map((key) => ({ 'key': key, 'value': fileProperties[key] }));
  }

  destroyUploader() {
    this.hasBaseDropZoneOver = false;

    this.responses = [];
    this.files = [];
    this.previewImageFile = null;
    this.previewImageResponse = null;
  }

  initWorkUploaders(duelId, userId, tooltipUpdate) {
    var self = this;

    this.uploader = new FileUploader(this.uploaderOptions);
    this.previewImageUploader = new FileUploader(this.uploaderOptions);

    this.previewImageFile = null;
    this.previewImageResponse = null;

    this.workUploaderInitHandlers(duelId, userId, tooltipUpdate);
    this.previewImageUploaderInitHandlers(duelId, userId, tooltipUpdate);
  }

  initAvatarUploader(userId, uploadCallback) {
    var self = this;

    this.avatarUploader = new FileUploader(this.uploaderOptions);

    this.avatarFile = null;
    this.avatarResponse = null;

    this.avatarUploaderInitHandlers(userId, uploadCallback);
  }

  private upsertResponse = fileItem => {
    this.zone.run(() => {
      const existingId = this.responses.reduce((prev, current, index) => {
        if (current.file.name === fileItem.file.name && !current.status) {
          return index;
        }
        return prev;
      }, -1);
      if (existingId > -1) {
        this.responses[existingId] = Object.assign(this.responses[existingId], fileItem);
      } else {
        this.responses.push(fileItem);
      }
    });
  }

  private workUploaderInitHandlers(duelId, userId, tooltipUpdate) {
    this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      form.append('upload_preset', this.cloudinary.config().upload_preset);
      form.append('folder', "duels/duel-" + duelId + "/user-" + userId + "/work-images");
      form.append('file', fileItem);

      fileItem.withCredentials = false;
      return { fileItem, form };
    }

    this.uploader.onAfterAddingFile = (fileItem: FileItem) => {
      if(!this.validationService.isProImageValid(fileItem.file.type, true)) {
        this.uploader.removeFromQueue(fileItem);
      }
    }

    this.uploader.onCompleteItem = (item: any, response: string, status: number, headers: ParsedResponseHeaders) => {
      this.files.push(item.file);
      if(this.validationService.isStringValid(response)) {
        this.upsertResponse({
          file: item.file,
          status,
          data: JSON.parse(response)
        });

        tooltipUpdate();
      }
    }

    this.uploader.onProgressItem = (fileItem: any, progress: any) => {
      this.upsertResponse({
          file: fileItem.file,
          progress,
          data: {}
      });
    };

    this.uploader.onCompleteAll = () => {
      return void 0;
    }
  }

  private previewImageUploaderInitHandlers(duelId, userId, tooltipUpdate) {
    this.previewImageUploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      form.append('upload_preset', this.cloudinary.config().upload_preset);
      form.append('folder', "duels/duel-" + duelId + "/user-" + userId + "/preview-image");
      form.append('file', fileItem);

      fileItem.withCredentials = false;
      return { fileItem, form };
    }

    this.previewImageUploader.onCompleteItem = (item: any, response: string, status: number, headers: ParsedResponseHeaders) => {
      this.previewImageFile = item.file;
      if(this.validationService.isStringValid(response)) {
        this.previewImageResponse = JSON.parse(response);
        tooltipUpdate();
      }
    }
  }

  private avatarUploaderInitHandlers(userId, callback) {
    this.avatarUploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      form.append('upload_preset', this.cloudinary.config().upload_preset);
      form.append('folder', "avatars/" + userId);
      form.append('file', fileItem);

      fileItem.withCredentials = false;
      return { fileItem, form };
    } 

    this.avatarUploader.onCompleteItem = (item: any, response: string, status: number, headers: ParsedResponseHeaders) => {
      this.avatarFile = item.file;
      if(this.validationService.isStringValid(response)) {
        this.avatarResponse = JSON.parse(response);
        callback(this.avatarResponse.secure_url, this.avatarResponse.public_id, "");
      }
    }
  }
}
