import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';

@Injectable()
export class TwitterService {

  constructor(private http: Http) { }

  public getTwitterUrl() {
    return this.http.get(environment.serverEndpoint + "/api/get-twitter-url")
                    .map(res => res.json());
  }

  public verifyUser(token, verifier) {
    return this.http.get(environment.serverEndpoint + "/api/verify-twitter-user",
      {
        params: { 
          token: token,
          verifier: verifier
        } 
      })
      .map(res => res.json());
  }

  public onTwitterSignIn() {
    this.getTwitterUrl().subscribe(response => {
      if(response.success) {
        window.location.href = response.url;
      } else {
        console.log(response.error);
      }
    });
  }
}
