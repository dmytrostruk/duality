import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { AuthService } from '../auth/auth.service';
import { SocketService } from '../socket/socket.service';
import { Router } from '@angular/router';
import { RegisterService } from '../../components/register/services/register.service';
import { LoginService } from '../login/login.service';

declare const gapi: any;

@Injectable()
export class GoogleService {
  public auth2: any;

  constructor(
    private http: Http,
    private authService: AuthService,
    private socketService: SocketService,
    private registerService: RegisterService,
    private router: Router,
    private loginService: LoginService
  ) { }

  public initGoogleAuth(element) {
    setTimeout(() => {
      gapi.load('auth2', () => {
        this.auth2 = gapi.auth2.init({
          client_id: environment.google.clientId,
          cookiepolicy: 'single_host_origin',
          scope: 'profile email'
        });
  
        this.attachSignIn(element);
      });
    });
  }

  private attachSignIn(element) {
    var self = this;
    this.auth2.attachClickHandler(element, {},
      (googleUser) => {
        var profile = googleUser.getBasicProfile(),
            token = googleUser.getAuthResponse().id_token;

        this.verifyTokenAndAuthorize(token).subscribe(response => {
          if(response.success) {
            if(response.newUser) {
              this.registerService.initializeAuthRegister(response.user.email, response.user.fullname);
              this.router.navigate(["/register"]);
            } else {
              this.authService.storeUserData(response.token, response.user);
              this.socketService.addOnlineUser();
              this.router.navigate(["/following"]);
            }

            this.loginService.closeModal();
          } else {
            console.log(response.error);
          }
        });
      }, 
      (error) => {
        console.log(JSON.stringify(error));        
      });
  }

  private verifyTokenAndAuthorize(token) {
    var data = { token: token };

    return this.http.post(environment.serverEndpoint + "/api/verify-google-token", data)
                    .map(res => res.json());
  }
}
