import { TestBed, inject } from '@angular/core/testing';

import { DuelTimerService } from './duel-timer.service';

describe('DuelTimerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DuelTimerService]
    });
  });

  it('should be created', inject([DuelTimerService], (service: DuelTimerService) => {
    expect(service).toBeTruthy();
  }));
});
