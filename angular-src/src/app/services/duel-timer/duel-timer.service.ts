import { Injectable } from '@angular/core';

@Injectable()
export class DuelTimerService {
  timerInterval: Number = 1000;

  constructor() { }

  timer(endDate) {
    endDate = new Date(endDate).getTime();
    let startDate = new Date().getTime();

    if (isNaN(endDate) || endDate < startDate) {
      return null;
    }

    let timeRemaining = (endDate - startDate) / 1000;

    if (timeRemaining >= 0) {
      var days = timeRemaining / 86400;
      timeRemaining = timeRemaining % 86400;
      
      var hours = timeRemaining / 3600;
      timeRemaining = timeRemaining % 3600;
      
      var minutes = timeRemaining / 60;
      timeRemaining = timeRemaining % 60;
      
      var seconds = timeRemaining;
    }

    return {
      days: Math.floor(days).toString(),
      hours: this.getTimeNumber(Math.floor(hours)).toString(),
      minutes: this.getTimeNumber(Math.floor(minutes)).toString(),
      seconds: this.getTimeNumber(Math.floor(seconds)).toString()
    };
  }

  private getTimeNumber(digit) {
    return digit % 10 !== digit ? digit : "0" + digit;
  }
}