import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import { RequestHeadersProviderService } from '../request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../token-provider/token-provider.service';

@Injectable()
export class CategoryService {

  constructor(
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private tokenProviderService: TokenProviderService
  ) { }

  public pickCategory(category) {
    category.selected = !category.selected;
  }

  public pickOneCategory(category, categories) {
    categories.forEach(c => c.selected = false);
    this.pickCategory(category);
  }

  public collectCategories(categories) {
    var result = [];

    categories.forEach(categoryGroup => {
      var selectedCategories = categoryGroup.filter(category => category.selected) || [];
      selectedCategories.forEach(category => {
        result.push(category.id);
      });
    });

    return result;
  }

  public getCategories() {
    return this.http.get(environment.serverEndpoint + "/category/get-categories")
                    .map(res => res.json());
  }

  public getUserCategories(userId) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/category/get-user-categories",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          userId: userId
        } 
      })
      .map(res => res.json());
  }

  public saveUserCategories(userId, categories) {
    this.tokenProviderService.loadToken();

    var data = {
      userId: userId,
      categories: categories
    };

    return this.http.post(environment.serverEndpoint + "/category/save-user-categories", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }
}
