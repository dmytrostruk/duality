import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import { DuelStatus } from '../../enums/duel-status';
import { OrderDirection } from '../../enums/order-direction';
import { Router } from '@angular/router';
import { RequestHeadersProviderService } from '../../services/request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../../services/token-provider/token-provider.service';
import { AuthService } from '../auth/auth.service';
import 'rxjs/add/operator/map';

@Injectable()
export class VoteService {

  constructor(
    private tokenProviderService: TokenProviderService,
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private router: Router,
    private authService: AuthService
  ) { }

  getVoteInfo(duelId, orderDirection: OrderDirection) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/duel/vote-info",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          duelId: duelId,
          orderDirection: orderDirection,
          currentUserId: this.authService.getCurrentUserId()
        } 
      })
      .map(res => res.json());
  }

  addComment(duelId, text, orderDirection: OrderDirection) {
    this.tokenProviderService.loadToken();

    var data = {
      duelId: duelId,
      currentUserId: this.authService.getCurrentUser().id,
      text: text,
      orderDirection: orderDirection
    };

    return this.http.post(environment.serverEndpoint + "/duel/add-comment", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  deleteComment(commentId, duelId, orderDirection: OrderDirection) {
    this.tokenProviderService.loadToken();

    var data = {
      commentId: commentId,
      duelId: duelId,
      currentUserId: this.authService.getCurrentUser().id,
      orderDirection: orderDirection
    };

    return this.http.post(environment.serverEndpoint + "/duel/delete-comment", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  getComments(duelId, orderDirection: OrderDirection, page: number) {
    this.tokenProviderService.loadToken();

    var data = {
      duelId: duelId,
      orderDirection: orderDirection,
      page: page
    };

    return this.http.post(environment.serverEndpoint + "/duel/get-comments", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  addVote(duelId, selectedUserId, text, orderDirection: OrderDirection) {
    this.tokenProviderService.loadToken();

    var data = {
      duelId: duelId,
      selectedUserId: selectedUserId,
      currentUserId: this.authService.getCurrentUser().id,
      text: text,
      orderDirection: orderDirection
    };

    return this.http.post(environment.serverEndpoint + "/duel/add-vote", data,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  openVotePage(duel) {
    if(duel.status === DuelStatus.Voting || duel.status === DuelStatus.Ended) {
      this.router.navigate(["/duel", duel.numericId]);
    }
  }
}
