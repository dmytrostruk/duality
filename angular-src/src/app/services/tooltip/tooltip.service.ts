import { Injectable } from '@angular/core';

declare var $: any;

@Injectable()
export class TooltipService {

  constructor() { }

  initializeTooltip(container) {
    $(container).off("mouseenter mouseleave");
    $(container).hover((e) => {
      var currentTooltip = $($(e.currentTarget).children()[0]);
      const offset = 25;
      var tooltipWidth = currentTooltip.width();
      if(tooltipWidth > 400) {
        currentTooltip.css("width", 400 + "px");
        currentTooltip.css("white-space", "normal");
        tooltipWidth = currentTooltip.width();
      }
      currentTooltip.css("margin-left", - (tooltipWidth + offset) / 2 + "px");
    });
  }
}
