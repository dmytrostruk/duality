import { Injectable } from '@angular/core';

declare var $: any;

@Injectable()
export class AlertService {

  private readonly alertContainerId: string = "alertContainer";
  private readonly alertWrapperId: string = "alertWrapper";
  
  private currentMessage: string = "";
  private isSuccess: boolean;

  constructor() { }

  success(message: string) {
    this.isSuccess = true;
    this.currentMessage = message;
    this.animateAlert();
  }

  error(message: string) {
    this.isSuccess = false;
    this.currentMessage = message;
    this.animateAlert();
  }

  private animateAlert() {
    $('#' + this.alertContainerId).animate({top: '+=137px'});
    $('#' + this.alertWrapperId).animate({opacity: 1});

    setTimeout(() => {
      $('#' + this.alertContainerId).animate({top: '-=137px'});
      $('#' + this.alertWrapperId).animate({opacity: 0});
    }, 3000);
  }
}
