import { Injectable } from '@angular/core';

declare var $: any;

@Injectable()
export class LoginService {

  private readonly modalId: string = "loginModal";

  constructor() { }

  openModal() {
    this.getModal().modal();
  }

  closeModal() {
    this.getModal().modal('hide');
  }

  private getModal() {
    return $('#' + this.modalId);
  }
}
