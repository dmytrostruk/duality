import { Injectable } from '@angular/core';

@Injectable()
export class PagerService {

  private pageSize: number = 10;
  private pageMaxSize: number = 5;
  private pageMinSize: number = 3;
  private pageStartFlag: number = 7;
  private pageDelimiter: string = "...";

  constructor() { }

  isLastElement(totalItems: number) {
    return totalItems % this.pageSize === 1;
  }

  getLastPage(totalItems: number) {
    return Math.ceil(totalItems / this.pageSize);
  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
    this.pageSize = pageSize;

    var totalPages = Math.ceil(totalItems / this.pageSize),
        startPage = 1,
        result = [];

    if(totalPages <= this.pageStartFlag) {
      return this.getPagingArray(startPage, totalPages, currentPage);
    } else {
      if(currentPage < this.pageMinSize) {
        result = this.getPagingArray(startPage, this.pageMinSize, currentPage);
        result.push(this.getDelimiterObject());
        result.push(this.getPageObject(totalPages, currentPage));

        return result;
      }

      if(currentPage >= this.pageMaxSize - 2 && currentPage < this.pageMaxSize && totalPages > this.pageMaxSize + 2) {
        result = this.getPagingArray(startPage, this.pageMaxSize, currentPage);
        result.push(this.getDelimiterObject());
        result.push(this.getPageObject(totalPages, currentPage));

        return result;
      }

      if(currentPage >= this.pageMinSize && currentPage < this.pageMaxSize - 2) {
        result = this.getPagingArray(startPage, currentPage + 2, currentPage);
        result.push(this.getDelimiterObject());
        result.push(this.getPageObject(totalPages, currentPage));

        return result;
      }

      if(currentPage >= this.pageMaxSize && currentPage < totalPages - this.pageMaxSize + 2) {
        result = this.getPagingArray(currentPage - 1, currentPage + 1, currentPage);

        result.unshift(this.getDelimiterObject());
        result.unshift(this.getPageObject(startPage, currentPage));
        result.push(this.getDelimiterObject());
        result.push(this.getPageObject(totalPages, currentPage));

        return result;
      }

      if(currentPage > totalPages - this.pageMaxSize - 2 && currentPage <= totalPages - this.pageMinSize + 1) {
        result = this.getPagingArray(currentPage - 1, totalPages, currentPage);
        result.unshift(this.getDelimiterObject());
        result.unshift(this.getPageObject(startPage, currentPage));

        return result;
      }
 
      if(currentPage >= totalPages - this.pageMaxSize && currentPage < totalPages - this.pageMinSize + 1) {
        result = this.getPagingArray(totalPages - this.pageMaxSize + 1, totalPages, currentPage);
        result.unshift(this.getDelimiterObject());
        result.unshift(this.getPageObject(startPage, currentPage));

        return result;
      }

      if(currentPage >= totalPages - this.pageMinSize) {
        result = this.getPagingArray(totalPages - this.pageMinSize + 1, totalPages, currentPage);
        result.unshift(this.getDelimiterObject());
        result.unshift(this.getPageObject(startPage, currentPage));

        return result;
      }
    }
  }

  private getPagingArray(startPage, endPage, currentPage) {
    return Array.from(Array((endPage + 1) - startPage).keys()).map(i => {return this.getPageObject(startPage + i, currentPage)});
  }

  private getPageObject(page: number, currentPage: number) {
    return {
      symbol: page,
      isActive: true,
      isCurrentPage: currentPage === page
    };
  }

  private getDelimiterObject() {
    return {
      symbol: this.pageDelimiter,
      isActive: false
    };
  }
}
