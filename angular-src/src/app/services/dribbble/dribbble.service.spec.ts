import { TestBed, inject } from '@angular/core/testing';

import { DribbbleService } from './dribbble.service';

describe('DribbbleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DribbbleService]
    });
  });

  it('should be created', inject([DribbbleService], (service: DribbbleService) => {
    expect(service).toBeTruthy();
  }));
});
