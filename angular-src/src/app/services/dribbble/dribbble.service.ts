import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { EmptyObservable } from 'rxjs/observable/EmptyObservable';
import 'rxjs/add/operator/map';

import { RequestHeadersProviderService } from '../request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../token-provider/token-provider.service';

@Injectable()
export class DribbbleService {

  constructor(
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private tokenProviderService: TokenProviderService
  ) { }

  authorize() {
    var config = environment.dribbble;
    window.location.href = this.getDribbbleAuthorizeUrl(config.clientId, config.stateKey);
  }

  getToken(code, state) {
    this.tokenProviderService.loadToken();

    var config = environment.dribbble;

    if(!code || state !== config.stateKey) {
      return new EmptyObservable();
    }

    var data = {
      clientId: config.clientId,
      clientSecret: config.clientSecret,
      code: code
    };

    return this.http.post(environment.serverEndpoint + "/api/get-dribbble-token", data)
                    .map(res => res.json());
  }

  getUserData(token) {
    return this.http.get(this.getDribbbleUserLink(),
      {
        headers: this.headersProviderService.getProtectedHeaders("Bearer " + token)
      })
      .map(res => res.json());
  }

  private getDribbbleAuthorizeUrl(clientId, state) {
    return `https://dribbble.com/oauth/authorize?client_id=${clientId}&state=${state}`;
  }

  private getDribbbleUserLink() {
    return "https://api.dribbble.com/v2/user";
  }
}
