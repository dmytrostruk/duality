import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { TokenProviderService } from '../../services/token-provider/token-provider.service';
import { Http } from '@angular/http';
import { RequestHeadersProviderService } from '../../services/request-headers-provider/request-headers-provider.service';
import { AuthService } from '../../services/auth/auth.service';

declare var $: any;

@Injectable()
export class ReportService {
  private readonly reportCommentModalId = "#reportCommentModal";

  private selectedCommentId: any = null;

  constructor(
    private tokenProviderService: TokenProviderService,
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private authService: AuthService
  ) { }

  openReportCommentModal(commentId) {
    this.selectedCommentId = commentId;
    $(this.reportCommentModalId).modal();
  }

  closeReportCommentModal() {
    $(this.reportCommentModalId).modal("hide");
  }

  reportComment() {
    if(this.selectedCommentId) {
      this.tokenProviderService.loadToken();

      var data = {
        commentId: this.selectedCommentId,
        currentUserId: this.authService.getCurrentUser().id,
      };

      this.selectedCommentId = null;

      return this.http.post(environment.serverEndpoint + "/duel/report-comment", data,
        {
          headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        })
        .map(res => res.json());
      }
  }
}
