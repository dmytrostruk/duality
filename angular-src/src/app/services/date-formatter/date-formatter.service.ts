import { Injectable } from '@angular/core';

@Injectable()
export class DateFormatterService {

  constructor() { }

  toShortFormat(date) {
    date = this.getDateObject(date);

    return date.day + " " + date.month + ", " + date.year; 
  }

  toShortFormatWithoutYear(date) {
    date = this.getDateObject(date);

    return date.month + " " + date.day; 
  }

  toShortFormatWithDashes(date) {
    date = this.getNumericDateObject(date);

    return date.year + "-" + this.formatDigits(date.month) + "-" + this.formatDigits(date.day);
  }

  toRelativeFormat(date) {
    var now = new Date(),
        dateObject = this.getDateObject(date),
        nowObject = this.getDateObject(now);

    date = new Date(date);

    var differenceInMinutes = this.getDateDifferenceInMinutes(date, now);

    if(differenceInMinutes < 1) {
      return "just now";
    }

    if(differenceInMinutes < 60) {
      return `${differenceInMinutes} ${differenceInMinutes === 1 ? "minute" : "minutes"} ago`;
    } 

    var differenceInHours = this.getDateDifferenceInHours(date, now);

    if(differenceInHours < 24) {
      return `${differenceInHours} ${differenceInHours === 1 ? "hour" : "hours"} ago`;
    }

    if(differenceInHours > 24 && nowObject.year === dateObject.year) {
      return dateObject.day + " " + dateObject.month;
    } else {
      return this.toShortFormat(date);
    }
  }

  getDateDifferenceInDays(firstDate, secondDate) {
    firstDate = new Date(firstDate);
    secondDate = new Date(secondDate);

    const diffDays = Math.ceil(Math.abs(this.getDateDifference(firstDate, secondDate)) / (1000 * 60 * 60 * 24)); 

    return diffDays;
  }

  private getDateDifferenceInHours(firstDate, secondDate) {
    var oneHour = 1000 * 60 * 60;
    return Math.floor(this.getDateDifference(firstDate, secondDate) / oneHour);
  }

  private getDateDifferenceInMinutes(firstDate, secondDate) {
    var oneMinute = 1000 * 60;
    return Math.floor(this.getDateDifference(firstDate, secondDate) / oneMinute);
  }

  private getDateDifference(firstDate, secondDate) {
    return secondDate.getTime() - firstDate.getTime();
  }

  private getDateObject(date) {
    date = new Date(date);
    
    return {
      day: date.getDate(),
      month: date.toLocaleString("en-US", { month: "long" }),
      year: date.getFullYear()
    };
  }

  private getNumericDateObject(date) {
    date = new Date(date);
    
    return {
      day: date.getDate(),
      month: date.getMonth() + 1,
      year: date.getFullYear()
    };
  }

  private formatDigits(digit) {
    return digit < 10 ? "0" + digit : digit;
  }
}
