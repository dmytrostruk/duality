import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import * as io from 'socket.io-client';
import { AuthService } from '../auth/auth.service';
import { AlertService } from '../alert/alert.service';
import { NotificationService } from '../notification/notification.service';

@Injectable()
export class SocketService {

  public onlineUsers: Array<any> = [];
  public usersAmount: number = 0;
  private socket: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private alertService: AlertService,
    private notificationService: NotificationService
  ) { }

  initializeSocket() {
    this.socket = io(environment.serverEndpoint);
    this.initHandlers();
    this.addOnlineUser();
  }

  addOnlineUser() {
    var user = this.authService.getCurrentUser();

    if (user !== null) {
      this.socket.emit('user-connect', user.id);
    }
  }

  removeOnlineUser() {
    var user = this.authService.getCurrentUser();

    if (user !== null) {
      this.socket.emit('user-disconnect', user.id);
    }
  }

  initHandlers() {
    this.socket.on('user-connect', users => {
      this.onlineUsers = users;
    });

    this.socket.on('disconnect', users => {
      this.onlineUsers = users;
    });

    this.socket.on('user-disconnect', users => {
      this.onlineUsers = users;
    });

    this.socket.on('account-created', usersAmount => {
      this.usersAmount = usersAmount;
    });

    this.socket.on('new-notification', userId => {
      var user = this.authService.getCurrentUser();
      if (user && user.id === userId) {
        this.notificationService.getNotifications(1);
      }
    });

    this.socket.on('user-banned', userId => {
      var user = this.authService.getCurrentUser();
      if (user && user.id === userId) {
        this.alertService.error("Unfortunately, you are banned. Please, contact administrators!")
        setTimeout(() => {
          this.authService.logout();
          window.location.reload();
        }, 2000);
      }
    });
  }
}
