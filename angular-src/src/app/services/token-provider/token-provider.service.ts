import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class TokenProviderService {
  authToken = null;

  private tokenKey = "id_token_duelity";

  constructor() { }
  
  loadToken() {
    this.authToken = this.getToken();
  }

  getToken() {
    return localStorage.getItem(this.tokenKey);
  }

  saveToken(token) {
    return localStorage.setItem(this.tokenKey, token);
  }

  isTokenValid() {
    return tokenNotExpired(this.tokenKey);
  }
}
