import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/map';

import { RequestHeadersProviderService } from '../request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../token-provider/token-provider.service';

@Injectable()
export class AuthService {
  user: any = null;

  constructor(
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private tokenProviderService: TokenProviderService
  ) { }

  registerUser(user) {
    return this.http.post(environment.serverEndpoint + "/api/register", user, 
      { headers: this.headersProviderService.getJSONHeaders()})
      .map(res => res.json());
  }

  authenticateUser(user) {
    return this.http.post(environment.serverEndpoint + "/api/authenticate", user, 
      { headers: this.headersProviderService.getJSONHeaders()})
      .map(res => res.json());
  }

  loggedIn() {
    return this.tokenProviderService.isTokenValid();
  }

  getUserProfile() {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/api/profile", 
      { headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken) })
      .map(res => res.json());
  }

  checkUserEmail(email) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/api/check-user-email", 
      { 
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: {
          email: email
        }
      })
      .map(res => res.json());
  }

  checkUserPassword(password) {
    this.tokenProviderService.loadToken();

    var data = {
      currentUserId: this.getCurrentUser().id,
      password: password
    };

    return this.http.post(environment.serverEndpoint + "/user/check-password", data, 
      { headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken)})
      .map(res => res.json());
  }

  logout() {
    this.tokenProviderService.authToken = null;
    this.user = null;

    localStorage.clear();
  }

  storeUserData(token, user) {
    this.tokenProviderService.saveToken(token);
    this.tokenProviderService.authToken = token;
    this.updateUserData(user);
  }

  updateUserData(user) {
    localStorage.setItem("user", JSON.stringify(user));
    this.user = user;
  }

  getCurrentUser() {
    return this.user || JSON.parse(localStorage.getItem("user"));
  }

  getCurrentUserId() {
    var currentUser = this.getCurrentUser();
    return currentUser ? currentUser.id : null;
  }
}
