import { TestBed, inject } from '@angular/core/testing';

import { StartDuelService } from './start-duel.service';

describe('StartDuelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StartDuelService]
    });
  });

  it('should be created', inject([StartDuelService], (service: StartDuelService) => {
    expect(service).toBeTruthy();
  }));
});
