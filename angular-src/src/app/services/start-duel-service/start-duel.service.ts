import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import { RequestHeadersProviderService } from '../../services/request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../../services/token-provider/token-provider.service';
import { AuthService } from '../../services/auth/auth.service';
import 'rxjs/add/operator/map';

@Injectable()
export class StartDuelService {

  selectedUser: any;

  constructor(
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private tokenProviderService: TokenProviderService,
    private authService: AuthService
  ) { }

  getStartDuelInfo() {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/start-duel/get-info",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          username: this.selectedUser && this.selectedUser.username,
          currentUsername: this.authService.getCurrentUser().username
        } 
      })
      .map(res => res.json());
  }

  getOpponents(search) {
    this.tokenProviderService.loadToken();

    return this.http.get(environment.serverEndpoint + "/start-duel/get-opponents",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          currentUsername: this.authService.getCurrentUser().username,
          search: search
        } 
      })
      .map(res => res.json());
  }

  startDuel(rightUser, duelCategory, duelTask, deadline) {
    this.tokenProviderService.loadToken();

    var date = new Date();
    var endDate = date.setDate(date.getDate() + 1);
    //var endDate = date.setSeconds(date.getSeconds() + 30);

    return this.http.get(environment.serverEndpoint + "/start-duel/create-duel",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          leftUser: this.authService.getCurrentUser().id,
          rightUser: rightUser,
          duelCategory: duelCategory,
          duelTask: duelTask,
          deadline: deadline,
          endDate: endDate  
        }
      })
      .map(res => res.json());
  }
}
