import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { RequestHeadersProviderService } from '../../services/request-headers-provider/request-headers-provider.service';
import { TokenProviderService } from '../../services/token-provider/token-provider.service';
import { AuthService } from '../auth/auth.service';
import { NotificationType } from '../../enums/notification-type';
import 'rxjs/add/operator/map';
import { DuelService } from '../duel-service/duel.service';
import { PagerService } from '../pager/pager.service';

declare var $: any;

@Injectable()
export class NotificationService {

  public notificationIcons: any = {
    [NotificationType.CreatedDuel]: "created-duel",
    [NotificationType.AcceptedDuel]: "accepted-duel",
    [NotificationType.DeclinedDuel]: "declined-duel",
    [NotificationType.OpponentUploadedWork]: "uploaded-work",
    [NotificationType.NewVote]: "new-vote",
    [NotificationType.WonDuel]: "won-duel",
    [NotificationType.LostDuel]: "lost-duel",
    [NotificationType.DrawDuel]: "draw-duel",
    [NotificationType.NewFollow]: "new-follow",
    [NotificationType.FeaturedDuel]: "featured-duel",
    [NotificationType.VotedDuelWon]: "new-vote",
    [NotificationType.VotedDuelLost]: "new-vote",
    [NotificationType.PopularDuel]: "popular-duel",
    [NotificationType.EndedSubscription]: "ended-subscription",
    [NotificationType.StartedVoting]: "started-voting"
  }

  public notifications: Array<any> = [];
  public headerNotifications: Array<any> = [];
  public pages: Array<any> = [];
  public headerNotificationsAmount: number = 0;
  public isHeaderOpened: boolean = false;

  private readonly headerPageSize: number = 4;
  private readonly pageSize: number = 20;
  private readonly notificationsRoute = "notifications";

  constructor(
    private tokenProviderService: TokenProviderService,
    private http: Http,
    private headersProviderService: RequestHeadersProviderService,
    private router: Router,
    private authService: AuthService,
    private duelService: DuelService,
    private pager: PagerService
  ) { }

  getNotifications(page, isActive = true, isCurrentPage = false) {
    if (isActive && !isCurrentPage) {
      this.getNotificationsRequest(page).subscribe(response => {
        if (response.success) {
          this.notifications = response.notifications;
          this.headerNotifications = this.notifications.slice(0, this.headerPageSize);
          this.headerNotificationsAmount = response.newNotificationCount;
          this.initializeNotifications(this.goToPage.bind(this));
          this.pages = this.pager.getPager(response.totalCount, page, this.pageSize);

          var unreadNotificationIds = this.notifications.filter(notification => notification.isNewNotification)
            .map(notification => notification.id);

          if(this.shouldMarkAsRead()) {
            this.markNotificationsAsRead(unreadNotificationIds);
          }
        } else {
          console.log(response.error);
        }
      });
    };
  }

  markNotificationsAsRead(notificationIds) {
    this.markNotificationsAsReadRequest(notificationIds).subscribe(response => {
      if (response.success) {
        setTimeout(() => {
          this.headerNotifications.filter(notification => notificationIds.indexOf(notification.id) !== -1).map(notification => {
            notification.isNewNotification = false;
          });

          this.notifications.filter(notification => notificationIds.indexOf(notification.id) !== -1).map(notification => {
            notification.isNewNotification = false;
          });

          this.headerNotificationsAmount = response.newNotificationCount;
          this.initializeNotifications(this.goToPage.bind(this));
        }, 1000);
      } else {
        console.log("Something went wrong during marking notifications as read");
      }
    });
  }

  //#region API Requests

  getNotificationsRequest(page: number, pageSize: number = 20) {
    this.tokenProviderService.loadToken();

    var data = {
      userId: this.authService.getCurrentUser().id
    };

    return this.http.get(environment.serverEndpoint + "/notification/get-notifications",
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
        params: { 
          userId: this.authService.getCurrentUser().id,
          page: page,
          pageSize: pageSize
        } 
      })
      .map(res => res.json());
  }

  markNotificationsAsReadRequest(notificationIds) {
    this.tokenProviderService.loadToken();

    var notificationInfo = {
      notificationIds: notificationIds,
      currentUserId: this.authService.getCurrentUser().id
    };

    return this.http.post(environment.serverEndpoint + "/notification/mark-as-read", notificationInfo,
      {
        headers: this.headersProviderService.getJSONProtectedHeaders(this.tokenProviderService.authToken),
      })
      .map(res => res.json());
  }

  //#endregion

  initializeNotifications(goToPage) {
    setTimeout(() => {
      $(".notification-container .notification-text-block span").click((e) => {
        var link = e.target;

        goToPage($(link).data("page"), $(link).data("identifier"))
      });
    });
  }

  private goToPage(page, parameter) {
    if (this.duelService.getDuelPage() === page) {
      this.duelService.goToDuel(parameter);
    } else {
      this.router.navigate([page, parameter]);
    }
  }

  private shouldMarkAsRead() : boolean {
    return this.router.url.includes(this.notificationsRoute) || this.isHeaderOpened;
  }
}
