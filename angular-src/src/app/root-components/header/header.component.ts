import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { SocketService } from '../../services/socket/socket.service';
import { AlertService } from '../../services/alert/alert.service';
import { RegisterService } from '../../components/register/services/register.service';
import { LoginService } from '../../services/login/login.service';
import { ValidationService } from '../../services/validation/validation.service';
import { StartDuelService } from '../../services/start-duel-service/start-duel.service';
import { HeaderService } from './services/header.service';
import { NotificationService } from '../../services/notification/notification.service';
import { DateFormatterService } from '../../services/date-formatter/date-formatter.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private readonly searchModalId: string = "searchModal";
  private readonly searchUrl: string = "search";
  private readonly notificationsBlockId: string = "notificationsBlock";

  searchQuery: string = "";

  constructor(
    private router: Router,
    private authService: AuthService,
    private socketService: SocketService,
    private alertService: AlertService,
    public registerService: RegisterService,
    public loginService: LoginService,
    private validationService: ValidationService,
    private headerService: HeaderService,
    private notificationService: NotificationService,
    private dateFormatter: DateFormatterService
  ) { }

  ngOnInit() {
    if(this.authService.loggedIn()) {
      this.notificationService.getNotifications(1);
      setTimeout(() => {this.initializeNotificationsBlock();});
    }
  }

  goToPerson() {
    var username = this.authService.getCurrentUser().username;
    this.router.navigate(["/user", username]);
  }

  logout() {
    this.socketService.removeOnlineUser();
    this.authService.logout();
    this.alertService.success("Signed out. See you soon!");
    window.location.reload();
  }

  openSearchModal() {
    if (this.router.url.indexOf(this.searchUrl) === -1) {
      this.searchQuery = "";
      $('#' + this.searchModalId).modal();
      setTimeout(() => {
        $(`#${this.searchModalId} input`).focus();
      }, 300);
    } else {
      $('#searchMainInput').focus();
    }
  }

  makeSearch() {
    if (this.validationService.isStringValid(this.searchQuery)) {
      this.router.navigate(["/search", this.searchQuery]);
      $('#' + this.searchModalId).modal('hide');
    }
  }
  
  getDuelsLeft() {
    var user = this.authService.getCurrentUser();
    return user.isPaidAccount ? '∞' : user.duelsLeft;
  }

  private initializeNotificationsBlock() {
    $('#' + this.notificationsBlockId)
    .on('mouseenter', () => {
      this.notificationService.isHeaderOpened = true;

      var unreadNotificationIds = this.notificationService.headerNotifications.filter(notification => notification.isNewNotification)
            .map(notification => notification.id);

      this.notificationService.markNotificationsAsRead(unreadNotificationIds);
    })
    .on('mouseleave', () => {
      this.notificationService.isHeaderOpened = false;
    });
  }
}
