import { Injectable } from '@angular/core';
import { NotificationService } from '../../../services/notification/notification.service';
import { SocketService } from '../../../services/socket/socket.service';
import { DuelService } from '../../../services/duel-service/duel.service';
import { Router } from '@angular/router';

@Injectable()
export class HeaderService {

  constructor(
    private notificationService: NotificationService,
    private duelService: DuelService,
    private router: Router
  ) { }
}
