export enum DuelStatus {
    Waiting = 0,
    InProgress,
    Voting,
    Ended
}