export enum SettingsType {
    General = 0,
    DuelCategories,
    Notifications,
    Billing
}