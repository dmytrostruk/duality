export enum InputType {
    Text = 0,
    Password = 1,
    Textarea = 2
}