export enum NotificationType {
    CreatedDuel = 0,
    AcceptedDuel,
    DeclinedDuel,
    OpponentUploadedWork,
    NewVote,
    WonDuel,
    LostDuel,
    DrawDuel,
    NewFollow,
    FeaturedDuel,
    VotedDuelWon,
    VotedDuelLost,
    PopularDuel,
    EndedSubscription,
    StartedVoting
}