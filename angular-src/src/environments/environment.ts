// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  serverEndpoint: "http://localhost:8080",
  cloudinary: {
    cloud_name: "duelity",
    upload_preset: 'nriiui3e'
  },
  dribbble: {
    clientId: "c92a382355cc8d5394c5cb603d1ecf81918677515181288bdeaa41ebb36de2c6",
    clientSecret: "e386d8f2acb407c111c6a5877b2e99a4e14bf37f67e141b6103b8cc761c91c22",
    stateKey: "7fa93bdd21af0d7fddcb4752f3cbf363"
  },
  google: {
    clientId: '841095366646-3eueb06gd85kdl1kcgn6t68dj4j18mdr.apps.googleusercontent.com'
  }
};
