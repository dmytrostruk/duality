const express = require('express');
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const config = require("../config/database");

const User = require("../models/user");
const Duel = require("../models/duel");
const Report = require('../models/report');

const SocketService = require("../services/socket-service");

/*
To protect a route,
you should put as a second parameter the next string:
passport.authenticate("jwt", {session: false})
*/

// Authenticate
router.post("/authenticate", (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    // Users can login either by username or email
    User.getAdmins({username: username, email: username})
        .then((user) => {
            if(!user || user.isBanned) {
                return res.json({success: false, message: "Access is prohibited"});
            }

            User.comparePassword(password, user.password, (error, isMatch) => {
                if(error) {
                    throw error;
                }

                if(isMatch) {
                    var jwtUser = {
                        id: user._id,
                        fullname: user.fullname,
                        username: user.username,
                        email: user.email,
                        password: user.password
                    };

                    const token = jwt.sign(jwtUser, config.secret, {
                        expiresIn: 604800 // 1 week
                    });

                    res.json({
                        success: true,
                        token: "JWT " + token
                    });
                } else {
                    res.json({success: false, message: "Wrong password!"});
                }
            });
        })
        .catch((error) => {
            throw error;
        });
});

// Get Users Amount
router.get("/users-amount", async (req, res, next) => {
    try {
        res.json({success: true, data: await User.getUsersAmount()});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: "Something went wrong during fetching overview data"});
    }
}); 

// Get overview data
router.get("/overview-data", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    try {
        var data = {
            duelsAmount: await Duel.getDuelsAmount(),
            paidAccountsAmount: await User.getPaidAccountsAmount(),
            usersAmount: await User.getUsersAmount()
        };

        res.json({success: true, data: data});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: "Something went wrong during fetching overview data"});
    }
}); 

// Get users data
router.get("/users-data", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    try {
        var searchString = req.query.searchString,
            orderDirection = req.query.orderDirection,
            users = await User.getUsersByUsernameOrFullName({searchString: searchString, orderDirection: parseInt(orderDirection)});

        res.json({success: true, data: users.map(User.mapToAdminModel)});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: "Something went wrong during fetching users data"});
    }
}); 

// Change user ban status
router.post("/change-user-ban-status", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    try {
        var userId = req.body.userId,
            searchString = req.body.searchString,
            user = await User.findById(userId);

        if(!user) throw "User not found";

        user.isBanned = !user.isBanned;
        await user.save();

        if(user.isBanned) {
            SocketService.onUserBan(userId);
        }

        var users = await User.getUsersByUsernameOrFullName({searchString: searchString});

        res.json({success: true, data: users.map(User.mapToAdminModel)});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: "Something went wrong during changing user ban status"});
    }
}); 

// Get reports
router.get("/reports", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    try {
        var reportType = req.query.reportEntityType,
            reports = await Report.getReports(reportType);

        res.json({success: true, reports: reports});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: "Something went wrong during fetching reports"});
    }
}); 

module.exports = router;