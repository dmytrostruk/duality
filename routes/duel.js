const express = require('express');
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const path = require("path");
const fs = require("fs");

const DuelJob = require("../jobs/duel-job");
const DuelStatus = require("../enums/duel-status");
const NotificationType = require("../enums/notification-type");
const DuelType = require("../enums/duel-type");
const ReportEntityType = require("../enums/report-entity-type");

const User = require("../models/user");
const Duel = require("../models/duel");
const Notification = require("../models/notification");
const Comment = require("../models/comment");
const Report = require("../models/report");

const ImageService = require("../services/image-service");
const SocketService = require("../services/socket-service");

const RegexHelper = require("../helpers/regex-helper");

/*
To protect a route,
you should put as a second parameter the next string:
passport.authenticate("jwt", {session: false})
*/

// Remove duel
router.post("/remove", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var duelId = req.body.duelId,
        currentUserId = req.body.currentUserId;

    try {
        var foundedDuel = await Duel.findById(duelId);
        
        await Duel.removeDuel(duelId);

        var users = await User.getSkillRating(),
            data = await Duel.getDuelsByUserId(currentUserId)

        // Sorting duels from newest to oldest
        data.sort((a, b) => {
            return new Date(b.endDate) - new Date(a.endDate);
        });

        var data = data.map(duel => {
            duel.leftUser.rating = users.indexOf(users.filter((user) => user._id == duel.leftUser.userId)[0]) + 1;
            duel.rightUser.rating = users.indexOf(users.filter((user) => user._id == duel.rightUser.userId)[0]) + 1;

            return Duel.mapToSafeModel(duel, currentUserId);
        });

        var currentUser = await User.findById(currentUserId);

        var notificationParams = {
            userId: foundedDuel.leftUser.userId,
            type: NotificationType.DeclinedDuel,
            textParams: {
                fullname: currentUser.fullname,
                username: currentUser.username
            },
            associatedUser: currentUser.id
        };

        Notification.pushNotification(notificationParams);

        res.json({success: true, duels: data}); 

    } catch (err) {
        console.log(err);
        res.json({success: false, error: error});
    }
});

// Accept duel
router.post("/accept", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var duelId = req.body.duelId,
        currentUserId = req.body.currentUserId;
    
    try {
        var foundedDuel = await Duel.findById(duelId);

        foundedDuel.status = DuelStatus.InProgress;
        foundedDuel.endDate = foundedDuel.deadline;
        foundedDuel.save();

        DuelJob.inProgressDuelJob(foundedDuel);

        var users = await User.getSkillRating(),
            data = await Duel.getDuelsByUserId(currentUserId);

        // Sorting duels from newest to oldest
        data.sort((a, b) => {
            return new Date(b.endDate) - new Date(a.endDate);
        });

        var data = data.map(duel => {
            duel.leftUser.rating = users.indexOf(users.filter((user) => user._id == duel.leftUser.userId)[0]) + 1;
            duel.rightUser.rating = users.indexOf(users.filter((user) => user._id == duel.rightUser.userId)[0]) + 1;

            return Duel.mapToSafeModel(duel, currentUserId);
        });

        var currentUser = await User.findById(currentUserId);

        var notificationParams = {
            userId: foundedDuel.leftUser.userId,
            type: NotificationType.AcceptedDuel,
            textParams: {
                fullname: currentUser.fullname,
                username: currentUser.username,
                duelId: duelId
            },
            associatedUser: currentUser.id
        };

        Notification.pushNotification(notificationParams);

        var duelsAmount = await Duel.getDuelsAmount();
        SocketService.onDuelCreated(duelsAmount);

        res.json({success: true, duels: data}); 
        
    } catch (err) {
        console.log(err);
        res.json({success: false, error: err});
    }
});

// Change duel visibility
router.post("/change-visibility", passport.authenticate("jwt", {session: false}), async (req, res, next) => { 
    var currentUserId = req.body.currentUserId,
        duelId = req.body.duelId,
        hide = req.body.hide;

    try {
        var foundedDuel = await Duel.findById(duelId);

        var getCurrentUser = duel => duel.leftUser.userId === currentUserId ? duel.leftUser 
                                     : duel.rightUser.userId === currentUserId ? duel.rightUser : null;

        var duelUser = getCurrentUser(foundedDuel);

        if(!duelUser) {
            throw "Incorrect user!";
        }

        duelUser.isHidden = hide;
        await foundedDuel.save();

        var users = await User.getSkillRating(),
            data = await Duel.getDuelsByUserId(currentUserId),
            statuses = [DuelStatus.Voting, DuelStatus.Ended];

        // Filtering by status
        if(statuses.length > 0) {
            data = data.filter(duel => statuses.indexOf(duel.status) !== -1);
        }

        // Sorting duels from newest to oldest
        data.sort((a, b) => {
            return new Date(b.deadline) - new Date(a.deadline);
        });

        var data = data.map(duel => {
            duel.leftUser.rating = users.indexOf(users.filter((user) => user._id == duel.leftUser.userId)[0]) + 1;
            duel.rightUser.rating = users.indexOf(users.filter((user) => user._id == duel.rightUser.userId)[0]) + 1;

            return Duel.mapToSafeModel(duel, currentUserId);
        });

        res.json({success: true, duels: data}); 
    } catch (err) {
        console.log(err);
        res.json({success: false, error: err});
    }
});

// Edit work
router.get("/work", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    try {
        if(!RegexHelper.isOnlyNumbers(req.query.duelId)) {
            res.json({success: true, message: "Duel not found.", duel: null});
            return;
        }

        var duelId = parseInt(req.query.duelId),
            userId = req.query.userId;

        var duel = await Duel.getDuelByNumericId(duelId);

        if(duel === null || duel === undefined) {
            res.json({success: true, message: "Duel not found.", duel: null});
            return;
        }

        if(duel.leftUser.userId !== userId && duel.rightUser.userId !== userId) {
            res.json({success: true, message: "You don't have rights to edit this work.", duel: null});
            return;
        }

        res.json({success: true, duel: Duel.mapToEditModel(duel)});
    } catch (err) {
        console.log(err);
        res.json({success: false, error: "Something went wrong during fetching work info"});
    }
}); 

// Upload work
router.post("/upload", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var duelId = req.body.duelId,
        userId = req.body.userId,
        workDescription = req.body.workDescription,
        imageUrls = req.body.imageUrls,
        imagesForDelete = req.body.imagesForDelete,
        previewImage = req.body.previewImage;

    var getCurrentUser = (duel) => {
        return duel.leftUser.userId === userId ? duel.leftUser : duel.rightUser;
    }

    var getOpponentUser = (duel) => {
        return duel.leftUser.userId !== userId ? duel.leftUser : duel.rightUser;
    }

    try {
        var duel = await Duel.getDuelById(duelId);

        if(duel !== null) {
            var currentUser = getCurrentUser(duel);

            currentUser.workDescription = workDescription;
            currentUser.imageUrls = currentUser.imageUrls.filter(image => imagesForDelete.indexOf(image.publicId) === -1);

            if(currentUser.previewImageUrl && currentUser.previewImageUrl.publicId !== previewImage.publicId) {
                imagesForDelete.push(currentUser.previewImageUrl.publicId);
            }

            currentUser.previewImageUrl = previewImage;

            var opponentUser = getOpponentUser(duel),
                notificationParams = {
                    userId: opponentUser.user.id,
                    textParams: {
                        fullname: currentUser.user.fullname,
                        username: currentUser.user.username,
                        duelId: duelId
                    },
                    associatedUser: currentUser.user.id
                };

            if(opponentUser.previewImageUrl && opponentUser.previewImageUrl.url) {
                var date = new Date();

                duel.status = DuelStatus.Voting;
                //duel.endDate = date.setDate(date.getDate()+ 1);
                duel.endDate = date.setSeconds(date.getSeconds() + 600);
                duel.deadline = date;

                DuelJob.votingDuelJob(duel);

                notificationParams.type = NotificationType.StartedVoting;
            } else {
                notificationParams.type = NotificationType.OpponentUploadedWork;
            }

            Notification.pushNotification(notificationParams);

            ImageService.deleteImagesFromStorage(imagesForDelete)
            .then(() => {
                for(let i = 0; i < imageUrls.length; i++) {
                    currentUser.imageUrls.push(imageUrls[i]);
                }
    
                duel.save();

                res.json({success: true});    
            }).catch(error => {
                res.json({success: false, error: error});
            });    
        } else {
            res.json({success: false, message: "Something goes wrong during work uploading..."});
        }
    } catch (error) {
        res.json({success: false, error: error});
    }
});

// Delete temporary image
router.post("/delete-temp-images", passport.authenticate("jwt", {session: false}), (req, res, next) => {
    var imagesForDelete = req.body.imagesForDelete;

    ImageService.deleteImagesFromStorage(imagesForDelete)
    .then(() => {
        res.json({success: true});    
    }).catch(error => {
        res.json({success: false, error: error});
    });   
});

// Vote page
router.get("/vote-info", async (req, res, next) => {
    try {
        if(!RegexHelper.isOnlyNumbers(req.query.duelId)) {
            res.json({success: true, message: "Duel not found.", duel: null});
            return;
        }

        var duelId = parseInt(req.query.duelId),
            orderDirection = req.query.orderDirection,
            currentUserId = req.query.currentUserId;

        var duel = await Duel.getDuelByNumericId(duelId);

        if(duel === null || duel === undefined) {
            res.json({success: true, message: "Duel not found.", duel: null});
            return;
        }

        var users = await User.getSkillRating(),
            commentsData = await Comment.getDuelComments({duelId: duel._id, orderDirection: parseInt(orderDirection)});

        duel.leftUser.rating = users.indexOf(users.filter((user) => user._id == duel.leftUser.userId)[0]) + 1;
        duel.rightUser.rating = users.indexOf(users.filter((user) => user._id == duel.rightUser.userId)[0]) + 1;

        res.json({
            success: true, 
            duel: Duel.mapToVoteModel(duel, currentUserId), 
            commentsData: commentsData
        });
    } catch (err) {
        console.log(err);
        res.json({success: false, error: "Error during fetching vote info."});
    }
});

// Add Comment
router.post("/add-comment", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var duelId = req.body.duelId,
        currentUserId = req.body.currentUserId,
        text = req.body.text,
        timestamp = new Date(),
        orderDirection = req.body.orderDirection;

    var comment = new Comment({ duelId: duelId, user: currentUserId, text: text, timestamp: timestamp });
    
    await comment.save();

    res.json({success: true});
});

// Delete Comment
router.post("/delete-comment", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var duelId = req.body.duelId,
        orderDirection = req.body.orderDirection,
        commentId = req.body.commentId,
        currentUserId = req.body.currentUserId;

    var comment = await Comment.findById(commentId);

    if(comment && comment.user != currentUserId) {
        res.json({success: false, message: "You cannot delete not yours comment."});
        return;
    }
    
    await Comment.removeComment(commentId);

    res.json({success: true});
});

// Report Comment
router.post("/report-comment", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    try {
        var userId = req.body.currentUserId,
        commentId = req.body.commentId;

        var report = new Report({ 
            entityId: commentId, 
            entityType: ReportEntityType.Comment, 
            timestamp: new Date(),
            reporter: userId,
            isResolved: false
        });

        await report.save();

        res.json({success: true});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: "Something goes wrong during reporting a comment"});
    }
});

// Get Paged Comments
router.post("/get-comments", async (req, res, next) => {
    var duelId = req.body.duelId,
        orderDirection = req.body.orderDirection,
        page = req.body.page;

    var commentsData = await Comment.getDuelComments({duelId: duelId, orderDirection: orderDirection, page: page});

    res.json({success: true, commentsData: commentsData});
});

// Add Vote
router.post("/add-vote", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var duelId = req.body.duelId,
        currentUserId = req.body.currentUserId,
        selectedUserId = req.body.selectedUserId,
        text = req.body.text,
        timestamp = new Date(),
        orderDirection = req.body.orderDirection;

    var comment = new Comment({ duelId: duelId, user: currentUserId, text: text, timestamp: timestamp });
    await comment.save();

    var duel = await Duel.getDuelById(duelId),
        users = await User.getSkillRating(),
        selectedUser = duel.leftUser.user.id === selectedUserId ? duel.leftUser : duel.rightUser,
        currentUser = await User.findById(currentUserId),
        opponentUserId = duel.leftUser.user.id !== selectedUserId ? duel.leftUser.user.id : duel.rightUser.user.id,
        opponentUser = await User.findById(opponentUserId);

    selectedUser.votes = selectedUser.votes || [];
    selectedUser.votes.push(currentUserId);

    selectedUser.votesAmount = selectedUser.votes.length;
    selectedUser.lastVotesAmount = selectedUser.votesAmount;

    await duel.save();

    var notificationParams = {
        userId: selectedUserId,
        type: NotificationType.NewVote,
        textParams: {
            fullname: currentUser.fullname,
            username: currentUser.username,
            duelId: duelId,
            opponentFullname: opponentUser.fullname,
            opponentUsername: opponentUser.username
        },
        associatedUser: currentUser.id
    };

    Notification.pushNotification(notificationParams);

    var commentsData = await Comment.getDuelComments({duelId: duelId, orderDirection: orderDirection});

    duel.leftUser.rating = users.indexOf(users.filter((user) => user._id == duel.leftUser.userId)[0]) + 1;
    duel.rightUser.rating = users.indexOf(users.filter((user) => user._id == duel.rightUser.userId)[0]) + 1;

    res.json({
        success: true, 
        duel: Duel.mapToVoteModel(duel, currentUserId),
        commentsData: commentsData
    });
});

// Get duel status
router.get("/get-status", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var duelId = req.query.duelId;

    try {
        var duel = await Duel.getDuelById(duelId);

        if(duel === null) {
            res.json({success: false, error: "Duel was not found"});
            return;
        }
    
        res.json({success: true, status: duel.status});
    } catch (error) {
        res.json({success: false, error: error});
    }
});

// Get duels by type
router.get("/get-duels-by-type", async (req, res, next) => {
    var duelType = req.query.duelType,
        currentUserId = req.query.currentUserId,
        page = parseInt(req.query.page),
        pageSize = parseInt(req.query.pageSize),
        isPopularDuels = false;

    try {
        var duels = [];
        var totalAmount = 0;

        switch(parseInt(duelType)) {
            case parseInt(DuelType.Following) :
                duels = await Duel.getFollowingDuels({currentUserId: currentUserId, page: page, pageSize: pageSize});
                totalAmount = await Duel.getFollowingDuelsCount(currentUserId);

                if(totalAmount === 0) {
                    duels = await Duel.getPopularDuels({page: page, pageSize: pageSize});
                    totalAmount = await Duel.getPopularDuelsCount();
                    isPopularDuels = true;
                }

                break;
            case parseInt(DuelType.New) :
                duels = await Duel.getNewDuels({page: page, pageSize: pageSize});
                totalAmount = await Duel.getNewDuelsCount();
                break;
            case parseInt(DuelType.Popular) :
                duels = await Duel.getPopularDuels({page: page, pageSize: pageSize});
                totalAmount = await Duel.getPopularDuelsCount();
                break;
            case parseInt(DuelType.Featured) :
                duels = await Duel.getFeaturedDuels({page: page, pageSize: pageSize});
                totalAmount = await Duel.getFeaturedDuelsCount();
                break;
        }
    
        var users = await User.getSkillRating();

        // Sorting duels from newest to oldest
        duels.sort((a, b) => {
            return new Date(b.endDate) - new Date(a.endDate);
        });

        duels = duels.map(duel => {
            duel.leftUser.rating = users.indexOf(users.filter((user) => user._id == duel.leftUser.userId)[0]) + 1;
            duel.rightUser.rating = users.indexOf(users.filter((user) => user._id == duel.rightUser.userId)[0]) + 1;

            return Duel.mapToSafeModel(duel, currentUserId);
        });

        res.json({success: true, duels: duels, totalAmount: totalAmount, isPopularDuels: isPopularDuels});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: error});
    }
});

module.exports = router;    