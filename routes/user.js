const express = require('express');
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const config = require("../config/database");

const User = require("../models/user");
const UserFollower = require("../models/userfollower");
const Duel = require('../models/duel');
const Notification = require("../models/notification");
const NotificationType = require("../enums/notification-type");
const DuelStatus = require("../enums/duel-status");

const ImageService = require("../services/image-service");

/*
To protect a route,
you should put as a second parameter the next string:
passport.authenticate("jwt", {session: false})
*/

// User Profile
router.get("/profile", async (req, res, next) => {
    var username = req.query.username,
        currentUsername = req.query.currentUsername;

    var requestedUser = await User.getUserByUsernameOrEmail({username: username, email: null});

    if(requestedUser) {
        var currentUser = await User.getUserByUsernameOrEmail({username: currentUsername, email: null}),
            canStartDuel = currentUser ? await User.canStartDuel(currentUser, requestedUser) : false;
        
        requestedUser.rating = await User.getSkillRatingByUserId(requestedUser._id);
        var user = User.mapToSafeModel(requestedUser);
        user.duelsWon = await User.getDuelsWonAmount(user.id);

        var data = {
            user: user,
            canStartDuel: canStartDuel
        };

        res.json({success: true, data: data});
    } else {
        res.json({success: false, message: "User not found"});
    }
});

// Account Settings
router.get("/account-settings", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var currentUserId = req.query.currentUserId;

    try {
        var user = await User.findById(currentUserId);

        if(user) {
            res.json({success: true, data: User.mapToSettingsModel(user)});
        } else {
            res.json({success: false, error: "User not found."});
        }

    } catch (error) {
        console.log(error);
        res.json({success: false, error: error});
    }
});

// Save account settings
router.post("/save-account-settings", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var userId = req.body.id,
        settings = {
        fullname: req.body.fullname,
        description: req.body.description,
        username: req.body.username,
        email: req.body.email,
        behance: req.body.behance,
        dribbble: req.body.dribbble,
        twitter: req.body.twitter,
        instagram: req.body.instagram,
        password: req.body.newPassword,
        imageUrl: req.body.imageUrl,
        imageColor: req.body.imageColor
    };

    try {
        var userWithUsername = await User.getUserByUsernameOrEmail({username: settings.username, email: settings.email});

        if(userWithUsername === null || userWithUsername._id.toString() === userId) {
            var user = await User.saveUserSettings(userId, settings);

            res.json({success: true, data: User.mapToSettingsModel(user), userData: User.mapToSafeModel(user)});
        } else {
            var errorMessage = userWithUsername.username === settings.username ? 
                               `User ${settings.username} already exists` : 
                               `User with email ${settings.email} already exists`;

            res.json({success: false, error: errorMessage});
        }
    } catch (error) {
        console.log(error);
        res.json({success: false, error: error});
    }
});

// Save Avatar
router.post("/save-avatar", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var currentUserId = req.body.currentUserId,
        imageUrl = req.body.imageUrl,
        imagePublicId = req.body.imagePublicId,
        imageColor = req.body.imageColor;

    try {
        var user = await User.findById(currentUserId);

        if(user) {

            if(user.imagePublicId) {
                await ImageService.deleteImagesFromStorage([user.imagePublicId]);
            }

            user.imageUrl = imageUrl;
            user.imagePublicId = imagePublicId;
            user.imageColor = imageColor;

            user.save();

            res.json({success: true, data: User.mapToSettingsModel(user), userData: User.mapToSafeModel(user)});
        } else {
            res.json({success: false, error: "User not found."});
        }

    } catch (error) {
        console.log(error);
        res.json({success: false, error: error});
    }
});

// Check password
router.post("/check-password", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var currentUserId = req.body.currentUserId,
        password = req.body.password;

    try {
        var user = await User.findById(currentUserId);

        User.comparePassword(password, user.password, (error, isMatch) => {
            if(error) {
                throw error;
            }

            if(isMatch) {
                res.json({success: true});
            } else {
                res.json({success: false, message: "Wrong password!"});
            }

            return;
        });
    } catch (error) {
        console.log(error);
        res.json({success: false, error: error});
    }
});

// Follow
router.post("/follow", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var newUserFollower = new UserFollower({
        userId: req.body.userId,
        followerId: req.body.followerId
    });

    try {
        var userFollower = await UserFollower.addUserFollower(newUserFollower);
        var currentUser = await User.findById(req.body.followerId);

        var notificationParams = {
            userId: req.body.userId,
            type: NotificationType.NewFollow,
            textParams: {
                fullname: currentUser.fullname,
                username: currentUser.username
            },
            associatedUser: currentUser.id
        };

        Notification.pushNotification(notificationParams);

        if(userFollower) {
            res.json({success: true});
        } else {
            res.json({success: false, message: "Something went wrong."});
        }
    } catch (err) {
        res.json({success: false, message: err});
    }
});

// Follow
router.post("/unfollow", passport.authenticate("jwt", {session: false}), (req, res, next) => {
    var userFollower = {
        userId: req.body.userId,
        followerId: req.body.followerId
    };

    UserFollower.deleteUserFollower(userFollower)
    .then(data => {
        res.json({success: true});
    })
    .catch(error => {
        res.json({success: false, error: error});
    });
});

// Get User List
router.get("/get-user-list", async (req, res, next) => {
    var request = {
        userIds: req.query.userIds,
        page: parseInt(req.query.page),
        pageSize: parseInt(req.query.pageSize),
        orderCriteria: parseInt(req.query.orderCriteria)
    };

    try {
        var data = await User.getUsersByIds(request);

        return res.json({success: true, data: data});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: "Something went wrong during getting user list"});
    }
});

// Duels
router.post("/duels", (req, res, next) => {
    var userId = req.body.userId,
        statuses = req.body.statuses,
        currentUserId = req.body.currentUserId;

    User.getSkillRating()
    .then(users => {
        Duel.getDuelsByUserId(userId)
        .then(data => {

            var duelsAmount = {
                waitingDuelsCount: data.filter(duel => duel.status === DuelStatus.Waiting).length,
                inProgressDuelsCount: data.filter(duel => duel.status === DuelStatus.InProgress).length,
                votingDuelsCount: data.filter(duel => duel.status === DuelStatus.Voting).length
            };

            // Filtering by status
            if(statuses.length > 0) {
                data = data.filter(duel => statuses.indexOf(duel.status) !== -1);
            }

            // Filtering by visibility
            if(userId !== currentUserId) {
                data = data.filter(duel => (duel.leftUser.user.id === userId && !duel.leftUser.isHidden) || (duel.rightUser.user.id === userId && !duel.rightUser.isHidden));
            }

            // Sorting duels from newest to oldest
            data.sort((a, b) => {
                return new Date(b.deadline) - new Date(a.deadline);
            });

            var data = data.map(duel => {
                duel.leftUser.rating = users.indexOf(users.filter((user) => user._id == duel.leftUser.userId)[0]) + 1;
                duel.rightUser.rating = users.indexOf(users.filter((user) => user._id == duel.rightUser.userId)[0]) + 1;

                return Duel.mapToSafeModel(duel, currentUserId);
            });

            res.json({success: true, duels: data, duelsAmount: duelsAmount}); 
        }); 
    });
});

// Turn off help
router.post("/turn-off-help", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var userId = req.body.userId;

    var user = await User.findById(userId);

    if (user !== null) {
        user.isHelpOff = true;
        user.save();
        res.json({success: true, user: User.mapToSafeModel(user)});
    } else {
        res.json({success: false, error: "Something went wrong during turning off user help"});
    }
});

module.exports = router;