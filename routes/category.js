const express = require('express');
const router = express.Router();
const passport = require("passport");

const Category = require("../models/category");
const User = require("../models/user");

/*
To protect a route,
you should put as a second parameter the next string:
passport.authenticate("jwt", {session: false})
*/

// Get Categories
router.get("/get-categories", async (req, res, next) => { 
    try {
        var categories = await Category.getCategories(),
            categories = categories.map(Category.mapToViewModel);

        return res.json({success: true, categories: Category.getGroupedCategories(categories)});
    } catch (err) {
        console.log(err);
        return res.json({success: false, error: "Something went wrong during fetching categories"});
    }
});

// Get User Categories
router.get("/get-user-categories", passport.authenticate("jwt", {session: false}), async (req, res, next) => { 
    try {
        var userId = req.query.userId,
            user = await User.findById(userId),
            categories = await Category.getUserCategories(user.categories);

        return res.json({success: true, categories: Category.getGroupedCategories(categories)});
    } catch (err) {
        console.log(err);
        return res.json({success: false, error: "Something went wrong during fetching user categories"});
    }
});

// Save User Categories
router.post("/save-user-categories", passport.authenticate("jwt", {session: false}), async (req, res, next) => { 
    try {
        var userId = req.body.userId,
            categories = req.body.categories,
            user = await User.findById(userId);

        user.categories = categories;
        await user.save();
            
        var categories = await Category.getUserCategories(user.categories);

        return res.json({success: true, categories: Category.getGroupedCategories(categories)});
    } catch (err) {
        console.log(err);
        return res.json({success: false, error: "Something went wrong during saving user categories"});
    }
});

module.exports = router;