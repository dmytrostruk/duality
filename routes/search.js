const express = require('express');
const router = express.Router();
const passport = require("passport");

const User = require("../models/user");
const Duel = require("../models/duel");
const OrderDirection = require("../enums/order-direction");

/*
To protect a route,
you should put as a second parameter the next string:
passport.authenticate("jwt", {session: false})
*/

// Get users
router.get("/get-users", async (req, res, next) => {
    try {
        var searchString = req.query.searchString.trim(),
            page = req.query.page,
            users = await User.getUsersByUsernameOrFullName({data: searchString, orderDirection: OrderDirection.Desc, page: page}),
            duelsCount = await Duel.getDuelsByTaskCount(searchString),
            usersCount = await User.getUsersByUsernameOrFullNameCount(searchString);

        res.json({success: true, data: { users: users.map(User.mapToSearchModel), usersCount: usersCount, duelsCount: duelsCount}});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: "Something went wrong during fetching users data"});
    }
}); 

// Get duels
router.get("/get-duels", async (req, res, next) => {
    try {
        var searchString = req.query.searchString.trim(),
            page = req.query.page,
            duels = await Duel.getDuelsByTask({task: searchString, page: page}),
            usersCount = await User.getUsersByUsernameOrFullNameCount(searchString),
            duelsCount = await Duel.getDuelsByTaskCount(searchString);

        res.json({success: true, data: { duels: duels.map(Duel.mapToSafeModel), usersCount: usersCount, duelsCount: duelsCount}});
    } catch (error) {
        console.log(error);
        res.json({success: false, error: "Something went wrong during fetching duels data"});
    }
}); 

module.exports = router;