const express = require('express');
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");

const User = require("../models/user");
const Duel = require("../models/duel");
const Notification = require("../models/notification");
const DuelStatus = require("../enums/duel-status");
const NotificationType = require("../enums/notification-type");
const DuelJob = require("../jobs/duel-job");

/*
To protect a route,
you should put as a second parameter the next string:
passport.authenticate("jwt", {session: false})
*/

// Start Duel Information
router.get("/get-info", passport.authenticate("jwt", {session: false}), (req, res, next) => { 
    var username = req.query.username;
    var currentUsername = req.query.currentUsername;
    var search = ""; // We need to fetch all possible user opponents

    User.getUserByUsernameOrEmail({username: currentUsername, email: null})
    .then(currentUser => {
        if(currentUser) {
            User.getUserByUsernameOrEmail({username: username, email: null})
            .then(requestedUser => {
                User.getDuelsInformation(currentUser.id)
                .then(duelsInfo => {
                    User.getUserOpponents(currentUser.id, currentUser.skillPoints, search)
                    .then(opponents => {
                        opponents = opponents.map(opponent => User.mapOpponentToStartDuelModel(currentUser, opponent));
                        opponents = opponents.filter(opponent => opponent.commonCategories.length > 0);
                        
                        var suggestedSkills = User.getSkillSuggestions(currentUser.skillPoints, opponents.map(opponent => opponent.skillPoints));

                        var suggestedOpponents = {
                            furthestMinus: User.getRandomOpponentWithSameSkill(opponents, suggestedSkills["furthestMinus"]),
                            middleMinus: User.getRandomOpponentWithSameSkill(opponents, suggestedSkills["middleMinus"]),
                            closestMinus: User.getRandomOpponentWithSameSkill(opponents, suggestedSkills["closestMinus"]),
                            closestPlus: User.getRandomOpponentWithSameSkill(opponents, suggestedSkills["closestPlus"]),
                            middlePlus: User.getRandomOpponentWithSameSkill(opponents, suggestedSkills["middlePlus"]),
                            furthestPlus: User.getRandomOpponentWithSameSkill(opponents, suggestedSkills["furthestPlus"])
                        };

                        suggestedOpponents = User.fixSuggestedOpponents(suggestedOpponents);

                        res.json({
                            success: true, 
                            data: {
                                duelsLeft: duelsInfo.duelsLeft,
                                currentDuels: duelsInfo.currentDuels.length,
                                isPaidAccount: currentUser.isPaidAccount,
                                commonCategories: requestedUser ? User.getCommonCategories(currentUser, requestedUser) : [],
                                suggestedOpponents: suggestedOpponents,
                                currentUser: User.mapToSafeModel(currentUser)
                            }
                        });
                    });
                });
            });
        } else {
            res.json({success: false, message: "User not found"});
        }
    });
});

// Get Opponents
router.get("/get-opponents", passport.authenticate("jwt", {session: false}), (req, res, next) => { 
    var currentUsername = req.query.currentUsername;
    var search = req.query.search;

    if(search.trim() == "") {
        res.json({success: true, data: []});
        return;
    }

    User.getUserByUsernameOrEmail({username: currentUsername, email: null})
    .then(currentUser => {
        if(currentUser) {
            User.getUserOpponents(currentUser.id, currentUser.skillPoints, search)
            .then(opponents => {
                opponents = opponents.map(opponent => User.mapOpponentToStartDuelModel(currentUser, opponent));
                opponents = opponents.filter(opponent => opponent.commonCategories.length > 0);

                res.json({success: true, data: opponents});
            });
        } else {
            res.json({success: false, message: "User not found"});
        }
    });
});

// Start Duel Action
router.get("/create-duel", passport.authenticate("jwt", {session: false}), async (req, res, next) => { 
    var duel = new Duel({
        leftUser: { 
            votesAmount: 0,
            lastVotesAmount: 0,
            userId: req.query.leftUser,
            user: req.query.leftUser,
        },
        rightUser: {
            votesAmount: 0,
            lastVotesAmount: 0,
            userId: req.query.rightUser,
            user: req.query.rightUser
        },
        endDate: req.query.endDate,
        duelTask: req.query.duelTask,
        duelCategory: req.query.duelCategory,
        status: DuelStatus.Waiting,
        deadline: req.query.deadline,
        createdAt: new Date(),
        isFeatured: false,
        isPopular: false
    });

    try {
        var createdDuel = await Duel.createDuel(duel);
        var currentUser = await User.findById(req.query.leftUser);
        
        if(createdDuel) {
            var notificationParams = {
                userId: req.query.rightUser,
                type: NotificationType.CreatedDuel,
                textParams: {
                    fullname: currentUser.fullname,
                    username: currentUser.username,
                    duelId: createdDuel._id
                },
                associatedUser: currentUser.id
            };

            Notification.pushNotification(notificationParams);

            DuelJob.waitingToRemovedDuelJob(createdDuel);

            res.json({success: true});
        } else {
            res.json({success: false, message: "Something went wrong during starting a duel..."});
        }

    } catch (err) {
        res.json({success: false, error: err});
    }
});

module.exports = router;