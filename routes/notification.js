const express = require('express');
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");

const Notification = require("../models/notification");

/*
To protect a route,
you should put as a second parameter the next string:
passport.authenticate("jwt", {session: false})
*/

// Get Notifications
router.get("/get-notifications", passport.authenticate("jwt", {session: false}), async (req, res, next) => { 
    var userId = req.query.userId,
        page = parseInt(req.query.page),
        pageSize = parseInt(req.query.pageSize);

    try {
        var data = await Notification.getNotificationsByUserId({userId: userId, page: page, pageSize: pageSize});
        var newNotificationCount = await Notification.getNewNotificationsByUserId(userId);

        res.json({success: true, 
            notifications: data.notifications.map(Notification.mapToSafeModel), 
            totalCount: data.totalCount,
            newNotificationCount: newNotificationCount
        });
    } catch (err) {
        res.json({success: false, error: err});
    }
});

// Get New Notifications
router.get("/get-new-notifications", passport.authenticate("jwt", {session: false}), async (req, res, next) => { 
    var userId = req.query.userId;
    try {
        var count = await Notification.getNewNotificationsByUserId(userId);

        res.json({success: true, count: count});
    } catch (err) {
        res.json({success: false, error: err});
    }
});

// Mark as read
router.post("/mark-as-read", passport.authenticate("jwt", {session: false}), async (req, res, next) => { 
    var currentUserId = req.body.currentUserId,
        notificationIds = req.body.notificationIds;
        
    try {
        await Notification.markAsRead(notificationIds, currentUserId);
        var newNotificationCount = await Notification.getNewNotificationsByUserId(currentUserId);

        res.json({success: true, newNotificationCount: newNotificationCount});
    } catch (err) {
        res.json({success: false});
    }
});

module.exports = router;