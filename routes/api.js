const express = require('express');
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const config = require("../config/database");
const request = require("request");
const md5 = require("md5");
const uuidv1 = require('uuid/v1');

const User = require("../models/user");
const PasswordRestore = require("../models/passwordrestore");
const Notification = require("../models/notification");

const AvatarRandomizer = require("../helpers/avatar-randomizer");

const SocketService = require("../services/socket-service");
const GoogleService = require("../services/google-service");
const TwitterService = require("../services/twitter-service");
const EmailService = require("../services/email-service");

const dribbbleTokenUrl = "https://dribbble.com/oauth/token";

/*
To protect a route,
you should put as a second parameter the next string:
passport.authenticate("jwt", {session: false})
*/

// Register
router.post("/register", (req, res, next) => {
    let newUser = new User({
        fullname: req.body.name,
        email: req.body.email,
        password: req.body.password,
        categories: req.body.categories,
        isPaidAccount: false,
        accountCreationDate: new Date(),
        skillPoints: 0,
        featuredPoints: 0,
        imageColor: AvatarRandomizer.generateAvatarColor()
    });

    User.generateUsername(newUser.fullname)
     .then((username) => {
        newUser.username = username;

        User.getUserByUsernameOrEmail({username: null, email: newUser.email})
            .then((user) => {
                if(user) {
                    return res.json({success: false, message: "User with that email already exists!"});
                }

                User.saveUser(newUser, async (error, user) => {
                    var message = error ? "Failed to register user!"
                                        : "User was successfully registered!";

                    var success = !error;

                    if(success) {
                        var usersAmount = await User.getUsersAmount();
                        SocketService.onAccountCreated(usersAmount);
                    }
            
                    return res.json({
                        success: true,
                        token: "JWT " + getAuthToken(user),
                        user: User.mapToSafeModel(user)
                    });
                });
            })
            .catch((error) => {
                throw err;
            });
     });
});

// Authenticate
router.post("/authenticate", (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    // Users can login either by username or email
    User.getUserByUsernameOrEmail({username: username, email: username})
        .then(async (user) => {
            if(!user) {
                return res.json({success: false, message: "User not found"});
            }

            if(user.isBanned) {
                return res.json({success: false, message: "Unfortunately, you are banned. Please, contact administrators!"});
            }

            var duelsInfo = await User.getDuelsInformation(user.id);
            user.duelsLeft = duelsInfo.duelsLeft;

            User.comparePassword(password, user.password, (error, isMatch) => {
                if(error) {
                    throw error;
                }

                if(isMatch && password) {
                    res.json({
                        success: true,
                        token: "JWT " + getAuthToken(user),
                        user: User.mapToSafeModel(user)
                    });
                } else {
                    res.json({success: false, message: "Wrong password!"});
                }
            });
        })
        .catch((error) => {
            throw error;
        });
});

// Check User Email
router.get("/check-user-email", async (req, res, next) => {
    try {
        var email = req.query.email,
            user = await User.getUserByUsernameOrEmail({username: null, email: email});

        return res.json({success: true, exists: user !== null});
    } catch (err) {
        console.log(err);
        return res.json({success: false, error: "Something went wrong during checking user email"});
    }
});

// Profile
router.get("/profile", passport.authenticate("jwt", {session: false}), async (req, res, next) => {
    var user = req.user;
    user.rating = await User.getSkillRatingByUserId(user._id);

    res.json({user: User.mapToSafeModel(req.user)});
});

// Get Dribbble Token
router.post("/get-dribbble-token", async (req, res, next) => {
    var dribbbleData = {
        client_id: req.body.clientId,
        client_secret: req.body.clientSecret,
        code: req.body.code
    };

    request.post(dribbbleTokenUrl, { json: dribbbleData }, (error, response, body) => {
        if (!error && response.statusCode == 200) {
            res.json({success: true, token: body.access_token});
        } else {
            res.json({success: false, error: "Something went wrong during access Dribbble information"});
        }
    });
});

// Verify Google Token
router.post("/verify-google-token", async (req, res, next) => {
    var token = req.body.token;

    try {
        var user = await GoogleService.verifyTokenAndGetUserInfo(token);
        return await authenticateUserByEmail(res, user);
    } catch (error) {
        console.log(error);
        return res.json({success: false, error: "Something went wrong during verifying Google token"});
    }
});

// Get Twitter url
router.get("/get-twitter-url", async (req, res, next) => {
    TwitterService.getInstance().getRequestToken((err, requestToken, requestSecret) => {
        if(!err) {
            TwitterService.saveRequestSecret(requestSecret);
            return res.json({success: true, url: `https://api.twitter.com/oauth/authenticate?oauth_token=${requestToken}`});
        } else {
            console.log(err);
            return res.json({success: false, error: "Something went wrong during getting Twitter url"});
        }
    });
});

// Verify Twitter User
router.get("/verify-twitter-user", async (req, res, next) => {
    var token = req.query.token,
        verifier = req.query.verifier,
        requestSecret = TwitterService.getRequestSecret(),
        twitter = TwitterService.getInstance();

    twitter.getAccessToken(token, requestSecret, verifier, (err, accessToken, accessSecret) => {
        if(!err) {
            twitter.verifyCredentials(accessToken, accessSecret, {include_email: true}, async (verifyErr, user) => {
                if(!verifyErr && user) {
                    TwitterService.saveRequestSecret(null);
                    return await authenticateUserByEmail(res, {email: user.email, fullname: user.name});
                } else {
                    console.log(verifyErr);
                    return res.json({success: false, error: "Something went wrong during getting Twitter access token"});
                }
            });
        } else {
            console.log(err);
            return res.json({success: false, error: "Something went wrong during getting Twitter access token"});
        }
    });
});

// Restore password
router.get("/restore-password", async (req, res, next) => {
    try {
        var email = req.query.email,
            user = await User.getUserByUsernameOrEmail({username: null, email: email});

        if(!user) {
            return res.json({success: false, error: "There is no account associated with this email :("});
        }

        var code = md5(uuidv1());

        var passwordRestore = new PasswordRestore({
            email: email,
            code: code,
            timestamp: new Date(),
            isUsed: false
        });

        passwordRestore.save();

        var mailOptions = {
            from: 'duelity.co',
            to: email,
            subject: "Restore password",
            text: "",
            html: `<p>To reset your Duelity account password click below. Please note that reset link will expire in 24 hours. If you didn’t request password restore link, ignore this letter.</p><br><a target="_blank" href="http://duelity.herokuapp.com/restore?code=${code}">Restore password</a>`
        };

        EmailService.sendEmail(mailOptions);

        return res.json({success: true});
    } catch (error) {
        console.log(error);
        return res.json({success: false, error: "Something went wrong during restoring password"});
    }
});

// Validate Restore Code
router.get("/validate-restore-code", async (req, res, next) => {
    try {
        var code = req.query.code,
        passwordRestore = await PasswordRestore.getPasswordRestoreByCode(code);

        if(passwordRestore !== null) {
            var hoursDifference = (new Date() - passwordRestore.timestamp) / 36e5;

            return hoursDifference < 24 ? 
                res.json({success: true, email: passwordRestore.email}) : 
                res.json({success: false, error: "Restore code is obsolete"});

        } else {
            return res.json({success: false, error: "Invalid restore code"});   
        }
    } catch (error) { 
        console.log(error);
        return res.json({success: false, error: "Something went wrong during restore code validation"});
    }
});

// Save New Password
router.post("/save-new-password", async (req, res, next) => {
    var email = req.body.email,
        password = req.body.password,
        code = req.body.code;

    try {
        var user = await User.getUserByUsernameOrEmail({username: null, email: email});

        if(user) {
            user.password = password;
            User.saveUser(user, async (error, savedUser) => {
                if(error) {
                    throw error;
                }

                var passwordRestore = await PasswordRestore.getPasswordRestoreByCode(code);

                passwordRestore.isUsed = true;
                passwordRestore.save();

                return await authenticateUserByEmail(res, {email: savedUser.email});
            });
        } else {
            return res.json({success: false, error: "User does not exist!"});
        }

    } catch (error) {
        console.log(error);
        return res.json({success: false, error: "Something went wrong during saving new password"});
    }
});

//#region Private Methods

var authenticateUserByEmail = async (res, userInfo) => {
    var user = await User.getUserByUsernameOrEmail({email: userInfo.email});

    if(user) {
        if(user.isBanned) {
            return res.json({success: false, message: "Unfortunately, you are banned. Please, contact administrators!"});
        }

        return res.json({
            success: true,
            token: "JWT " + getAuthToken(user),
            user: User.mapToSafeModel(user)
        });
    } else {
        return res.json({success: true, newUser: true, user: userInfo});
    }
}

var getAuthToken = (user) => {
    var jwtUser = {
        id: user._id,
        fullname: user.fullname,
        username: user.username,
        email: user.email,
        password: user.password
    };

    return jwt.sign(jwtUser, config.secret, {
        expiresIn: 604800 // 1 week
    });
}

//#endregion

module.exports = router;