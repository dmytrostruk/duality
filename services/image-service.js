const cloudinary = require("cloudinary");

const ImageService = {
    deleteImagesFromStorage: async (imagesForDelete) => {
        return new Promise((resolve, reject) => {
            if(imagesForDelete && imagesForDelete.length > 0) {
                let i = 0;
                var deleteWork = () => {
                    cloudinary.v2.uploader.destroy(imagesForDelete[i], {}, (error, result) => {
                        i++;
    
                        if(i < imagesForDelete.length) {
                            deleteWork();
                        }
    
                        resolve();
                    });
                }
    
                deleteWork();
            } else {
                resolve();
            }
        });  
    }
}

module.exports = ImageService;