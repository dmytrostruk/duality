const { OAuth2Client } = require('google-auth-library');
const config = require('../config/google');

const client = new OAuth2Client(config.clientId);

const GoogleService = {
    verifyTokenAndGetUserInfo: async (token) => {
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: config.clientId
        });

        var payload = ticket.getPayload();
        return { email: payload.email, fullname: payload.name };
    }
}

module.exports = GoogleService;