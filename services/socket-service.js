const onlineUsers = [];
const disconnectTimeout = 3000;
var globalIo = {};

const SocketService = {
    initializeSocket: (io) => {
        io.on('connection', socket => {
            var self = this;
            self.socket = socket;
            globalIo = io;

            socket.on('user-connect', userId => {
                self.socket.userDisconnected = false;
                socket.userId = userId;
                onlineUsers.push(userId);
                io.emit('user-connect', onlineUsers);
            });

            socket.on('disconnect', () => {
                self.socket.userDisconnected = true;
                setTimeout(() => {
                    if(self.socket.userDisconnected) {
                        onlineUsers.splice(onlineUsers.indexOf(socket.userId), 1);
                        io.emit('disconnect', onlineUsers);
                    }
                }, disconnectTimeout);
            });

            socket.on('user-disconnect', userId => {
                for(let i = onlineUsers.length - 1; i >= 0; i--){
                    if (onlineUsers[i] === userId) {
                        onlineUsers.splice(i, 1);
                    }
                }

                io.emit('user-disconnect', onlineUsers);
            });
        });
    },
    onDuelCreated: (duelsAmount) => {
        globalIo.emit('duel-created', duelsAmount);
    },
    onAccountCreated: (usersAmount) => {
        globalIo.emit('account-created', usersAmount);
    },
    onUserBan: (userId) => {
        globalIo.emit('user-banned', userId);
    },
    onNewNotification: (userId) => {
        globalIo.emit('new-notification', userId);
    }
}

module.exports = SocketService;