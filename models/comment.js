const mongoose = require("mongoose");
const OrderDirection = require("../enums/order-direction");

const User = require("../models/user");
const Duel = require("../models/duel");

const CommentSchema = mongoose.Schema({
    duelId: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    text: {
        type: String,
        required: true
    }
});

const Comment = module.exports = mongoose.model("Comment", CommentSchema, 'comments');

module.exports.getDuelComments = async (request) => {
    var page = request.page || 1;
    var pageSize = request.pageSize || 10;

    var comments = await Comment.find(getCommentsByDuel(request.duelId))
                                .populate("user")
                                .sort(`${getOrderSign(request.orderDirection || OrderDirection.Asc)}timestamp`)
                                .skip((page - 1) * pageSize)
                                .limit(pageSize);

    var duel = await Duel.getDuelById(request.duelId),
        allComments = await Comment.find(getCommentsByDuel(request.duelId));

    return {
        comments: comments.map(comment => Comment.mapToSafeModel(comment, duel)),
        totalCount: allComments.length
    };
}

module.exports.removeComment = (commentId) => {
    return Comment.deleteOne({_id: commentId});
}

//#region Mappers

module.exports.mapToSafeModel = (comment, duel) => {
    return {
        id: comment.id,
        text: comment.text,
        timestamp: comment.timestamp,
        user: User.mapToCommentModel(comment.user, duel)
    }
}

//#endregion

//#region Private Methods 

var getOrderSign = (orderDirection) => {
    return orderDirection === OrderDirection.Asc ? "" : "-";
}

//#endregion

//#region Queries

var getCommentsByDuel = (duelId) => {
    return { duelId: duelId };
}

//#endregion