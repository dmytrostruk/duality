const mongoose = require("mongoose");
const ReportEntityType = require("../enums/report-entity-type");

const Comment = require("./comment");
const Duel = require("./duel");

const ReportSchema = mongoose.Schema({
    entityId: {
        type: String,
        required: true
    },
    entityType: {
        type: Number,
        required: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    reporter: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    reason: {
        type: Number
    },
    isResolved: {
        type: Boolean,
        return: true
    }
});

const Report = module.exports = mongoose.model("Report", ReportSchema, "reports");

module.exports.getReports = async (reportType) => {
    var result = [];
    var reports = await Report.find(commentsQuery(reportType))
                              .populate("reporter")
                              .sort({timestamp: 'desc'});

    for(var i = 0; i < reports.length; i++) {
        var mappedReport = await Mappers[reportType](reports[i]);
        result.push(mappedReport);
    }

    return result;
}

//#region Mappers

var mapReportToComment = async (report) => {
    var comment = await Comment.findById(report.entityId).populate("user");

    return {
        reporterFullname: report.reporter.fullname,
        authorId: comment.user.id,
        authorFullname: comment.user.fullname,
        commentText: comment.text,
        commentTimestamp: comment.timestamp,
        reportTimestamp: report.timestamp
    };
}

var mapReportToDuel = async (report) => {
    var result = {};
    return result;
}   

const Mappers = {
    [ReportEntityType.Comment]: mapReportToComment,
    [ReportEntityType.Duel]: mapReportToDuel
}

//#endregion

//#region Queries

var commentsQuery = (reportType) => {
    return { $and: [{entityType: reportType}, {isResolved: false}]};
}

//#endregion