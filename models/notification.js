const mongoose = require("mongoose");
const NotificationBuilder = require("../helpers/notification-builder");
const SocketService = require("../services/socket-service");

const NotificationSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    type: {
        type: Number,
        required: true
    },
    isNewNotification: {
        type: Boolean,
        required: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    associatedUser: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: "User"
    },
});

const Notification = module.exports = mongoose.model("Notification", NotificationSchema, 'notifications');

module.exports.pushNotification = async (params) => {
    var notification = new Notification({
        userId: params.userId,
        text: NotificationBuilder.buildNotification(params.type, params.textParams), 
        type: params.type,
        isNewNotification: true,
        timestamp: new Date(),
        associatedUser: params.associatedUser
    });

    await notification.save();

    SocketService.onNewNotification(params.userId);

    return true;
}

module.exports.getNotificationsByUserId = async (request) => {
    var page = request.page || 1;
    var pageSize = request.pageSize || 20;

    var notifications = await Notification.find(getNotificationsByUserIdQuery(request.userId))
    .sort("-timestamp")
    .populate("associatedUser")
    .skip((page - 1) * pageSize)
    .limit(pageSize);

    var allNotifications = await Notification.find(getNotificationsByUserIdQuery(request.userId));

    return {
        notifications: notifications,
        totalCount: allNotifications.length
    }
}

module.exports.getNewNotificationsByUserId = async (userId) => {
    var count = await Notification.countDocuments(getNewNotificationsByUserIdQuery(userId));
    return count;
}

module.exports.markAsRead = async (notificationIds, userId) => {
    await Notification.update(getMarkAsReadQuery(notificationIds, userId), { isNewNotification : false }, { multi: true });
}

//#region Mappings

module.exports.mapToSafeModel = (notification) => {
    return {
        id: notification.id,
        isNewNotification: notification.isNewNotification,
        type: notification.type,
        text: notification.text,
        timestamp: notification.timestamp,
        userId: notification.associatedUser.id,
        fullname: notification.associatedUser.fullname,
        imageUrl: notification.associatedUser.imageUrl,
        imageColor: notification.associatedUser.imageColor
    }
}

//#endregion

//#region Queries

var getNotificationsByUserIdQuery = (userId) => {
    return { userId: userId };
}

var getNewNotificationsByUserIdQuery = (userId) => {
    return { $and: [{userId: userId}, {isNewNotification: true}]};
}

var getMarkAsReadQuery = (notificationIds, userId) => {
    return { $and: [{ _id: { $in: notificationIds }}, {userId: userId}] };
}

//#endregion