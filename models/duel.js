const mongoose = require("mongoose");
const User = require("./user");
const Category = require("./category");
const UserFollower = require("./userfollower");
const DuelStatus = require("../enums/duel-status");
const { SkillCalculation } = require("../constants/constants");

// Additional Schemas
const UserDuelSchema = mongoose.Schema({
    user: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: "User"
    },
    userId: {
        type: String,
        required: true
    },
    votesAmount: {
        type: Number,
        required: true
    },
    lastVotesAmount: {
        type: Number,
        required: true
    },
    votes: {
        type: Array, 
    },
    previewImageUrl: {
        type: Object
    },
    imageUrls: {
        type: Array
    },
    workDescription: {
        type: String
    },
    isHidden: {
        type: Boolean
    }
});

const DuelSchema = mongoose.Schema({
    leftUser: UserDuelSchema,
    rightUser: UserDuelSchema,
    endDate: {
        type: Date,
        required: true
    },
    deadline: {
        type: Date
    },
    duelTask: {
        type: String,
        required: true
    },
    duelCategory: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: "Category"
    },
    status: {
        type: Number
    },
    isFeatured: {
        type: Boolean,
    },
    isPopular: {
        type: Boolean
    },
    numericId: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        required: true
    }
});

const Duel = module.exports = mongoose.model("Duel", DuelSchema, 'duels');

module.exports.createDuel = async (duel) => {
    var duels = await Duel.find().sort(`-numericId`);

    if(duels.length === 0) {
        duel.numericId = 1;
    } else {
        var lastNumericId = duels[duels.length - 1].numericId
        duel.numericId = lastNumericId + 1;
    }

    return duel.save();
}

module.exports.getDuelsAmount = async () => {
    var duels = await Duel.find();
    duels = duels || [];

    return duels.length;
}

module.exports.getDuelById = (duelId) => {
    return Duel.findById(duelId)
    .populate("leftUser.user")
    .populate("rightUser.user")
    .populate("duelCategory");
}

module.exports.getDuelByNumericId = async (numericId) => {
    var duels = await Duel.find({numericId: numericId})
                          .populate("leftUser.user")
                          .populate("rightUser.user")
                          .populate("duelCategory");

    return duels[0];
}

module.exports.getDuelsByStatus = (status) => {
    return Duel.find(getDuelsByStatusQuery(status));
}

module.exports.getDuelsByUserId = (userId) => {
    return Duel.find(getDuelsByUserIdQuery(userId))
    .populate("leftUser.user")
    .populate("rightUser.user")
    .populate("duelCategory");
}

module.exports.getFeaturedDuels = (request) => {
    var page = request.page || 1;
    var pageSize = request.pageSize || 40;

    return Duel.find(getFeaturedDuelsQuery())
    .populate("leftUser.user")
    .populate("rightUser.user")
    .populate("duelCategory")
    .skip((page - 1) * pageSize)
    .limit(pageSize);
}

module.exports.getFeaturedDuelsCount = () => {
    return Duel.countDocuments(getFeaturedDuelsQuery());
}

module.exports.getPopularDuels = (request) => {
    var page = request.page || 1;
    var pageSize = request.pageSize || 40;

    return Duel.find(getPopularDuelsQuery())
    .populate("leftUser.user")
    .populate("rightUser.user")
    .populate("duelCategory")
    .skip((page - 1) * pageSize)
    .limit(pageSize);
}

module.exports.getPopularDuelsCount = () => {
    return Duel.countDocuments(getPopularDuelsQuery());
}

module.exports.getFollowingDuels = async (request) => {
    var page = request.page || 1;
    var pageSize = request.pageSize || 40;
    var currentUserId = request.currentUserId;

    var followingIds = await UserFollower.getUserFollowings(currentUserId);

    return Duel.find(getFollowingDuelsQuery(followingIds))
               .populate("leftUser.user")
               .populate("rightUser.user")
               .populate("duelCategory")
               .skip((page - 1) * pageSize)
               .limit(pageSize);
}

module.exports.getFollowingDuelsCount = async (currentUserId) => {
    var followingIds = await UserFollower.getUserFollowings(currentUserId);
    return Duel.countDocuments(getFollowingDuelsQuery(followingIds));
}

module.exports.getNewDuels = (request) => {
    var page = request.page || 1;
    var pageSize = request.pageSize || 40;

    return Duel.find(getNewDuelsQuery())
    .populate("leftUser.user")
    .populate("rightUser.user")
    .populate("duelCategory")
    .skip((page - 1) * pageSize)
    .limit(pageSize);
}

module.exports.getNewDuelsCount = () => {
    return Duel.countDocuments(getNewDuelsQuery());
}

module.exports.getDuelsByTask = (request) => {
    var page = request.page || 1;
    var pageSize = request.pageSize || 40;

    return Duel.find(getDuelsByTaskQuery(request.task))
    .populate("leftUser.user")
    .populate("rightUser.user")
    .populate("duelCategory")
    .skip((page - 1) * pageSize)
    .limit(pageSize);
}

module.exports.getDuelsByTaskCount = (task) => {
    return Duel.countDocuments(getDuelsByTaskQuery(task));
}

module.exports.removeDuel = (duelId) => {
    return Duel.deleteOne({_id: duelId});
}

module.exports.mapToSafeModel = (duel, currentUserId) => {
    return {
        id: duel._id,
        numericId: duel.numericId,
        status: duel.status,
        endDate: duel.endDate,
        deadline: duel.deadline,
        duelTask: duel.duelTask,
        duelCategory: duel.duelCategory,
        createdAt: duel.createdAt,
        isFeatured: duel.isFeatured,
        leftUser: mapUserToDuelModel(duel.leftUser, currentUserId),
        rightUser: mapUserToDuelModel(duel.rightUser, currentUserId),
    }
}

module.exports.mapToVoteModel = (duel, currentUserId) => {
    return {
        id: duel._id,
        status: duel.status,
        endDate: duel.endDate,
        deadline: duel.deadline,
        duelTask: duel.duelTask,
        duelCategory: duel.duelCategory,
        isFeatured: duel.isFeatured,
        leftUser: mapUserToVoteModel(duel.leftUser, currentUserId),
        rightUser: mapUserToVoteModel(duel.rightUser, currentUserId),
    }
}

module.exports.mapToEditModel = (duel) => {
    return {
        id: duel._id,
        duelTask: duel.duelTask,
        leftUser: mapUserToEditDuelModel(duel.leftUser),
        rightUser: mapUserToEditDuelModel(duel.rightUser),
    }
}

module.exports.updateSkillPoints = async (duel, leftUser, rightUser) => {

    var getPoints = (firstUser, secondUser) => {
        var points = SkillCalculation.MaximumWinPoints 
        * ((SkillCalculation.MaximumSkillDifference - (firstUser.user.skillPoints - secondUser.user.skillPoints)) 
        / (SkillCalculation.MaximumSkillDifference * 2)) 
        * (getValuePercentage(firstUser.votesAmount, firstUser.votesAmount + secondUser.votesAmount) / 100);

        var minWinPoints = SkillCalculation.MaximumWinPoints * 0.20; 

        return points >= minWinPoints ? points : minWinPoints;
    }

    if(duel.leftUser.votesAmount === duel.rightUser.votesAmount) {
        var leftUserPoints = getPoints(duel.leftUser, duel.rightUser),
            rightUserPoints = getPoints(duel.rightUser, duel.leftUser);

        duel.leftUser.user.skillPoints += duel.leftUser.user.isPaidAccount ? 
                                          leftUserPoints * SkillCalculation.ProAccountMultiplier : 
                                          leftUserPoints;

        duel.rightUser.user.skillPoints += duel.rightUser.user.isPaidAccount ? 
                                           rightUserPoints * SkillCalculation.ProAccountMultiplier :
                                           rightUserPoints; 

    } else {
        var winner = Duel.getWinner(duel.leftUser, duel.rightUser),
        loser = Duel.getLoser(duel.leftUser, duel.rightUser);

        var points = getPoints(winner, loser);

        winner.user.skillPoints += winner.user.isPaidAccount ? points * SkillCalculation.ProAccountMultiplier : points;
        loser.user.skillPoints = loser.user.skillPoints - points > 0 ? loser.user.skillPoints - points : 0;
    }

    duel.leftUser.user.skillPoints = Math.round(duel.leftUser.user.skillPoints);
    duel.rightUser.user.skillPoints = Math.round(duel.rightUser.user.skillPoints);

    if(leftUser && rightUser) {
        leftUser.skillPoints = duel.leftUser.user.skillPoints;
        rightUser.skillPoints = duel.rightUser.user.skillPoints;
    
        leftUser.save();
        rightUser.save();
    }
}

module.exports.getDuelVotesAmount = async () => {
    var votes = 0,
        duels = await Duel.find(getVotingOrEndedDuelsQuery());

    duels.map(duel => { votes += duel.leftUser.votesAmount + duel.rightUser.votesAmount; });

    return votes;
}

module.exports.getWinner = (leftUser, rightUser) => {
    return leftUser.votesAmount > rightUser.votesAmount ? leftUser : rightUser;
}

module.exports.getLoser = (leftUser, rightUser) => {
    return leftUser.votesAmount < rightUser.votesAmount ? leftUser : rightUser; 
}

//#region Mappings

var mapUserToDuelModel = (userInfo, currentUserId) => {
    return { 
        votesAmount: userInfo.votesAmount,
        rating: userInfo.rating,
        isSelected: userInfo.votes.indexOf(currentUserId) !== -1,
        previewImageUrl: userInfo.previewImageUrl || {},
        isHidden: userInfo.isHidden,
        user: {
            id: userInfo.userId,
            fullname: userInfo.user.fullname,
            isPaidAccount: userInfo.user.isPaidAccount,
        }
    };
}

var mapUserToVoteModel = (userInfo, currentUserId) => {
    return { 
        votesAmount: userInfo.votesAmount,
        rating: userInfo.rating,
        previewImageUrl: userInfo.previewImageUrl || {},
        imageUrls: userInfo.imageUrls,
        workDescription: userInfo.workDescription,
        isSelected: userInfo.votes.indexOf(currentUserId) !== -1,
        user: {
            id: userInfo.userId,
            fullname: userInfo.user.fullname,
            imageUrl: userInfo.user.imageUrl,
            imageColor: userInfo.user.imageColor,
            isPaidAccount: userInfo.user.isPaidAccount
        }
    };
}

var mapUserToEditDuelModel = (userInfo) => {
    return { 
        previewImageUrl: userInfo.previewImageUrl || {},
        imageUrls: userInfo.imageUrls,
        workDescription: userInfo.workDescription,
        user: {
            id: userInfo.userId,
            fullname: userInfo.user.fullname,
            imageUrl: userInfo.user.imageUrl,
            imageColor: userInfo.user.imageColor,
            isPaidAccount: userInfo.user.isPaidAccount
        }
    };
}

//#endregion

//#region Queries

var getDuelsByUserIdQuery = (userId) => {
    return { $or: [{"leftUser.userId": userId}, {"rightUser.userId": userId}] };
}

var getDuelsByStatusQuery = (status) => {
    return { status: status };
}

var getFeaturedDuelsQuery = () => {
    return { $and: [{isFeatured: true}, getVotingOrEndedDuelsQuery()] };
}

var getPopularDuelsQuery = () => {
    return { $and: [{isPopular: true}, getVotingOrEndedDuelsQuery()] };
}

var getFollowingDuelsQuery = (followingIds) => {
    return { $and: [{ $or: [{"leftUser.userId": {$in: followingIds}}, {"rightUser.userId": {$in: followingIds}}]}, getVotingOrEndedDuelsQuery()]};
}

var getNewDuelsQuery = () => {
    return { $and: [{status: DuelStatus.Voting}, {isPopular: false}] };
}

var getVotingOrEndedDuelsQuery = () => {
    return { $or: [{status: DuelStatus.Voting}, {status: DuelStatus.Ended}] };
}

var getDuelsByTaskQuery = (task) => {
    return { $and: [{duelTask: new RegExp(task, "i")}, getVotingOrEndedDuelsQuery()] };
}

//#endregion

//#region Helpers

var getValuePercentage = (value, totalValue) => {
    return totalValue === 0 ? 0 : Math.round(value * 100 / totalValue);
}

//#endregion