const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Category = require("./category");
const Duel = require('./duel');
const DuelStatus = require('../enums/duel-status');
const OrderDirection = require("../enums/order-direction");
const UserListOrderCriteria = require("../enums/user-list-order-criteria");
const { StartDuelValidationMessages, SkillCalculation, DuelLimitations } = require("../constants/constants");

// User Schema
const UserSchema = mongoose.Schema({
    fullname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    imageUrl: {
        type: String
    },
    imageColor: {
        type: String,
    },
    imagePublicId: {
        type: String
    },
    isPaidAccount: {
        type: Boolean
    },
    isAdmin: {
        type: Boolean
    },
    socialNetworks: {
        type: Object
    },
    location: {
        type: String
    },
    accountCreationDate: {
        type: Date,
        required: true
    },
    skillPoints: {
        type: Number,
        required: true
    },
    featuredPoints: {
        type: Number,
        required: true
    },
    categories: [
        { 
            type: mongoose.Schema.Types.ObjectId, 
            ref: "Category"
        }
    ],
    duels: [
        { 
            type: mongoose.Schema.Types.ObjectId, 
            ref: "Duel"
        }
    ],
    isBanned: {
        type: Boolean
    },
    isHelpOff: {
        type: Boolean
    }
});

UserSchema.virtual('followers', {
    ref: 'UserFollower',
    localField: "_id",
    foreignField: "userId"
});

UserSchema.virtual('following', {
    ref: 'UserFollower',
    localField: "_id",
    foreignField: "followerId"
});

const User = module.exports = mongoose.model("User", UserSchema);

const pointsRange = SkillCalculation.MaximumSkillDifference;
const maxDuelsPerWeek = DuelLimitations.MaxDuelsPerWeek;

const userListOrderMapping = {
    [UserListOrderCriteria.SkillPoints]: "skillPoints",
    [UserListOrderCriteria.FeaturedPoints]: "featuredPoints"
};

module.exports.getSkillRating = () => {
    return User.find().sort({skillPoints: 'desc'});
}

module.exports.getSkillRatingByUserId = async (userId) => {
    var userRatings = await User.getSkillRating();
    userRatings = userRatings.map((user, index) => { return { userId: user._id, rating: index + 1}});

    return userRatings.filter(userRating => userRating.userId.toString() === userId.toString())[0].rating;
}

module.exports.getUserById = (id) => {
    return User.findById(id)
    .populate("categories")
    .populate("following")
    .populate("followers");
}

module.exports.getPaidAccountsAmount = async () => {
    var paidAccounts = await User.find({isPaidAccount: true});
    paidAccounts = paidAccounts || [];

    return paidAccounts.length;
}

module.exports.getUsersAmount = async () => {
    var users = await User.find();
    users = users || [];

    return users.length;
}

module.exports.getUserOpponents = (currentUserId, skillPoints, search) => {
    return User.find(getOpponentsQuery(currentUserId, skillPoints, search))
    .populate("categories");
}

module.exports.getUsersByIds = async (request) => {
    var page = request.page || 1,
        pageSize = request.pageSize || 6,
        query = request.userIds && request.userIds.length > 0 
                ? getUsersByIds(request.userIds) : {};

    var users = await User.find(query)
                          .sort(`-${userListOrderMapping[request.orderCriteria]}`)
                          .skip((page - 1) * pageSize)
                          .limit(pageSize);

    var allUsers = await User.find(query);

    return {
        users: users.map(User.mapToSafeModel),
        totalCount: allUsers.length
    };
}

module.exports.getUserByIdWithDuels = (id) => {
    return User.findById(id)
    .populate("duels");
}

module.exports.getUserByUsernameOrEmail = (data) => {
    const query = { $or: [{username: data.username}, {email: data.email}] };
    return User.findOne(query)
    .populate("categories")
    .populate("following")
    .populate("followers");
}

module.exports.getUsersByUsernameOrFullName = (request) => {
    var page = request.page || 1;
    var pageSize = request.pageSize || 20;

    return User.find(getUsersByUsernameOrFullNameQuery(request.data))
               .populate("following")
               .populate("followers")
               .sort(`${getOrderSign(request.orderDirection)}skillPoints`)
               .skip((page - 1) * pageSize)
               .limit(page * pageSize);
}

module.exports.getUsersByUsernameOrFullNameCount = (data) => {
    return User.countDocuments(getUsersByUsernameOrFullNameQuery(data));
}

module.exports.getAdmins = (data) => {
    const query = { $and: [{$or: [{username: data.username}, {email: data.email}]}, {isAdmin: true}] };
    return User.findOne(query);
}

module.exports.saveUser = (newUser, callback) => {
    // Generating random key
    bcrypt.genSalt(10, (error, salt) => {
        bcrypt.hash(newUser.password, salt, (error, hash) => {
            if (error) {
                throw error;
            }

            newUser.password = hash;
            newUser.save(callback);
        })
    });
}

module.exports.comparePassword = (candidatePassword, hash, callback) => {
    bcrypt.compare(candidatePassword, hash, (error, isMatch) => {
        if(error) {
            throw error;
        }

        callback(null, isMatch);
    });
}

module.exports.generateUsername = (fullname) => {
    var initialValue = fullname.split(" ").join("").toLowerCase();
    var candidateValue = initialValue;
    var counter = 1;

    var getUsername = function(counter) {
        return new Promise((resolve, reject) => {
            User.getUserByUsernameOrEmail({username: candidateValue, email: null})
                .then((user) => {
                    if(user) {
                        candidateValue = initialValue + counter.toString();
                        resolve(getUsername(++counter));
                    } else {
                        return resolve(candidateValue);
                    }
                }); 
        });
    }

    return getUsername(counter);
}

module.exports.getDuelsWonAmount = async (userId) => {
    var userDuels = await Duel.getDuelsByUserId(userId);
    return userDuels.filter(duel => (duel.leftUser.userId === userId && duel.leftUser.votesAmount > duel.rightUser.votesAmount) || 
        (duel.rightUser.userId === userId && duel.rightUser.votesAmount > duel.leftUser.votesAmount)).length;
}

module.exports.saveUserSettings = async (userId, settings) => {
    var user = await User.findById(userId);

    user.fullname = settings.fullname;
    user.description = settings.description;
    user.username = settings.username;
    user.email = settings.email;
    user.imageColor = settings.imageColor;
    user.imageUrl = settings.imageUrl;

    user.socialNetworks = new Object();
    user.socialNetworks.behance = settings.behance;
    user.socialNetworks.dribbble = settings.dribbble;
    user.socialNetworks.twitter = settings.twitter;
    user.socialNetworks.instagram = settings.instagram;

    if(settings.password) {
        bcrypt.genSalt(10, (error, salt) => {
            bcrypt.hash(settings.password, salt, (error, hash) => {
                if (error) {
                    throw error;
                }
    
                user.password = hash;
                user.save();
            })
        });
    } else {
        user.save();
    }

    return user;
}

module.exports.mapToSafeModel = (user) => {
    return {
        id: user.id,
        email: user.email,
        fullname: user.fullname,
        username: user.username,
        description: user.description,
        imageUrl: user.imageUrl,
        imageColor: user.imageColor,
        isPaidAccount: user.isPaidAccount,
        socialNetworks: user.socialNetworks,
        location: user.location,
        categories: getCategoriesString(user.categories),
        accountCreationDate: user.accountCreationDate,
        followers: user.followers || [],
        following: user.following || [],
        skillPoints: user.skillPoints,
        featuredPoints: user.featuredPoints,
        rating: user.rating,
        isHelpOff: user.isHelpOff,
        duelsLeft: user.duelsLeft
    };
}

module.exports.mapToCommentModel = (user, duel) => {
    var selectedUserId = duel.leftUser.votes.indexOf(user._id) !== -1 ? duel.leftUser.user.id : 
                         duel.rightUser.votes.indexOf(user._id) !== -1 ? duel.rightUser.user.id : 
                         null;

    return {
        id: user._id,
        imageUrl: user.imageUrl,
        imageColor: user.imageColor,
        fullname: user.fullname,
        username: user.username, 
        selectedUserId: selectedUserId
    };
}

module.exports.mapToSettingsModel = (user) => {
    var settingsModel = {
        id: user._id,
        fullname: user.fullname,
        description: user.description,
        username: user.username,
        email: user.email,
        imageUrl: user.imageUrl,
        imageColor: user.imageColor,
        imagePublicId: user.imagePublicId
    }

    if(user.socialNetworks) {
        settingsModel.behance = user.socialNetworks.behance;
        settingsModel.dribbble = user.socialNetworks.dribbble;
        settingsModel.twitter = user.socialNetworks.twitter;
        settingsModel.instagram = user.socialNetworks.instagram;
    }

    return settingsModel;
}

module.exports.mapOpponentToStartDuelModel = (currentUser, opponent) => {
    return {
        id: opponent._id,
        value: opponent.fullname,
        commonCategories: User.getCommonCategories(currentUser, opponent),
        skillPoints: opponent.skillPoints,
        fullname: opponent.fullname,
        imageUrl: opponent.imageUrl,
        imageColor: opponent.imageColor
    }
}

module.exports.mapToAdminModel = (user) => {
    return {
        id: user._id,
        fullname: user.fullname,
        isBanned: user.isBanned,
        isPaidAccount: user.isPaidAccount,
        isAdmin: user.isAdmin,
        accountCreationDate: user.accountCreationDate,
        imageUrl: user.imageUrl,
        imageColor: user.imageColor
    };
}

module.exports.mapToUserListModel = (user) => {
    return {
        id: user.id,
        fullname: user.fullname,
        username: user.username,
        imageUrl: user.imageUrl,
        imageColor: user.imageColor,
        isPaidAccount: user.isPaidAccount,
        skillPoints: user.skillPoints
    };
}

module.exports.mapToSearchModel = (user) => {
    return {
        id: user.id,
        fullname: user.fullname,
        username: user.username,
        imageUrl: user.imageUrl,
        imageColor: user.imageColor,
        isPaidAccount: user.isPaidAccount,
        skillPoints: user.skillPoints,
        followers: user.followers || [],
        following: user.following || [],
    };
}

module.exports.canStartDuel = (currentUser, requestedUser) => {
    return new Promise(async (resolve, reject) => {
        var currentUserData = await User.getDuelsInformation(currentUser.id),
            requestedUserData = await User.getDuelsInformation(requestedUser.id);

        var skillPointsCondition = Math.abs(currentUser.skillPoints - requestedUser.skillPoints) <= pointsRange,
            categoriesCondition = User.getCommonCategories(currentUser, requestedUser).length > 0,
            duelsLimitCondition = currentUserData.lastWeekDuels.length < maxDuelsPerWeek,
            currentDuelsLimitCondition = currentUserData.currentDuels.length === 0,
            opponentDuelsLimitCondition = requestedUserData.lastWeekDuels.length < maxDuelsPerWeek,
            opponentCurrentDuelsLimitCondition = requestedUserData.currentDuels.length === 0;

        var message = "You cannot start a duel because of ",
            messagesArray = [];

        if(!skillPointsCondition) messagesArray.push(StartDuelValidationMessages.SkillDifference);
        if(!categoriesCondition) messagesArray.push(StartDuelValidationMessages.CategoriesInCommon);
        if(!duelsLimitCondition) messagesArray.push(StartDuelValidationMessages.UserDuelsLimit);
        if(!opponentDuelsLimitCondition) messagesArray.push(StartDuelValidationMessages.OpponentDuelsLimit);
        if(!currentDuelsLimitCondition) messagesArray.push(StartDuelValidationMessages.UserActiveDuel);
        if(!opponentCurrentDuelsLimitCondition) messagesArray.push(StartDuelValidationMessages.OpponentActiveDuel);
        
        if(messagesArray.length > 0) {
            message += messagesArray.join(" and ") + ".";
        } else {
            message = null;
        }

        var canStartDuel = skillPointsCondition && 
                            categoriesCondition && 
                            (currentUser.isPaidAccount || (duelsLimitCondition && currentDuelsLimitCondition)) &&
                            (requestedUser.isPaidAccount || (currentDuelsLimitCondition && opponentCurrentDuelsLimitCondition));
        
        var result = {
            canStartDuel: canStartDuel,
            message: message
        };

        resolve(result); 
    });
}

module.exports.getCommonCategories = (currentUser, requestedUser) => {
    var currentUserCategoriesIds = currentUser.categories.map(category => category.id);
    var requestedUserCategoriesIds = requestedUser.categories.map(category => category.id);

    var commonCategoriesIds = currentUserCategoriesIds.filter(categoryId => requestedUserCategoriesIds.indexOf(categoryId) !== -1);

    var commonCategories = (currentUser.categories.filter(category => commonCategoriesIds.indexOf(category.id) !== -1)
    .concat(requestedUser.categories.filter(category => commonCategoriesIds.indexOf(category.id) !== -1)))

    return removeDuplicatesFromArray(commonCategories, "name");
}

module.exports.getDuelsInformation = (userId) => {
    return new Promise((resolve, reject) => {
        var date = new Date();
        date.setDate(date.getDate() - 7); // One week

        Duel.getDuelsByUserId(userId)
        .then(duels => {
            var lastWeekDuels = duels.filter(duel => duel.endDate >= date && duel.status === DuelStatus.Ended);
            var currentDuels = duels.filter(duel => duel.status !== DuelStatus.Waiting && duel.status !== DuelStatus.Ended);

            resolve({
                lastWeekDuels: lastWeekDuels,
                currentDuels: currentDuels,
                duelsLeft: maxDuelsPerWeek - lastWeekDuels.length
            });
        });
    });
}

module.exports.getSkillSuggestions = (currentSkill, skillArray) => {
    skillArray.sort((a, b) => a - b);

    var leftSide = skillArray.filter(skill => skill < currentSkill);
    var rightSide = skillArray.filter(skill => skill > currentSkill);

    var result = {
        furthestMinus: leftSide[0],
        closestMinus: leftSide[leftSide.length - 1],
        closestPlus: rightSide[0],
        furthestPlus: rightSide[rightSide.length - 1]
    };

    result.middleMinus = getMiddleSkill(leftSide, (result.furthestMinus + result.closestMinus) / 2);
    result.middlePlus = getMiddleSkill(rightSide, (result.closestPlus + result.furthestPlus) / 2);

    return result; 
}

module.exports.getRandomOpponentWithSameSkill = (opponentsArray, skillPoints) => {
    if(skillPoints === null || skillPoints === undefined) {
        return null;
    }

    var filteredOpponents = opponentsArray.filter(opponent => opponent.skillPoints === skillPoints);
    return filteredOpponents[Math.floor(Math.random() * filteredOpponents.length)];
}

module.exports.fixSuggestedOpponents = (suggested) => {
    if(suggested.middleMinus !== null && suggested.furthestMinus !== null && suggested.closestMinus !== null) {
        if(suggested.middleMinus.id === suggested.furthestMinus.id || suggested.middleMinus.id === suggested.closestMinus.id) {
            suggested.middleMinus = null;
        }

        if(suggested.furthestMinus.id === suggested.closestMinus.id) {
            suggested.furthestMinus = null;
        }
    }

    if(suggested.middlePlus !== null && suggested.furthestPlus !== null && suggested.closestPlus !== null) {
        if(suggested.middlePlus.id === suggested.furthestPlus.id || suggested.middlePlus.id === suggested.closestPlus.id) {
            suggested.middlePlus = null;
        }
    
        if(suggested.furthestPlus.id === suggested.closestPlus.id) {
            suggested.furthestPlus = null;
        }
    }

    return suggested;
}

//#region Helpers

var getOpponentsQuery = (currentUserId, skillPoints, search) => {
    return { $and: [{skillPoints: {$lte: skillPoints + pointsRange, $gte: skillPoints - pointsRange}}, {fullname: new RegExp(search, "i")}, {_id: {$ne: currentUserId}}] };
}

var getUsersByUsernameOrFullNameQuery = (data) => {
    return { $or: [{username: data}, {fullname: new RegExp(data, "i")}] };
}

var getUsersByIds = (userIds) => {
    return { _id: { $in: userIds } };
}

var getCategoriesString = (categories) => {
    return categories.length > 0 ? categories.map((category) => category.name).join(", ") : "";
}

var removeDuplicatesFromArray = (arr, prop) => {
    return arr.filter((value, index, arr) => {
        return arr.map(obj => obj[prop]).indexOf(value[prop]) === index;
    });
};

var getMiddleSkill = (skillArray, averageValue) => {
    var arr = [];

    if(skillArray.length === 0) {
        return null;
    }

    // Get differences array
    for(var i = 0; i < skillArray.length; i++) {
        arr.push({value: skillArray[i], difference: Math.abs(skillArray[i] - averageValue)});
    }

    // Get value with the smallest difference
    return arr.reduce((prev, curr) => prev.difference < curr.difference ? prev : curr).value;
}

var getOrderSign = (orderDirection) => {
    return orderDirection === OrderDirection.Asc ? "" : "-";
}

//#endregion