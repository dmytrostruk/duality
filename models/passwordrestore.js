const mongoose = require("mongoose");

const PasswordRestoreSchema = mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    isUsed: {
        type: Boolean
    }
});

const PasswordRestore = module.exports = mongoose.model("PasswordRestore", PasswordRestoreSchema, 'passwordrestore');

module.exports.getPasswordRestoreByCode = (code) => {
    return PasswordRestore.findOne({$and: [{code: code}, {isUsed: false}]});
}