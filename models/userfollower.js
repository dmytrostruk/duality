const mongoose = require("mongoose");

const UserFollowerSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    followerId: {
        type: String,
        required: true
    }
});

const UserFollower = module.exports = mongoose.model("UserFollower", UserFollowerSchema, 'usersfollowers');

module.exports.addUserFollower = (userFollower) => {
    return userFollower.save();
}

module.exports.getUserFollower = (userFollower) => {
    return UserFollower.findOne(getQuery(userFollower.userId, userFollower.followerId));
}

module.exports.deleteUserFollower = (userFollower) => {
    return UserFollower.deleteOne(getQuery(userFollower.userId, userFollower.followerId));
}

module.exports.getUserFollowings = async (userId) => {
    var followings = await UserFollower.find(getUserFollowingsQuery(userId));

    return followings.map(following => following.userId);
}

//#region Queries

var getQuery = (userId, followerId) => {
    return { $and: [{userId: userId}, {followerId: followerId}] };
}

var getUserFollowingsQuery = (userId) => {
    return { followerId: userId };
}

//#endregion