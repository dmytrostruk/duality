const mongoose = require("mongoose");
const ArrayHelper = require("../helpers/array-helper");

const CategorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    group: {
        type: String,
        required: true
    }
});

const Category = module.exports = mongoose.model("Category", CategorySchema, 'categories');

module.exports.getCategoryById = (id) => {
    return Category.findById(id);
}

module.exports.getCategories = () => {
    return Category.find();
}

module.exports.getUserCategories = async (categoriesId) => {
    var categories = await Category.getCategories();
    categories = categories.map(Category.mapToViewModel);

    categories.forEach(category => {
        if(categoriesId.indexOf(category.id) >= 0) {
            category.selected = true;
        }
    });

    return categories;
}

module.exports.mapToViewModel = (category) => {
    return {
        id: category._id,
        name: category.name,
        group: category.group
    };
}

module.exports.getGroupedCategories = (categories) => {
    return ArrayHelper.groupBy(categories, "group");
}